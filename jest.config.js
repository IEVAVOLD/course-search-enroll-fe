module.exports = {
  globals: {
    'ts-jest': {
      tsconfig: '<rootDir>/src/tsconfig.spec.json',
    },
  },
  preset: 'jest-preset-angular',
  testRunner: 'jest-jasmine2',
  roots: ['<rootDir>/src/'],
  testMatch: ['**/+(*.)+(spec).+(ts)'],
  setupFilesAfterEnv: ['<rootDir>/src/test.ts'],
  collectCoverage: true,
  verbose: true,
  reporters: [ "default", "jest-junit" ],
  coverageReporters: ['json', 'html'],
  coverageDirectory: 'coverage/course-search-enroll-fe',
  moduleNameMapper: {
    '@app/(.*)': '<rootDir>/src/app/$1',
    '@env/(.*)': '<rootDir>/src/environments/$1',
    '@search/(.*)': '<rootDir>/src/app/search/$1',
    '@my-courses/(.*)': '<rootDir>/src/app/my-courses/$1',
    '@scheduler/(.*)': '<rootDir>/src/app/scheduler/$1',
    '@degree-planner/(.*)': '<rootDir>/src/app/degree-planner/$1',
    '@dars/(.*)': '<rootDir>/src/app/dars/$1',
  },
};
module.exports.setupFilesAfterEnv = ['<rootDir>/src/test-setup.ts'];
