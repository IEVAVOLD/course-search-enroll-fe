export class SchemaAssertionError {
  public message: string;

  constructor(
    public expected: string,
    public found: unknown,
    public path: string = '',
  ) {
    const e = expected;
    const p = path ? path : 'the object root';
    const f = JSON.stringify(found);
    this.message = `at ${p} expected ${e}, found ${f}`;
  }
}

export default interface Schema<T> {
  toString(): string;
  matches(u: unknown): u is T;
  asserts(u: unknown): asserts u is T;
}

export type FromSchema<T> = T extends Schema<infer U> ? U : never;

export const any: Schema<any> = {
  toString: () => 'any',
  matches: (_): _ is any => true,
  asserts: (_): asserts _ is any => {},
};

export const unknown: Schema<unknown> = {
  toString: () => 'unknown',
  matches: (_): _ is unknown => true,
  asserts: (_): asserts _ is unknown => {},
};

export const boolean: Schema<boolean> = {
  toString: () => 'boolean',
  matches: (u): u is boolean => typeof u === 'boolean',
  asserts: (u): asserts u is boolean => {
    if (typeof u !== 'boolean') {
      throw new SchemaAssertionError('boolean', u);
    }
  },
};

export const string: Schema<string> = {
  toString: () => 'string',
  matches: (u): u is string => typeof u === 'string',
  asserts: (u): asserts u is string => {
    if (typeof u !== 'string') {
      throw new SchemaAssertionError('string', u);
    }
  },
};

export const number: Schema<number> = {
  toString: () => 'number',
  matches: (u): u is number => typeof u === 'number' && !isNaN(u),
  asserts: (u): asserts u is number => {
    if (typeof u !== 'number' || isNaN(u)) {
      throw new SchemaAssertionError('number', u);
    }
  },
};

export const constant = <T extends string | number | boolean>(
  val: T,
): Schema<T> => {
  const s = typeof val === 'string' ? `"${val}"` : `${val}`;
  return {
    toString: () => s,
    matches: (u): u is T => u === val,
    asserts: (u): asserts u is T => {
      if (u !== val) {
        throw new SchemaAssertionError(s, u);
      }
    },
  };
};

export const nullable = <T>(t: Schema<T>): Schema<T | null> => {
  const s = `null | ${t}`;
  return {
    toString: () => s,
    matches: (u): u is T | null => u === null || t.matches(u),
    asserts: (u): asserts u is T | null => {
      if (u !== null) {
        t.asserts(u);
      }
    },
  };
};

export const pattern = (pat: RegExp): Schema<string> => {
  return {
    toString: () => `${pat}`,
    matches: (u): u is string => typeof u === 'string' && pat.test(u),
    asserts: (u): asserts u is string => {
      if (typeof u !== 'string' || pat.test(u) === false) {
        throw new SchemaAssertionError(`${pat}`, u);
      }
    },
  };
};

export const array = <T>(t: Schema<T>): Schema<T[]> => {
  const s = `[${t}]`;
  return {
    toString: () => s,
    matches: (u): u is T[] => Array.isArray(u) && u.every(v => t.matches(v)),
    asserts: (u): asserts u is T[] => {
      if (!Array.isArray(u)) {
        throw new SchemaAssertionError(s, u);
      }

      u.forEach((v, i) => {
        try {
          t.asserts(v);
        } catch (err) {
          if (err instanceof SchemaAssertionError) {
            throw new SchemaAssertionError(
              err.expected,
              err.found,
              `[${i}]${err.path}`,
            );
          } else {
            throw err;
          }
        }
      });
    },
  };
};

export const one = <T>(t: Schema<T>): Schema<[T]> => {
  const s = `[${t}]`;
  return {
    toString: () => s,
    matches: (u): u is [T] => {
      return Array.isArray(u) && u.length === 1 && t.matches(u[0]);
    },
    asserts: (u): asserts u is T[] => {
      if (!Array.isArray(u)) {
        throw new SchemaAssertionError(s, u);
      } else if (u.length !== 1) {
        throw new SchemaAssertionError(s, u);
      }

      try {
        t.asserts(u[0]);
      } catch (err) {
        if (err instanceof SchemaAssertionError) {
          throw new SchemaAssertionError(
            err.expected,
            err.found,
            `[0]${err.path}`,
          );
        } else {
          throw err;
        }
      }
    },
  };
};

export const record = <T>(s: Schema<T>): Schema<Record<string, T>> => {
  const str = `{ [key: string]: ${s} }`;
  return {
    toString: () => str,
    matches: (u): u is Record<string, T> => {
      if (typeof u !== 'object' || u === null) {
        return false;
      }

      return Object.entries(u).every(([key, value]) => {
        return typeof key === 'string' && s.matches(value);
      });
    },
    asserts: (u): asserts u is Record<string, T> => {
      if (typeof u !== 'object' || u === null) {
        throw new SchemaAssertionError(str, u);
      }

      Object.entries(u).forEach(([key, value]) => {
        try {
          s.asserts(value);
        } catch (err) {
          if (err instanceof SchemaAssertionError) {
            throw new SchemaAssertionError(
              err.expected,
              err.found,
              `["${key}"]${err.path}`,
            );
          } else {
            throw err;
          }
        }
      });
    },
  };
};

export const object = <T>(d: { [K in keyof T]: Schema<T[K]> }): Schema<T> => {
  const fields = Object.entries(d)
    .map(([key, scheme]) => `${key}: ${scheme}`)
    .join(', ');
  const s = fields ? `{ ${fields} }` : '{}';
  return {
    toString: () => s,
    matches: (u): u is T => {
      if (typeof u !== 'object' || u === null || Array.isArray(u)) {
        return false;
      }

      for (const key in d) {
        if (d.hasOwnProperty(key)) {
          if (d[key].matches((u as any)[key] ?? null) === false) {
            return false;
          }
        }
      }

      return true;
    },
    asserts: (u): asserts u is T => {
      if (typeof u !== 'object' || u === null || Array.isArray(u)) {
        throw new SchemaAssertionError(s, u);
      }

      for (const key in d) {
        if (d.hasOwnProperty(key)) {
          try {
            (d[key] as any).asserts((u as any)[key] ?? null);
          } catch (err) {
            if (err instanceof SchemaAssertionError) {
              throw new SchemaAssertionError(
                err.expected,
                err.found,
                `.${key}${err.path}`,
              );
            } else {
              throw err;
            }
          }
        }
      }
    },
  };
};

export const custom = <T>(s: string, fn: (u: unknown) => u is T): Schema<T> => {
  return {
    toString: () => s,
    matches: (u): u is T => fn(u),
    asserts: (u): asserts u is T => {
      if (!fn(u)) {
        throw new SchemaAssertionError(s, u);
      }
    },
  };
};

/**
 * Or-combinator
 *
 * Combine one or more schemas such that the whole schema matches if at least
 * one of the sub-schemas matches.
 *
 * The combined schema is built using a chain of calls to the `or` method:
 *
 * ```typescript
 * import * as s from 'schema';
 *
 * const combined = s
 *   .or(s.string)
 *   .or(s.number)
 *   .or(s.null)
 * ```
 */

interface OrSchema<A> extends Schema<A> {
  or: <B>(b: Schema<B>) => OrSchema<A | B>;
}

const _or =
  <A>(a: Schema<A>) =>
  <B>(b: Schema<B>): OrSchema<A | B> => {
    const ab: Schema<A | B> = {
      toString: () => `${a} | ${b}`,
      matches: (u): u is A | B => a.matches(u) || b.matches(u),
      asserts: (u): asserts u is A | B => {
        try {
          a.asserts(u);
        } catch {
          b.asserts(u);
        }
      },
    };

    return {
      ...ab,
      or: _or(ab),
    };
  };

export const or = <A>(a: Schema<A>): OrSchema<A> => {
  return {
    ...a,
    or: _or(a),
  };
};

/**
 * And-combinator
 *
 * Combine one or more schemas such that the whole schema matches if all of the
 * sub-schemas match.
 *
 * The combined schema is built using a chain of calls to the `and` method:
 *
 * ```typescript
 * import * as s from 'schema';
 *
 * const combined = s
 *   .and(s.object({ foo: s.string }))
 *   .and(s.object({ bar: s.number }))
 * ```
 */

interface AndSchema<A> extends Schema<A> {
  and: <B>(b: Schema<B>) => AndSchema<A & B>;
}

const _and =
  <A>(a: Schema<A>) =>
  <B>(b: Schema<B>): AndSchema<A & B> => {
    const ab: Schema<A & B> = {
      toString: () => `${a} & ${b}`,
      matches: (u): u is A & B => a.matches(u) && b.matches(u),
      asserts: (u): asserts u is A & B => {
        a.asserts(u);
        b.asserts(u);
      },
    };

    return {
      ...ab,
      and: _and(ab),
    };
  };

export const and = <A>(a: Schema<A>): AndSchema<A> => {
  return {
    ...a,
    and: _and(a),
  };
};
