import * as s from '@app/types/schema';

export type CreditValue = s.FromSchema<typeof CreditValue>;
export const CreditValue = s.number;

export type CreditRange = s.FromSchema<typeof CreditRange>;
export const CreditRange = s.object({ min: CreditValue, max: CreditValue });

export type HasCreditValue = s.FromSchema<typeof HasCreditValue>;
export const HasCreditValue = s.object({ creditValue: CreditValue });

export interface MaybeHasCreditValue {
  creditValue: CreditValue | null;
}

export type HasCreditRange = s.FromSchema<typeof HasCreditRange>;
export const HasCreditRange = s.object({ creditRange: CreditRange });

export interface MaybeHasCreditRange {
  creditRange: CreditRange | null;
}
