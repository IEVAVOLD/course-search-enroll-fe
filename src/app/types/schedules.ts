import {
  Building,
  Course,
  CurrentCourse,
  CurrentCourseRef,
  CurrentExamMeeting,
  DayOfTheWeek,
  EnrollmentPackage,
  ExamSchedule,
  MeetingLocation,
  MeetingSchedule,
  RawMeeting,
  RawSectionType,
  RoadmapCourse,
  RoadmapCourseRef,
  RoadmapId,
} from './courses';
import * as s from '@app/types/schema';
import { UNLOADED } from './loadable';
import { Term, TermCode } from './terms';
import { parseCreditRange } from '@app/core/utils';
import { Block } from '@app/shared/pipes/layout-blocks.pipe';
import {
  CST_OFFSET_IN_SECONDS_WITHOUT_DST,
  toHourMinuteMeridianUtc,
  toHourMinuteMeridianWithoutDST,
} from '@app/shared/date-formats';
import { ColorQueue, FullColorQueue, Styles } from './colors';

export interface RawSchedulerMeeting {
  day: string;
  start: number;
  duration: number;

  courseNumber?: string;
  sectionNumber?: string;
  location?: string;
  color?: string;
  course?: RawSchedulingEntity;
}

export interface RawSchedulingRequestMeeting {
  instructors: string[];
  daysOfWeek: DayOfTheWeek[];
  meetingDays: string;
  startTime: number;
  endTime: number;
  sectionType: RawSectionType;
  sectionNumber: string;
  building: Building;
  room: string | null;
}

export interface RawSchedulerEnrollmentPackage {
  enrollmentOptions: {
    honors: boolean;
    waitlist: boolean;
  };
  creditRange: string;
  classNumber: number;
  packageEnrollmentStatus: {
    status: 'OPEN' | 'WAITLISTED' | 'CLOSED';
  };
  meetings: RawSchedulingRequestMeeting[];
}

export interface RawSchedulingEntity {
  courseId: string;
  subjectCode: string;
  name: string;
  selectedEnrollmentPackage: RawSchedulerEnrollmentPackage;
  subjectShortDescription: string;
  catalogNumber: string;
  roadmapCourse?: RoadmapCourse;
}

export interface RawSchedulerSolution {
  hasConflicts: boolean;
  schedulingEntities: RawSchedulingEntity[];
  schedulingRequest: any; // TODO: write a type
}

export interface RawCourseEntity {
  courseId: string;
  subjectCode: string;
  name: string;
  catalogNumber: string;
  subjectShortDescription: string;
  courseOrigin: 'ENROLLED' | 'ROADMAP';
  selectedEnrollmentPackage: {
    packageId: string; // similar to the docId
    classNumber: number;
    creditRange: string;
    packageEnrollmentStatus: {
      status: string | null;
    };
    enrollmentOptions: {
      honors: unknown;
      waitlist: unknown;
      relatedClasses: number[];
    };
    meetings: Array<{
      meetingType: 'CLASS' | 'EXAM';
      meetingTimeStart: number;
      meetingTimeEnd: number;
      meetingDaysList: DayOfTheWeek[];
      building: Building;
      room: string | null;
      sectionType: string;
      sectionNumber: string;
      instructors: string[];
    }>;
    exam: RawMeeting | null;
  };
}

export namespace RawCourseEntity {
  export const schema = s.object<RawCourseEntity>({
    courseId: s.string,
    subjectCode: s.string,
    name: s.string,
    catalogNumber: s.string,
    subjectShortDescription: s.string,
    courseOrigin: s.or(s.constant('ENROLLED')).or(s.constant('ROADMAP')),
    selectedEnrollmentPackage: s.object({
      packageId: s.string,
      classNumber: s.number,
      creditRange: s.string,
      packageEnrollmentStatus: s.object({
        status: s.nullable(s.string),
      }),
      enrollmentOptions: s.object({
        waitlist: s.unknown,
        honors: s.unknown,
        relatedClasses: s.array(s.number),
      }),
      meetings: s.array(
        s.object({
          meetingType: s.or(s.constant('CLASS')).or(s.constant('EXAM')),
          meetingTimeStart: s.number,
          meetingTimeEnd: s.number,
          meetingDaysList: s.array(DayOfTheWeek.schema),
          building: Building.schema,
          room: s.nullable(s.string),
          sectionType: s.string,
          sectionNumber: s.string,
          instructors: s.array(s.string),
        }),
      ),
      exam: s.nullable(RawMeeting.schema),
    }),
  });
}

interface RawBreakEntity {
  name: 'SCHEDULE-BLOCKS';
  courseOrigin: 'SCHEDULE-BLOCKS';
  scheduleBlocks: Array<{
    id: number;
    name: string;
    endDate: number;
    startTime: number;
    endTime: number;
    storedDaysOfWeek: string; // like "MWF"
  }>;
}

namespace RawBreakEntity {
  export const schema = s.object<RawBreakEntity>({
    name: s.constant('SCHEDULE-BLOCKS'),
    courseOrigin: s.constant('SCHEDULE-BLOCKS'),
    scheduleBlocks: s.array(
      s.object({
        id: s.number,
        name: s.string,
        endDate: s.number,
        startTime: s.number,
        endTime: s.number,
        storedDaysOfWeek: s.string,
      }),
    ),
  });
}

type RawEntity = RawCourseEntity | RawBreakEntity;

namespace RawEntity {
  export const schema = s.or(RawBreakEntity.schema).or(RawCourseEntity.schema);
}

interface RawSchedule {
  hasConflicts: boolean;
  schedulingEntities: RawEntity[];
}

namespace RawSchedule {
  export const schema = s.object<RawSchedule>({
    hasConflicts: s.boolean,
    schedulingEntities: s.array(RawEntity.schema),
  });
}

export interface NewRawSchedules {
  // If all possible schedules have conflicts, the best solution is a schedule
  // with conflicts that's the least bad.
  bestSolution: RawSchedule;

  // If there were schedules without conflicts, all the schedules will be
  // contained in this list.
  feasibleSolutions: RawSchedule[];
}

export namespace NewRawSchedules {
  export const schema = s.object<NewRawSchedules>({
    bestSolution: RawSchedule.schema,
    feasibleSolutions: s.array(RawSchedule.schema),
  });
}

export interface RawSchedules {
  meetings?: RawSchedulerMeeting[];
  feasibleSolutions: RawSchedulerSolution[];
  bestSolution: RawSchedulerSolution;
}

export namespace RawSchedules {
  export const schema = s.object<RawSchedules>({
    feasibleSolutions: s.array(s.any),
    bestSolution: s.any,
  });
}

export interface CourseEntityBlock extends Block {
  data: {
    kind: 'course';
    name: string;
    sectionCatalogRef: string;
    time: string;
    isLocked: boolean;
    isEnrolled: boolean;
    course: {
      termCode: TermCode;
      courseId: string;
      subjectCode: string;
      shortCatalog: string;
      ref: RoadmapCourseRef | CurrentCourseRef | null;
      credits: { min: number; max: number };
    };
    package: {
      classNumber: number;
      relatedClassNumbers: number[];
      status: string;
      instructors: string[];
      sections: Array<{
        name: string;
        schedule: MeetingSchedule | null;
        location: MeetingLocation;
      }>;
    };
  };
}

export interface CourseEntity {
  kind: 'course';
  course: Course;
  ref: RoadmapCourseRef | CurrentCourseRef | null;
  blocks: CourseEntityBlock[];
  package: {
    termCode: TermCode;
    classNumber: number;
    relatedClassNumbers: number[];
    supportsWaitlist: boolean;
    supportsHonors: boolean;
  };
  exam: RawMeeting | null;
}

export namespace CourseEntity {
  const toCourse = (term: Term, raw: RawCourseEntity): Course => {
    const subject = term.subjects.find(s => s.code === raw.subjectCode)!;
    console.assert(subject !== undefined);
    return {
      title: raw.name,
      shortCatalog: `${raw.subjectShortDescription} ${raw.catalogNumber}`,
      longCatalog: `${subject.description} ${raw.catalogNumber}`,
      subject,
      courseId: raw.courseId,
      credits: parseCreditRange(raw.selectedEnrollmentPackage.creditRange),
      details: UNLOADED,
    };
  };

  const toBlocks = (
    termCode: TermCode,
    course: Course,
    ref: RoadmapCourseRef | CurrentCourseRef | null,
    marked: MarkedClassNumbers,
    cache: StyleCache,
    raw: RawCourseEntity,
  ): CourseEntityBlock[] => {
    const sections: Array<{
      name: string;
      schedule: MeetingSchedule | null;
      location: MeetingLocation;
    }> = raw.selectedEnrollmentPackage.meetings
      .filter(meeting => meeting.meetingType === 'CLASS')
      .map(m => {
        return {
          name: `${m.sectionType} ${m.sectionNumber}`,
          schedule: MeetingSchedule.from(m),
          location: MeetingLocation.from(m),
        };
      });

    return raw.selectedEnrollmentPackage.meetings
      .filter(meeting => meeting.meetingType === 'CLASS')
      .flatMap(m => {
        return m.meetingDaysList.map(day => {
          const timeStart = toHourMinuteMeridianUtc(
            m.meetingTimeStart + CST_OFFSET_IN_SECONDS_WITHOUT_DST * 1000,
          );
          const timeEnd = toHourMinuteMeridianUtc(
            m.meetingTimeEnd + CST_OFFSET_IN_SECONDS_WITHOUT_DST * 1000,
          );

          const classNumber = raw.selectedEnrollmentPackage.classNumber;

          return {
            day,
            start:
              m.meetingTimeStart / 1000 + CST_OFFSET_IN_SECONDS_WITHOUT_DST,
            duration: (m.meetingTimeEnd - m.meetingTimeStart) / 1000,
            appearance: { ...cache.getColor(course) },
            data: {
              kind: 'course',
              name: `${course.shortCatalog} (${m.sectionType} ${m.sectionNumber})`,
              sectionCatalogRef: `${m.sectionType} ${m.sectionNumber}`,
              time: `${timeStart}-${timeEnd}`,
              isLocked: marked.locked.includes(classNumber),
              isEnrolled: marked.enrolled.includes(classNumber),
              course: {
                termCode,
                courseId: raw.courseId,
                subjectCode: raw.subjectCode,
                shortCatalog: course.shortCatalog,
                ref,
                credits: course.credits,
              },
              package: {
                classNumber: raw.selectedEnrollmentPackage.classNumber,
                relatedClassNumbers:
                  raw.selectedEnrollmentPackage.enrollmentOptions
                    .relatedClasses,
                status:
                  raw.selectedEnrollmentPackage.packageEnrollmentStatus
                    .status || 'MISSING STATUS',
                instructors: m.instructors,
                sections,
              },
            },
          };
        });
      });
  };

  export const from = (
    term: Term<TermCode>,
    marked: MarkedClassNumbers,
    cache: StyleCache,
    raw: RawCourseEntity,
  ): CourseEntity => {
    const selected = raw.selectedEnrollmentPackage;

    const sessionCode = selected.packageId.split('-')[1];
    const session = term.sessions.find(s => s.code === sessionCode)!;
    console.assert(session !== undefined);

    const ref = marked.toCourseRef(raw);
    const course = toCourse(term, raw);

    return {
      kind: 'course',
      course,
      ref: ref,
      blocks: toBlocks(term.termCode, course, ref, marked, cache, raw),
      package: {
        termCode: term.termCode,
        classNumber: raw.selectedEnrollmentPackage.classNumber,
        relatedClassNumbers:
          raw.selectedEnrollmentPackage.enrollmentOptions.relatedClasses,
        supportsWaitlist:
          raw.selectedEnrollmentPackage.enrollmentOptions.waitlist === true,
        supportsHonors:
          raw.selectedEnrollmentPackage.enrollmentOptions.honors === true,
      },
      exam: raw.selectedEnrollmentPackage.exam,
    };
  };
}

export interface BreakEntityBlock extends Block {
  data: {
    kind: 'break';
    name: string;
    time: string;
    id: number;
    schedule: MeetingSchedule;
  };
}

/**
 * JSON structure used when communicating with the backend schedule blocks API
 */
export interface NewScheduleBreak {
  label: string;
  days: {
    sun: boolean;
    mon: boolean;
    tue: boolean;
    wed: boolean;
    thu: boolean;
    fri: boolean;
    sat: boolean;
  };
  times: {
    startSeconds: number;
    endSeconds: number;
  };
}

export namespace NewScheduleBreak {
  export const schema = s.object<NewScheduleBreak>({
    label: s.string,
    days: s.object({
      sun: s.boolean,
      mon: s.boolean,
      tue: s.boolean,
      wed: s.boolean,
      thu: s.boolean,
      fri: s.boolean,
      sat: s.boolean,
    }),
    times: s.object({
      startSeconds: s.number,
      endSeconds: s.number,
    }),
  });
}

export interface ExistingScheduleBreak extends NewScheduleBreak {
  id: number;
}

export namespace ExistingScheduleBreak {
  export const schema = s
    .and(NewScheduleBreak.schema)
    .and(s.object({ id: s.number }));
}

export interface BreakEntity {
  kind: 'break';
  id: number;
  name: string;
  schedule: MeetingSchedule;
  blocks: BreakEntityBlock[];
}

const BREAK_STYLES: Styles = {
  background: '#333',
};

const mod = (m: number, n: number) => ((m % n) + n) % n;

const CST_MS_OFFSET = 6 * 3600 * 1000;
const MS_PER_DAY = 24 * 3600 * 1000;

export const normalizeScheduledBreakTimestamp = (raw: number) => {
  const milliPlusOffset = raw;
  const milliNoOffset = milliPlusOffset - CST_MS_OFFSET;
  const milliClampedToDay = mod(milliNoOffset, MS_PER_DAY);
  return milliClampedToDay / 1000;
};

export namespace BreakEntity {
  export const from = (raw: RawBreakEntity): BreakEntity[] => {
    return raw.scheduleBlocks.map(brk => {
      /**
       * IMPORTANT: The backend records break start/end timestamps in UTC. This
       *            is DIFFERENT from how course meeting timestamps are recorded.
       */
      const startSeconds = normalizeScheduledBreakTimestamp(brk.startTime);
      const endSeconds = normalizeScheduledBreakTimestamp(brk.endTime);

      const schedule: MeetingSchedule = {
        type: 'meeting',
        days: brk.storedDaysOfWeek,
        daysList: DayOfTheWeek.fromString(brk.storedDaysOfWeek),
        start: toHourMinuteMeridianWithoutDST(mod(brk.startTime, MS_PER_DAY)),
        startSeconds,
        end: toHourMinuteMeridianWithoutDST(mod(brk.endTime, MS_PER_DAY)),
        durationSeconds: endSeconds - startSeconds,
      };

      return {
        kind: 'break',
        id: brk.id,
        name: brk.name,
        schedule,
        blocks: Block.fromSchedule(schedule).map(block => ({
          ...block,
          appearance: BREAK_STYLES,
          data: {
            kind: 'break',
            name: brk.name,
            time: `${schedule.start}-${schedule.end}`,
            id: brk.id,
            schedule,
          },
        })),
      };
    });
  };
}

export type Entity = CourseEntity | BreakEntity;

export namespace Entity {
  export const from = (
    term: Term<TermCode>,
    marked: MarkedClassNumbers,
    cache: StyleCache,
    raw: RawEntity,
  ): Entity[] => {
    if (raw.courseOrigin === 'SCHEDULE-BLOCKS') {
      return BreakEntity.from(raw);
    } else {
      return [CourseEntity.from(term, marked, cache, raw)];
    }
  };
}

export interface CourseWithExam {
  shortCatalog: string;
  title: string;
  package: {
    examMeetings: CurrentExamMeeting[];
  };
}

export interface Schedule {
  hasConflicts: boolean;
  termCode: TermCode;
  entities: Entity[];
  exams: CourseWithExam[];
}

class StyleCache {
  private cache: Array<[[courseId: string, subjectCode: string], Styles]> = [];

  constructor(private queue: ColorQueue = new FullColorQueue()) {}

  private lookup(course: Course) {
    return this.cache.find(([[courseId, subjectCode]]) => {
      return (
        courseId === course.courseId && subjectCode === course.subject.code
      );
    })?.[1];
  }

  public getColor(course: Course): Styles {
    const cached = this.lookup(course);
    if (cached) {
      return cached;
    }

    const styles = this.queue.pick();
    this.cache.push([[course.courseId, course.subject.code], styles]);
    return styles;
  }
}

interface MarkedClassNumbers {
  locked: number[];
  enrolled: number[];
  toCourseRef: (
    entity: RawCourseEntity,
  ) => RoadmapCourseRef | CurrentCourseRef | null;
}

export namespace Schedule {
  const getExams = (entities: Entity[]) => {
    const courseEntities = <CourseEntity[]>(
      entities.filter(e => e.kind === 'course')
    );
    return courseEntities.flatMap(entity => {
      const convertLocation = (location: MeetingLocation) => {
        if (location.type === 'classroom' && location.building === '') {
          return {
            type: 'unknown',
            building: null,
          } as MeetingLocation;
        } else {
          return location;
        }
      };

      return entity.exam
        ? {
            shortCatalog: entity.course.shortCatalog,
            title: entity.course.title,
            package: {
              examMeetings: [
                {
                  sectionType: 'LEC', // value does not matter here
                  schedule: ExamSchedule.from(entity.exam),
                  location: convertLocation(MeetingLocation.from(entity.exam)),
                },
              ],
            },
          }
        : [];
    });
  };
  export const from = (
    term: Term<TermCode>,
    marked: MarkedClassNumbers,
    raw: RawSchedule,
    allHaveConflicts: boolean,
  ): Schedule => {
    const cache = new StyleCache();
    const entities = raw.schedulingEntities.flatMap(raw => {
      return Entity.from(term, marked, cache, raw);
    });
    return {
      termCode: term.termCode,
      hasConflicts: raw.hasConflicts || allHaveConflicts,
      entities,
      exams: getExams(entities),
    };
  };

  export const toBlocks = (schedule: Schedule): Block[] => {
    return schedule.entities.flatMap((entity): Block[] => {
      switch (entity.kind) {
        case 'course':
          return entity.blocks;
        case 'break':
          return entity.blocks;
      }
    });
  };
}

export interface GenerateRequest {
  termCode: string;
  cartItems: number[];
  selectedPackages: Record<RoadmapId, DocId[]>;
  enrolled: number[];
  options: {
    waitlistedPackages: boolean;
    specialGroupsPackages: boolean;
    allowConflicts: boolean;
  };
}

type DocId = EnrollmentPackage['docId'];

export namespace GenerateRequest {
  export const from = (
    termCode: TermCode,
    roadmap: RoadmapCourse[],
    selectedPackages: Record<RoadmapId, DocId[]>,
    current: CurrentCourse[],
    options: GenerateRequest['options'],
  ): GenerateRequest => {
    return {
      termCode: TermCode.encode(termCode),
      cartItems: roadmap.map(course => parseInt(course.ref.roadmapId, 10)),
      selectedPackages,
      enrolled: current.flatMap(course => {
        const classNums = [course.package.classNumber];

        if (course.package.relatedClassNumber1 !== null) {
          classNums.push(course.package.relatedClassNumber1);
        }

        if (course.package.relatedClassNumber2 !== null) {
          classNums.push(course.package.relatedClassNumber2);
        }

        classNums.push(...course.package.autoEnrollClasses);
        return classNums;
      }),
      options,
    };
  };

  export const schema = s.object<GenerateRequest>({
    termCode: s.string,
    cartItems: s.array(s.number),
    selectedPackages: s.any,
    enrolled: s.array(s.number),
    options: s.object({
      waitlistedPackages: s.boolean,
      specialGroupsPackages: s.boolean,
      allowConflicts: s.boolean,
    }),
  });
}
