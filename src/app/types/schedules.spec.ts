import { normalizeScheduledBreakTimestamp } from './schedules';

describe('scheduled break times', () => {
  const data = [
    { raw: 0, normalized: 64_800, label: 'should handle 6pm' },
    { raw: 61_200_000, normalized: 39_600, label: 'should handle 11am' },
    { raw: 64_800_000, normalized: 43_200, label: 'should handle 12pm' },
    { raw: 178_200_000, normalized: 70_200, label: 'should handle 7:30pm' },
    { raw: 183_600_000, normalized: 75_600, label: 'should handle 9pm' },
  ];

  data.forEach(({ raw, normalized, label }) => {
    it(label, () => {
      expect(normalizeScheduledBreakTimestamp(raw)).toBe(normalized);
    });
  });
});
