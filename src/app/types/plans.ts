import * as s from '@app/types/schema';

export type PlanId = s.FromSchema<typeof PlanId>;
export const PlanId = s.number;

export type RawPlan = s.FromSchema<typeof RawPlan>;
export const RawPlan = s.object({
  roadmapId: PlanId,
  name: s.string,
  primary: s.boolean,
});
