import Schema, * as s from '@app/types/schema';
import { Term, TermCode } from '@app/types/terms';
import {
  RawInstructor,
  RawMeeting,
  Meeting,
  EnrollmentPackage,
} from './packages';
import { RawDetails, Details } from './details';
import { Loadable } from 'loadable.ts';
import {
  curry2,
  parseCreditRange,
  parseJsonWithoutThrowing,
} from '@app/core/utils';
import { Course, HasTermCode, HasLazyPackages } from './attributes';
import { RoadmapCourseRef } from './refs';

export interface RawRoadmapMeeting extends RawMeeting {
  sectionType: string;
  sectionNumber: string;
  instructor: RawInstructor | null;
}

export namespace RawRoadmapMeeting {
  export const schema: Schema<RawRoadmapMeeting> = s.and(RawMeeting.schema).and(
    s.object({
      sectionType: s.string,
      sectionNumber: s.string,
      instructor: s.nullable(RawInstructor.schema),
    }),
  );
}

export interface RoadmapMeeting extends Meeting {
  sectionType: string;
  sectionNumber: string;
  instructor: RawInstructor | null;
}

export namespace RoadmapMeeting {
  export const from = (raw: RawRoadmapMeeting): RoadmapMeeting => {
    return {
      ...Meeting.from(raw),
      sectionType: raw.sectionType,
      sectionNumber: raw.sectionNumber,
      instructor: raw.instructor,
    };
  };
}

export interface PartialPackageChoices {
  credits:
    | { hasChoice: true; total: number | null }
    | { hasChoice: false; total: number };
  waitlist: 'notAvailable' | 'notChosen' | 'chosen';
  honors: 'notAvailable' | 'notChosen' | 'chosen';
}

export interface CompletedPackageChoices extends PartialPackageChoices {
  credits:
    | { hasChoice: true; total: number }
    | { hasChoice: false; total: number };
}

/**
 * Used for roadmap courses (courses in the cart) which have classNumber: null
 * when the user hasn't picked an enrollment package yet.
 */
export interface MaybeHasPackage {
  package: null | {
    docId: string;
    classNumber: number;
    credits: { min: number; max: number };
    relatedClassNumbers: number[];
    classMeetings: RoadmapMeeting[];
    examMeetings: RoadmapMeeting[];
    choices: PartialPackageChoices;
    details: null | Loadable<EnrollmentPackage>;
    classPermissionNumberNeeded: boolean;
  };
}

export interface RawEnrollmentResult {
  /**
   * Usually a string like:
   *
   * - `Failed`
   * - `New`
   * - `Added`
   */
  enrollmentState: unknown;
  enrollmentMessages: unknown;
}

export namespace RawEnrollmentResult {
  export const schema: Schema<RawEnrollmentResult> = s.object({
    enrollmentState: s.unknown,
    enrollmentMessages: s.unknown,
  });

  export const parse = (results: RawEnrollmentResult[]): string[] => {
    results = results.filter(result => result.enrollmentState === 'Failed');

    if (results.length === 0) {
      return [];
    }

    const encoded = results[results.length - 1].enrollmentMessages;
    if (typeof encoded !== 'string') {
      return [];
    }

    const lastMessages = parseJsonWithoutThrowing(encoded);
    if (!Array.isArray(lastMessages) || lastMessages.length === 0) {
      return [];
    }

    return lastMessages
      .filter(s.object({ description: s.string }).matches)
      .map(({ description }) => {
        return description;
      });
  };
}

export interface RawValidationResult {
  validationState: unknown;
  validationMessages: unknown;
}

export namespace RawValidationResult {
  export const schema: Schema<RawValidationResult> = s.object({
    validationState: s.unknown,
    validationMessages: s.unknown,
  });

  export const parse = (results: RawValidationResult[]): string[] => {
    results = results.filter(result => result.validationState === 'Invalid');

    if (results.length === 0) {
      return [];
    }

    if (typeof results[0].validationMessages !== 'string') {
      return [];
    }

    const firstMessages = parseJsonWithoutThrowing(
      results[0].validationMessages,
    );

    if (!Array.isArray(firstMessages) || firstMessages.length === 0) {
      return [];
    }

    return firstMessages
      .filter(s.object({ description: s.string }).matches)
      .map(({ description }) => {
        return description;
      });
  };
}

export interface RawRoadmapCourse {
  id: number;
  courseId: string;
  termCode: string;
  title: string;
  subjectCode: string;
  catalogNumber: string;
  credits: number;
  creditMin: number;
  creditMax: number;
  classMeetings: RawRoadmapMeeting[] | null;
  details: RawDetails;
  classPermissionNumber: null | string | number;
  packageDetails: null | {
    docId: string;
    id: string;
    // CAREFUL! This can differ from the course credit range. For this reason
    // when possible credit choices should be based on _this_ range and not the
    // course credit range.
    creditRange: string;
    enrollmentOptions: {
      classPermissionNumberNeeded: unknown;
      relatedClasses: number[];
      waitlist: unknown;
      honors: unknown;
    };
  };
  enrollmentResults: RawEnrollmentResult[];
  validationResults: RawValidationResult[];
  waitlist: null | 'Y' | 'N';
  honors: null | 'Y' | 'N';
}

export namespace RawRoadmapCourse {
  export const schema = s.object<RawRoadmapCourse>({
    id: s.number,
    courseId: s.string,
    termCode: s.string,
    title: s.string,
    subjectCode: s.string,
    catalogNumber: s.string,
    credits: s.number,
    creditMin: s.number,
    creditMax: s.number,
    classMeetings: s.nullable(s.array(RawRoadmapMeeting.schema)),
    details: RawDetails.schema,
    classPermissionNumber: s.nullable(s.or(s.string).or(s.number)),
    packageDetails: s.nullable(
      s.object({
        docId: s.string,
        id: s.pattern(/^\d+$/),
        creditRange: s.string,
        enrollmentOptions: s.object({
          classPermissionNumberNeeded: s.unknown,
          relatedClasses: s.array(s.number),
          waitlist: s.unknown,
          honors: s.unknown,
        }),
      }),
    ),
    enrollmentResults: s.array(RawEnrollmentResult.schema),
    validationResults: s.array(RawValidationResult.schema),
    waitlist: s.nullable(s.or(s.constant('Y')).or(s.constant('N'))),
    honors: s.nullable(s.or(s.constant('Y')).or(s.constant('N'))),
  });
}

export type RoadmapId = string;

export interface RoadmapCourse
  extends Course,
    HasTermCode,
    MaybeHasPackage,
    HasLazyPackages {
  ref: RoadmapCourseRef;
  classPermissionNumber: number | null;
  details: Details & { mode: 'loaded' };
  validationMessages: string[];
  enrollmentMessages: string[];

  enrollmentResults: unknown;
  validationResults: unknown;

  /**
   * A store for arbitrary data about the course. Be careful, don't assume
   * anything about the contents of this object.
   */
  data: Record<string, unknown>;
}

export interface RoadmapCourseReady extends RoadmapCourse {
  package: NonNullable<MaybeHasPackage['package']> & {
    choices: CompletedPackageChoices;
  };
}

export namespace RoadmapCourse {
  export const isReady = (c: RoadmapCourse): c is RoadmapCourseReady => {
    return c.package !== null && c.package.choices.credits.total !== null;
  };

  const inRange = (min: number, max: number, val: number) => {
    return val >= min && val <= max;
  };

  export const is = (course: Course | null): course is RoadmapCourse => {
    return s.object({ ref: RoadmapCourseRef.schema }).matches(course);
  };

  type HasPackage = NonNullable<MaybeHasPackage['package']>;
  type PackageCredits = HasPackage['choices']['credits'];

  const parseCredits = (raw: RawRoadmapCourse): PackageCredits => {
    if (raw.creditMin !== raw.creditMax) {
      return {
        hasChoice: true,
        total: inRange(raw.creditMin, raw.creditMax, raw.credits)
          ? raw.credits
          : null,
      };
    } else {
      return { hasChoice: false, total: raw.creditMin };
    }
  };

  const parseMaybeHasPackage = (
    raw: RawRoadmapCourse,
  ): MaybeHasPackage['package'] => {
    if (raw.packageDetails === null) {
      return null;
    }

    const docId = raw.packageDetails.docId;

    const classNumber = parseInt(raw.packageDetails.id, 10);

    const relatedClassNumbers =
      raw.packageDetails.enrollmentOptions.relatedClasses;

    const credits = parseCredits(raw);

    const waitlist =
      raw.waitlist === 'Y'
        ? 'chosen'
        : raw.packageDetails.enrollmentOptions.waitlist === true
        ? 'notChosen'
        : 'notAvailable';

    const honors =
      raw.honors === 'Y'
        ? 'chosen'
        : raw.packageDetails.enrollmentOptions.honors === true
        ? 'notChosen'
        : 'notAvailable';

    const classMeetings = (raw.classMeetings || [])
      .filter(meeting => meeting.meetingType === 'CLASS')
      .map(RoadmapMeeting.from);

    const examMeetings = (raw.classMeetings || [])
      .filter(meeting => meeting.meetingType === 'EXAM')
      .map(RoadmapMeeting.from);

    // The package details have to be loaded from another API endpoint. The
    // null value here indicates that the details haven't been loaded from that
    // API yet.
    const details = null;

    const classPermissionNumberNeeded =
      raw.packageDetails?.enrollmentOptions?.classPermissionNumberNeeded ===
      true;

    return {
      docId,
      classNumber,
      credits: parseCreditRange(raw.packageDetails.creditRange),
      relatedClassNumbers,
      classMeetings,
      examMeetings,
      details,
      choices: {
        credits,
        waitlist,
        honors,
      },
      classPermissionNumberNeeded,
    };
  };

  export const from = (
    term: Term<TermCode>,
    raw: RawRoadmapCourse,
  ): RoadmapCourse => {
    const subject = term.subjects.find(({ code }) => code === raw.subjectCode);

    if (!subject) {
      throw new Error(
        `unknown subject-code ${raw.subjectCode} for ${raw.termCode}`,
      );
    }

    return {
      ref: {
        kind: 'roadmap',
        termCode: term.termCode,
        roadmapId: `${raw.id}`,
      },

      // HasTitle
      shortCatalog: raw.details.courseDesignation,
      longCatalog: raw.details.fullCourseDesignation,
      title: raw.title,

      // HasSubject
      subject,

      // HasCourseId
      courseId: raw.courseId,

      // HasTermCode
      termCode: TermCode.decodeOrThrow(raw.termCode),

      // HasCredits
      credits: { min: raw.creditMin, max: raw.creditMax },

      // MaybeHasPackage
      package: parseMaybeHasPackage(raw),

      // HasLazyDetails
      details: { mode: 'loaded', ...Details.from(raw.details) },

      // HasLazyPackages
      packages: null,

      // Other fields
      classPermissionNumber: toNumberIfPossible(raw.classPermissionNumber),
      validationMessages: RawValidationResult.parse(raw.validationResults),
      enrollmentMessages: RawEnrollmentResult.parse(raw.enrollmentResults),

      enrollmentResults: null,
      validationResults: null,

      data: {},
    };
  };

  export const fromCurry = curry2(from);
}

const toNumberIfPossible = (thing: null | string | number): number | null => {
  if (typeof thing === 'number') {
    return thing;
  } else if (typeof thing === 'string' && /^\d+$/.test(thing)) {
    return parseInt(thing, 10);
  } else {
    return null;
  }
};
