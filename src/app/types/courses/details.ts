import Schema, * as s from '@app/types/schema';
import { TermCode, CrossListing, Level } from '../terms';

interface RawSchoolCollege {
  academicOrgCode: string;
  academicGroupCode: string;
  shortDescription: string;
  formalDescription: string;
}

namespace RawSchoolCollege {
  export const schema: Schema<RawSchoolCollege> = s.object({
    academicOrgCode: s.string,
    academicGroupCode: s.string,
    shortDescription: s.string,
    formalDescription: s.string,
  });
}

interface RawSubject {
  termCode: string;
  subjectCode: string;
  description: string;
  shortDescription: string;
  formalDescription: string;
  undergraduateCatalogURI: string | null;
  graduateCatalogURI: string | null;
  departmentURI: string | null;
  uddsFundingSource: string;
  schoolCollege: RawSchoolCollege;
  footnotes: string[];
}

namespace RawSubject {
  export const schema: Schema<RawSubject> = s.object({
    termCode: s.string,
    subjectCode: s.string,
    description: s.string,
    shortDescription: s.string,
    formalDescription: s.string,
    undergraduateCatalogURI: s.nullable(s.string),
    graduateCatalogURI: s.nullable(s.string),
    departmentURI: s.nullable(s.string),
    uddsFundingSource: s.string,
    schoolCollege: RawSchoolCollege.schema,
    footnotes: s.array(s.string),
  });
}

export interface RawDetails {
  termCode: string;
  courseId: string;
  catalogNumber: string;
  title: string;
  subject: RawSubject;
  description: string;
  enrollmentPrerequisites: string | null;
  creditRange: string;
  minimumCredits: number;
  maximumCredits: number;
  lastTaught: string | null;
  allCrossListedSubjects: CrossListing[];
  breadths: Array<{ description: string }>;
  lettersAndScienceCredits: null | { description: string };
  generalEd: null | { description: string };
  ethnicStudies: null | { description: string };
  levels: Level[];
  sustainability: null | { description: 'Sustainability' };
  gradCourseWork: null | true;
  workplaceExperience: null | { description: string };
  foreignLanguage: null | { description: string };
  courseDesignation: string;
  fullCourseDesignation: string;
  gradingBasis: { code: string; description: string };
  repeatable: string;
  honors: unknown;
}

export namespace RawDetails {
  export const schema: Schema<RawDetails> = s.object({
    termCode: s.string,
    courseId: s.string,
    catalogNumber: s.string,
    title: s.string,
    subject: RawSubject.schema,
    description: s.string,
    enrollmentPrerequisites: s.nullable(s.string),
    creditRange: s.string,
    minimumCredits: s.number,
    maximumCredits: s.number,
    lastTaught: s.nullable(s.string),
    allCrossListedSubjects: s.array(CrossListing.schema),
    breadths: s.array(s.object({ description: s.string })),
    lettersAndScienceCredits: s.nullable(s.object({ description: s.string })),
    generalEd: s.nullable(s.object({ description: s.string })),
    ethnicStudies: s.nullable(s.object({ description: s.string })),
    levels: s.array(Level.schema),
    sustainability: s.nullable(
      s.object({ description: s.constant('Sustainability') }),
    ),
    gradCourseWork: s.nullable(s.constant(true)),
    workplaceExperience: s.nullable(s.object({ description: s.string })),
    foreignLanguage: s.nullable(s.object({ description: s.string })),
    courseDesignation: s.string,
    fullCourseDesignation: s.string,
    repeatable: s.string,
    gradingBasis: s.object({ code: s.string, description: s.string }),
    honors: s.unknown,
  });
}

export interface Details {
  description: string;
  prereqs: string;
  levels: string;
  credits: { min: number; max: number };
  lettersAndSciencesCredits: string;
  generalEd: string;
  ethnicStudies: string;
  lastTaught: TermCode | null;
  crossListed: string;
  breadth: string;
  subjectNotes: string[];
  sustainability: boolean;
  gradCourseWork: boolean;
  workplaceExperience: string | null;
  foreignLanguage: string | null;
  departmentURI: string | null;
  undergraduateCatalogURI: string | null;
  graduateCatalogURI: string | null;
  repeatable: boolean;

  /**
   * Set to true iff the `RawDetails.gradingBasis.code == "CNC"`
   */
  isPassFail: boolean;
}

export namespace Details {
  export const from = (raw: RawDetails): Details => {
    return {
      description: raw.description,
      prereqs: raw.enrollmentPrerequisites ?? 'None',
      levels: raw.levels.map(l => l.description).join(', '),
      credits: {
        min: raw.minimumCredits,
        max: raw.maximumCredits,
      },
      lettersAndSciencesCredits:
        raw.lettersAndScienceCredits?.description ?? '',
      generalEd: raw.generalEd?.description ?? '',
      ethnicStudies: raw.ethnicStudies?.description ?? '',
      lastTaught: TermCode.decodeOrNull(raw.lastTaught),
      crossListed: raw.allCrossListedSubjects
        .map(c => `${c.shortDescription} ${raw.catalogNumber}`)
        .join(', '),
      breadth: raw.breadths.map(b => b.description).join(' or '),
      subjectNotes: raw.subject.footnotes.flatMap(note => note.split(/\n+/)),
      sustainability: !!raw.sustainability,
      gradCourseWork: !!raw.gradCourseWork,
      workplaceExperience: raw.workplaceExperience?.description ?? null,
      foreignLanguage: raw.foreignLanguage?.description ?? null,
      departmentURI: raw.subject.departmentURI,
      undergraduateCatalogURI: raw.subject.undergraduateCatalogURI,
      graduateCatalogURI: raw.subject.graduateCatalogURI,
      repeatable: raw.repeatable === 'Y',
      isPassFail: raw.gradingBasis.code === 'CNC',
    };
  };
}
