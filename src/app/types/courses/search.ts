import { RawDetails, Details } from './details';
import { WithEra, TermCode } from '@app/types/terms';
import { curry2 } from '@app/core/utils';
import { Course, HasTermCodeWithEra } from './attributes';

export interface RawSearchResult extends RawDetails {
  hasChanges: unknown;
}

export interface RawSearchResults {
  found: number;
  hits: RawSearchResult[];
  message: null;
  success: boolean;
}

export type SearchResult = Course &
  HasTermCodeWithEra & {
    hasChanges: boolean;
  };

export namespace SearchResult {
  export const from = (
    active: TermCode[],
    raw: RawSearchResult,
  ): SearchResult => {
    return {
      // HasTitle
      shortCatalog: `${raw.subject.shortDescription} ${raw.catalogNumber}`,
      longCatalog: `${raw.subject.description} ${raw.catalogNumber}`,
      title: raw.title,

      // HasSubject
      subject: {
        code: raw.subject.subjectCode,
        name: raw.subject.shortDescription,
        description: raw.subject.description,
      },

      // HasCourseId
      courseId: raw.courseId,

      // HasTermCode
      termCode: WithEra.fromTermCode(
        active,
        TermCode.decodeOrThrow(raw.termCode),
      ),

      // HasCredits
      credits: { min: raw.minimumCredits, max: raw.maximumCredits },

      // HasLazyDetails
      details: { mode: 'loaded', ...Details.from(raw) },

      // other SearchResult fields
      hasChanges: !!raw.hasChanges,
    };
  };

  export const fromCurry = curry2(from);
}
