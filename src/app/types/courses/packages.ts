import Schema, * as s from '@app/types/schema';
import {
  toHourMinuteMeridianWithDST,
  toHourMinuteMeridianWithoutDST,
  toMonthDayWithDST,
  toMonthDayYearWithDST,
} from '@app/shared/date-formats';
import { TermCode, Session } from '../terms';
import { curry2, notNull } from '@app/core/utils';
import { HasClassNumber, HasRelatedClassNumbers } from './attributes';

export interface RawInstructor {
  email: string | null;
  name: {
    first: string | null;
    last: string | null;
  };
}

export namespace RawInstructor {
  export const schema: Schema<RawInstructor> = s.object({
    email: s.nullable(s.string),
    name: s.object({
      first: s.nullable(s.string),
      last: s.nullable(s.string),
    }),
  });

  export const equals = (a: RawInstructor, b: RawInstructor) => {
    return (
      a.email === b.email &&
      a.name.first === b.name.first &&
      a.name.last === b.name.last
    );
  };

  export const dedupe = (acc: RawInstructor[], next: RawInstructor) => {
    if (acc.some(prev => equals(next, prev))) {
      return acc;
    } else {
      return acc.concat(next);
    }
  };
}

interface RealBuilding {
  buildingCode: string;
  buildingName: string;
  streetAddress: string;
  latitude: number;
  longitude: number;
}

namespace RealBuilding {
  export const schema: Schema<RealBuilding> = s.object({
    buildingCode: s.string,
    buildingName: s.string,
    streetAddress: s.string,
    latitude: s.number,
    longitude: s.number,
  });
}

interface OnlineBuilding {
  buildingCode: '0000C';
  buildingName: 'ONLINE';
}

namespace OnlineBuilding {
  export const schema: Schema<OnlineBuilding> = s.object({
    buildingCode: s.constant('0000C'),
    buildingName: s.constant('ONLINE'),
  });
}

interface UnknownBuilding {
  buildingCode: unknown; // could be `0000`
  buildingName: string;
}

namespace UnknownBuilding {
  export const schema: Schema<UnknownBuilding> = s.object({
    buildingCode: s.unknown,
    buildingName: s.string,
  });
}

export type Building = RealBuilding | OnlineBuilding | UnknownBuilding | null;

export namespace Building {
  export const schema: Schema<Building> = s.nullable(
    s
      .or(RealBuilding.schema)
      .or(OnlineBuilding.schema)
      .or(UnknownBuilding.schema),
  );

  export const isOnline = (building: Building): building is OnlineBuilding => {
    return building?.buildingName === 'ONLINE';
  };
}

export const isOnlineBuilding = (b: Building): b is OnlineBuilding => {
  return b !== null && b.buildingName === 'ONLINE';
};

export type DayOfTheWeek =
  | 'MONDAY'
  | 'TUESDAY'
  | 'WEDNESDAY'
  | 'THURSDAY'
  | 'FRIDAY'
  | 'SATURDAY'
  | 'SUNDAY';

export namespace DayOfTheWeek {
  const shortToLong: Record<string, DayOfTheWeek> = {
    N: 'SUNDAY',
    M: 'MONDAY',
    T: 'TUESDAY',
    W: 'WEDNESDAY',
    R: 'THURSDAY',
    F: 'FRIDAY',
    S: 'SATURDAY',
  };

  export const longToShort: Record<DayOfTheWeek, string> = {
    SUNDAY: 'N',
    MONDAY: 'M',
    TUESDAY: 'T',
    WEDNESDAY: 'W',
    THURSDAY: 'R',
    FRIDAY: 'F',
    SATURDAY: 'S',
  };

  export const abbrToLong: Record<string, DayOfTheWeek> = {
    sun: 'SUNDAY',
    mon: 'MONDAY',
    tue: 'TUESDAY',
    wed: 'WEDNESDAY',
    thu: 'THURSDAY',
    fri: 'FRIDAY',
    sat: 'SATURDAY',
  };

  export const short = (day: DayOfTheWeek) => {
    switch (day) {
      case 'MONDAY':
        return 'Mon';
      case 'TUESDAY':
        return 'Tue';
      case 'WEDNESDAY':
        return 'Wed';
      case 'THURSDAY':
        return 'Thu';
      case 'FRIDAY':
        return 'Fri';
      case 'SATURDAY':
        return 'Sat';
      case 'SUNDAY':
        return 'Sun';
    }
  };

  export const fromString = (days: string): DayOfTheWeek[] => {
    return Object.entries(shortToLong)
      .map(([short, long]) => {
        if (days.indexOf(short) > -1) {
          return long;
        } else {
          return null;
        }
      })
      .filter(notNull);
  };

  export const toString = (days: DayOfTheWeek[]): string => {
    return days.map(day => longToShort[day]).join('');
  };
}

export namespace DayOfTheWeek {
  export const schema: Schema<DayOfTheWeek> = s
    .or(s.constant('MONDAY'))
    .or(s.constant('TUESDAY'))
    .or(s.constant('WEDNESDAY'))
    .or(s.constant('THURSDAY'))
    .or(s.constant('FRIDAY'))
    .or(s.constant('SATURDAY'))
    .or(s.constant('SUNDAY'));
}

export interface RawMeeting {
  meetingType: 'CLASS' | 'EXAM';
  meetingTimeStart: number | null; // Careful! This is a MILLISECOND timestamp in 1970
  meetingTimeEnd: number | null; // Careful! This is a MILLISECOND timestamp in 1970
  meetingDays: string | null;
  meetingDaysList: DayOfTheWeek[];
  building: Building;
  room: string | null;
  examDate: number | null;
  startDate: unknown;
  endDate: unknown;
}

export namespace RawMeeting {
  export const schema: Schema<RawMeeting> = s.object({
    meetingType: s.or(s.constant('CLASS')).or(s.constant('EXAM')),
    meetingTimeStart: s.nullable(s.number),
    meetingTimeEnd: s.nullable(s.number),
    meetingDays: s.nullable(s.string),
    meetingDaysList: s.array(DayOfTheWeek.schema),
    building: Building.schema,
    room: s.nullable(s.string),
    examDate: s.nullable(s.number),
    startDate: s.any,
    endDate: s.any,
  });
}

export interface MeetingSchedule {
  type: 'meeting';
  days: string;
  daysList: DayOfTheWeek[];
  start: string;
  startSeconds: number;
  end: string;
  durationSeconds: number;
}

export interface ExamSchedule {
  type: 'exam';
  startMonthDayYear: string;
  startHourMinuteMeridian: string;
  endHourMinuteMeridian: string;
  timestamp: number;
}

export namespace ExamSchedule {
  export const from = (raw: RawMeeting): ExamSchedule => {
    console.assert(raw.meetingType === 'EXAM');
    console.assert(typeof raw.examDate === 'number');
    console.assert(typeof raw.meetingTimeStart === 'number');
    console.assert(typeof raw.meetingTimeEnd === 'number');
    return {
      type: 'exam',
      startMonthDayYear: toMonthDayYearWithDST(raw.examDate!),
      startHourMinuteMeridian: toHourMinuteMeridianWithDST(
        raw.meetingTimeStart!,
      ),
      endHourMinuteMeridian: toHourMinuteMeridianWithDST(raw.meetingTimeEnd!),
      timestamp: raw.examDate! + raw.meetingTimeStart!,
    };
  };
}

export namespace MeetingSchedule {
  const minimumMeeting = s.object({
    meetingDays: s.string,
    meetingDaysList: s.array(DayOfTheWeek.schema),
    meetingTimeStart: s.number,
    meetingTimeEnd: s.number,
  });

  export const from = (raw: unknown): MeetingSchedule | null => {
    // Madison, WI is 6 hours after GMT
    const CST_OFFSET = 6 * 3600;

    if (minimumMeeting.matches(raw)) {
      return {
        type: 'meeting',
        days: raw.meetingDays,
        daysList: raw.meetingDaysList,
        start: toHourMinuteMeridianWithoutDST(raw.meetingTimeStart),
        startSeconds: raw.meetingTimeStart / 1000 - CST_OFFSET,
        end: toHourMinuteMeridianWithoutDST(raw.meetingTimeEnd),
        durationSeconds: (raw.meetingTimeEnd - raw.meetingTimeStart) / 1000,
      };
    }

    return null;
  };
}

export type MeetingLocation =
  | { type: 'online' }
  | { type: 'classroom'; building: string; room: string | null }
  | { type: 'unknown'; building: string | null };

export namespace MeetingLocation {
  interface HasBuildingAndRoom {
    building: Building;
    room: string | null;
  }

  export const from = (raw: HasBuildingAndRoom): MeetingLocation => {
    if (Building.isOnline(raw.building)) {
      return { type: 'online' };
    } else if (RealBuilding.schema.matches(raw.building)) {
      return {
        type: 'classroom',
        building: raw.building.buildingName,
        room: raw.room ?? null,
      };
    } else {
      return {
        type: 'unknown',
        building: raw.building?.buildingName ?? null,
      };
    }
  };

  export const schema: Schema<MeetingLocation> = s
    .or(s.object({ type: s.constant('online') }))
    .or(
      s.object({
        type: s.constant('unknown'),
        building: s.nullable(s.string),
      }),
    )
    .or(
      s.object({
        type: s.constant('classroom'),
        building: s.string,
        room: s.nullable(s.string),
      }),
    );
}

export interface Meeting {
  location: MeetingLocation;
  schedule: MeetingSchedule | null;
}

export namespace Meeting {
  export const from = (raw: RawMeeting): Meeting => {
    return {
      location: MeetingLocation.from(raw),
      schedule: MeetingSchedule.from(raw),
    };
  };

  export const equals = (a: Meeting, b: Meeting) => {
    /**
     * Since there are sub-types of Meeting, don't stringify the entire object
     * but instead stringify each property individually and compare.
     */
    return (
      JSON.stringify(a.location) === JSON.stringify(b.location) &&
      JSON.stringify(a.schedule) === JSON.stringify(b.schedule)
    );
  };

  export const dedupe = (acc: Meeting[], next: Meeting) => {
    if (acc.some(prev => equals(next, prev))) {
      return acc;
    } else {
      return acc.concat(next);
    }
  };
}

export type DocId = string;

export interface HasDocId {
  docId: DocId;
}

export interface EnrollmentSection extends RawSection {
  sessionDescription: string;
}

export interface EnrollmentPackage
  extends HasClassNumber,
    HasRelatedClassNumbers,
    HasDocId {
  id: string;
  termCode: TermCode;
  /**
   * It's uncommon that this field will be `null` but it can happen especially
   * after the release of a new term catalog when data hasn't synced between all
   * layers of the stack.
   */
  status: 'OPEN' | 'WAITLISTED' | 'CLOSED' | null;
  totalAvailableSeats: number | null;
  totalCurrentlyEnrolled: number | null;
  totalCapacity: number | null;
  waitlistCapacity: number | null;
  requisites: string[];
  creditRange: string;
  credits: { min: number; max: number };
  classNotes: { type: string; text: string }[];
  commB: boolean;
  honors: string | null;

  /**
   * True iff the package's enrollment options indicate that users can join a
   * waitlist if this package is full when they try and enroll.
   */
  supportsWaitlist: boolean;

  classPermissionNumberNeeded: boolean;
  addConsent: string | null;
  dropConsent: string | null;
  exams: Exam[];
  sections: EnrollmentSection[];
  instructors: RawInstructor[];
  session: Session;
  instructorProvidedContent: InstructorProvidedContent;
  materials: ClassMaterial[];
  includesElectronicTextbook: boolean;
  isAsynchronous: boolean;
}

export interface Exam {
  when: string;
  where?: string;
}

/**
 * Most likely one of the following strings:
 *
 * - "LEC"
 * - "DIS"
 * - "LAB"
 * - "SEM"
 * - "IND"
 * - "FLD"
 * - "CON" (inactive)
 * - "RG1" (inactive)
 *
 * The type is deliberately a string and not something more specific because the
 * app should accommodate new or previouslly unknown section types without
 * causing an error. The only section type that is checked is LEC.
 */
export type RawSectionType = string;

export namespace RawSectionType {
  export const schema: Schema<RawSectionType> = s.string;
}

export interface RawClassAttribute {
  attributeCode: string;
  attributeDisplayName: string;
  valueCode: string;
  valueDescription: string | null;
}

export namespace RawClassAttribute {
  export const schema: Schema<RawClassAttribute> = s.object({
    attributeCode: s.string,
    attributeDisplayName: s.string,
    valueCode: s.string,

    // This nullability might not be strictly necessary but was added out of
    // paranoia - Isaac
    valueDescription: s.nullable(s.string),
  });
}

/**
 * Usually a constant like `"REQUIRED"` or `"RECOMMENDED"`
 */
export type MaterialRequirement = string;

export interface RawTextbook {
  title: string;
  isbn: string | null;
  publisher: string | null;
  author: string | null;
  year: string | null;
  edition: string | null;
  materialRequirement: MaterialRequirement;
  notes: unknown;
}

interface RawClassMaterial {
  noMaterialsInstructorMessage: string | null;
  sectionNotes: string | null;
  relatedUrls: string[];
  otherMaterials: {
    description: string;
    materialRequirement: MaterialRequirement;
    notes: unknown;
  }[];
  textbooks: RawTextbook[];
}

namespace RawClassMaterial {
  export const schema = s.object<RawClassMaterial>({
    noMaterialsInstructorMessage: s.nullable(s.string),
    sectionNotes: s.nullable(s.string),
    relatedUrls: s.array(s.string),
    otherMaterials: s.array(
      s.object({
        description: s.string,
        materialRequirement: s.string,
        notes: s.unknown,
      }),
    ),
    textbooks: s.array(
      s.object({
        title: s.string,
        isbn: s.nullable(s.string),
        publisher: s.nullable(s.string),
        author: s.nullable(s.string),
        year: s.nullable(s.string),
        edition: s.nullable(s.string),
        materialRequirement: s.string,
        notes: s.unknown,
      }),
    ),
  });
}

export interface RawSection {
  type: RawSectionType;
  sectionNumber: string;
  classUniqueId: { termCode: string; classNumber: number };
  instructionMode: string;
  footnotes: string[];
  honors: 'HONORS_ONLY' | 'INSTRUCTOR_APPROVED' | 'HONORS_LEVEL' | null;
  addConsent: { code: string; description: string | null } | null;
  dropConsent: { code: string; description: string | null } | null;
  comB: boolean;
  classMeetings: RawMeeting[];
  classAttributes: RawClassAttribute[];
  classMaterials: RawClassMaterial[];
  instructors: RawInstructor[];
  topic: null | {
    shortDescription: string;
    longDescription: string;
  };

  // Session data
  sessionCode: string;
}

export const NON_RESERVED_ATTR_CODES = ['SVCL', 'TEXT'];

export const isReservedSection = (sec: RawSection): boolean => {
  return (
    !!sec.classAttributes.length &&
    NON_RESERVED_ATTR_CODES.indexOf(sec.classAttributes[0].attributeCode) === -1
  );
};

export namespace RawSection {
  export const schema: Schema<RawSection> = s.object({
    type: RawSectionType.schema,
    sectionNumber: s.string,
    classUniqueId: s.object({ termCode: s.string, classNumber: s.number }),
    instructionMode: s.string,
    footnotes: s.array(s.string),
    honors: s.nullable(
      s
        .or(s.constant('HONORS_ONLY'))
        .or(s.constant('INSTRUCTOR_APPROVED'))
        .or(s.constant('HONORS_LEVEL')),
    ),
    addConsent: s.nullable(
      s.object({ code: s.string, description: s.nullable(s.string) }),
    ),
    dropConsent: s.nullable(
      s.object({
        code: s.string,
        description: s.nullable(s.string),
      }),
    ),
    comB: s.boolean,
    classMeetings: s.array(RawMeeting.schema),
    classAttributes: s.array(RawClassAttribute.schema),
    classMaterials: s.array(RawClassMaterial.schema),
    instructors: s.array(RawInstructor.schema),
    topic: s.nullable(
      s.object({
        shortDescription: s.string,
        longDescription: s.string,
      }),
    ),
    sessionCode: s.string,
  });
}

export type RawInstructorProvidedContent = null | {
  instructorDescription: string | null;
  format: string | null;
  labeledURIs: { uri: string; label: string }[] | null;
  keywords: string[] | null;
  typicalTopicsAndOrSchedule: string | null;
};

namespace RawInstructorProvidedContent {
  export const schema: Schema<RawInstructorProvidedContent> = s.nullable(
    s.object({
      instructorDescription: s.nullable(s.string),
      format: s.nullable(s.string),
      labeledURIs: s.nullable(
        s.array(
          s.object({
            uri: s.string,
            label: s.string,
          }),
        ),
      ),
      keywords: s.nullable(s.array(s.string)),
      typicalTopicsAndOrSchedule: s.nullable(s.string),
    }),
  );
}

export type InstructorProvidedContent = null | {
  description: string[];
  format: string[];
  labeledURIs: { uri: string; label: string }[];
  keywords: string[];
  typicalTopicsAndOrSchedule: string[];
};

export namespace InstructorProvidedContent {
  const process = (thing: unknown): string[] => {
    if (typeof thing === 'string') {
      return thing
        .split(/\n+/)
        .map(str => str.trim())
        .filter(str => str !== '');
    } else {
      return [];
    }
  };

  export const from = (
    raw: RawInstructorProvidedContent,
  ): InstructorProvidedContent => {
    const description = process(raw?.instructorDescription);
    const format = process(raw?.format);
    const labeledURIs = raw?.labeledURIs ?? [];
    const keywords = raw?.keywords ?? [];
    const typicalTopicsAndOrSchedule = process(raw?.typicalTopicsAndOrSchedule);

    const allEmpty =
      raw === null ||
      (!description.length &&
        !format.length &&
        !labeledURIs.length &&
        !keywords.length &&
        !typicalTopicsAndOrSchedule.length);

    if (allEmpty) {
      return null;
    } else {
      return {
        description,
        format,
        labeledURIs,
        keywords,
        typicalTopicsAndOrSchedule,
      };
    }
  };
}

export interface ClassMaterial {
  forSection: string;
  noMaterialsInstructorMessage: string[];
  sectionNotes: string[];
  relatedUrls: string[];
  otherMaterials: Array<{
    description: string;
    materialRequirement: string;
    notes: unknown;
  }>;
  textbooks: RawTextbook[];
}

namespace ClassMaterial {
  const process = (thing: unknown): string[] => {
    if (typeof thing === 'string') {
      return thing
        .split(/\n+/)
        .map(str => str.trim())
        .filter(str => str !== '');
    } else {
      return [];
    }
  };

  export const from = (
    sec: RawSection,
    raw: RawClassMaterial,
  ): ClassMaterial => {
    return {
      forSection: `${sec.type} ${sec.sectionNumber}`,
      noMaterialsInstructorMessage: process(raw.noMaterialsInstructorMessage),
      sectionNotes: process(raw.sectionNotes),
      relatedUrls: raw.relatedUrls.flatMap(process),
      otherMaterials: raw.otherMaterials,
      textbooks: raw.textbooks,
    };
  };

  export const fromCurry = curry2(from);

  export const isNotEmpty = (material: ClassMaterial): boolean => {
    return !(
      !material.noMaterialsInstructorMessage.length &&
      !material.sectionNotes.length &&
      !material.relatedUrls.length &&
      !material.otherMaterials.length &&
      !material.textbooks.length
    );
  };
}

export interface RawEnrollmentPackage {
  id: string;
  courseId: string;
  subjectCode: string;
  catalogNumber: string;
  termCode: string;
  packageEnrollmentStatus: {
    /**
     * It's uncommon that this field will be `null` but it can happen especially
     * after the release of a new term catalog when data hasn't synced between
     * all layers of the stack.
     */
    status: 'OPEN' | 'CLOSED' | 'WAITLISTED' | null;
  };
  enrollmentOptions: {
    classPermissionNumberNeeded: unknown;
    waitlist: unknown;
    relatedClasses: number[];
  };
  enrollmentStatus: null | {
    aggregateCapacity: number | null;
    aggregateCurrentlyEnrolled: number | null;
    aggregateWaitlistCapacity: number | null;
    capacity: number;
    currentlyEnrolled: number;
    openSeats: number;
    waitlistCapacity: number;
  };
  enrollmentRequirementGroups: null | {
    catalogRequirementGroups: { code: string; description: string }[];
    classAssociationRequirementGroups: { code: string; description: string }[];
  };
  classMeetings: RawMeeting[];
  sections: RawSection[];
  creditRange: string;
  docId: string;
  instructorProvidedClassDetails: RawInstructorProvidedContent;
  isAsynchronous: boolean;
}

export namespace RawEnrollmentPackage {
  export const schema: Schema<RawEnrollmentPackage> = s.object({
    id: s.string,
    courseId: s.string,
    subjectCode: s.string,
    catalogNumber: s.string,
    termCode: s.string,
    packageEnrollmentStatus: s.object({
      status: s.nullable(
        s
          .or(s.constant('OPEN'))
          .or(s.constant('CLOSED'))
          .or(s.constant('WAITLISTED')),
      ),
    }),
    enrollmentOptions: s.object({
      classPermissionNumberNeeded: s.unknown,
      waitlist: s.unknown,
      relatedClasses: s.array(s.number),
    }),
    enrollmentStatus: s.nullable(
      s.object({
        aggregateCapacity: s.nullable(s.number),
        aggregateCurrentlyEnrolled: s.nullable(s.number),
        aggregateWaitlistCapacity: s.nullable(s.number),
        capacity: s.number,
        currentlyEnrolled: s.number,
        openSeats: s.number,
        waitlistCapacity: s.number,
      }),
    ),
    enrollmentRequirementGroups: s.nullable(
      s.object({
        catalogRequirementGroups: s.array(
          s.object({ code: s.string, description: s.string }),
        ),
        classAssociationRequirementGroups: s.array(
          s.object({ code: s.string, description: s.string }),
        ),
      }),
    ),
    classMeetings: s.array(RawMeeting.schema),
    sections: s.array(RawSection.schema),
    creditRange: s.string,
    docId: s.string,
    instructorProvidedClassDetails: RawInstructorProvidedContent.schema,
    isAsynchronous: s.boolean,
  });
}

export namespace EnrollmentPackage {
  const requisites = (raw: RawEnrollmentPackage) => {
    const groups = raw.enrollmentRequirementGroups;
    const catalogReqs = groups?.catalogRequirementGroups ?? [];
    const classReqs = groups?.classAssociationRequirementGroups ?? [];
    return [
      ...catalogReqs.map(g => g.description),
      ...classReqs.map(g => g.description),
    ];
  };

  const classNotes = (raw: RawEnrollmentPackage) => {
    return raw.sections
      .filter(s => s.footnotes.length > 0)
      .map(s => ({
        type: s.type,
        text: s.footnotes.flatMap(l => l.split(/\n+/)).join(' '),
      }));
  };

  const commB = (raw: RawEnrollmentPackage) => {
    return raw.sections.some(s => s.comB);
  };

  const honors = (raw: RawEnrollmentPackage) => {
    const sec = raw.sections.find(s => s.honors);
    if (sec) {
      return sec.honors;
    } else {
      return null;
    }
  };

  const addConsent = (raw: RawEnrollmentPackage) => {
    const sec = raw.sections.find(s => s.addConsent?.code !== 'N');
    if (sec && sec.addConsent) {
      return sec.addConsent.description ?? sec.addConsent.code;
    } else {
      return null;
    }
  };

  const dropConsent = (raw: RawEnrollmentPackage) => {
    const sec = raw.sections.find(s => s.dropConsent?.code !== 'N');
    if (sec && sec.dropConsent) {
      return sec.dropConsent.description ?? sec.dropConsent.code;
    } else {
      return null;
    }
  };

  const exams = (raw: RawEnrollmentPackage) => {
    const found = raw.classMeetings
      .filter(m => m.meetingType === 'EXAM')
      .map(Exam.from);
    return found;
  };

  const instructors = (raw: RawEnrollmentPackage): RawInstructor[] => {
    return raw.sections.flatMap(s => s.instructors);
  };

  const session = (raw: RawEnrollmentPackage, sessions: Session[]): Session => {
    const sessionCode =
      raw.sections.length > 0 ? raw.sections[0].sessionCode : 'A1';
    const found = sessions.find(s => s.code === sessionCode);
    console.assert(!!found);
    return found!;
  };

  const credits = (raw: string): { min: number; max: number } => {
    if (/^(\d+)-(\d+)$/.test(raw)) {
      const min = parseInt(raw.split('-')[0], 10);
      const max = parseInt(raw.split('-')[1], 10);
      return { min, max };
    } else {
      const val = parseInt(raw, 10);
      console.assert(!isNaN(val));
      return { min: val, max: val };
    }
  };

  export const from = (
    sessions: Session[],
    raw: RawEnrollmentPackage,
  ): EnrollmentPackage => {
    const { enrollmentStatus: enstat } = raw;
    const totalAvailableSeats = enstat?.openSeats ?? null;
    const totalCurrentlyEnrolled =
      enstat?.aggregateCurrentlyEnrolled ?? enstat?.currentlyEnrolled ?? null;
    const totalCapacity = enstat?.aggregateCapacity ?? enstat?.capacity ?? null;
    const waitlistCapacity =
      enstat?.aggregateWaitlistCapacity ?? enstat?.waitlistCapacity ?? null;

    return {
      id: raw.id,
      classNumber: parseInt(raw.id, 10),
      relatedClassNumbers: raw.enrollmentOptions.relatedClasses,
      termCode: TermCode.decodeOrThrow(raw.termCode),
      status: raw.packageEnrollmentStatus.status,
      totalAvailableSeats,
      totalCurrentlyEnrolled,
      totalCapacity,
      waitlistCapacity,
      requisites: requisites(raw),
      creditRange: raw.creditRange,
      credits: credits(raw.creditRange),
      classNotes: classNotes(raw),
      commB: commB(raw),
      honors: honors(raw),
      supportsWaitlist: raw.enrollmentOptions.waitlist === true,
      classPermissionNumberNeeded:
        raw.enrollmentOptions.classPermissionNumberNeeded === true,
      addConsent: addConsent(raw),
      dropConsent: dropConsent(raw),
      exams: exams(raw),
      sections: raw.sections.map(sec => {
        const code = sec.sessionCode;
        const session = sessions.find(s => s.code === code);

        if (session) {
          const begin = toMonthDayWithDST(session.beginDate);
          const end = toMonthDayWithDST(session.endDate);
          return { ...sec, sessionDescription: `${begin} - ${end} (${code})` };
        } else {
          return { ...sec, sessionDescription: `(${code})` };
        }
      }),
      instructors: instructors(raw),
      session: session(raw, sessions),
      instructorProvidedContent: InstructorProvidedContent.from(
        raw.instructorProvidedClassDetails,
      ),
      materials: raw.sections
        .flatMap(section => {
          return section.classMaterials.map(ClassMaterial.fromCurry(section));
        })
        .filter(ClassMaterial.isNotEmpty),
      includesElectronicTextbook: raw.sections
        .flatMap(section => section.classAttributes)
        .some(attr => attr.attributeCode === 'TEXT'),
      docId: raw.docId,
      isAsynchronous: raw.isAsynchronous === true,
    };
  };

  export const fromCurry = curry2(from);
}

export namespace Exam {
  const when = (exam: RawMeeting): string => {
    if (
      exam.examDate === null ||
      exam.meetingTimeStart === null ||
      exam.meetingTimeEnd === null
    ) {
      return '';
    }

    const startDate = toMonthDayYearWithDST(exam.examDate);
    const startTime = toHourMinuteMeridianWithDST(exam.meetingTimeStart);
    const endTime = toHourMinuteMeridianWithDST(exam.meetingTimeEnd);
    return `${startDate} from ${startTime} - ${endTime}`;
  };

  const where = (exam: RawMeeting): string | undefined => {
    if (isOnlineBuilding(exam.building)) {
      return 'Online';
    } else if (exam.room !== null && exam.building !== null) {
      return `${exam.room} ${exam.building.buildingName}`;
    } else {
      return undefined;
    }
  };

  export const from = (raw: RawMeeting): Exam => {
    console.assert(raw.meetingType === 'EXAM');
    return {
      when: when(raw),
      where: where(raw),
    };
  };
}

export interface Exam {
  when: string;
  where?: string;
}
