import * as s from '@app/types/schema';
import { Term, Zero } from '@app/types/terms';
import { Course } from './attributes';
import { SavedCourseRef } from './refs';

export interface RawSavedCourse {
  catalogNumber: string | null;
  courseId: string;
  id: number;
  subjectCode: string;
  title: string;
  topicId: number;
}

export namespace RawSavedCourse {
  export const schema = s.object<RawSavedCourse>({
    catalogNumber: s.nullable(s.string),
    courseId: s.string,
    id: s.number,
    subjectCode: s.string,
    title: s.string,
    topicId: s.number,
  });
}

export type SavedId = number;

export interface SavedCourse extends Course {
  ref: SavedCourseRef;
}

export namespace SavedCourse {
  export const is = (course: Course | null): course is SavedCourse => {
    return s.object({ ref: SavedCourseRef.schema }).matches(course);
  };

  export const from = (term: Term<Zero>, raw: RawSavedCourse): SavedCourse => {
    const subject = term.subjects.find(({ code }) => code === raw.subjectCode);

    if (!subject) {
      throw new Error(`unknown subject-code ${raw.subjectCode} `);
    }

    return {
      ref: {
        kind: 'saved',
        savedId: raw.id,
      },

      // HasTitle
      shortCatalog: `${subject.name} ${raw.catalogNumber}`,
      longCatalog: `${subject.description} ${raw.catalogNumber}`,
      title: raw.title,

      // HasSubject
      subject,

      // HasCourseId
      courseId: raw.courseId,

      // HasCredits
      credits: { min: 0, max: 0 },

      // HasLazyDetails
      details: { mode: 'unloaded' },
    };
  };
}
