/**
 * In the curricular data space, there are a bunch of different kinds of
 * course-like objects. All of these different objects have slightly different
 * structures and fields. This creates tension because sometimes the code needs
 * to thuroughly describe the shape of an object and sometimes the code can
 * accept any object that has a few known fields.
 *
 * The types in this file try to resolve these differences by providing a large
 * number of small interfaces that can be combined to describe shared features
 * between different coruse-like objects while not over-constraining the code.
 */

import { Subject, TermCode, TermCodeOrZero, WithEra } from '@app/types/terms';
import { Loadable as CustomLoadable } from '../loadable';
import { Loadable } from 'loadable.ts';
import { Details } from './details';
import { EnrollmentPackage } from './packages';
import { CourseRef } from './refs';

export interface NamedCourse {
  name: string;
  title: string;
}

export type SubjectCode = string;
export type CourseId = string;

export interface CampusCourse extends NamedCourse {
  subjectCode: SubjectCode;
  courseId: CourseId;
}

/**
 * **All** course-like objects should conform to this interface as a minimum.
 */
export interface Course {
  /**
   * A human-readable description of the course. Something like "Introductory
   * Biology". Usually much longer than a course's catalog reference so take
   * care when creating layouts.
   */
  title: string;

  /**
   * The subject short description combined with the course's catalog nubmer. For
   * example "COMP SCI 200".
   */
  shortCatalog: string;

  /**
   * The subject long description combined with the course's catalog nubmer. For
   * example "COMPUTER SCIENCE 200".
   */
  longCatalog: string;

  /**
   * All course-like objects will have a subject. When the backend only provides
   * an object with a subject code, it's the responsibility of the frontend to
   * turn that into a more useful Subject object.
   */
  subject: Subject;

  /**
   * A NON-UNIQUE identifier attached to a course. Multiple courses in the same
   * term can share courseIds. Has the pattern `\d{6}(\.\d{2})?`
   */
  courseId: string;

  /**
   * A tuple containing the minimum and maximum of a course's credit range. This
   * DOES NOT represent which credits a user chose for a course. Instead it
   * represents the range of credit values supported by this course.
   */
  credits: { min: number; max: number };

  /**
   * Some course objects are sent from the backend with all course details
   * included in the object. Other course objects require a second API call to
   * load the course details. Both of these cases are handled by this property.
   */
  details: CustomLoadable<Details>;
}

export interface HasCourseRef<Ref extends CourseRef = CourseRef> {
  ref: Ref;
}

/**
 * Used for courses with an enrollment status (enrolled, waitlisted, dropped)
 * and cart courses when the user HAS picked an enrollment package.
 */
export interface HasClassNumber {
  classNumber: number;
}

export interface HasRelatedClassNumbers {
  relatedClassNumbers: number[];
}

/**
 * Most course objects (except saved-for-later courses) have an associated
 * TermCode.
 */
export interface HasTermCode {
  termCode: TermCode;
}

/**
 * Some course objects need a TermCode AND to know whether that TermCode is
 * before, within, or after the active TermCode set.
 */
export interface HasTermCodeWithEra {
  termCode: WithEra;
}

/**
 * A much weaker constraint that captures all course-like objects that have a
 * TermCode AND course-like objects that do not (like saved-for-later courses).
 */
export interface HasTermCodeOrZero {
  termCode: TermCodeOrZero;
}

export interface HasLazyPackages {
  packages: Loadable<EnrollmentPackage[]> | null;
}

/**
 * All courses (including saved-for-later) have am associated subject.
 */
export interface HasSubject {
  subject: Subject;
}

/**
 * All courses (including saved-for-later) has an associated subject.
 */
export interface HasSubjectCode {
  subject: { code: string };
}

/**
 * All courses (including saved-for-later) have am associated courseId.
 */
export interface HasCourseId {
  courseId: string;
}

export interface HasShortCatalog {
  /**
   * The subject short description combined with the course's catalog nubmer. For
   * example "COMP SCI 200".
   */
  shortCatalog: string;
}
