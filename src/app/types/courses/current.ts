import {
  EnrollmentPackage,
  ExamSchedule,
  Meeting,
  MeetingLocation,
  MeetingSchedule,
  RawMeeting,
} from './packages';
import Schema, * as s from '@app/types/schema';
import { UNLOADED } from '@app/types/loadable';
import { curry2 } from '@app/core/utils';
import { Term, TermCode } from '@app/types/terms';
import { Course, HasTermCode } from './attributes';
import { Loadable } from 'loadable.ts';
import { CurrentCourseRef } from './refs';

export interface RawCurrentMeeting extends RawMeeting {
  sectionType: string;
}

export namespace RawCurrentMeeting {
  export const schema: Schema<RawCurrentMeeting> = s
    .and(RawMeeting.schema)
    .and(s.object({ sectionType: s.string }));
}

export interface CurrentMeeting extends Meeting {
  sectionType: string;
}

export interface CurrentExamMeeting {
  sectionType: string;
  location: MeetingLocation;
  schedule: ExamSchedule;
}

export namespace CurrentMeeting {
  export const from = (raw: RawCurrentMeeting): CurrentMeeting => {
    return {
      ...Meeting.from(raw),
      sectionType: raw.sectionType,
    };
  };
}

/**
 * Used for current courses (enrolled, waitlisted, dropped courses).
 *
 * BUG: There's a potential bug where the credit range for the chosen package is
 * different from the credit range for the overall course. In this case the only
 * credit range available to the frontend is the overall course credit range. If
 * that range is used to provide the user with a list of credit values to pick
 * from the user may be allowed to pick a credit value that the package doesn't
 * actually support.
 */
export interface HasPackage {
  package: {
    classNumber: number;
    credits: number;
    docId: string;
    classMeetings: CurrentMeeting[];
    examMeetings: CurrentExamMeeting[];
    autoEnrollClasses: number[];
    relatedClassNumber1: number | null;
    relatedClassNumber2: number | null;
    waitlist: 'notAvailable' | 'notChosen' | 'chosen';
    honors: 'notAvailable' | 'notChosen' | 'chosen';
    schedules: Record<string, Array<MeetingSchedule | null>>;
    locations: Record<string, MeetingLocation[]>;
    details: null | Loadable<EnrollmentPackage>;
    classPermissionNumberNeeded: boolean;
  };
}

export type EnrolledStatus = 'enrolled' | 'waitlisted' | 'dropped';

export namespace EnrolledStatus {
  export const isDropped = (course: { enrolledStatus: EnrolledStatus }) => {
    return course.enrolledStatus === 'dropped';
  };

  export const isEnrolled = (course: { enrolledStatus: EnrolledStatus }) => {
    return course.enrolledStatus === 'enrolled';
  };

  export const isWaitlisted = (course: { enrolledStatus: EnrolledStatus }) => {
    return course.enrolledStatus === 'waitlisted';
  };
}

export interface Section {
  // currently only this field is needed
  classNumber: string;
  sectionType: string;
  sectionNumber: string;
}

export namespace Section {
  export const schema = s.object<Section>({
    classNumber: s.string,
    sectionType: s.string,
    sectionNumber: s.string,
  });
}

export interface RawCurrentCourse {
  id: string;
  courseId: string;
  termCode: string;
  subjectCode: string;
  catalogNumber: string;
  credits: number;
  classNumber: number;
  docId: string;
  classMeetings: RawCurrentMeeting[];
  studentEnrollmentStatus: 'D' | 'E' | 'W';
  honors: boolean;
  details: {
    title: string;
    courseDesignation: string;
    fullCourseDesignation: string;
    maximumCredits: number;
    minimumCredits: number;
  };
  autoEnrollClasses: string[] | null;
  enrollmentOptions: {
    classPermissionNumberNeeded: unknown;
    relatedClasses: number[];
    waitlist: unknown;
    honors: unknown;
  };
  sections: Array<{
    classSSRComponent: string; // I think this is equivalent to `sectionType` - Isaac
    classSection: string; // I think this is equivalent to `sectionNumber` - Isaac
    classNumber: string;
  }>;
}

export namespace RawCurrentCourse {
  export const schema = s.object<RawCurrentCourse>({
    id: s.string,
    courseId: s.string,
    termCode: s.string,
    subjectCode: s.string,
    catalogNumber: s.string,
    credits: s.number,
    classNumber: s.number,
    docId: s.string,
    classMeetings: s.array(
      s.and(s.object({ sectionType: s.string })).and(RawMeeting.schema),
    ),
    studentEnrollmentStatus: s
      .or(s.constant('D'))
      .or(s.constant('E'))
      .or(s.constant('W')),
    honors: s.boolean,
    details: s.object({
      title: s.string,
      courseDesignation: s.string,
      fullCourseDesignation: s.string,
      maximumCredits: s.number,
      minimumCredits: s.number,
    }),
    autoEnrollClasses: s.nullable(s.array(s.string)),
    enrollmentOptions: s.object({
      classPermissionNumberNeeded: s.unknown,
      relatedClasses: s.array(s.number),
      waitlist: s.unknown,
      honors: s.unknown,
    }),
    sections: s.array(
      s.object({
        classSSRComponent: s.string,
        classSection: s.string,
        classNumber: s.string,
      }),
    ),
  });
}

export type CurrentId = string;

export interface HasCurrentId {
  currentId: CurrentId;
}

export namespace HasCurrentId {
  export const equals = (a: HasCurrentId, b: HasCurrentId): boolean => {
    return a.currentId === b.currentId;
  };

  export const equalsCurry = curry2(equals);
}

export type CurrentCourse = { kind: 'current' } & Course &
  HasCurrentId &
  HasTermCode &
  HasPackage & {
    ref: CurrentCourseRef;
    enrolledStatus: EnrolledStatus;
    sections: Section[];
    classPermissionNumber: number | null;

    /**
     * A store for arbitrary data about the course. Be careful, don't assume
     * anything about the contents of this object.
     */
    data: Record<string, unknown>;
  };

export namespace CurrentCourse {
  export const is = (course: Course | null): course is CurrentCourse => {
    return s.object({ kind: s.constant('current') }).matches(course);
  };

  const toEnrollmentStatus = (
    raw: RawCurrentCourse['studentEnrollmentStatus'],
  ): EnrolledStatus => {
    switch (raw) {
      case 'E':
        return 'enrolled';
      case 'W':
        return 'waitlisted';
      case 'D':
        return 'dropped';
    }
  };

  const toWaitlist = (
    raw: RawCurrentCourse,
  ): CurrentCourse['package']['waitlist'] => {
    if (raw.studentEnrollmentStatus === 'W') {
      return 'chosen';
    } else if (raw.enrollmentOptions.waitlist === true) {
      return 'notChosen';
    } else {
      return 'notAvailable';
    }
  };

  const toHonors = (
    raw: RawCurrentCourse,
  ): CurrentCourse['package']['honors'] => {
    if (raw.honors === true) {
      return 'chosen';
    } else if (raw.enrollmentOptions.honors === true) {
      return 'notChosen';
    } else {
      return 'notAvailable';
    }
  };

  export const from = (
    term: Term<TermCode>,
    raw: RawCurrentCourse,
  ): CurrentCourse => {
    const subject = term.subjects.find(({ code }) => code === raw.subjectCode);

    if (!subject) {
      throw new Error(
        `unknown subject-code ${raw.subjectCode} for ${raw.termCode}`,
      );
    }

    return {
      kind: 'current',
      currentId: raw.id,
      ref: {
        kind: 'current',
        termCode: term.termCode,
        currentId: raw.id,
      },

      // HasTitle
      shortCatalog: raw.details.courseDesignation,
      longCatalog: raw.details.fullCourseDesignation,
      title: raw.details.title,

      // HasSubject
      subject,

      // HasCourseId
      courseId: raw.courseId,

      // HasTermCode
      termCode: TermCode.decodeOrThrow(raw.termCode),

      // HasCredits
      credits: {
        min: raw.details.minimumCredits,
        max: raw.details.maximumCredits,
      },

      // HasPackage
      package: {
        classNumber: raw.classNumber,
        credits: raw.credits,
        docId: raw.docId,
        classMeetings: (raw.classMeetings || [])
          .filter(meeting => meeting.meetingType === 'CLASS')
          .map(CurrentMeeting.from),
        examMeetings: (raw.classMeetings || [])
          .filter(meeting => meeting.meetingType === 'EXAM')
          .map(exam => {
            return {
              sectionType: exam.sectionType,
              location: MeetingLocation.from(exam),
              schedule: ExamSchedule.from(exam),
            };
          }),
        schedules: raw.classMeetings
          .filter(meeting => meeting.meetingType === 'CLASS')
          .reduce((acc, meeting) => {
            const catalogRef = `${meeting.sectionType}`;
            if (!acc[catalogRef]) {
              acc[catalogRef] = [];
            }

            acc[catalogRef].push(MeetingSchedule.from(meeting));

            return acc;
          }, {} as HasPackage['package']['schedules']),
        locations: raw.classMeetings
          .filter(meeting => meeting.meetingType === 'CLASS')
          .reduce((acc, meeting) => {
            const catalogRef = `${meeting.sectionType}`;
            if (!acc[catalogRef]) {
              acc[catalogRef] = [];
            }

            acc[catalogRef].push(MeetingLocation.from(meeting));

            return acc;
          }, {} as HasPackage['package']['locations']),
        autoEnrollClasses: (raw.autoEnrollClasses ?? []).map(classNum => {
          return parseInt(classNum, 10);
        }),
        relatedClassNumber1: raw.enrollmentOptions.relatedClasses[0] ?? null,
        relatedClassNumber2: raw.enrollmentOptions.relatedClasses[1] ?? null,
        waitlist: toWaitlist(raw),
        honors: toHonors(raw),
        details: null,
        classPermissionNumberNeeded:
          raw.enrollmentOptions.classPermissionNumberNeeded === true,
      },

      // HasLazyDetails
      details: UNLOADED,

      // other fields
      enrolledStatus: toEnrollmentStatus(raw.studentEnrollmentStatus),
      sections: raw.sections.map(sec => ({
        sectionType: sec.classSSRComponent,
        sectionNumber: sec.classSection,
        classNumber: sec.classNumber,
      })),

      classPermissionNumber: null,

      data: {},
    };
  };

  export const fromCurry = curry2(from);
}
