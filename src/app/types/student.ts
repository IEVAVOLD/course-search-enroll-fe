import { UnixMilliseconds } from '@app/types';
import Schema, * as s from '@app/types/schema';

export interface RawStudentInfo {
  enrollmentImpacts: {
    registrationAppointments: {
      termCode: string;
      registrationDateTime: UnixMilliseconds;
    }[];
  } | null;
}

export namespace RawStudentInfo {
  export const schema: Schema<RawStudentInfo> = s.object({
    enrollmentImpacts: s.nullable(
      s.object({
        registrationAppointments: s.array(
          s.object({
            termCode: s.string,
            registrationDateTime: s.number,
          }),
        ),
      }),
    ),
  });
}

export interface StudentInfo {
  [key: string]: unknown;
}

export namespace StudentInfo {
  export const schema = s.nullable(s.object<StudentInfo>({}));
}
