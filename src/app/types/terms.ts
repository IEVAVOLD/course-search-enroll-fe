import { curry2 } from '@app/core/utils';
import Schema, * as s from './schema';

type Digit = '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9';

namespace Digit {
  export const schema: Schema<Digit> = s.pattern(/^\d$/) as Schema<any>;
}

export const FALL = '2' as const;
export const WINTER = '3' as const;
export const SPRING = '4' as const;
export const SUMMER = '6' as const;

type TermDigit = typeof FALL | typeof WINTER | typeof SPRING | typeof SUMMER;

namespace TermDigit {
  export const schema: Schema<TermDigit> = s
    .or(s.constant('2'))
    .or(s.constant('3'))
    .or(s.constant('4'))
    .or(s.constant('6'));
}

export interface YearCode {
  readonly century: Digit;
  readonly year: [Digit, Digit];
}

export interface TermCode extends YearCode {
  readonly kind: 'termcode';
  readonly term: TermDigit;
}

export enum Era {
  Past,
  Active,
  Future,
}

export interface WithEra extends TermCode {
  era: Era;
}

export interface Zero {
  readonly kind: 'zero';
}

export type TermCodeOrZero = TermCode | Zero;

export namespace YearCode {
  const PATTERN = /^\d{3}$/i;

  export const isValid = (t: string): boolean => {
    return PATTERN.test(t);
  };

  export const decodeOrNull = (u: unknown): YearCode | null => {
    if (typeof u === 'string' && isValid(u)) {
      const [century, tens, ones] = u;
      return {
        century: century as Digit,
        year: [tens, ones] as [Digit, Digit],
      };
    }
    return null;
  };

  export const decodeOrThrow = (u: unknown): YearCode => {
    const decoded = decodeOrNull(u);
    if (decoded === null) {
      throw new Error(`'${u}' is not a valid year-code`);
    } else {
      return decoded;
    }
  };

  export const encode = ({ century, year: [tens, ones] }: YearCode) => {
    return `${century}${tens}${ones}`;
  };

  export const toFall = (y: YearCode): TermCode => {
    return { ...y, kind: 'termcode', term: FALL };
  };

  export const toSpring = (y: YearCode): TermCode => {
    return { ...y, kind: 'termcode', term: SPRING };
  };

  export const toSummer = (y: YearCode): TermCode => {
    return { ...y, kind: 'termcode', term: SUMMER };
  };

  export const beginningYear = (y: YearCode): number => {
    return endingYear(y) - 1;
  };

  export const endingYear = (y: YearCode): number => {
    const century = 1900 + parseInt(y.century, 10) * 100;
    const year = century + parseInt(y.year.join(''), 10);
    return year;
  };

  export const equals = (a: YearCode, b: YearCode): boolean => {
    return encode(a) === encode(b);
  };

  export const comesBefore = (before: YearCode, after: YearCode) => {
    return encode(before) < encode(after);
  };

  export const comesAfter = (after: YearCode, before: YearCode) => {
    return encode(after) > encode(before);
  };

  export const equalsCurry = curry2(equals);
  export const comesBeforeCurry = curry2(comesBefore);
  export const comesAfterCurry = curry2(comesAfter);

  export const sort = (a: YearCode, b: YearCode): number => {
    if (comesBefore(a, b)) {
      return -1;
    } else if (comesAfter(a, b)) {
      return 1;
    } else {
      return 0;
    }
  };

  export const next = (y: YearCode): YearCode => {
    const [tens, ones] = y.year;
    const century: Digit =
      tens === '9' && ones === '9' ? nextDigit(y.century) : y.century;
    const year: [Digit, Digit] =
      ones === '9'
        ? [tens === '9' ? '0' : nextDigit(tens), '0']
        : [tens, nextDigit(ones)];
    return { ...y, century, year };
  };

  export const describe = (y: YearCode): string => {
    return `${beginningYear(y)}-${endingYear(y)}`;
  };
}

export namespace TermCode {
  const PATTERN = /^\d{3}[2346]$/i;

  export const isValid = (t: string): boolean => {
    return PATTERN.test(t);
  };

  export const encode = ({ century, year: [tens, ones], term }: TermCode) => {
    return `${century}${tens}${ones}${term}`;
  };

  /**
   * Given any input, return a TermCode iff the input is a string that can be
   * parsed into a valid non-zero term-code. In all other cases return null.
   *
   * @param u unknown
   * @returns TermCode | null
   */
  export const decodeOrNull = (u: unknown): TermCode | null => {
    if (typeof u === 'string' && isValid(u)) {
      const [century, tens, ones, term] = u;
      return {
        kind: 'termcode',
        century: century as Digit,
        year: [tens, ones] as [Digit, Digit],
        term: term as TermDigit,
      };
    }
    return null;
  };

  /**
   * Given any input, return a TermCode iff the input is a string that can be
   * parsed into a valid non-zero term-code. In all other cases throw an error.
   *
   * @param u unknown
   * @returns TermCode
   * @throws Error
   */
  export const decodeOrThrow = (u: unknown): TermCode => {
    const decoded = decodeOrNull(u);
    if (decoded === null) {
      throw new Error(`'${u}' is not a valid term-code`);
    } else {
      return decoded;
    }
  };

  export const capitalTermName = (t: TermCode) => {
    switch (t.term) {
      case '2':
      case '3':
        return 'Fall';
      case '4':
        return 'Spring';
      case '6':
        return 'Summer';
    }
  };

  export const lowerTermName = (t: TermCode) => {
    switch (t.term) {
      case '2':
      case '3':
        return 'fall';
      case '4':
        return 'spring';
      case '6':
        return 'summer';
    }
  };

  export const describe = (t: TermCode): string => {
    const term = capitalTermName(t);
    const year =
      term === 'Fall' ? YearCode.beginningYear(t) : YearCode.endingYear(t);
    return `${term} ${year}`;
  };

  export const equals = (a: TermCode, b: TermCode) => encode(a) === encode(b);

  export const notEquals = (a: TermCode, b: TermCode) => !equals(a, b);

  export const comesBefore = (before: TermCode, after: TermCode) => {
    return encode(before) < encode(after);
  };

  export const comesAfter = (after: TermCode, before: TermCode) => {
    return encode(after) > encode(before);
  };

  export const equalsCurry = curry2(equals);
  export const comesBeforeCurry = curry2(comesBefore);
  export const comesAfterCurry = curry2(comesAfter);

  export const sort = (a: TermCode, b: TermCode): number => {
    if (comesBefore(a, b)) {
      return -1;
    } else if (comesAfter(a, b)) {
      return 1;
    } else {
      return 0;
    }
  };

  const isDigit = (u: unknown): u is Digit => {
    return typeof u === 'string' && u.length === 1 && u >= '0' && u <= '9';
  };

  export const is = (u: unknown): u is TermCode => {
    if (typeof u !== 'object' || u === null) {
      return false;
    }

    const { kind, century, year, term } = u as any;
    if (kind === 'termcode') {
      return (
        isDigit(century) &&
        Array.isArray(year) &&
        year.length === 2 &&
        isDigit(year[0]) &&
        isDigit(year[1]) &&
        isDigit(term) &&
        /^(2|3|4|6)$/.test(term)
      );
    }

    return false;
  };

  export const schema: Schema<TermCode> = s.object({
    kind: s.constant('termcode'),
    century: Digit.schema,
    year: s.custom('year', (u): u is [Digit, Digit] => {
      return (
        Array.isArray(u) &&
        u.length === 2 &&
        Digit.schema.matches(u[0]) &&
        Digit.schema.matches(u[1])
      );
    }),
    term: TermDigit.schema,
  });

  export const next = (t: TermCode): TermCode => {
    switch (t.term) {
      case FALL:
      case WINTER:
        return { ...t, term: SPRING };
      case SPRING:
        return { ...t, term: SUMMER };
      case SUMMER: {
        const [tens, ones] = t.year;
        const century: Digit =
          tens === '9' && ones === '9' ? nextDigit(t.century) : t.century;
        const year: [Digit, Digit] =
          ones === '9'
            ? [tens === '9' ? '0' : nextDigit(tens), '0']
            : [tens, nextDigit(ones)];
        return { ...t, century, year, term: FALL };
      }
    }
  };

  export const isActive = (active: TermCode[], t: TermCode): boolean => {
    return active.some(TermCode.equalsCurry(t));
  };

  export const isFuture = (active: TermCode[], t: TermCode): boolean => {
    return active.some(TermCode.comesAfterCurry(t));
  };

  export const isPast = (active: TermCode[], t: TermCode): boolean => {
    return active.every(TermCode.comesBeforeCurry(t));
  };

  export const findFirst = (termCodes: TermCode[]): TermCode => {
    console.assert(termCodes.length > 0);

    const sorted = termCodes.slice();
    sorted.sort(TermCode.sort);
    return sorted[0];
  };

  export const isContainedInArray = (
    needle: TermCode,
    haystack: TermCode[],
  ) => {
    return haystack.some(TermCode.equalsCurry(needle));
  };
}

export namespace WithEra {
  export const fromTermCodeAndEra = (era: Era, t: TermCode): WithEra => {
    return { ...t, era };
  };

  export const fromTermCode = (active: TermCode[], t: TermCode): WithEra => {
    console.assert(active.length > 0);

    if (TermCode.isActive(active, t)) {
      return { ...t, era: Era.Active };
    } else if (TermCode.isFuture(active, t)) {
      return { ...t, era: Era.Future };
    } else {
      return { ...t, era: Era.Past };
    }
  };

  export const fromYearCode = (active: TermCode[], y: YearCode) => {
    return {
      fall: WithEra.fromTermCode(active, YearCode.toFall(y)),
      spring: WithEra.fromTermCode(active, YearCode.toSpring(y)),
      summer: WithEra.fromTermCode(active, YearCode.toSummer(y)),
    };
  };

  export const fromTermCodeCurry = curry2(fromTermCode);

  export const is = (u: unknown): u is WithEra => {
    if (TermCode.is(u)) {
      const { era } = u as any;
      return Era[era] !== undefined;
    }

    return false;
  };

  export const next = (active: TermCode[], t: WithEra): WithEra => {
    return fromTermCode(active, TermCode.next(t));
  };

  export const isActive = (t: WithEra): boolean => {
    return t.era === Era.Active;
  };
}

export const ZERO: Zero = { kind: 'zero' };

export namespace Zero {
  export const isValid = (t: string): boolean => {
    return t === '0000';
  };

  export const encode = (_: Zero): string => {
    return '0000';
  };

  export const decodeOrNull = (u: unknown): Zero | null => {
    return typeof u === 'string' && isValid(u) ? ZERO : null;
  };

  export const decodeOrThrow = (u: unknown): Zero => {
    const decoded = decodeOrNull(u);
    if (decoded === null) {
      throw new Error(`'${u}' is not a valid term-code`);
    } else {
      return decoded;
    }
  };

  export const describe = (_: Zero): string => {
    return 'Any term';
  };

  export const is = (u: unknown): u is Zero => {
    if (typeof u !== 'object' || u === null) {
      return false;
    }

    const { kind } = u as any;
    return kind === 'zero';
  };
}

export namespace TermCodeOrZero {
  export const isValid = (t: string): boolean => {
    return Zero.isValid(t) || TermCode.isValid(t);
  };

  export const encode = (t: TermCodeOrZero): string => {
    switch (t.kind) {
      case 'zero':
        return Zero.encode(t);
      case 'termcode':
        return TermCode.encode(t);
    }
  };

  export const decodeOrNull = (u: unknown): TermCodeOrZero | null => {
    return typeof u === 'string'
      ? Zero.decodeOrNull(u) ?? TermCode.decodeOrNull(u)
      : null;
  };

  export const decodeOrThrow = (u: unknown): TermCodeOrZero => {
    return Zero.decodeOrNull(u) ?? TermCode.decodeOrThrow(u);
  };

  export const describe = (t: TermCodeOrZero): string => {
    switch (t.kind) {
      case 'zero':
        return Zero.describe(t);
      case 'termcode':
        return TermCode.describe(t);
    }
  };

  export const is = (u: unknown): u is TermCodeOrZero => {
    return Zero.is(u) || TermCode.is(u);
  };

  export const equals = (a: TermCodeOrZero, b: TermCodeOrZero) => {
    return encode(a) === encode(b);
  };

  export const comesBefore = (
    before: TermCodeOrZero,
    after: TermCodeOrZero,
  ) => {
    return encode(before) < encode(after);
  };

  export const comesAfter = (after: TermCodeOrZero, before: TermCodeOrZero) => {
    return encode(after) > encode(before);
  };

  export const equalsCurry = curry2(equals);
  export const comesBeforeCurry = curry2(comesBefore);
  export const comesAfterCurry = curry2(comesAfter);

  export const sort = (a: TermCodeOrZero, b: TermCodeOrZero): number => {
    if (comesBefore(a, b)) {
      return -1;
    } else if (comesAfter(a, b)) {
      return 1;
    } else {
      return 0;
    }
  };
}

export interface RawSubject {
  termCode: string;
  subjectCode: string;
  formalDescription: string;
}

export interface Subject {
  code: string;
  name: string;
  description: string;
}

export namespace Subject {
  export const schema: Schema<Subject> = s.object({
    code: s.string,
    name: s.string,
    description: s.string,
  });

  export const from = (subjects: Record<string, string>) => {
    return (raw: RawSubject): Subject => {
      return {
        code: raw.subjectCode,
        name: subjects[raw.subjectCode] ?? 'UNKNOWN',
        description: upperToSentenceCase(raw.formalDescription),
      };
    };
  };

  export const sort = (a: Subject, b: Subject): -1 | 0 | 1 => {
    if (a.description > b.description) {
      return +1;
    } else if (a.description < b.description) {
      return -1;
    } else {
      return 0;
    }
  };
}

const STOP_WORDS = ['and', 'of', 'in', 'the', 'to', 'that'];

const upperToSentenceCase = (str: string): string => {
  return str
    .split(/(\W)/)
    .map(w => {
      w = w.toLowerCase();
      if (STOP_WORDS.includes(w) === false) {
        w = w.charAt(0).toUpperCase() + w.substr(1);
      }
      return w;
    })
    .join('');
};

export interface RawSession {
  termCode: string;
  sessionCode: string;
  sessionDescription: string;
  beginDate: number;
  endDate: number;

  addDate: number | null;
  dropDate: number | null;
  drwDate: number | null;
  editCourseDeadlineDate: number | null;
  editCourseDeadlineGradDate: number | null;
}

export interface Session {
  code: string;
  description: string;
  beginDate: number;
  endDate: number;
  termCode: string;
  addDate: number | null;
  dropDate: number | null;
  drwDate: number | null;
  editCourseDeadlineDate: number | null;
  editCourseDeadlineGradDate: number | null;
}

export namespace Session {
  export const schema: Schema<Session> = s.object({
    code: s.string,
    description: s.string,
    beginDate: s.number,
    endDate: s.number,
    termCode: s.string,
    addDate: s.nullable(s.number),
    dropDate: s.nullable(s.number),
    drwDate: s.nullable(s.number),
    editCourseDeadlineDate: s.nullable(s.number),
    editCourseDeadlineGradDate: s.nullable(s.number),
  });

  export const from = (raw: RawSession): Session => {
    return {
      code: raw.sessionCode,
      description: raw.sessionDescription,
      beginDate: raw.beginDate,
      endDate: raw.endDate,
      termCode: raw.termCode,
      addDate: raw.addDate,
      dropDate: raw.dropDate,
      drwDate: raw.drwDate,
      editCourseDeadlineDate: raw.editCourseDeadlineDate,
      editCourseDeadlineGradDate: raw.editCourseDeadlineGradDate,
    };
  };
}

export type Level =
  | { code: 'E'; description: 'Elementary' }
  | { code: 'I'; description: 'Intermediate' }
  | { code: 'A'; description: 'Advanced' };

export namespace Level {
  // prettier-ignore
  export const schema: Schema<Level> = s
    .or(s.object({ code: s.constant('E'), description: s.constant('Elementary') }))
    .or(s.object({ code: s.constant('I'), description: s.constant('Intermediate') }))
    .or(s.object({ code: s.constant('A'), description: s.constant('Advanced') }));

  export const sort = (a: Level, b: Level): -1 | 0 | 1 => {
    if (a.code === 'E' || (a.code === 'I' && b.code === 'A')) {
      return -1;
    } else if (b.code === 'E' || (b.code === 'I' && a.code === 'A')) {
      return +1;
    } else {
      return 0;
    }
  };
}

export interface CrossListing {
  termCode: string;
  subjectCode: string;
  description: string;
  shortDescription: string;
  formalDescription: string;
}

export namespace CrossListing {
  export const schema: Schema<CrossListing> = s.object({
    termCode: s.string,
    subjectCode: s.string,
    description: s.string,
    shortDescription: s.string,
    formalDescription: s.string,
  });
}

export interface RawSpecialGroup {
  termCode: string;
  attributeCode: string;
  attributeDisplayName: string;
  valueCode: string;
  valueDescription: string;
}

export interface SpecialGroup {
  code: string;
  attribute: string;
  description: string;
}

export namespace SpecialGroup {
  export const isPlausibleAttr = (candidate: string): boolean => {
    return /^[A-Z]+$/.test(candidate);
  };

  export const schema: Schema<SpecialGroup> = s.object({
    code: s.string,
    attribute: s.string,
    description: s.string,
  });

  export const isAcceptable = (raw: RawSpecialGroup): boolean => {
    return (
      raw.valueCode !== null &&
      raw.attributeCode !== null &&
      raw.valueDescription !== null
    );
  };

  export const from = (raw: RawSpecialGroup): SpecialGroup => {
    return {
      code: raw.valueCode,
      attribute: raw.attributeCode,
      description: raw.valueDescription,
    };
  };
}

export type SpecialGroups = Array<{
  attr: SpecialGroup['attribute'];
  desc: string;
  groups: SpecialGroup[];
}>;

export namespace SpecialGroups {
  export const from = (raw: RawSpecialGroup[]): SpecialGroups => {
    const attrs = [];
    const grouped: Record<string, SpecialGroups[number]> = {};

    for (const rawGroup of raw) {
      if (grouped.hasOwnProperty(rawGroup.attributeCode) === false) {
        attrs.push(rawGroup.attributeCode);
        grouped[rawGroup.attributeCode] = {
          attr: rawGroup.attributeCode,
          desc: rawGroup.attributeDisplayName,
          groups: [],
        };
      }

      grouped[rawGroup.attributeCode].groups.push(SpecialGroup.from(rawGroup));
    }

    // Give some consistency to the order that the group filter is displayed in.
    attrs.sort();

    return attrs.map(attr => grouped[attr]);
  };
}

/**
 * TERMS
 */

export interface RawTerm {
  termCode: string;
  shortDescription: string;
  longDescription: string;
  beginDate: number;
  endDate: number;
  instructionBeginDate: number;
  instructionEndDate: number;
  academicYear: string;
  pastTerm: boolean;
}

/**
 * The shape of a RawTerms object is meant to match the shape of the JSON blobs
 * returned by the `enrollment-cache-updater` lambda that updates the data
 * behind the /aggregate endpoint every day.
 */
export interface RawTerms {
  terms: RawTerm[];
  subjects: { [termCode: string]: RawSubject[] };
  sessions: { termCode: string; sessions: RawSession[] }[];
  specialGroups: RawSpecialGroup[][];
}

export interface Term<T extends TermCodeOrZero = TermCodeOrZero> {
  termCode: T;
  subjects: Subject[];
  sessions: Session[];
  specialGroups: SpecialGroups;
}

export const toTermCodes = (terms: Terms): TermCode[] => {
  return Object.values(terms)
    .map(term => term.termCode)
    .filter(TermCode.is)
    .sort(TermCode.sort);
};

export interface Terms {
  zero: Term<Zero>;
  nonZero: { [termCode: string]: Term<TermCode> };
  activeTermCodes: TermCode[];
}

export const EMPTY_TERMS: Terms = {
  zero: {
    termCode: ZERO,
    subjects: [],
    sessions: [],
    specialGroups: [],
  },
  nonZero: {},
  activeTermCodes: [],
};

const toZeroTerm = (
  raw: RawTerms,
  subjects: Record<string, string>,
): Term<Zero> => {
  return {
    ...EMPTY_TERMS.zero,
    subjects: (raw.subjects['0000'] ?? []).map(Subject.from(subjects)),
  };
};

const toTerm = (
  subjects: Record<string, string>,
  termCode: TermCode,
  raw: RawTerms,
): Term<TermCode> => {
  const encoded = TermCode.encode(termCode);
  const inTerm = (withTermCode: { termCode: string }) => {
    return withTermCode.termCode === encoded;
  };
  return {
    termCode,
    subjects: (raw.subjects[encoded] ?? []).map(Subject.from(subjects)),
    sessions: (raw.sessions.find(inTerm)?.sessions ?? []).map(Session.from),
    specialGroups: SpecialGroups.from(raw.specialGroups.flat().filter(inTerm)),
  };
};

export const toTerms = (
  raw: RawTerms,
  subjects: Record<string, string>,
): Terms => {
  const terms: Terms = {
    zero: toZeroTerm(raw, subjects),
    nonZero: {},
    activeTermCodes: [],
  };

  for (const { termCode } of raw.terms) {
    const decoded = TermCode.decodeOrThrow(termCode);
    terms.activeTermCodes.push(decoded);
    terms.nonZero[termCode] = toTerm(subjects, decoded, raw);
  }

  return terms;
};

const nextDigit = (d: Digit): Digit => {
  //         cast the digit to a number
  //         |    then increment by 1
  //         |    |    then mod 10 to wrap numbers >9 back to single digits
  //         |    |    |
  //         v    v    v
  return `${(+d + 1) % 10}` as Digit;
};
