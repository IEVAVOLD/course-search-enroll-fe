import { RoadmapCourse } from './courses';
import * as s from '@app/types/schema';

/**
 * A JSON object sent to the `/api/enroll` endpoint to start an enrollment. The
 * logic around constructing this object is complex and should be handled by
 * the `EnrollRequest.from` function.
 */
export interface EnrollRequest {
  subjectCode: string;
  courseId: string;
  classNumber: string;
  options: {
    credits: number;
    waitlist: true | null;
    honors: true | null;
    relatedClassNumber1: string | null;
    relatedClassNumber2: string | null;
    classPermissionNumber: number | null;
  };
}

export namespace EnrollRequest {
  export const schema = s.object<EnrollRequest>({
    subjectCode: s.string,
    courseId: s.string,
    classNumber: s.string,
    options: s.object({
      credits: s.number,
      waitlist: s.nullable(s.constant(true)),
      honors: s.nullable(s.constant(true)),
      relatedClassNumber1: s.nullable(s.string),
      relatedClassNumber2: s.nullable(s.string),
      classPermissionNumber: s.nullable(s.number),
    }),
  });
}

/**
 * Checks some basic things about a course before triggering an API call to
 * start enrolling. Checks include:
 *
 * - Has the user picked an enrollment package for the course?
 * - Does the enrollment package have a selected credit value?
 */
export const passesEnrollSanityCheck = (course: RoadmapCourse): boolean => {
  if (course.package === null) {
    // Course is missing an enrollment package
    return false;
  }

  if (
    course.package.choices.credits.hasChoice &&
    course.package.choices.credits.total === null
  ) {
    // Package lets the user pick a credit value
    // but the user hasn't picked one yet.
    return false;
  }

  return true;
};
