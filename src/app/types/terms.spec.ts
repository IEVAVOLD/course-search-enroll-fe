import { RawTerms, TermCode, toTerms } from '@app/types/terms';

import aggregateJson from '../../../testdata/aggregate.json';
import subjectsJson from '../../../testdata/subjects-0000.json';

describe('TermCode', () => {
  it('should be a term code', () => {
    let isATermCode = TermCode.is(TermCode.decodeOrNull('1222'));

    expect(isATermCode).toEqual(true);
  });
  it('should format Fall term code', () => {
    let code = TermCode.decodeOrNull('1222');

    expect(code).toBeTruthy();

    if (code) {
      let desc = TermCode.describe(code);
      expect(desc).toEqual('Fall 2021');
    }
  });
  it('should format winter term code as fall', () => {
    let code = TermCode.decodeOrNull('1223');

    expect(code).toBeTruthy();

    if (code) {
      let desc = TermCode.describe(code);
      expect(desc).toEqual('Fall 2021');
    }
  });
  it('should format Spring term code', () => {
    let code = TermCode.decodeOrNull('1224');

    expect(code).toBeTruthy();

    if (code) {
      let desc = TermCode.describe(code);
      expect(desc).toEqual('Spring 2022');
    }
  });
  it('should format Summer term code', () => {
    let code = TermCode.decodeOrNull('1226');

    expect(code).toBeTruthy();

    if (code) {
      let desc = TermCode.describe(code);
      expect(desc).toEqual('Summer 2022');
    }
  });
  it('should fail invalid term code', () => {
    let code = TermCode.decodeOrNull('1225');

    expect(code).toBeFalsy();
  });
  it('equals should work', () => {
    let code = TermCode.decodeOrNull('1222');
    let code2 = TermCode.decodeOrNull('1232');
    let code3 = TermCode.decodeOrNull('1222');

    expect(code).not.toEqual(code2);
    expect(TermCode.equals(code as any, code2 as any)).toBe(false);

    expect(code).toEqual(code3);
    expect(TermCode.equals(code as any, code3 as any)).toBe(true);
    // This line below throws an error, since TermCode.equals does not handle null input values,
    // which is due to TermCode.encode not handling null input values.
    // expect(TermCode.equals(code as any,TermCode.decodeOrNull('1225') as any)).toThrow()
  });
  it('should handle toTerms', () => {
    let termData = toTerms(aggregateJson as RawTerms, subjectsJson);
    // didn't end up with an empty result
    expect(termData).toBeTruthy();
    // should be one active term in the captured test data (1222)
    expect(termData.activeTermCodes.length).toEqual(1);
    // the number of subjects in 0000 agrees with the short description list for 0000
    expect(termData.zero.subjects.length).toEqual(
      Object.keys(subjectsJson).length,
    );
    // the active term has the correct term code
    expect(termData.nonZero['1222'].termCode).toEqual(
      TermCode.decodeOrThrow('1222'),
    );
    // the active term does not have an empty subject list
    expect(termData.nonZero['1222'].subjects.length).toBeGreaterThan(0);
    //the active term does not have more subjects than the 0000 term
    expect(termData.nonZero['1222'].subjects.length).toBeLessThanOrEqual(
      Object.keys(subjectsJson).length,
    );
    // it's easy enough to just check that they are all properly mapped to the short descriptions
    // so check all subject.description values in the output for the matching subject in the short description list
    termData.zero.subjects.forEach(subject => {
      expect(subject?.description === (subjectsJson as any)[subject.code]);
    });
    termData.nonZero['1222'].subjects.forEach(subject => {
      expect(subject?.description === (subjectsJson as any)[subject.code]);
    });
  });
});

describe('WithEra', () => {
  const expectFuture = (active: string[], termCode: string) => {
    const a = active.map(TermCode.decodeOrThrow);
    const t = TermCode.decodeOrThrow(termCode);
    expect(TermCode.isPast(a, t)).toBeFalsy();
    expect(TermCode.isActive(a, t)).toBeFalsy();
    expect(TermCode.isFuture(a, t)).toBeTruthy();
  };

  const expectPast = (active: string[], termCode: string) => {
    const a = active.map(TermCode.decodeOrThrow);
    const t = TermCode.decodeOrThrow(termCode);
    expect(TermCode.isPast(a, t)).toBeTruthy();
    expect(TermCode.isActive(a, t)).toBeFalsy();
    expect(TermCode.isFuture(a, t)).toBeFalsy();
  };

  it('should consider a term future if any active terms are prior', () => {
    const spring2022 = '1224';
    const fall2022 = '1232';
    const summer2022 = '1226';
    const summer2023 = '1236';

    expectFuture([spring2022, fall2022], summer2022); // note summer 2022 is *between* active terms
    expectFuture([spring2022, fall2022], summer2023);
  });

  it('should consider a term past if all active terms are subsequent', () => {
    const spring2022 = '1224';
    const fall2022 = '1232';
    const fall2021 = '1222';
    const spring2000 = '1006';

    expectPast([spring2022, fall2022], fall2021);
    expectPast([spring2022, fall2022], spring2000);
  });
});
