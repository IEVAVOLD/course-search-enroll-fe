import { Component, Input } from '@angular/core';

@Component({
  selector: 'cse-filter-group',
  templateUrl: './filter-group.component.html',
  styleUrls: ['./filter-group.component.scss'],
})
export class FilterGroupComponent {
  @Input() public icon!: string;
  @Input() public label!: string;
}
