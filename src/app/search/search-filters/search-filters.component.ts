import { Component, Inject, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  State,
  FORM_ID,
  MIN_CREDIT_VALUE,
  MAX_CREDIT_VALUE,
  MIN_CATALOG_NUMBER,
  MAX_CATALOG_NUMBER,
  FormState,
} from '@search/store/state';
import { combineLatest, Observable, ReplaySubject, Subscription } from 'rxjs';
import * as globalSelectors from '@app/selectors';
import * as searchSelectors from '@search/store/selectors';
import {
  TermCodeOrZero,
  Zero,
  Subject,
  Session,
  SpecialGroup,
  TermCode,
  ZERO,
  SpecialGroups,
} from '@app/types/terms';
import * as actions from '@search/store/actions';
import { box, ResetAction, SetValueAction } from 'ngrx-forms';
import { fuzzy } from '@search/fuzzy-search';
import { UntypedFormControl } from '@angular/forms';
import { map, filter, mergeMap, first } from 'rxjs/operators';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { ifSuccessThenUnwrap, notNull } from '@app/core/utils';
import { DialogService } from '@app/shared/services/dialog.service';
import { AddByClassNumDialogComponent } from '../add-by-class-num-dialog/add-by-class-num-dialog.component';
import { IS_PRIVATE } from '@app/services/is-private.token';
import { MatRadioChange } from '@angular/material/radio';
import { isSuccess } from 'loadable.ts';
import { MatSelectChange } from '@angular/material/select';

@Component({
  selector: 'cse-search-filters',
  templateUrl: './search-filters.component.html',
  styleUrls: ['./search-filters.component.scss'],
})
export class SearchFiltersComponent implements OnDestroy {
  public form = this.store.select(searchSelectors.getForm);

  public termCodesOrArray: Observable<TermCode[]> = this.store
    .select(globalSelectors.terms.getActiveTermCodes)
    .pipe(
      map(termCodes => {
        if (termCodes.success) {
          return termCodes.value;
        } else {
          return [];
        }
      }),
    );

  public isZeroTerm: Observable<boolean> = this.store
    .select(searchSelectors.chosenTermCode)
    .pipe(map(termCode => termCode === null || Zero.is(termCode)));

  public currentSessions: Observable<Session[]> = this.store
    .select(searchSelectors.chosenTermCode)
    .pipe(
      filter(notNull),
      mergeMap(termCode =>
        this.store.select(globalSelectors.terms.getSessionsForTerm, termCode),
      ),
      ifSuccessThenUnwrap(),
    );

  public currentSubjects: Observable<Subject[]> = this.store
    .select(searchSelectors.chosenTermCode)
    .pipe(
      mergeMap(termCode =>
        this.store.select(
          globalSelectors.terms.getSubjectsForTerm,
          termCode ?? ZERO,
        ),
      ),
      ifSuccessThenUnwrap(),
    );

  public reservedSectionsAttr = new UntypedFormControl('all');
  public reservedSectionsCode = new UntypedFormControl(null);
  private _reservedSectionsForTerm$ = combineLatest([
    this.store
      .select(searchSelectors.chosenTermCode)
      .pipe(map<TermCode | null, TermCodeOrZero>(term => term ?? ZERO)),
    this.store.select(globalSelectors.terms.getAll),
  ]).pipe(
    map(([termCode, allTermData]): SpecialGroups | null => {
      if (!isSuccess(allTermData)) {
        return null;
      }

      const term = Zero.is(termCode)
        ? allTermData.value.zero
        : allTermData.value.nonZero[TermCode.encode(termCode)];

      if (!term) {
        return null;
      }

      return term.specialGroups;
    }),
  );
  public filteredReservedSections$ = combineLatest([
    this._reservedSectionsForTerm$,
    this.store.select(searchSelectors.getReservedSections),
  ]).pipe(
    map(([specialGroups, value]): Array<SpecialGroup> => {
      if (!specialGroups || value === 'all' || value === 'none') {
        return [];
      }

      const withSameAttr = ({ attr }: { attr: string }) =>
        attr === value.value.attr;
      const matchingGroups = specialGroups.find(withSameAttr)?.groups;
      return matchingGroups ?? [];
    }),
  );
  public availableReservedSectionAttrs$ = this._reservedSectionsForTerm$.pipe(
    map(specialGroups => specialGroups ?? []),
  );

  public isFormDisabled = this.store.select(searchSelectors.isFormDisabled);

  public formId = FORM_ID;

  public subjectFilterCtrl = new UntypedFormControl();
  public filteredSubjects = new ReplaySubject<Array<Subject | null>>(1);

  // Updating this list of subjects isn't ideal but it's an easy way to build
  // a synchronous function to filter subjects.
  private subjectCache: Array<Subject | null> = [];

  public termFilterLabel = 'All terms';
  public subjectFilterLabel = 'All subjects';

  public absoluteMinCreditValue = MIN_CREDIT_VALUE;
  public absoluteMaxCreditValue = MAX_CREDIT_VALUE;

  public MIN_CATALOG_NUMBER = MIN_CATALOG_NUMBER;
  public MAX_CATALOG_NUMBER = MAX_CATALOG_NUMBER;

  public updatedMinCreditValue: Observable<number> = this.store.select(
    searchSelectors.minCreditValue,
    MIN_CREDIT_VALUE,
  );

  public updatedMaxCreditValue: Observable<number> = this.store.select(
    searchSelectors.maxCreditValue,
    MAX_CREDIT_VALUE,
  );

  private subscriptions: Subscription[] = [];

  constructor(
    public store: Store<State>,
    private announcer: LiveAnnouncer,
    private dialog: DialogService,
    @Inject(IS_PRIVATE) public isPrivate: boolean,
  ) {
    /**
     * Pass an empty array to the filtered subjects list so that the there's
     * always _some_ value to render the filterable list. When the user starts
     * typing, the `filteredSubjects` will be updated from the list of subjects
     * in the current term.
     */
    this.filteredSubjects.next([]);

    this.subscriptions = [
      /**
       * Listen for the chosen term to change. When the term changes, enable or
       * disable some of the filters.
       */
      store.select(searchSelectors.chosenTermCode).subscribe(termCode => {
        this.termFilterLabel = termCode ? 'Term' : 'All terms';
      }),

      combineLatest([
        store.select(searchSelectors.chosenTermCode),
        store.select(searchSelectors.getReservedSections),
      ]).subscribe(([termCode, reservedSections]) => {
        if (!termCode && reservedSections !== 'all') {
          this._setReservedSections('all');
        }
      }),

      store.select(searchSelectors.getReservedSections).subscribe(value => {
        if (value === 'all' || value === 'none') {
          this.reservedSectionsAttr.setValue(value);
          this.reservedSectionsCode.setValue(null);
        } else {
          const { attr, code } = value.value;
          this.reservedSectionsAttr.setValue(attr);
          this.reservedSectionsCode.setValue(code);
        }
      }),

      store.select(searchSelectors.chosenSubject).subscribe(subject => {
        this.subjectFilterLabel = subject ? 'Subject' : 'All subjects';
      }),

      /**
       * Update the list of subjects whenever the term changes.
       */
      this.currentSubjects.subscribe(subjects => {
        this.subjectCache = [null, ...subjects.slice().sort(Subject.sort)];
        this.applySubjectFilter();
      }),

      /**
       * Update the filtered list of subjects when the subject filter changes
       */
      this.subjectFilterCtrl.valueChanges.subscribe(() => {
        this.applySubjectFilter();
      }),

      store.select(searchSelectors.getReservedSections).subscribe(value => {
        if (value === 'all' || value === 'none') {
          this.reservedSectionsAttr.setValue(value);
          this.reservedSectionsCode.setValue(null);
          this.reservedSectionsCode.disable();
        } else {
          this.reservedSectionsCode.enable();
        }
      }),
    ];
  }

  private _setReservedSections(value: FormState['reservedSections']) {
    const controlId = `${FORM_ID}.reservedSections`;
    const action = new SetValueAction(controlId, value);
    this.store.dispatch(action);
  }

  setReservedSectionsAttr({ value }: MatRadioChange) {
    let newValue: FormState['reservedSections'] = 'all';
    if (value === 'all' || value === 'none') {
      newValue = value;
    } else if (SpecialGroup.isPlausibleAttr(value)) {
      newValue = box({ attr: value, code: null });
    } else {
      // If for some reason the new attribute value couldn't be reasonable
      // parsed, ignore the change
      return;
    }

    this._setReservedSections(newValue);
  }

  setReservedSectionsCode({ value }: MatSelectChange) {
    if (typeof value !== 'string') {
      return;
    }

    this._setReservedSections(
      box({
        attr: this.reservedSectionsAttr.value,
        code: value,
      }),
    );
  }

  applySubjectFilter() {
    const rawFilter: unknown = this.subjectFilterCtrl.value;
    if (!rawFilter) {
      this.filteredSubjects.next(this.subjectCache);
    } else {
      const search = `${rawFilter ?? ''}`.toLowerCase();
      const filtered = fuzzy<null | Subject>(search, this.subjectCache, s => {
        return s?.description ?? 'All subjects';
      });
      this.filteredSubjects.next(filtered);
    }
  }

  async onAddByClassNumDialog() {
    const termCode = await this.store
      .select(searchSelectors.chosenTermCode)
      .pipe(first())
      .toPromise();

    if (!TermCode.is(termCode)) {
      // Don't show the dialog if the current term is zero.
      return;
    }

    const sessions = await this.store
      .select(globalSelectors.terms.getSessionsForTerm, termCode)
      .pipe(ifSuccessThenUnwrap(), first())
      .toPromise();

    await this.dialog.small(AddByClassNumDialogComponent, {
      termCode,
      sessions,
    });
  }

  onClearSubjectFilter() {
    this.store.dispatch(new SetValueAction(`${FORM_ID}.subject`, null));
    this.store.dispatch(new ResetAction(`${FORM_ID}.subject`));
  }

  onClearSessionFilter() {
    this.store.dispatch(new SetValueAction(`${FORM_ID}.session`, null));
    this.store.dispatch(new ResetAction(`${FORM_ID}.session`));
  }

  onSubmit() {
    this.announcer.announce('Searching for courses', 'assertive');
    this.store.dispatch(actions.search.withCurrentFilters());
  }

  onReset(): void {
    this.store.dispatch(actions.filters.reset());
    this.store.dispatch(new ResetAction(FORM_ID));
  }

  compareSubjects(s1: unknown, s2: unknown): boolean {
    if (s1 === s2) {
      return true;
    } else if (Subject.schema.matches(s1) && Subject.schema.matches(s2)) {
      return s1.code === s2.code;
    } else {
      return false;
    }
  }

  compareTermCodes(t1: unknown, t2: unknown): boolean {
    return (
      TermCodeOrZero.is(t1) &&
      TermCodeOrZero.is(t2) &&
      TermCodeOrZero.equals(t1, t2)
    );
  }

  compareSessions(s1: unknown, s2: unknown): boolean {
    if (s1 === s2) {
      return true;
    } else if (Session.schema.matches(s1) && Session.schema.matches(s2)) {
      return s1.code === s2.code;
    } else {
      return false;
    }
  }

  compareSpecialGroups(g1: unknown, g2: unknown): boolean {
    if (g1 === g2) {
      return true;
    } else if (
      SpecialGroup.schema.matches(g1) &&
      SpecialGroup.schema.matches(g2)
    ) {
      return g1.code === g2.code;
    } else {
      return false;
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
