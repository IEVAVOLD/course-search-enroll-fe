import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  Inject,
} from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Store } from '@ngrx/store';
import { OrderBy, isOrderBy } from '@search/store/state';
import * as actions from '@search/store/actions';
import {
  Observable,
  Subscription,
  ReplaySubject,
  combineLatest,
  Subject,
} from 'rxjs';
import * as globalSelectors from '@app/selectors';
import * as searchSelectors from '@search/store/selectors';
import * as globalActions from '@app/actions';
import {
  filter,
  first,
  map,
  pairwise,
  startWith,
  withLatestFrom,
} from 'rxjs/operators';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Breakpoints } from '@app/shared/breakpoints';
import {
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { GlobalState } from '@app/state';
import { EnrollmentPackage, SearchResult } from '@app/types/courses';
import { DialogService } from '@app/shared/services/dialog.service';
import { AddToPlanDialogComponent } from '@search/add-to-plan-dialog/add-to-plan-dialog.component';
import { failed, isSuccess, LOADING, success } from 'loadable.ts';
import * as s from '@app/types/schema';
import { TermCode } from '@app/types/terms';
import { RawPlan } from '@app/types/plans';
import { MatSidenav } from '@angular/material/sidenav';
import { Filters as PackageFilters } from '@app/shared/components/course-packages/course-packages.component';
import { notNull } from '@app/core/utils';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { IS_PRIVATE } from '@app/services/is-private.token';

interface PackageWithEnrollOptions {
  supportsWaitlist: boolean;
  supportsHonors: boolean;
  supportsCredits: boolean;
  classPermissionNumberNeeded: boolean;
  credits: { min: number; max: number };
  group: UntypedFormGroup;
}

@Component({
  selector: 'cse-search-view',
  templateUrl: './search-view.component.html',
  styleUrls: ['./search-view.component.scss'],
  animations: [
    trigger('pane', [
      transition('* <=> onscreen', animate('.3s ease-in')),
      state('offscreenLeft', style({ transform: 'translateX(-100%)' })),
      state('onscreen', style({ transform: 'translateX(0)' })),
      state('offscreenRight', style({ transform: 'translateX(100%)' })),
    ]),
  ],
})
export class SearchViewComponent implements OnInit, OnDestroy {
  @ViewChild('detailCollapseButton')
  public detailsCollapseButtonRef!: ElementRef;

  @ViewChild('sidenav') public sidenavRef!: MatSidenav;

  public focusOnIndex = new Subject<number>();
  public isMobile = new ReplaySubject<boolean>();
  private subscriptions: Subscription[] = [];
  public renderPackages = false;
  public paneState: PaneState;
  public orderBy!: Observable<OrderBy>;
  public orderByCtrl = new UntypedFormControl('relevance');

  public selectedCourse: Observable<SearchResult | null> = this.store.select(
    searchSelectors.getSelectedCourse,
  );

  public resultsLabel$: Observable<string | null> = this.store
    .select(searchSelectors.getTotalResults)
    .pipe(
      map(results => {
        if (typeof results === 'number') {
          return `${results.toLocaleString()} ${
            results === 1 ? 'result' : 'results'
          }`;
        } else if (results === 'loading') {
          return 'Loading results';
        } else {
          return null;
        }
      }),
    );

  public showOrderByDropdown$: Observable<boolean> = this.store
    .select(searchSelectors.getResults)
    .pipe(map(res => res !== 'unloaded' && res.total !== 'loading'));

  public detailsLabel: Observable<string | null> = this.store
    .select(searchSelectors.details)
    .pipe(map(details => details?.shortCatalog ?? null));

  public packagesForSelectedCourse$ = this.store
    .select(searchSelectors.sectionsState)
    .pipe(
      map(packages => {
        if (packages.mode === 'error') {
          return failed(null);
        } else if (packages.mode === 'loaded') {
          return success(packages.packages);
        } else {
          return LOADING;
        }
      }),
    );

  public initialPackageFilters$: Observable<PackageFilters> = this.store
    .select(searchSelectors.getCurrentFilters)
    .pipe(
      filter(notNull),
      map((formState): PackageFilters => {
        let includeReserved: PackageFilters['includeReserved'] = 'include';
        if (formState.reservedSections === 'none') {
          includeReserved = 'exclude';
        } else if (
          formState.reservedSections !== 'all' &&
          formState.reservedSections.value.code !== null
        ) {
          includeReserved = {
            attr: formState.reservedSections.value.attr,
            code: formState.reservedSections.value.code,
          };
        }

        return {
          includeOpen: formState.open,
          includeWaitlisted: formState.waitlisted,
          includeClosed: formState.closed,
          includeHonors:
            formState.honorsOnly ||
            formState.honorsOptional ||
            formState.acceleratedHonors,
          includeReserved,
          includeMode: formState.modeOfInstruction,
        };
      }),
    );

  public packageChoices = {} as Record<string, PackageWithEnrollOptions>;

  constructor(
    private store: Store<GlobalState>,
    private dialog: DialogService,
    private announcer: LiveAnnouncer,
    breakpoints: BreakpointObserver,
    @Inject(IS_PRIVATE) public isPrivate: boolean,
  ) {
    this.paneState = PaneState.init(breakpoints.isMatched(Breakpoints.Mobile));

    // Initialize subscriptions
    this.subscriptions = [
      combineLatest([
        this.store.select(searchSelectors.getResults).pipe(
          map((res): null | 'unloaded' | 'loading' | 'loaded' | 'error' => {
            if (res === 'unloaded') {
              return 'unloaded';
            } else if (res.total === 'loading' && res.next === 'loading') {
              return 'loading';
            } else if (res.total === 'loading' && res.next === 'error') {
              return 'error';
            } else {
              return 'loaded';
            }
          }),
          startWith(null),
          pairwise(),
        ),
        this.store.select(searchSelectors.details).pipe(
          map(details => (details ? 'loaded' : 'unloaded')),
          startWith(null),
          pairwise(),
        ),
      ]).subscribe(([[fromResults, toResults], [fromDetails, toDetails]]) => {
        if (toResults === 'loading') {
          // If the results just started loading, always show the results
          this.showResults();
        } else if (toDetails === 'loaded') {
          // If the details are loaded, show the details
          this.showDetails();
        } else if (fromDetails === 'loaded' && toDetails === 'unloaded') {
          if (fromResults === 'loaded' && toResults === 'unloaded') {
            // If both the details and results were hidden, show the filters
            this.showFilters();
          } else {
            // If the details were hidden, show the results
            this.showResults();
          }
        } else if (fromResults === null && toResults !== 'unloaded') {
          // When switching into the search view from another view like DARS,
          // there may already be results loaded. If there aren't details
          // loaded either, show the results
          this.showResults();
        }
      }),

      /**
       * Listen for changes to whether the sections should be shown/hidden.
       */
      this.store.select(searchSelectors.sectionsIsShown).subscribe(isShown => {
        if (isShown === false) {
          this.hideSections();
        }
      }),

      /**
       * Listen for changes to the screen size. If one crosses a breakpoint, update the layout.
       */
      breakpoints
        .observe(Breakpoints.Mobile)
        .subscribe(({ matches: isMobile }) => {
          this.isMobile.next(isMobile);

          if (isMobile) {
            this.paneState = this.paneState.switchToMobile();
          } else {
            this.paneState = this.paneState.switchToDesktop();
          }
        }),

      /**
       * Whenever a list of packages are loaded for a course, generate a form
       * for each package. This form will let the user manage that package's
       * enrollment options before adding the package to their cart.
       */
      this.store
        .select(searchSelectors.sectionsState)
        .pipe(
          map(packages => {
            if (packages.mode !== 'loaded' || packages.isShown === false) {
              return {};
            }

            const choices: Record<string, PackageWithEnrollOptions> = {};

            packages.packages.forEach(
              ({
                credits,
                sections,
                supportsWaitlist,
                docId,
                classPermissionNumberNeeded,
              }) => {
                const supportsHonors = sections.some(
                  sec => sec.honors === 'INSTRUCTOR_APPROVED',
                );

                const defaultCredits =
                  credits.min === credits.max ? credits.min : null;

                const supportsCredits = credits.min !== credits.max;

                const group = new UntypedFormGroup({
                  honors: new UntypedFormControl(
                    supportsHonors ? 'without' : null,
                  ),
                  waitlist: new UntypedFormControl(
                    supportsWaitlist ? 'without' : null,
                  ),
                  credits: new UntypedFormControl(defaultCredits, [
                    Validators.required,
                    Validators.min(credits.min),
                    Validators.max(credits.max),
                  ]),
                  classPermissionNumber: new UntypedFormControl(null),
                });

                choices[docId] = {
                  supportsHonors,
                  supportsWaitlist,
                  supportsCredits,
                  classPermissionNumberNeeded,
                  credits,
                  group,
                };
              },
            );

            return choices;
          }),
        )
        .subscribe(packages => {
          this.packageChoices = packages;
        }),
    ];
  }

  ngOnInit() {
    this.store.dispatch(actions.lifecycle.init());

    this.orderBy = this.store.select(searchSelectors.getOrderBy);

    this.subscriptions.push(
      ...[
        /**
         * Keep the orderBy state in-sync with the orderByCtrl. This is typically
         * needed when the user changes the orderBy dropdown which will trigger
         * a refresh of the search results.
         */
        this.orderByCtrl.valueChanges
          .pipe(withLatestFrom(this.orderBy))
          .subscribe(([val, oldOrderBy]) => {
            if (isOrderBy(val) && val !== oldOrderBy) {
              this.store.dispatch(actions.search.withOrderBy({ orderBy: val }));
            }
          }),

        /**
         * Keep the orderByCtrl in-sync with the orderBy state. This typically is
         * needed after the user performs a new search since all new searches
         * start with an orderBy 'relevance'.
         */
        this.orderBy.subscribe(newVal => {
          this.orderByCtrl.setValue(newVal);
        }),
      ],
    );

    /**
     * When the view loads, check if the state wants the packages shown
     * immediately. If so, open the packages sidenav.
     */
    this.store
      .select(searchSelectors.sectionsIsShown)
      .pipe(first())
      .toPromise()
      .then(isShown => {
        if (isShown) {
          this.onShowPackages();
        }
      });
  }

  async onAddCourse() {
    const course = await this.store
      .select(searchSelectors.details)
      .pipe(first())
      .toPromise();

    if (course) {
      this.store.dispatch(
        globalActions.courses.roadmap.addCourseWithoutPack({
          course,
          showToast: true,
        }),
      );
    }
  }

  async onAddPackage(pack: EnrollmentPackage, choices: UntypedFormGroup) {
    const withHonors = choices.get('honors')?.value === 'with' ?? null;
    const joinWaitlist = choices.get('waitlist')?.value === 'with' ?? null;
    const credits = choices.get('credits')?.value!;
    const course = await this.store
      .select(searchSelectors.details)
      .pipe(first())
      .toPromise();
    const classPermissionNumber = toNumberOrNull(
      choices.get('classPermissionNumber')?.value!,
    );

    if (course) {
      this.store.dispatch(
        globalActions.courses.roadmap.addCourseWithPack({
          course,
          pack,
          credits,
          joinWaitlist,
          withHonors,
          classPermissionNumber,
          showToast: true,
        }),
      );
    }
  }

  private showFilters(): void {
    this.paneState = this.paneState.focusOnFilters();
  }

  private showResults(): void {
    this.paneState = this.paneState.focusOnResults();
  }

  private showDetails(): void {
    if (this.detailsCollapseButtonRef) {
      (this.detailsCollapseButtonRef as any).focus();
    }

    this.paneState = this.paneState.focusOnDetails();

    this.announcer.announce(`Showing course details`, 'assertive');
  }

  async onShowPackages() {
    this.store.dispatch(actions.packages.show());
    this.sidenavRef.open();

    this.announcer.announce('Showing course sections', 'assertive');
  }

  private hideSections(): void {
    this.paneState = this.paneState.focusOnDetails();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  /**
   * Fired when the open animation STARTS. We want to start rendering the
   * package list so that it's rendering _while_ the sidenav is expanding
   * across the screen.
   */
  onSidenavOpenStart() {
    this.renderPackages = true;
  }

  /**
   * Fired when the open and close animations FINISHES. We don't want to
   * un-render the package list until it's fully off the screen.
   */
  onSidenavOpenedChange(isOpen: boolean) {
    if (isOpen === false) {
      this.renderPackages = false;
      this.onHidePackages();
    }
  }

  onHideResults() {
    this.showFilters();
    this.store.dispatch(actions.page.clearAll());

    this.announcer.announce(`Showing search filters`, 'assertive');
  }

  async onHideDetails() {
    const selectedIndex = await this.store
      .select(searchSelectors.getSelectedIndexOrNull)
      .pipe(first())
      .toPromise();

    if (selectedIndex !== null) {
      this.focusOnIndex.next(selectedIndex);
    }

    this.store.dispatch(actions.details.hide());

    this.announcer.announce(`Closed course details`, 'assertive');
  }

  onHidePackages() {
    this.store.dispatch(actions.packages.hide());
    this.paneState = this.paneState.focusOnDetails();
    this.sidenavRef.close();

    this.announcer.announce(`Closed course sections`, 'assertive');
  }

  async onAddToPlan() {
    const activeTermCodes = await this.store
      .select(globalSelectors.terms.getActiveTermCodes)
      .pipe(first())
      .toPromise();

    const course = await this.store
      .select(searchSelectors.details)
      .pipe(first())
      .toPromise();

    if (!isSuccess(activeTermCodes) || !course) {
      return;
    }

    const maybeAddToPlan = await this.dialog.small(
      AddToPlanDialogComponent,
      activeTermCodes.value,
    );

    const schema = s.object({
      plan: RawPlan,
      termCode: TermCode.schema,
    });

    if (schema.matches(maybeAddToPlan)) {
      this.store.dispatch(
        actions.course.addToPlan({
          plan: maybeAddToPlan.plan,
          termCode: maybeAddToPlan.termCode,
          course,
        }),
      );
    }
  }

  async onSaveForLater() {
    const course = await this.store
      .select(searchSelectors.details)
      .pipe(first())
      .toPromise();

    if (course) {
      this.store.dispatch(globalActions.saved.add({ course }));
    }
  }
}

/**
 * An interface that encodes the animation states available to each pane of
 * the layout. Keeping all of the animation states in a single object reduces
 * the chances of a bug causing the states to become out-of-sync.
 */
interface PaneStateInstance {
  filters: 'offscreenLeft' | 'onscreen';
  results: 'offscreenLeft' | 'onscreen' | 'offscreenRight';
  details: 'offscreenLeft' | 'onscreen' | 'offscreenRight';
}

interface PaneStateInstancePair {
  mobile: PaneStateInstance;
  desktop: PaneStateInstance;
}

class PaneState {
  /**
   * A series of constants capturing the legal combinations of pane animation
   * states. These constant objects will be passed to the `animState` observable
   * inside the SearchViewComponent class.
   */
  public static FOCUS_ON_FILTERS: PaneStateInstancePair = {
    mobile: {
      filters: 'onscreen',
      results: 'offscreenRight',
      details: 'offscreenRight',
    },
    desktop: {
      filters: 'onscreen',
      results: 'onscreen',
      details: 'offscreenLeft',
    },
  };

  public static FOCUS_ON_RESULTS: PaneStateInstancePair = {
    mobile: {
      filters: 'offscreenLeft',
      results: 'onscreen',
      details: 'offscreenRight',
    },
    desktop: {
      filters: 'onscreen',
      results: 'onscreen',
      details: 'offscreenLeft',
    },
  };

  public static FOCUS_ON_DETAILS: PaneStateInstancePair = {
    mobile: {
      filters: 'offscreenLeft',
      results: 'offscreenLeft',
      details: 'onscreen',
    },
    desktop: {
      filters: 'onscreen',
      results: 'onscreen',
      details: 'onscreen',
    },
  };

  private constructor(
    public readonly breakpoint: 'mobile' | 'desktop',
    public readonly pair: PaneStateInstancePair,
  ) {}

  get filters(): PaneStateInstance['filters'] {
    return this.pair[this.breakpoint].filters;
  }

  get results(): PaneStateInstance['results'] {
    return this.pair[this.breakpoint].results;
  }

  get details(): PaneStateInstance['details'] {
    return this.pair[this.breakpoint].details;
  }

  public static init(isMobile: boolean): PaneState {
    return new PaneState(
      isMobile ? 'mobile' : 'desktop',
      PaneState.FOCUS_ON_FILTERS,
    );
  }

  public switchToMobile(): PaneState {
    return new PaneState('mobile', this.pair);
  }

  public switchToDesktop(): PaneState {
    return new PaneState('desktop', this.pair);
  }

  public focusOnFilters(): PaneState {
    return new PaneState(this.breakpoint, PaneState.FOCUS_ON_FILTERS);
  }

  public focusOnResults(): PaneState {
    return new PaneState(this.breakpoint, PaneState.FOCUS_ON_RESULTS);
  }

  public focusOnDetails(): PaneState {
    return new PaneState(this.breakpoint, PaneState.FOCUS_ON_DETAILS);
  }
}

const toNumberOrNull = (thing: unknown): number | null => {
  if (typeof thing === 'string' && /^\d+$/.test(thing)) {
    return parseInt(thing, 10);
  } else if (typeof thing === 'number') {
    return thing;
  } else {
    return null;
  }
};
