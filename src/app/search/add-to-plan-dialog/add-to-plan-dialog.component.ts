import { Component, Inject } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RawPlan } from '@app/types/plans';
import { TermCode } from '@app/types/terms';
import * as s from '@app/types/schema';
import { ApiService } from '@app/services/api.service';

@Component({
  selector: 'cse-add-to-plan-dialog',
  templateUrl: './add-to-plan-dialog.component.html',
  styleUrls: ['./add-to-plan-dialog.componen.scss'],
})
export class AddToPlanDialogComponent {
  public planErrorMessage: string | null = null;
  public degreePlans: RawPlan[] = [];
  public form: UntypedFormGroup;
  public termCodes: TermCode[];

  constructor(
    @Inject(MAT_DIALOG_DATA) activeTermCodes: TermCode[],
    public dialogRef: MatDialogRef<AddToPlanDialogComponent>,
    api: ApiService,
    formBuilder: UntypedFormBuilder,
  ) {
    const TOTAL_TERM_CODES = 8 * 3; // eight years of terms, 3 terms per year

    this.termCodes = activeTermCodes.slice();
    console.assert(this.termCodes.length > 0);

    while (this.termCodes.length < TOTAL_TERM_CODES) {
      const last = this.termCodes[this.termCodes.length - 1];
      const next = TermCode.next(last);
      this.termCodes.push(next);
    }

    this.form = formBuilder.group({
      plan: [{ value: null, disabled: true }, Validators.required],
      termCode: [null, Validators.required],
    });

    api
      .getDegreePlans()
      .toPromise()
      .then(degreePlans => {
        this.form.get('plan')?.enable();
        this.degreePlans = degreePlans;
      })
      .catch(() => {
        this.form.get('plan')?.disable();
        this.planErrorMessage = 'Unable to load your degree plans';
      });
  }

  comparePlans(a: unknown, b: unknown): boolean {
    const schema = s.object({ roadmapId: s.unknown });
    return (
      schema.matches(a) && schema.matches(b) && a.roadmapId === b.roadmapId
    );
  }

  compareTermCodes(t1: unknown, t2: unknown): boolean {
    return TermCode.is(t1) && TermCode.is(t2) && TermCode.equals(t1, t2);
  }
}
