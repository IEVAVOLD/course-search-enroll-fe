import { FormState, ModeOfInstruction, OrderBy } from '@search/store/state';
import { Session, TermCode } from '@app/types/terms';
import { notNull } from '@app/core/utils';
import { SearchParameters } from '@app/services/api.service';
import * as s from '@app/types/schema';

export const DEFAULT_SEARCH_PAGE_SIZE = 50;

/**
 * When the user submits a search, build a JSON ElasticSearch query from the
 * filters that the user selected. Some of the filters structures are kind of
 * esoteric and most of this code is a re-interpretation of the original
 * AngularJS implementation.
 *
 * @param state FormState
 * @param page number
 */
export const stateToElasticQuery = (
  state: FormState,
  page: number,
): SearchParameters => {
  let filters: unknown[] = [
    ...enrollmentStatusFilters(state),
    ...subjectFilters(state),
    ...genEdFilters(state),
    ...breadthFilters(state),
    ...levelFilters(state),
    ...languageFilters(state),
    ...honorsFilters(state),
    ...reservedSectionsFilters(state),
    ...modeOfInstructionFilters(state),
    ...sustainabilityFilter(state),
    ...workplaceExperienceFilter(state),
    ...gradCourseworkFilter(state),
    ...communityLearningFilter(state),
    ...repeatableForCreditFilter(state),
    ...minCreditsFilter(state),
    ...maxCreditsFilter(state),
    ...sessionFilter(state),
    ...courseIdFilter(state),
    ...catalogNumFilter(state),
    ...topicIdFilter(state),
    ...publishedFilter(state),
  ];

  filters = mergeChildQueries(filters);

  return {
    selectedTerm: selectedTerm(state),
    queryString: queryString(state),
    filters,
    page,
    pageSize: DEFAULT_SEARCH_PAGE_SIZE,
    sortOrder: orderByFilter(state.orderBy),
  };
};

const selectedTerm = (state: FormState): string => {
  return state.term ? TermCode.encode(state.term.value) : '0000';
};

const queryString = (state: FormState): string => {
  return !(state.keywords ?? '').trim() ? '*' : state.keywords!.trim();
};

const enrollmentStatusFilters = (state: FormState): unknown[] => {
  const statuses: string[] = [];

  if (state.open) {
    statuses.push('OPEN');
  }

  if (state.waitlisted) {
    statuses.push('WAITLISTED');
  }

  if (state.closed) {
    statuses.push('CLOSED');
  }

  if (TermCode.is(state.term?.value) && statuses.length > 0) {
    return [
      {
        has_child: {
          type: 'enrollmentPackage',
          query: {
            match: {
              'packageEnrollmentStatus.status': statuses.join(' '),
            },
          },
        },
      },
    ];
  } else {
    return [];
  }
};

const subjectFilters = (state: FormState): unknown[] => {
  if (state.subject !== null) {
    return [
      {
        term: {
          'subject.subjectCode': state.subject.value.code,
        },
      },
    ];
  } else {
    return [];
  }
};

const genEdFilters = (state: FormState): unknown[] => {
  const should: unknown[] = [];

  const generalEdCode: string[] = [
    state.commA ? 'COM A' : null,
    state.commB ? 'COM B' : null,
    state.quantA ? 'QR-A' : null,
    state.quantB ? 'QR-B' : null,
  ].filter(notNull);

  if (generalEdCode.length > 0) {
    should.push({ terms: { 'generalEd.code': [generalEdCode] } });
  }

  if (state.ethnicStudies) {
    should.push({ term: { 'ethnicStudies.code': 'ETHNIC ST' } });
  }

  if (state.commB) {
    should.push({
      has_child: {
        type: 'enrollmentPackage',
        query: { match: { 'sections.comB': true } },
      },
    });
  }

  if (should.length > 0) {
    return [{ bool: { should } }];
  } else {
    return [];
  }
};

const breadthFilters = (state: FormState): unknown[] => {
  const relevant: string[] = [
    state.biologicalSciences ? 'B' : null,
    state.humanities ? 'H' : null,
    state.literature ? 'L' : null,
    state.naturalSciences ? 'N' : null,
    state.physicalSciences ? 'P' : null,
    state.socialSciences ? 'S' : null,
  ].filter(notNull);

  if (relevant.length > 0) {
    return [{ terms: { 'breadths.code': relevant } }];
  } else {
    return [];
  }
};

const levelFilters = (state: FormState): unknown[] => {
  const relevant: string[] = [
    state.elementary ? 'E' : null,
    state.intermediate ? 'I' : null,
    state.advanced ? 'A' : null,
  ].filter(notNull);

  if (relevant.length > 0) {
    return [{ terms: { 'levels.code': relevant } }];
  } else {
    return [];
  }
};

const languageFilters = (state: FormState): unknown[] => {
  if (state.language !== 'all') {
    const ENCODING = {
      first: 'FL1',
      second: 'FL2',
      third: 'FL3',
      fourth: 'FL4',
      fifth: 'FL5',
    };

    return [
      {
        query: { match: { 'foreignLanguage.code': ENCODING[state.language] } },
      },
    ];
  } else {
    return [];
  }
};

const honorsFilters = (state: FormState): unknown[] => {
  const relevant: string[] = [
    state.honorsOnly ? 'HONORS_ONLY' : null,
    state.acceleratedHonors ? 'HONORS_LEVEL' : null,
    state.honorsOptional ? 'INSTRUCTOR_APPROVED' : null,
  ].filter(notNull);

  const orClause = relevant.map(value => {
    return [{ match: { 'sections.honors': value } }];
  });

  if (orClause.length > 1) {
    return [
      {
        has_child: {
          type: 'enrollmentPackage',
          query: { bool: { should: orClause } },
        },
      },
    ];
  }

  if (orClause.length === 1) {
    return [
      {
        has_child: {
          type: 'enrollmentPackage',
          query: orClause[0],
        },
      },
    ];
  }

  return [];
};

const reservedSectionsFilters = (state: FormState): unknown[] => {
  if (!TermCode.is(state.term?.value) || state.reservedSections === 'all') {
    // Don't apply any reserved section filers for non-active terms
    return [];
  }

  if (state.reservedSections === 'none') {
    return [
      {
        has_child: {
          type: 'enrollmentPackage',
          query: {
            bool: {
              should: [
                {
                  bool: {
                    must_not: [
                      { exists: { field: 'sections.classAttributes' } },
                    ],
                  },
                },
                {
                  match: {
                    'sections.classAttributes.attributeCode': 'TEXT SVCL',
                  },
                },
              ],
            },
          },
        },
      },
    ];
  }

  const { attr, code } = state.reservedSections.value;
  const predicate =
    code !== null
      ? { match: { 'sections.classAttributes.valueCode': code } }
      : { match: { 'sections.classAttributes.attributeCode': attr } };

  return [{ has_child: { type: 'enrollmentPackage', query: predicate } }];
};

const modeOfInstructionFilters = (state: FormState): unknown[] => {
  const modeFilters: Record<ModeOfInstruction, unknown> = {
    all: null,
    classroom: {
      must: [{ match: { modesOfInstruction: 'Instruction' } }],
      must_not: [
        { match: { modesOfInstruction: 'some' } },
        { match: { modesOfInstruction: 'Only' } },
        { match: { modesOfInstruction: 'EMPTY' } },
      ],
    },
    hybrid: {
      must: [{ match: { modesOfInstruction: 'some' } }],
      must_not: [
        { match: { modesOfInstruction: 'Instruction' } },
        { match: { modesOfInstruction: 'Only' } },
        { match: { modesOfInstruction: 'EMPTY' } },
      ],
    },
    async: {
      must: [
        { match: { modesOfInstruction: 'Only' } },
        { match: { isAsynchronous: true } },
      ],
      must_not: [
        { match: { modesOfInstruction: 'Instruction' } },
        { match: { modesOfInstruction: 'some' } },
        { match: { isAsynchronous: false } },
        { match: { modesOfInstruction: 'EMPTY' } },
      ],
    },
    sync: {
      must: [
        { match: { modesOfInstruction: 'Only' } },
        { match: { isAsynchronous: false } },
      ],
      must_not: [
        { match: { modesOfInstruction: 'Instruction' } },
        { match: { modesOfInstruction: 'some' } },
        { match: { isAsynchronous: true } },
        { match: { modesOfInstruction: 'EMPTY' } },
      ],
    },
    either: {
      must: [{ match: { modesOfInstruction: 'Only' } }],
      must_not: [
        { match: { modesOfInstruction: 'Instruction' } },
        { match: { modesOfInstruction: 'some' } },
        { match: { modesOfInstruction: 'EMPTY' } },
      ],
    },
  };

  const chosenFilters = modeFilters[state.modeOfInstruction];

  if (!chosenFilters) {
    return [];
  }

  return [
    {
      has_child: {
        type: 'enrollmentPackage',
        query: { bool: chosenFilters },
      },
    },
  ];
};

const sustainabilityFilter = (state: FormState): unknown[] => {
  return state.sustainability
    ? [{ query: { exists: { field: 'sustainability' } } }]
    : [];
};

const gradCourseworkFilter = (state: FormState): unknown[] => {
  return state.graduateCourseworkRequirement
    ? [{ term: { gradCourseWork: true } }]
    : [];
};

const workplaceExperienceFilter = (state: FormState): unknown[] => {
  return state.workplaceExperience
    ? [{ query: { exists: { field: 'workplaceExperience' } } }]
    : [];
};

const communityLearningFilter = (state: FormState): unknown[] => {
  return state.communityBasedLearning
    ? [
        {
          has_child: {
            type: 'enrollmentPackage',
            query: {
              bool: {
                must: [
                  {
                    match: { 'sections.classAttributes.valueCode': '25 PLUS' },
                  },
                ],
              },
            },
          },
        },
      ]
    : [];
};

const repeatableForCreditFilter = (state: FormState): unknown[] => {
  return state.repeatableForCredit ? [{ match: { repeatable: 'Y' } }] : [];
};

const minCreditsFilter = (state: FormState): unknown[] => {
  if (state.credits.min !== null) {
    return [{ range: { minimumCredits: { gte: state.credits.min } } }];
  } else {
    return [];
  }
};

const maxCreditsFilter = (state: FormState): unknown[] => {
  if (state.credits.max !== null) {
    return [{ range: { maximumCredits: { lte: state.credits.max } } }];
  } else {
    return [];
  }
};

const sessionFilter = (state: FormState): unknown[] => {
  const sessions: Session[] = state.sessions?.value ?? [];

  const orClause = sessions.map(session => {
    return { match: { 'sections.sessionCode': session.code } };
  });

  if (orClause.length > 1) {
    return [
      {
        has_child: {
          type: 'enrollmentPackage',
          query: { bool: { should: orClause } },
        },
      },
    ];
  }

  if (orClause.length === 1) {
    return [
      {
        has_child: {
          type: 'enrollmentPackage',
          query: orClause[0],
        },
      },
    ];
  }

  return [];
};

const courseIdFilter = (state: FormState): unknown[] => {
  if (state.courseId !== null) {
    return [{ term: { courseId: state.courseId } }];
  } else {
    return [];
  }
};

const catalogNumFilter = (state: FormState): unknown[] => {
  const { min, max } = state.catalogNum;

  if (min !== null && max !== null) {
    if (min === max) {
      return [{ term: { catalogNumber: min } }];
    }

    return [
      {
        range: {
          catalogSort: {
            gte: `${min}`.padStart(5, '0'),
            lte: `${max}`.padStart(5, '0'),
          },
        },
      },
    ];
  } else if (min !== null) {
    return [{ range: { catalogSort: { gte: `${min}`.padStart(5, '0') } } }];
  } else if (max !== null) {
    return [{ range: { catalogSort: { lte: `${max}`.padStart(5, '0') } } }];
  }

  return [];
};

const topicIdFilter = (state: FormState): unknown[] => {
  if (state.topicId !== null) {
    return [{ term: { 'topics.id': state.topicId } }];
  } else {
    return [];
  }
};

const publishedFilter = (state: FormState): unknown[] => {
  if (TermCode.is(state.term?.value) === false) {
    // No packages exist in the zero term so if the term is 0000, don't require
    // courses to have published packages.
    return [];
  }

  return [
    {
      has_child: {
        type: 'enrollmentPackage',
        query: {
          match: {
            published: true,
          },
        },
      },
    },
  ];
};

const orderByFilter = (orderBy: OrderBy) => {
  switch (orderBy) {
    case 'subject':
      return 'SUBJECT';
    case 'catalog-number':
      return 'CATALOG_NUMBER';
    case 'relevance':
    default:
      return 'SCORE';
  }
};

const mergeChildQueries = (filters: unknown[]): unknown[] => {
  const passThrough: unknown[] = [];
  const childQueries: unknown[] = [];

  const schema = s.object({
    has_child: s.object({
      type: s.constant('enrollmentPackage'),
      query: s.unknown,
    }),
  });

  for (const filter of filters) {
    if (schema.matches(filter)) {
      childQueries.push(filter.has_child.query);
    } else {
      passThrough.push(filter);
    }
  }

  if (childQueries.length > 1) {
    return [
      ...passThrough,
      {
        has_child: {
          type: 'enrollmentPackage',
          query: { bool: { must: childQueries } },
        },
      },
    ];
  } else if (childQueries.length === 1) {
    return [
      ...passThrough,
      {
        has_child: { type: 'enrollmentPackage', query: childQueries[0] },
      },
    ];
  }

  return filters;
};
