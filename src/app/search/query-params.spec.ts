import { EMPTY_TERMS, TermCode, TermCodeOrZero, Terms } from '@app/types/terms';
import { fromParams, toParams } from '@search/query-params';
import { createEmptyFormState } from '@search/store/state';

const emptyTermsFromSet = (set: TermCode[]): Terms => {
  const terms: Terms = { ...EMPTY_TERMS };

  set.forEach(termCode => {
    terms.activeTermCodes.push(termCode);
    terms.nonZero[TermCode.encode(termCode)] = {
      termCode,
      subjects: [],
      sessions: [],
      specialGroups: [],
    };
  });

  return terms;
};

const dummy = {
  nonActivePast: TermCode.decodeOrThrow('1202'),
  nonActiveFuture: TermCode.decodeOrThrow('1216'),
  terms: emptyTermsFromSet([
    TermCode.decodeOrThrow('1212'),
    TermCode.decodeOrThrow('1214'),
  ]),
};

describe('url params to search form state', () => {
  interface TestPair {
    param: any;
    wanted: TermCode | null;
  }

  const aux = ({ param, wanted }: TestPair) => {
    const params = TermCodeOrZero.is(param)
      ? { term: TermCodeOrZero.encode(param) }
      : { term: param };
    const state = fromParams(dummy.terms, params);
    const found = state.term ? state.term.value : state.term;
    expect(found).toStrictEqual(wanted);
  };

  it('should use first active term if no term param', () => {
    aux({
      param: undefined,
      wanted: dummy.terms.activeTermCodes[0],
    });
  });

  it('should use zero term if term param invalid', () => {
    aux({
      param: 'blah',
      wanted: null,
    });
  });

  it('should use first active term if term param is past', () => {
    aux({
      param: dummy.nonActivePast,
      wanted: dummy.terms.activeTermCodes[0],
    });
  });

  it('should use first active term if term param is future', () => {
    aux({
      param: dummy.nonActiveFuture,
      wanted: dummy.terms.activeTermCodes[0],
    });
  });

  it('should use given term if term param is active', () => {
    dummy.terms.activeTermCodes.forEach(termCode => {
      aux({
        param: termCode,
        wanted: termCode,
      });
    });
  });

  it('should use zero term if term param is zero', () => {
    aux({
      param: '0000',
      wanted: null,
    });
  });
});

describe('form state to url params', () => {
  interface TestPair {
    term: TermCode | null;
    wanted: string | undefined;
  }

  const aux = ({ term, wanted }: TestPair) => {
    const state = createEmptyFormState(term);
    const params = toParams(dummy.terms.activeTermCodes, state);
    expect(params['term']).toStrictEqual(wanted);
  };

  it('should omit param if term is the first active term', () => {
    aux({ term: dummy.terms.activeTermCodes[0], wanted: undefined });
  });

  it('should include param if term is active but not the first', () => {
    dummy.terms.activeTermCodes
      .filter((_, index) => index > 0)
      .forEach(termCode => {
        aux({
          term: termCode,
          wanted: TermCode.encode(termCode),
        });
      });
  });

  it('should include param if term is zero', () => {
    aux({ term: null, wanted: '0000' });
  });
});
