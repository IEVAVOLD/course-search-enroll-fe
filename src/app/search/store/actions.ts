import { createAction, props } from '@ngrx/store';
import { FormState, OrderBy, SearchState } from './state';
import { Course, SearchResult, EnrollmentPackage } from '@app/types/courses';
import { RawPlan } from '@app/types/plans';
import { TermCode } from '@app/types/terms';

export namespace util {
  export const noop = createAction('search/util/noop');
}

export namespace lifecycle {
  export const init = createAction('search/lifecycle/init');

  export const success = createAction(
    'search/lifecycle/success',
    props<{
      defaultActiveTerm: TermCode;
      formDerrivedFromParams: FormState | null;
      autoSearchResults: SearchState['res'] | null;
    }>(),
  );
}

export namespace filters {
  export const reset = createAction('search/filters/reset');
}

export namespace search {
  export const withCurrentFilters = createAction(
    'search/search/withCurrentFilters',
  );

  export const withFilters = createAction(
    'search/search/withFilters',
    props<{ filters: FormState; orderBy?: OrderBy }>(),
  );

  export const withOrderBy = createAction(
    'search/search/withOrderBy',
    props<{ orderBy: OrderBy }>(),
  );
}

export namespace page {
  export const prev = createAction('search/page/prev');

  export const next = createAction('search/page/next');

  export const done = createAction(
    'search/page/done',
    props<{
      found: number;
      hits: SearchResult[];
    }>(),
  );

  export const error = createAction('search/page/error');

  export const retry = createAction('search/page/retry');

  export const clearAll = createAction('search/page/clearAll');
}

export namespace details {
  export const show = createAction(
    'search/details/show',
    props<{ selectedIndex: number }>(),
  );

  export const hide = createAction('search/details/hide');
}

export namespace packages {
  export const show = createAction('search/packages/show');

  export const hide = createAction('search/packages/hide');

  export const ready = createAction(
    'search/packages/ready',
    props<{ forIndex: number; packages: EnrollmentPackage[] }>(),
  );

  export const error = createAction(
    'search/packages/error',
    props<{ forIndex: number }>(),
  );
}

export namespace course {
  export const addToPlan = createAction(
    'search/course/addToPlan',
    props<{
      plan: RawPlan;
      termCode: TermCode;
      course: Course;
    }>(),
  );
}
