import { GlobalState } from '@app/state';
import { SearchResult, EnrollmentPackage } from '@app/types/courses';
import {
  Subject,
  Session,
  SpecialGroup,
  TermCode,
  TermCodeOrZero,
  Zero,
} from '@app/types/terms';
import {
  FormGroupState,
  createFormGroupState,
  Boxed,
  disable,
  box,
} from 'ngrx-forms';
import { Loadable, Toggleable, UNLOADED } from '@app/types/loadable';
import { Loadable as NewLoadable, LOADING } from 'loadable.ts';

export const MIN_CREDIT_VALUE = 1;
export const MAX_CREDIT_VALUE = 12;
export const MIN_CATALOG_NUMBER = 1;
export const MAX_CATALOG_NUMBER = 999;

export interface SearchState {
  wasInitialized: 'ready' | 'pending' | 'done';

  // The term-code that will populate the term filter when the user hasn't
  // manually picked a term to search. This term-code is chosen once when the
  // app loads and is the first term-code in the active term set.
  defaultActiveTerm: NewLoadable<TermCode>;

  form: FormGroupState<FormState>;

  /**
   * A copy of the filters that were used to generate the current search
   * results. Set to `null` if the user hasn't performed a search yet.
   */
  currentFilters: FormState | null;

  /**
   * A data structure that holds the pages of results that have already been
   * loaded and tracks state associated with the user paginating through the
   * results.
   */
  res:
    | 'unloaded'
    | {
        total: 'loading' | { pages: number; results: number };
        pages: Array<Array<SearchResult>>;
        viewing: { state: 'next' } | { state: 'loaded'; pageIndex: number };
        next: null | 'loading' | 'error';
      };

  details: Toggleable<{ selectedIndex: number }>;

  sections: Loadable<{ packages: EnrollmentPackage[] }, { isShown: boolean }>;
}

export interface State extends GlobalState {
  search: SearchState;
}

export interface FormState {
  orderBy: OrderBy;

  // Termcode of the chosen term. If the value is null, assume the 0000 term
  term: Boxed<TermCode> | null;

  // Which subject the user has chosen to search.
  subject: Boxed<Subject> | null;

  keywords: string | null;

  // Course statuses
  open: boolean;
  waitlisted: boolean;
  closed: boolean;

  // General education
  commA: boolean;
  commB: boolean;
  quantA: boolean;
  quantB: boolean;
  ethnicStudies: boolean;

  // Breadth
  biologicalSciences: boolean;
  humanities: boolean;
  literature: boolean;
  naturalSciences: boolean;
  physicalSciences: boolean;
  socialSciences: boolean;

  // Level
  elementary: boolean;
  intermediate: boolean;
  advanced: boolean;

  // Foreign language
  language: ForeignLanguage;

  // Honors
  honorsOnly: boolean;
  acceleratedHonors: boolean;
  honorsOptional: boolean;

  // Reserved sections
  reservedSections:
    | 'all'
    | 'none'
    | Boxed<{
        attr: SpecialGroup['attribute'];
        code: SpecialGroup['code'] | null;
      }>;

  // Mode of instruction
  modeOfInstruction: ModeOfInstruction;

  // Other options
  sustainability: boolean;
  graduateCourseworkRequirement: boolean;
  workplaceExperience: boolean;
  communityBasedLearning: boolean;
  repeatableForCredit: boolean;

  // Credits
  credits: { min: number | null; max: number | null };

  // Catalog number
  catalogNum: { min: string | null; max: string | null };

  // Sessions
  sessions: Boxed<Session[]> | null;

  // Course ID (NOT EXPOSED BY THE UI)
  courseId: string | null;

  // Topic ID (NOT EXPOSED BY THE UI)
  topicId: string | null;
}

export type WrappedFormState = FormGroupState<FormState>;

export const FOREIGN_LANGUAGE = [
  'all',
  'first',
  'second',
  'third',
  'fourth',
  'fifth',
] as const;

export type ForeignLanguage = typeof FOREIGN_LANGUAGE[number];

export const ORDER_BY = ['relevance', 'subject', 'catalog-number'] as const;

export type OrderBy = typeof ORDER_BY[number];

export const MODES_OF_INSTRUCTION = [
  'all',
  'classroom',
  'hybrid',
  'async',
  'sync',
  'either',
] as const;

export type ModeOfInstruction = typeof MODES_OF_INSTRUCTION[number];

export const DEFAULT_ORDER_BY: OrderBy = 'relevance';

export const isOrderBy = (o: unknown): o is OrderBy => {
  return typeof o === 'string' && ORDER_BY.includes(o as any);
};

const EMPTY_FORM_STATE: FormState = {
  orderBy: DEFAULT_ORDER_BY,

  term: null, // in this case null represents the 0000 term
  subject: null,
  keywords: null,

  // Course statues
  open: true,
  waitlisted: true,
  closed: false,

  // General education
  commA: false,
  commB: false,
  quantA: false,
  quantB: false,
  ethnicStudies: false,

  // Breadth
  biologicalSciences: false,
  humanities: false,
  literature: false,
  naturalSciences: false,
  physicalSciences: false,
  socialSciences: false,

  // Level
  elementary: false,
  intermediate: false,
  advanced: false,

  // Language
  language: 'all',

  // Honors
  honorsOnly: false,
  acceleratedHonors: false,
  honorsOptional: false,

  // Reserved sections
  reservedSections: 'all',

  // Mode of instruction
  modeOfInstruction: 'all',

  // Other options
  sustainability: false,
  graduateCourseworkRequirement: false,
  workplaceExperience: false,
  communityBasedLearning: false,
  repeatableForCredit: false,

  // Credits
  credits: { min: null, max: null },

  // Catalog number
  catalogNum: { min: null, max: null },

  // Sessions
  sessions: null,

  // Options not exposed by the UI (only provided for deep-linking)
  courseId: null,
  topicId: null,
};

export const createEmptyFormState = (
  termCode: TermCodeOrZero | null,
): FormState => {
  if (termCode === null || Zero.is(termCode)) {
    return { ...EMPTY_FORM_STATE, term: null };
  } else {
    return { ...EMPTY_FORM_STATE, term: box(termCode) };
  }
};

export const FORM_ID = 'cse-search-form';

export const WRAPPED_FORM_STATE = disable(
  createFormGroupState(FORM_ID, EMPTY_FORM_STATE),
);

export const EMPTY_STATE: SearchState = {
  wasInitialized: 'ready',
  defaultActiveTerm: LOADING,
  form: WRAPPED_FORM_STATE,
  currentFilters: null,
  res: 'unloaded',
  details: UNLOADED,
  sections: UNLOADED,
};
