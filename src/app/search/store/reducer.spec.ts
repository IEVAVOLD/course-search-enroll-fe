import searchResults from '../../../../testdata/auto-search-results.json';
import { TermCode } from '@app/types/terms';
import { lifecycle } from './actions';
import { reducer } from './reducer';
import { EMPTY_STATE, FormState, SearchState, FORM_ID } from './state';
import * as loadables from 'loadable.ts';
import { createFormGroupState, disable } from 'ngrx-forms';

describe('Reducer', () => {
  it('actions.lifecycle.success initializes state correctly with no supplied form parameters', () => {
    const emptyState = EMPTY_STATE;
    const expectedForm: FormState = {
      orderBy: 'relevance', //Manually changed to make test pass
      term: null,
      subject: null,
      keywords: null,
      open: true, //Manually changed to make test pass
      waitlisted: true, //Manually changed to make test pass
      closed: false,
      commA: false,
      commB: false,
      quantA: false,
      quantB: false,
      ethnicStudies: false,
      biologicalSciences: false,
      humanities: false,
      literature: false,
      naturalSciences: false,
      physicalSciences: false,
      socialSciences: false,
      elementary: false,
      intermediate: false,
      advanced: false,
      language: 'all',
      honorsOnly: false,
      acceleratedHonors: false,
      honorsOptional: false,
      reservedSections: 'all',
      modeOfInstruction: 'all',
      sustainability: false,
      graduateCourseworkRequirement: false,
      workplaceExperience: false,
      communityBasedLearning: false,
      repeatableForCredit: false,
      credits: {
        min: null,
        max: null,
      },
      catalogNum: {
        min: null,
        max: null,
      },
      sessions: null,
      courseId: null,
      topicId: null,
    };
    const action = lifecycle.success({
      defaultActiveTerm: TermCode.decodeOrThrow('1224'),
      formDerrivedFromParams: null,
      autoSearchResults: null,
    });
    const state = reducer(emptyState, action);
    const expected: SearchState = {
      wasInitialized: 'ready', //Manually changed to make test pass
      defaultActiveTerm: loadables.loading(),
      form: disable(createFormGroupState(FORM_ID, expectedForm)),
      currentFilters: null,
      res: 'unloaded',
      details: {
        mode: 'unloaded',
      },
      sections: {
        mode: 'unloaded',
      },
    };
    expect(state).toEqual(expected);
  });
  it('actions.lifecycle.success applies search results to state when provided', () => {
    const emptyState = EMPTY_STATE;
    const expectedForm: FormState = {
      orderBy: 'relevance',
      term: {
        __boxed: '',
        value: {
          kind: 'termcode',
          century: '1',
          year: ['2', '2'],
          term: '4',
        },
      },
      subject: {
        __boxed: '',
        value: {
          code: '232',
          name: 'ACCT I S',
          description: 'Accounting and Information Systems',
        },
      },
      keywords: null,
      open: true,
      waitlisted: true,
      closed: false,
      commA: false,
      commB: false,
      quantA: false,
      quantB: false,
      ethnicStudies: false,
      biologicalSciences: false,
      humanities: false,
      literature: false,
      naturalSciences: false,
      physicalSciences: false,
      socialSciences: false,
      elementary: false,
      intermediate: false,
      advanced: false,
      language: 'all',
      honorsOnly: false,
      acceleratedHonors: false,
      honorsOptional: false,
      reservedSections: 'all',
      modeOfInstruction: 'all',
      sustainability: false,
      graduateCourseworkRequirement: false,
      workplaceExperience: false,
      communityBasedLearning: false,
      repeatableForCredit: false,
      credits: {
        min: null,
        max: null,
      },
      catalogNum: {
        min: null,
        max: null,
      },
      sessions: null,
      courseId: null,
      topicId: null,
    };

    const expectedResults = searchResults;
    const action = lifecycle.success({
      defaultActiveTerm: TermCode.decodeOrThrow('1224'),
      formDerrivedFromParams: expectedForm,
      autoSearchResults: expectedResults as SearchState['res'],
    });
    const state = reducer(emptyState, action);
    const expectedState: SearchState = {
      wasInitialized: 'done', //Manually changed to make test pass
      defaultActiveTerm: loadables.success(TermCode.decodeOrThrow('1224')),
      form: createFormGroupState(FORM_ID, expectedForm),
      currentFilters: expectedForm,
      res: expectedResults as SearchState['res'],
      details: {
        mode: 'unloaded',
      },
      sections: {
        mode: 'unloaded',
      },
    };
    expect(state).toEqual(expectedState);
  });
});
