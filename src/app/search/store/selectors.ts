import { SearchState as Search, FormState } from './state';
import { createSelector, createFeatureSelector } from '@ngrx/store';
import { SearchResult } from '@app/types/courses';

const getSearchState = createFeatureSelector<Search>('search');

export const getWasInitialized = createSelector(
  getSearchState,
  state => state.wasInitialized,
);

/**
 * FORM SELECTORS
 */

export const getForm = createSelector(
  getSearchState,
  (state: Search) => state.form,
);

export const isFormDisabled = createSelector(
  getSearchState,
  (state: Search): boolean => {
    return state.form.isDisabled;
  },
);

export const getFormValue = createSelector(
  getSearchState,
  (state: Search) => state.form.value,
);

export const chosenTermCode = createSelector(
  getFormValue,
  (state: FormState) => state.term?.value ?? null,
);

export const chosenSubject = createSelector(
  getFormValue,
  (state: FormState) => state.subject?.value ?? null,
);

export const getReservedSections = createSelector(
  getFormValue,
  (state: FormState) => state.reservedSections,
);

export const minCreditValue = createSelector(
  getFormValue,
  (form: FormState, fallback: number) => form.credits.min ?? fallback,
);

export const maxCreditValue = createSelector(
  getFormValue,
  (form: FormState, fallback: number) => form.credits.max ?? fallback,
);

export const getOrderBy = createSelector(
  getFormValue,
  (form: FormState) => form.orderBy,
);

/**
 * RESULTS SELECTORS
 */

export const getSelectedCourse = createSelector(
  getSearchState,
  (state: Search): SearchResult | null => {
    if (
      state.details.mode === 'loaded' &&
      state.res !== 'unloaded' &&
      state.res.viewing.state === 'loaded'
    ) {
      const page = state.res.pages[state.res.viewing.pageIndex];
      const selectedIndex = state.details.selectedIndex;
      return page[selectedIndex] ?? null;
    }
    return null;
  },
);

export const getTotalResults = createSelector(
  getSearchState,
  (state: Search): 'unloaded' | 'loading' | 'error' | number => {
    if (state.res === 'unloaded') {
      return 'unloaded';
    } else if (state.res.total === 'loading') {
      return state.res.next === 'error' ? 'error' : 'loading';
    } else {
      return state.res.total.results;
    }
  },
);

export const getResults = createSelector(getSearchState, (state: Search) => {
  return state.res;
});

export const getCurrentFilters = createSelector(
  getSearchState,
  (state: Search) => state.currentFilters,
);

/**
 * DETAILS SELECTORS
 */

export const detailsState = createSelector(
  getSearchState,
  (state: Search) => state.details,
);

export const detailsMode = createSelector(
  detailsState,
  (state: Search['details']) => state.mode,
);

export const getSelectedIndexOrNull = createSelector(
  getSearchState,
  (state: Search): number | null => {
    if (state.details.mode === 'loaded') {
      return state.details.selectedIndex;
    } else {
      return null;
    }
  },
);

export const details = createSelector(
  getSelectedCourse,
  (course): SearchResult | null => {
    if (course && course.details.mode === 'loaded') {
      return course;
    } else {
      return null;
    }
  },
);

/**
 * SECTIONS SELECTORS
 */

export const sectionsState = createSelector(
  getSearchState,
  (state: Search) => state.sections,
);

export const sectionsIsShown = createSelector(
  sectionsState,
  (state: Search['sections']): boolean | null => {
    if (state.mode === 'unloaded') {
      return null;
    } else {
      return state.isShown;
    }
  },
);
