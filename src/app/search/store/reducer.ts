import { createReducer, on } from '@ngrx/store';
import {
  SearchState,
  EMPTY_STATE,
  MIN_CREDIT_VALUE,
  FormState,
  MAX_CREDIT_VALUE,
  createEmptyFormState,
  DEFAULT_ORDER_BY,
} from './state';
import {
  onNgrxForms,
  validate,
  wrapReducerWithFormStateUpdate,
  updateGroup,
  setValue,
  disable,
  enable,
  reset,
  box,
} from 'ngrx-forms';
import * as actions from './actions';
import { Zero } from '@app/types/terms';
import { greaterThanOrEqualTo, lessThanOrEqualTo } from 'ngrx-forms/validation';
import { UNLOADED } from '@app/types/loadable';
import { isSuccess, success as toSuccess } from 'loadable.ts';
import produce from 'immer';
import * as globalActions from '@app/actions';

export const reducer = wrapReducerWithFormStateUpdate(
  createReducer(
    EMPTY_STATE,

    on(
      actions.lifecycle.success,
      (
        state,
        { defaultActiveTerm, formDerrivedFromParams, autoSearchResults },
      ) => {
        if (!formDerrivedFromParams) {
          // If the action doesn't provide a FormState, assume that whatever state
          // is already present in the store is acceptable.
          return state;
        }

        if (autoSearchResults) {
          // If initial search results are provided, load those into the UI
          return {
            ...state,
            wasInitialized: 'done',
            defaultActiveTerm: toSuccess(defaultActiveTerm),
            form: setValue(enable(state.form), formDerrivedFromParams),
            currentFilters: formDerrivedFromParams,
            res: autoSearchResults,
          };
        }

        return {
          ...state,
          wasInitialized: 'done',
          defaultActiveTerm: toSuccess(defaultActiveTerm),
          form: setValue(enable(state.form), formDerrivedFromParams),
        };
      },
    ),

    on(globalActions.currentTermCode.set, (state, { currentTermCode }) => {
      return {
        ...state,
        form: updateGroup(state.form, {
          term: ctrl => setValue(ctrl, box(currentTermCode)),
        }),
      };
    }),

    on(actions.filters.reset, (state): SearchState => {
      if (isSuccess(state.defaultActiveTerm)) {
        const emptyForm = createEmptyFormState(state.defaultActiveTerm.value);
        return {
          ...state,
          form: reset(setValue(state.form, emptyForm)),
          currentFilters: null,
          res: 'unloaded',
        };
      }

      return state;
    }),
    on(actions.search.withOrderBy, (state, { orderBy }) => {
      return {
        ...state,
        form: updateGroup(state.form, {
          orderBy: ctrl => setValue(ctrl, orderBy),
        }),
        res: 'unloaded',
        details: { mode: 'unloaded' },
        sections: { mode: 'unloaded' },
      };
    }),
    on(actions.search.withCurrentFilters, (state): SearchState => {
      return produce(state, draft => {
        draft.currentFilters = draft.form.value;
        draft.res = 'unloaded';
        draft.details = { mode: 'unloaded' };
        draft.sections = { mode: 'unloaded' };
      });
    }),
    on(
      actions.search.withFilters,
      (state, { filters, orderBy }): SearchState => {
        if (orderBy) {
          state = {
            ...state,
            form: updateGroup(state.form, {
              orderBy: ctrl => setValue(ctrl, orderBy),
            }),
          };
        }

        return produce(state, draft => {
          draft.currentFilters = {
            ...filters,
            orderBy: orderBy || DEFAULT_ORDER_BY,
          };

          draft.res = 'unloaded';
          draft.details = { mode: 'unloaded' };
          draft.sections = { mode: 'unloaded' };
        });
      },
    ),

    on(actions.page.prev, (state): SearchState => {
      return produce(state, draft => {
        draft.details = { mode: 'unloaded' };
        draft.sections = { mode: 'unloaded' };

        if (draft.res === 'unloaded') {
          return;
        }

        if (draft.res.viewing.state === 'loaded') {
          // The user is currently viewing a loaded page of results. If there is
          // an already loaded page before this page, show that one. Or if there
          // are not prior pages, do nothing.
          if (draft.res.viewing.pageIndex > 0) {
            draft.res.viewing.pageIndex -= 1;
          }
        } else if (draft.res.viewing.state === 'next') {
          if (draft.res.pages.length > 0) {
            // If the user is currently viewing the state of the next page but
            // there is a previous loaded page, show that previous loaded page.
            draft.res.viewing = {
              state: 'loaded',
              pageIndex: draft.res.pages.length - 1,
            };
          }
        }
      });
    }),
    on(actions.page.next, (state): SearchState => {
      return produce(state, draft => {
        draft.details = { mode: 'unloaded' };
        draft.sections = { mode: 'unloaded' };

        if (draft.res === 'unloaded') {
          draft.res = {
            total: 'loading',
            pages: [],
            viewing: { state: 'next' },
            next: 'loading',
          };
          return;
        } else if (draft.res.total === 'loading') {
          return;
        }

        if (draft.res.viewing.state === 'loaded') {
          // The user is currently viewing a loaded page of results. If there is
          // an already loaded page after this page, show that one. If there are
          // more pages to load, indicate that the next page is loading. Or if
          // there are not more pages, do nothing.

          const pageIndex = draft.res.viewing.pageIndex;
          const totalPages = draft.res.total.pages; // 8
          const loadedPages = draft.res.pages.length; // 8

          if (pageIndex < loadedPages - 1) {
            // The next page is already loaded
            draft.res.viewing.pageIndex += 1;
          } else if (loadedPages < totalPages) {
            // Indicate that the next page has started loading
            draft.res.viewing = { state: 'next' };

            if (!draft.res.next) {
              draft.res.next = 'loading';
            }
          }
        }
      });
    }),
    on(actions.page.retry, (state): SearchState => {
      return produce(state, draft => {
        if (draft.res !== 'unloaded') {
          draft.res.next = 'loading';
        }
      });
    }),
    on(actions.page.done, (state, { found, hits }): SearchState => {
      return produce(state, draft => {
        if (draft.res === 'unloaded') {
          return;
        }

        // Make sure totals are populated if not already
        if (draft.res.total === 'loading') {
          draft.res.total = {
            results: found,
            pages: hits.length > 0 ? Math.ceil(found / hits.length) : 0,
          };
        }

        // Add another page to the list of loaded pages
        draft.res.pages.push(hits);

        // Update the page that the user is currently viewing ONLY IF the user is
        // waiting for this page to load.
        if (draft.res.viewing.state === 'next') {
          draft.res.viewing = {
            state: 'loaded',
            pageIndex: draft.res.pages.length - 1,
          };
        }

        // Clear the loading indicator for the next page
        draft.res.next = null;
      });
    }),
    on(actions.page.error, (state): SearchState => {
      return produce(state, draft => {
        if (draft.res === 'unloaded') {
          return;
        }

        // Make the next page as failing to load
        draft.res.next = 'error';
      });
    }),
    on(actions.page.clearAll, (state): SearchState => {
      return {
        ...state,
        currentFilters: null,
        res: 'unloaded',
      };
    }),

    on(actions.details.show, (state, { selectedIndex }): SearchState => {
      return {
        ...state,
        details: {
          mode: 'loaded',
          selectedIndex,
        },
        sections: UNLOADED,
      };
    }),
    on(actions.details.hide, (state): SearchState => {
      return { ...state, details: { mode: 'unloaded' } };
    }),
    on(actions.packages.ready, (state, { forIndex, packages }): SearchState => {
      if (
        state.details.mode === 'loaded' &&
        state.details.selectedIndex === forIndex
      ) {
        // Make sure that the course that these packages correspond to is still
        // the course that's loaded in the details view.
        const isShown =
          state.sections.mode === 'unloaded' ? false : state.sections.isShown;

        return {
          ...state,
          sections: { mode: 'loaded', packages, isShown },
        };
      }

      return state;
    }),
    on(actions.packages.error, (state, { forIndex }): SearchState => {
      if (
        state.details.mode === 'loaded' &&
        state.details.selectedIndex === forIndex
      ) {
        // Make sure that the course that these packages correspond to is still
        // the course that's loaded in the details view.
        const isShown =
          state.sections.mode === 'unloaded' ? false : state.sections.isShown;

        return {
          ...state,
          sections: { mode: 'error', isShown },
        };
      }

      return state;
    }),
    on(actions.packages.show, (state): SearchState => {
      if (state.details.mode === 'loaded') {
        const sections: SearchState['sections'] =
          state.sections.mode === 'unloaded' || state.sections.mode === 'error'
            ? { mode: 'loading', isShown: true }
            : { ...state.sections, isShown: true };
        return { ...state, sections };
      } else {
        return state;
      }
    }),
    on(actions.packages.hide, (state): SearchState => {
      if (
        state.details.mode === 'loaded' &&
        state.sections.mode !== 'unloaded'
      ) {
        const sections: SearchState['sections'] = {
          ...state.sections,
          isShown: false,
        };
        return { ...state, sections };
      } else {
        return state;
      }
    }),

    onNgrxForms(),
  ),
  state => state!.form,
  updateGroup<FormState>({
    open: (childState, parentState) => {
      return chosenTermIsNullOrZero(parentState)
        ? disable(childState)
        : enable(childState);
    },
    waitlisted: (childState, parentState) => {
      return chosenTermIsNullOrZero(parentState)
        ? disable(childState)
        : enable(childState);
    },
    closed: (childState, parentState) => {
      return chosenTermIsNullOrZero(parentState)
        ? disable(childState)
        : enable(childState);
    },
    credits: childState => {
      return updateGroup<FormState['credits']>(childState, {
        min: validate(
          greaterThanOrEqualTo(MIN_CREDIT_VALUE),
          lessThanOrEqualTo(childState.value.max ?? MAX_CREDIT_VALUE),
        ),
        max: validate(
          greaterThanOrEqualTo(childState.value.min ?? MIN_CREDIT_VALUE),
          lessThanOrEqualTo(MAX_CREDIT_VALUE),
        ),
      });
    },
    reservedSections: (childState, parentState) => {
      return chosenTermIsNullOrZero(parentState)
        ? disable(childState)
        : enable(childState);
    },
  }),
);

/**
 * HELPER FUNCTIONS
 */

const chosenTermIsNullOrZero = (state: SearchState['form']): boolean => {
  return state.value.term === null || Zero.is(state.value.term.value);
};
