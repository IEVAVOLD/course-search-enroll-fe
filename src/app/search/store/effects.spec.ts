import searchResults from '../../../../testdata/auto-search-results.json';
import rawAutoSearchResults from '../../../../testdata/raw-auto-search-results.json';
import aggregate1224Json from '../../../../testdata/aggregate1224.json';
import subjectsJson from '../../../../testdata/subjects-0000.json';
import { Injectable } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import {
  convertToParamMap,
  ParamMap,
  Params,
  ActivatedRoute,
} from '@angular/router';
import { ApiService } from '@app/services/api.service';
import * as selectors from './selectors';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import * as searchActions from './actions';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable, of, ReplaySubject } from 'rxjs';
import { EMPTY_STATE, SearchState, State } from './state';
import { SearchEffects } from './effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { success } from 'loadable.ts';
import { RawTerms, TermCode, toTerms } from '@app/types/terms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fromParams } from '../query-params';
import { RawSearchResults } from '@app/types/courses';

const initialState = {
  search: EMPTY_STATE,
  currentTermCode: success(TermCode.decodeOrThrow('1224')),
  terms: success(toTerms(aggregate1224Json as RawTerms, subjectsJson)),
};

/**
 * An ActivateRoute test double with a `paramMap` observable.
 * Use the `setParamMap()` method to add the next `paramMap` value.
 * This is needed by the termCode tests, since the effect checks it to
 * see which term to return.
 */
@Injectable()
class ActivatedRouteStub {
  // Use a ReplaySubject to share previous values with subscribers
  // and pump new values into the `paramMap` observable
  private subject = new ReplaySubject<ParamMap>();
  private subject2 = new ReplaySubject<ParamMap>();

  /** The mock paramMap observable */
  readonly paramMap = this.subject.asObservable();
  /** The mock paramMap observable */
  readonly queryParams = this.subject2.asObservable();

  /** Set the paramMap observables's next value */
  setParamMap(params?: Params) {
    if (params) {
      this.subject.next(convertToParamMap(params));
    } else {
      this.subject.next();
    }
  }
  setQueryMap(params?: Params) {
    if (params) {
      let paramMapValue = convertToParamMap(params);
      this.subject2.next((paramMapValue as any)['params']);
    } else {
      this.subject2.next();
    }
  }
}

class MockAPIService {
  postSearch(): Observable<RawSearchResults> {
    return of(rawAutoSearchResults as unknown as RawSearchResults);
  }
}

class MatSnackBarMock {
  open(_message: string, _action?: string, _config?: MatSnackBarConfig) {
    return this;
  }
  afterDismissed() {
    return of();
  }
  onAction() {
    return of();
  }
}

describe('Effects', () => {
  let actions$: Observable<any>;
  let effects: SearchEffects;
  let store: MockStore<State>;
  let apiService: ApiService;
  let activatedRoute: any = new ActivatedRouteStub();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [
        SearchEffects,
        provideMockActions(() => actions$),
        provideMockStore({ initialState }),
        { provide: MatSnackBar, useClass: MatSnackBarMock },
        { provide: ApiService, useClass: MockAPIService },
        { provide: ActivatedRoute, useValue: activatedRoute },
      ],
    }).compileComponents();

    effects = TestBed.inject(SearchEffects);
    store = TestBed.inject(MockStore);
    apiService = TestBed.inject(ApiService);
    activatedRoute = TestBed.inject(ActivatedRoute);
    activatedRoute.setQueryMap({});
  });

  describe('Effects', () => {
    it('should be created', () => {
      expect(effects).toBeTruthy();
      expect(store).toBeTruthy();
      expect(apiService).toBeTruthy();
      expect(activatedRoute).toBeTruthy();
    });
  });

  describe('LifecycleInit$', () => {
    it('should preserve the state if results are already loaded', done => {
      // Mock endpoint to return test data
      const spy = spyOn(apiService, 'postSearch').and.callThrough();
      // Provide a mock selector to return the search result data from the store
      store.overrideSelector(
        selectors.getResults,
        searchResults as SearchState['res'],
      );
      actions$ = of(searchActions.lifecycle.init);
      effects.LifecycleInit$.subscribe(result => {
        expect(result).toEqual(
          searchActions.lifecycle.success({
            defaultActiveTerm: TermCode.decodeOrThrow('1224'),
            formDerrivedFromParams: null,
            autoSearchResults: null,
          }),
        );
        expect(spy).toHaveBeenCalledTimes(0);
        done();
      });
    });
    it('should return the correct search state if query parameters are present', done => {
      // Here: Build out a json object with term, subject, etc and use that in setQueryMap, then pass the
      const testingParams: Params = { term: '0000', subject: '232' };
      // Set up query parameters
      activatedRoute.setQueryMap(testingParams);
      // Mock endpoint to return test data
      const spy = spyOn(apiService, 'postSearch').and.callThrough();
      // Expected form state to be returned
      const expectedForm = fromParams(initialState.terms.value, testingParams);
      // Provide a mock selector to return the search result data from the store
      store.overrideSelector(selectors.getResults, 'unloaded');
      // Trigger the action
      actions$ = of(searchActions.lifecycle.init);
      effects.LifecycleInit$.subscribe(result => {
        expect(result).toMatchObject(
          searchActions.lifecycle.success({
            defaultActiveTerm: TermCode.decodeOrThrow('1224'),
            formDerrivedFromParams: expectedForm,
            autoSearchResults: searchResults as SearchState['res'],
          }),
        );
        expect(spy).toHaveBeenCalledTimes(1);
        done();
      });
    });
    it('should return the correct search state if search results have not been loaded yet and no parameters are present', done => {
      // Mock endpoint to return test data
      const spy = spyOn(apiService, 'postSearch').and.callThrough();
      // Expected form state to be returned
      const expectedForm = fromParams(
        initialState.terms.value,
        {},
        TermCode.decodeOrThrow('1224'),
      );
      // Provide a mock selector to return the search result data from the store
      store.overrideSelector(selectors.getResults, 'unloaded');
      // Trigger the action
      actions$ = of(searchActions.lifecycle.init);
      effects.LifecycleInit$.subscribe(result => {
        expect(result).toEqual(
          searchActions.lifecycle.success({
            defaultActiveTerm: TermCode.decodeOrThrow('1224'),
            formDerrivedFromParams: expectedForm,
            autoSearchResults: null,
          }),
        );
        expect(spy).toHaveBeenCalledTimes(0);
        done();
      });
    });
  });
});
