import { Component, Inject, OnDestroy } from '@angular/core';
import {
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { unsubscribeAll } from '@app/core/utils';
import { ApiService } from '@app/services/api.service';
import {
  EnrollmentPackage,
  RoadmapMeeting,
  SearchResult,
} from '@app/types/courses';
import { Session, TermCode } from '@app/types/terms';
import { failed, isSuccess, Loadable, LOADING, success } from 'loadable.ts';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import * as globalActions from '@app/actions';
import { Store } from '@ngrx/store';
import { GlobalState } from '@app/state';

interface CourseWithPackage extends SearchResult {
  package: EnrollmentPackage & { classMeetings: RoadmapMeeting[] };
}

@Component({
  selector: 'cse-add-by-class-num-dialog',
  templateUrl: './add-by-class-num-dialog.component.html',
  styleUrls: ['./add-by-class-num-dialog.component.scss'],
})
export class AddByClassNumDialogComponent implements OnDestroy {
  public termCode: TermCode;
  public sessions: Session[];

  public classNumCtrl = new UntypedFormControl(null, [
    Validators.required,
    Validators.pattern(/^\d{5}$/),
  ]);

  public classNumLength$: Observable<number> =
    this.classNumCtrl.valueChanges.pipe(
      map(value => (typeof value === 'string' ? value.length : 0)),
      startWith(0),
    );

  public parsedClassNum$ = this.classNumCtrl.valueChanges.pipe(
    map(toClassNumber),
  );

  public loadablePackages: Loadable<
    CourseWithPackage[],
    'unableToLoadPackages' | 'noPackagesFound'
  > | null = null;

  public courseCtrl = new UntypedFormGroup({
    whichCourse: new UntypedFormControl(null, Validators.required),
    creditValue: new UntypedFormControl(null, Validators.required),
  });

  private subscriptions: Subscription[] = [];

  constructor(
    private api: ApiService,
    private store: Store<GlobalState>,
    @Inject(MAT_DIALOG_DATA) data: { termCode: TermCode; sessions: Session[] },
  ) {
    this.termCode = data.termCode;
    this.sessions = data.sessions;

    if (
      this.loadablePackages &&
      isSuccess(this.loadablePackages) &&
      this.loadablePackages.value.length === 1
    ) {
      this.courseCtrl
        .get('whichCourse')
        ?.setValue(this.loadablePackages.value[0]);
    }

    this.subscriptions.push(
      this.courseCtrl
        .get('whichCourse')!
        .valueChanges.subscribe((newVal: CourseWithPackage | null) => {
          this.courseCtrl.get('creditValue')!.reset();

          if (
            newVal &&
            newVal.package.credits.min === newVal.package.credits.max
          ) {
            this.courseCtrl
              .get('creditValue')!
              .setValue(newVal.package.credits.min);
          }
        }),
    );
  }

  ngOnDestroy() {
    unsubscribeAll(this.subscriptions);
  }

  async onCheckCredits() {
    const classNumber = toClassNumber(this.classNumCtrl.value);

    if (classNumber === null) {
      return;
    }

    this.loadablePackages = LOADING;

    try {
      const allPacks = await this.api
        .getPackageByClassNumber(this.termCode, classNumber)
        .toPromise();

      /**
       * Filter out courses that are children of parent topics (ex: 58451.22) We
       * want the parent topics course (ex: 58451)
       */
      const packsWithoutTopicChildren = allPacks.filter(
        pack =>
          !(pack as any).hasOwnProperty('topic') &&
          TermCode.decodeOrNull(pack.termCode) !== null,
      );

      const matchingCourses = await Promise.all(
        packsWithoutTopicChildren.map(
          async (raw): Promise<CourseWithPackage> => {
            const termCode = TermCode.decodeOrThrow(raw.termCode);
            const subjectCode = raw.subjectCode;
            const courseId = raw.courseId;
            const rawDetails = await this.api
              .getDetails(termCode, subjectCode, courseId)
              .toPromise();
            const classMeetings = raw.sections.flatMap(section => {
              return section.classMeetings
                .filter(meeting => meeting.meetingType === 'CLASS')
                .map(meeting => ({ ...meeting, sectionType: section.type }))
                .map(RoadmapMeeting.from as any);
            });
            return {
              ...SearchResult.from([this.termCode], {
                ...rawDetails,
                hasChanges: false,
              }),
              package: {
                ...EnrollmentPackage.from(this.sessions, raw),
                classMeetings: classMeetings as any,
              },
            };
          },
        ),
      );

      if (matchingCourses.length === 0) {
        this.loadablePackages = failed('noPackagesFound');
      } else {
        this.loadablePackages = success(matchingCourses);
      }

      if (matchingCourses.length === 1) {
        this.courseCtrl.get('whichCourse')?.setValue(matchingCourses[0]);
      }
    } catch {
      this.loadablePackages = failed('unableToLoadPackages');
    }
  }

  async onAddToCart() {
    const course: CourseWithPackage = this.courseCtrl.get('whichCourse')!.value;
    const credits = this.courseCtrl.get('creditValue')!.value;

    if (!course || typeof credits !== 'number') {
      return;
    }

    this.store.dispatch(
      globalActions.courses.roadmap.addCourseWithPack({
        course,
        pack: course.package,
        credits,
        joinWaitlist: false,
        withHonors: false,
        showToast: true,
        classPermissionNumber: null,
      }),
    );
  }
}

const toClassNumber = (u: unknown): number | null => {
  if (typeof u === 'string' && /^\d{5}$/.test(u)) {
    return parseInt(u, 10);
  } else {
    return null;
  }
};
