import {
  Component,
  OnDestroy,
  Input,
  ViewChildren,
  ElementRef,
  QueryList,
  AfterViewInit,
} from '@angular/core';
import { State } from '@search/store/state';
import { Observable, Subscription, of } from 'rxjs';
import { Store } from '@ngrx/store';
import * as sel from '@search/store/selectors';
import { SearchResult } from '@app/types/courses';
import * as actions from '@search/store/actions';
import { CourseListItemComponent } from '@app/shared/components/course-list-item/course-list-item.component';
import { unsubscribeAll } from '@app/core/utils';
import { map } from 'rxjs/operators';
import { LiveAnnouncer } from '@angular/cdk/a11y';

interface VisiblePage {
  pageNumber: number;
  totalPages: number;
  hasPrevPage: boolean;
  hasNextPage: boolean;
  results: 'loading' | 'error' | Array<SearchResult>;
}

@Component({
  selector: 'cse-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss'],
})
export class SearchResultsComponent implements OnDestroy, AfterViewInit {
  @Input() public focusOnIndex: Observable<number> = of();
  @ViewChildren(CourseListItemComponent, { read: ElementRef })
  public courseListItems!: QueryList<ElementRef<CourseListItemComponent>>;

  private subscriptions: Subscription[] = [];
  public selectedIndex = this.store.select(sel.getSelectedIndexOrNull);

  public visiblePage$ = this.store.select(sel.getResults).pipe(
    map((res): VisiblePage | 'loading' | 'error' | 'unloaded' => {
      if (res === 'unloaded') {
        return 'unloaded';
      } else if (res.total === 'loading') {
        if (res.viewing.state === 'next' && res.next === 'error') {
          return 'error';
        } else {
          return 'loading';
        }
      } else if (res.viewing.state === 'next') {
        // Viewing page as it loads
        return {
          pageNumber: res.pages.length + 1,
          totalPages: res.total.pages,
          hasPrevPage: res.pages.length > 0,
          hasNextPage: false,
          results: res.next ?? res.pages[res.pages.length - 1] ?? [],
        };
      } else {
        // Viewing an already loaded page
        const pageIndex = res.viewing.pageIndex;
        return {
          pageNumber: pageIndex + 1,
          totalPages: res.total.pages,
          hasPrevPage: pageIndex > 0,
          hasNextPage: res.total.pages > pageIndex + 1,
          results: res.pages[pageIndex],
        };
      }
    }),
  );

  constructor(public store: Store<State>, private announcer: LiveAnnouncer) {}

  ngAfterViewInit() {
    this.subscriptions.push(
      this.focusOnIndex.subscribe(index => {
        // Try and find a cse-course-list-item element with the same index as
        // provided by the `focusOnIndex` observable. If one is found, focus
        // on that component's first child which should be its button wrapper.
        const ref = this.courseListItems.find((_, i) => i === index);
        ((ref as any)?.nativeElement as any)?.firstChild?.focus();
      }),
    );
  }

  ngOnDestroy() {
    unsubscribeAll(this.subscriptions);
  }

  onRetryFailedSearch() {
    this.store.dispatch(actions.page.retry());
  }

  onShowDetails(selectedIndex: number) {
    this.store.dispatch(actions.details.show({ selectedIndex }));
  }

  onPrevPage(current: number, total: number) {
    this.announcer.announce(
      `Switched to page ${current - 1} of ${total}`,
      'assertive',
    );
    this.store.dispatch(actions.page.prev());
  }

  async onNextPage(current: number, total: number) {
    this.announcer.announce(
      `Switched to page ${current + 1} of ${total}`,
      'assertive',
    );
    this.store.dispatch(actions.page.next());
  }

  isArray(thing: unknown) {
    return Array.isArray(thing);
  }
}
