/**
 * This file contains functions to convert the form state into a set of query
 * params for use in the view's URL.
 *
 * Basically each key with type `T` in the `FormState` type is assigned a
 * serializer of type `Serializer<T>`. These `Serializer` types have
 * `serialize(T) => string` and `deserialize(string) => T | null` functions to
 * perform the conversion in each direction.
 *
 * The top-level `toParams` and `fromParams` functions then loop over the
 * `serializers` object containing all of the indicidual serializers doing the
 * appropriate conversion to generate query parameters from a `FormState` or to
 * generate a `FormState` from a map of query parameters.
 */

import {
  FormState,
  createEmptyFormState,
  FOREIGN_LANGUAGE,
  ForeignLanguage,
  isOrderBy,
  DEFAULT_ORDER_BY,
  MODES_OF_INSTRUCTION,
  ModeOfInstruction,
} from '@search/store/state';
import {
  Terms,
  TermCodeOrZero,
  Term,
  TermCode,
  SpecialGroup,
  Zero,
} from '@app/types/terms';
import { Params } from '@angular/router';
import { box, Boxed } from 'ngrx-forms';
import { notNull } from '@app/core/utils';

interface Serializer<T> {
  /**
   * This function will only be called if this parameter exists and the value
   * is a non-empty string.
   */
  deserialize: (param: string, term: Term) => T;

  /**
   * The result of this function will only be included in the query params map
   * if it returns a non-empty string and if that string is different from the
   * this key's serialied value in the EMPTY_FORM_STATE object.
   */
  serialize: (state: T) => string;
}

type FormSerializer = {
  [K in keyof FormState]: Serializer<FormState[K]>;
};

const boolSerializer: Serializer<boolean> = {
  deserialize: param => param === 'true',
  serialize: state => `${state}`,
};

const strSerializer: Serializer<string | null> = {
  deserialize: param => (param === '' ? null : param),
  serialize: param => (param ? param : ''),
};

const formSerializers: FormSerializer = {
  orderBy: {
    deserialize: param => (isOrderBy(param) ? param : DEFAULT_ORDER_BY),
    serialize: state => state,
  },

  term: {
    deserialize: param => {
      const parsed = TermCode.decodeOrNull(param);
      return parsed ? box(parsed) : null;
    },
    serialize: state => (state ? TermCode.encode(state.value) : '0000'),
  },

  subject: {
    deserialize: (param, term) => {
      const found = term.subjects.find(s => s.code === param);
      return found ? box(found) : null;
    },
    serialize: state => (state ? state.value.code : ''),
  },

  keywords: {
    deserialize: param => param.trim(),
    serialize: state => (state ?? '').trim(),
  },

  open: boolSerializer,
  waitlisted: boolSerializer,
  closed: boolSerializer,

  commA: boolSerializer,
  commB: boolSerializer,
  quantA: boolSerializer,
  quantB: boolSerializer,
  ethnicStudies: boolSerializer,

  biologicalSciences: boolSerializer,
  humanities: boolSerializer,
  literature: boolSerializer,
  naturalSciences: boolSerializer,
  physicalSciences: boolSerializer,
  socialSciences: boolSerializer,

  elementary: boolSerializer,
  intermediate: boolSerializer,
  advanced: boolSerializer,

  language: {
    deserialize: param => {
      return FOREIGN_LANGUAGE.includes(param as any)
        ? (param as ForeignLanguage)
        : 'all';
    },
    serialize: state => state,
  },

  honorsOnly: boolSerializer,
  acceleratedHonors: boolSerializer,
  honorsOptional: boolSerializer,

  reservedSections: {
    deserialize: param => {
      if (param === 'all') {
        return 'all';
      }

      if (param === 'none') {
        return 'none';
      }

      const [attr, code] = param.split('-');
      if (SpecialGroup.isPlausibleAttr(attr)) {
        return box({ attr, code: code ?? null });
      } else {
        return 'all';
      }
    },
    serialize: state => {
      if (state === 'all') {
        return 'all';
      }

      if (state === 'none') {
        return 'none';
      }

      if (state.value.code) {
        return `${state.value.attr}-${state.value.code}`;
      } else {
        return `${state.value.attr}`;
      }
    },
  },

  modeOfInstruction: {
    deserialize: param => {
      if (MODES_OF_INSTRUCTION.includes(param as any)) {
        return param as ModeOfInstruction;
      } else {
        return 'all';
      }
    },
    serialize: state => {
      return state;
    },
  },

  sustainability: boolSerializer,
  graduateCourseworkRequirement: boolSerializer,
  workplaceExperience: boolSerializer,
  communityBasedLearning: boolSerializer,
  repeatableForCredit: boolSerializer,

  credits: {
    deserialize: param => {
      let match: RegExpExecArray | null = null;
      if ((match = /^-(\d+)$/.exec(param))) {
        const max = parseInt(match[1], 10);
        return { min: null, max };
      } else if ((match = /^(\d+)-$/.exec(param))) {
        const min = parseInt(match[1], 10);
        return { min, max: null };
      } else if ((match = /^(\d+)-(\d+)$/.exec(param))) {
        const min = parseInt(match[1], 10);
        const max = parseInt(match[2], 10);
        return { min, max };
      } else {
        return { min: null, max: null };
      }
    },
    serialize: ({ min, max }) => {
      if (min !== null && max !== null) {
        return `${min}-${max}`;
      } else if (min !== null) {
        return `${min}-`;
      } else if (max !== null) {
        return `-${max}`;
      } else {
        return '';
      }
    },
  },

  catalogNum: {
    deserialize: (param): { min: string | null; max: string | null } => {
      let match: RegExpExecArray | null = null;
      if ((match = /^(\d+)$/.exec(param))) {
        return { min: match[1], max: match[1] };
      } else if ((match = /^-(\d+)$/.exec(param))) {
        const max = match[1];
        return { min: null, max };
      } else if ((match = /^(\d+)-$/.exec(param))) {
        const min = match[1];
        return { min, max: null };
      } else if ((match = /^(\d+)-(\d+)$/.exec(param))) {
        const min = match[1];
        const max = match[2];
        return { min, max };
      } else {
        return { min: null, max: null };
      }
    },
    serialize: ({ min, max }): string => {
      if (min !== null && max !== null) {
        if (min === max) {
          return `${min}`;
        }
        return `${min}-${max}`;
      } else if (min !== null) {
        return `${min}-`;
      } else if (max !== null) {
        return `-${max}`;
      } else {
        return '';
      }
    },
  },

  sessions: {
    deserialize: (param, term) => {
      const sessions = param
        .split(',')
        .map(code => term.sessions.find(s => s.code === code) ?? null)
        .filter(notNull);

      if (sessions.length > 0) {
        return box(sessions);
      } else {
        return null;
      }
    },
    serialize: state => {
      return (state ? state.value : []).map(s => s.code).join(',');
    },
  },

  courseId: strSerializer,
  topicId: strSerializer,
};

/**
 * For backwards compatibility
 */
type AliasFn = (params: Params) => [string, string] | void;

const paramAliases: AliasFn[] = [
  params => {
    const q = params['q'];
    if (typeof q === 'string') {
      return ['keywords', q];
    }
  },
  params => {
    const subjectCode = params['subjectCode'];
    if (typeof subjectCode === 'string') {
      return ['subject', subjectCode];
    }
  },
  params => {
    const instructionMode = params['instructionMode'];
    if (typeof instructionMode === 'string') {
      switch (instructionMode.toLowerCase()) {
        case 'classroom instruction':
          return ['modeOfInstruction', 'classroom'];
        case 'online (some classroom)':
          return ['modeOfInstruction', 'hybrid'];
        case 'online only':
          return ['modeOfInstruction', 'either'];
      }
    }
  },
  params => {
    const ethnicStudies = params['ethnicStudies'];
    if (
      typeof ethnicStudies === 'string' &&
      ethnicStudies.toLowerCase() === 'ethnicstudies'
    ) {
      return ['ethnicStudies', 'true'];
    }
  },
  params => {
    const attrCode = params['attrCode'];
    const attrValue = params['attrValue'];

    if (
      typeof attrCode === 'string' &&
      SpecialGroup.isPlausibleAttr(attrCode) &&
      typeof attrValue === 'string'
    ) {
      return ['reservedSections', `${attrCode}-${attrValue}`];
    }
  },
  params => {
    /**
     * Since ROAD-125 we want to let users select multiple sessions instead of
     * just one. The new query parameter name is "sessions" but if a URL arrives
     * with the old "session" parameter we want to trivially map it to the new
     * "sessions" parameter for backwards compatibility.
     */
    const session = params['session'];

    if (typeof session === 'string' && /^[a-z0-9]+$/i.test(session)) {
      return ['sessions', session];
    }
  },
];

export const toParams = (
  activeTermCodes: TermCode[] | null,
  paramState: FormState,
  alwaysInclude: Array<keyof FormState> = [],
): Params => {
  const params: Params = {};
  const emptyState = createEmptyFormState(
    activeTermCodes ? TermCode.findFirst(activeTermCodes) : null,
  );

  for (const key in paramState) {
    if (paramState.hasOwnProperty(key)) {
      type K = keyof FormState;
      const serializer = formSerializers[key as K] as Serializer<unknown>;
      const paramValue = serializer.serialize(paramState[key as K]);
      const emptyValue = serializer.serialize(emptyState[key as K]);
      if (
        alwaysInclude.includes(key as any) ||
        (paramValue !== '' && paramValue !== emptyValue)
      ) {
        params[key] = paramValue;
      }
    }
  }

  return params;
};

const chosenTermFromParams = (
  terms: Terms,
  params: Params,
  currentTermCode?: TermCode,
): Term => {
  const termCodeFromParams = TermCodeOrZero.decodeOrNull(params['term']);

  const currentTerm: Term | undefined = currentTermCode
    ? terms.nonZero[TermCode.encode(currentTermCode)]
    : undefined;

  if (TermCode.is(termCodeFromParams)) {
    const encoded = TermCodeOrZero.encode(termCodeFromParams);
    return terms.nonZero[encoded] ?? currentTerm ?? terms.zero;
  }

  if (Zero.is(termCodeFromParams)) {
    return terms.zero;
  }

  if (currentTerm) {
    return currentTerm;
  }

  const firstActive = TermCode.findFirst(terms.activeTermCodes);
  return terms.nonZero[TermCodeOrZero.encode(firstActive)];
};

export const fromParams = (
  terms: Terms,
  params: Params,
  currentTermCode?: TermCode,
): FormState => {
  const term = chosenTermFromParams(terms, params, currentTermCode);

  const defaultTermCode = TermCode.findFirst(terms.activeTermCodes);
  const emptyState = createEmptyFormState(defaultTermCode);

  const fields: Partial<FormState> & { term: FormState['term'] } = {
    term: term === terms.zero ? null : box(term.termCode as TermCode),
  };

  const dealiased: Params = { ...params };
  paramAliases.forEach(fn => {
    const maybeDealias = fn(params);
    if (maybeDealias) {
      dealiased[maybeDealias[0]] = maybeDealias[1];
    }
  });

  for (const key in dealiased) {
    if (emptyState.hasOwnProperty(key)) {
      const param = `${dealiased[key] ?? ''}`;
      if (param.trim().length === 0) {
        continue;
      }

      type K = keyof FormState;
      const serializer = formSerializers[key as K] as Serializer<unknown>;
      let state = serializer.deserialize(param, term);

      // Handle the case where the `term` parameter in the URL contained valid
      // term-code syntax but represented a term that isn't part of the active
      // term set.
      if (
        key === 'term' &&
        TermCode.is((state as FormState['term'])?.value) &&
        TermCode.isContainedInArray(
          (state as Boxed<TermCode>).value,
          terms.activeTermCodes,
        ) === false
      ) {
        state = box(defaultTermCode);
      }

      (fields as any)[key] = state;
    }
  }

  return { ...emptyState, ...fields };
};
