import { Component, OnInit, HostBinding, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { GlobalState } from './state';
import * as actions from './actions';
import {
  Router,
  ActivationStart,
  RouteConfigLoadStart,
  RouteConfigLoadEnd,
  NavigationEnd,
} from '@angular/router';
import { map, filter } from 'rxjs/operators';
import {
  BannerService,
  LAST_DISMISSED_BANNER_KEY,
} from '@app/services/banner.service';
import { IS_PRIVATE } from './services/is-private.token';
import { AnalyticsService } from './services/analytics.service';
import { WebsocketService } from './services/websocket.service';
import * as s from '@app/types/schema';
import { TermCode } from './types/terms';
import { notNull } from './core/utils';

@Component({
  selector: 'cse-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  @HostBinding('class') public classList = '';
  public jumpLinks: Observable<{ href: string; text: string }[]>;
  public loadCount = 0;
  public isLoading = false;
  public banner$: Observable<string | null>;
  public dismissedBannerThisSession = false;

  constructor(
    private store: Store<GlobalState>,
    @Inject(IS_PRIVATE) public isPrivate: boolean,
    router: Router,
    banners: BannerService,
    analytics: AnalyticsService,
    websocket: WebsocketService,
  ) {
    this.banner$ = banners.banner.pipe(map(banner => banner?.text ?? null));

    this.jumpLinks = router.events.pipe(
      filter((event): event is ActivationStart => {
        return event instanceof ActivationStart;
      }),
      map(event => {
        const { jumpLinks } = event.snapshot.data;
        if (Array.isArray(jumpLinks)) {
          return jumpLinks;
        } else {
          return [];
        }
      }),
    );

    router.events.subscribe(event => {
      if (event instanceof RouteConfigLoadStart) {
        this.loadCount++;
      } else if (event instanceof RouteConfigLoadEnd) {
        this.loadCount--;
      }
      this.isLoading = !!this.loadCount;
    });

    router.events
      .pipe(filter((e): e is NavigationEnd => e instanceof NavigationEnd))
      .subscribe(() => {
        analytics.pageview();
      });

    router.events
      .pipe(
        filter((event): event is ActivationStart => {
          return event instanceof ActivationStart;
        }),
        map(event => {
          if (typeof event.snapshot.data.scrollStrategy === 'string') {
            return event.snapshot.data.scrollStrategy;
          } else {
            return 'fixed';
          }
        }),
      )
      .subscribe(strategy => {
        this.classList = `scroll-strategy-${strategy}`;
      });

    // A tool for helping debug banners
    (window as any).clearDissmissedBanner =
      this.clearDissmissedBanner.bind(this);

    websocket.listenByType('cartInvalid').subscribe(payload => {
      const hasTermCodes = s.object({
        term_codes: s.array(s.string),
        event_id: s.string,
      });

      if (hasTermCodes.matches(payload)) {
        this.store.dispatch(
          actions.courses.roadmap.hintToRevalidate({
            eventId: payload.event_id,
            termCodes: payload.term_codes
              .map(TermCode.decodeOrNull)
              .filter(notNull),
          }),
        );
      }
    });
  }

  ngOnInit() {
    this.store.dispatch(actions.terms.fetch());

    if (this.isPrivate) {
      this.store.dispatch(actions.prefs.fetch());
      this.store.dispatch(actions.student.fetch());
    }
  }

  onDismissBanner(text: string) {
    this.dismissedBannerThisSession = true;
    this.store.dispatch(
      actions.prefs.setKey({
        key: LAST_DISMISSED_BANNER_KEY,
        value: text,
      }),
    );
  }

  clearDissmissedBanner() {
    this.store.dispatch(
      actions.prefs.setKey({
        key: LAST_DISMISSED_BANNER_KEY,
        value: null,
      }),
    );
  }
}
