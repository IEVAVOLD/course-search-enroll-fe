// noinspection JSUnusedLocalSymbols

import { Observable, of, throwError } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { ApiService } from '@app/services/api.service';
import { Effects } from '@app/effects';
import { courses, prefs, student, terms } from '@app/actions';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { GlobalState } from '@app/state';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogService } from '@app/shared/services/dialog.service';
import { RouterTestingModule } from '@angular/router/testing';
import * as selectors from '@app/selectors';

import {
  ActivatedRoute,
  convertToParamMap,
  ParamMap,
  Params,
} from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { Injectable } from '@angular/core';

import aggregateJson from '../../testdata/aggregate.json';
import subjectsJson from '../../testdata/subjects-0000.json';
import studentInfoJson from '../../testdata/studentInfo.json';
import enrollmentsJson from '../../testdata/current-enrollments.json';
import roadmapJson from '../../testdata/get-roadmap-courses.json';
import packagesJson from '../../testdata/getEnrollmentPackages.json';
import packageByIdJson from '../../testdata/getEnrollmentPackageById.json';
import courseDetailsJson from '../../testdata/courseDetails.json';
import favoritesJson from '../../testdata/favorites.json';

import { RawTerms, Session, TermCode, toTerms, Zero } from '@app/types/terms';
import { success } from 'loadable.ts';
import * as actions from '@app/actions';
import { ComponentType } from '@angular/cdk/portal';
import { HttpClientTestingModule } from '@angular/common/http/testing';

/**
 * An ActivateRoute test double with a `paramMap` observable.
 * Use the `setParamMap()` method to add the next `paramMap` value.
 * This is needed by the termCode tests, since the effect checks it to
 * see which term to return.
 */
@Injectable()
class ActivatedRouteStub {
  // Use a ReplaySubject to share previous values with subscribers
  // and pump new values into the `paramMap` observable
  private subject = new ReplaySubject<ParamMap>();
  private subject2 = new ReplaySubject<ParamMap>();

  /** The mock paramMap observable */
  readonly paramMap = this.subject.asObservable();
  /** The mock paramMap observable */
  readonly queryParams = this.subject2.asObservable();

  /** Set the paramMap observables's next value */
  setParamMap(params?: Params) {
    if (params) {
      this.subject.next(convertToParamMap(params));
    } else {
      this.subject.next();
    }
  }
  setQueryMap(params?: Params) {
    if (params) {
      let paramMapValue = convertToParamMap(params);
      this.subject2.next((paramMapValue as any)['params']);
    } else {
      this.subject2.next();
    }
  }
}

const TestPrefData1 = { key: 'TheKey', value: 'Pref 1' };
const TestPrefData2 = { key: 'TheKey', value: 'Pref 2' };

const initialState: Pick<
  GlobalState,
  'prefs' | 'student' | 'courses' | 'currentTermCode' | 'terms'
> = {
  prefs: success({ key: 'TheKey', value: 'Pref 1' }),
  student: success({}),
  courses: {},
  currentTermCode: success(TermCode.decodeOrThrow('1212')),
  terms: success(toTerms(aggregateJson as RawTerms, subjectsJson)),
};

class DialogMock {
  open() {
    return {
      afterClosed: () => of({}),
    };
  }
  cannotClose() {
    return {
      afterClosed: () => of({}),
    };
  }
  keepOpen(_headline: string, _message: string) {
    return of(() => {
      of({});
    });
  }
  small(_component: ComponentType<any>, _data?: any) {
    return of({});
  }
}

class MatSnackBarMock {
  open(_message: string, _action?: string, _config?: any) {
    return this;
  }
  afterDismissed() {
    return of();
  }
  onAction() {
    return of();
  }
}

describe('Effects', () => {
  let actions$: Observable<any>;
  let effects: Effects;
  let store: MockStore<GlobalState>;
  let apiService: ApiService;
  let activatedRoute: any = new ActivatedRouteStub();
  let dialog: DialogService;
  let snackBar: MatSnackBar;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [
        Effects,
        provideMockActions(() => actions$),
        provideMockStore({ initialState }),
        { provide: ApiService, useClass: ApiService },
        { provide: MatSnackBar, useClass: MatSnackBarMock },
        { provide: DialogService, useClass: DialogMock },
        { provide: ActivatedRoute, useValue: activatedRoute },
      ],
    }).compileComponents();
    effects = TestBed.inject(Effects);
    store = TestBed.inject(MockStore);
    apiService = TestBed.inject(ApiService);
    activatedRoute = TestBed.inject(ActivatedRoute);

    activatedRoute.setQueryMap({ term: '1222' });
    dialog = TestBed.inject(DialogService);
    snackBar = TestBed.inject(MatSnackBar);
  });
  describe('Effects', () => {
    // sanity check on our TestBed configuration
    it('should be created', () => {
      expect(effects).toBeTruthy();
      expect(store).toBeTruthy();
      expect(apiService).toBeTruthy();
      expect(activatedRoute).toBeTruthy();
      expect(dialog).toBeTruthy();
      expect(snackBar).toBeTruthy();
    });
  });
  describe('Effects.prefs', () => {
    it('should fire if prefs is null', done => {
      // Set up mock endpoint to return data
      const spy = spyOn(apiService, 'getPrefs').and.callFake(() => {
        return of(TestPrefData1);
      });
      // Trigger the action
      actions$ = of(prefs.fetch);
      // wait for the action to trigger the effect
      effects.fetchPrefs$.subscribe(res => {
        expect(res).toEqual(prefs.found({ prefs: TestPrefData1 }));
        expect(spy).toHaveBeenCalledTimes(1);
        done();
      });
    });
    it('set prefs should work', done => {
      // Set up mock endpoint to return data
      const postPrefsSpy = spyOn(apiService, 'postPrefs').and.callFake(
        prefs => {
          return of(prefs);
        },
      );
      // Provide a mock selector to return the prefs data from the store
      // since it gets merged in the effect
      store.overrideSelector(selectors.prefs.getAll, success(TestPrefData2));
      // Trigger the action
      actions$ = of(prefs.setKey(TestPrefData2));
      // wait for the action to trigger the effect
      effects.setPref$.subscribe(res => {
        expect(res).toEqual(TestPrefData2);
        expect(postPrefsSpy).toHaveBeenCalledTimes(1);
        done();
      });
    });
  });
  describe('Effects.terms', () => {
    it('should fetch terms', done => {
      // Set up mock endpoints to return data
      const getAggregateSpy = spyOn(apiService, 'getAggregate').and.callFake(
        () => {
          return of(aggregateJson);
        },
      );
      const getShortSubjectNamesSpy = spyOn(
        apiService,
        'getShortSubjectNames',
      ).and.callFake(() => {
        return of(subjectsJson);
      });

      // Trigger the action
      actions$ = of(terms.fetch);
      // wait for the action to trigger the effect
      effects.fetchTerms$.subscribe(res => {
        expect(res).toBeTruthy();

        expect((res as any)['terms']).toBeTruthy();
        expect((res as any)['terms']['nonZero']['1222']).toBeTruthy();

        // there should be the same number of subjects per term before the transformation as afterwards
        expect(aggregateJson.subjects['1222'].length).toEqual(
          (res as any)['terms']['nonZero']['1222']['subjects'].length,
        );
        expect(aggregateJson.subjects['0000'].length).toEqual(
          (res as any)['terms']['zero']['subjects'].length,
        );
        // check that the term code is set correctly
        expect((res as any)['currentTermCode']).toEqual(
          TermCode.decodeOrThrow('1222'),
        );

        // there should be two calls to API endpoints
        expect(getAggregateSpy).toHaveBeenCalledTimes(1);
        expect(getShortSubjectNamesSpy).toHaveBeenCalledTimes(1);
        done();
      });
    });
    it('fetch terms should handle failure with a dialog', done => {
      // set up a spy on the cannotClose mocked method. The dialog mock already noops it, but the spy
      // gives us a way to see if it was called. It normally creates a dialog without a close button.
      const cannotCloseSpy = spyOn(dialog, 'cannotClose').and.callThrough();
      // trigger the action that is sent when the terms data cannot be loaded
      actions$ = of(terms.failed());
      effects.termsFailed$.subscribe(res => {
        expect(res.type).toEqual(terms.failed.type);
        // check that we tried to open the dialog
        expect(cannotCloseSpy).toHaveBeenCalledTimes(1);
        done();
      });
    });
    it('fetch terms should fail correctly', done => {
      // Set up mock endpoints to return errors
      const getAggregateSpy = spyOn(apiService, 'getAggregate').and.callFake(
        () => {
          return throwError(new Error('Fake error'));
        },
      );
      const getShortSubjectNamesSpy = spyOn(
        apiService,
        'getShortSubjectNames',
      ).and.callFake(() => {
        return throwError(new Error('Fake error'));
      });

      // trigger the action
      actions$ = of(terms.fetch);
      // wait for the effect to fire
      effects.fetchTerms$.subscribe(res => {
        // We should get a failure action back
        expect(res).toBeTruthy();
        expect(res.type).toEqual(terms.failed.type);

        // We should have tried to call both endpoints
        expect(getAggregateSpy).toHaveBeenCalledTimes(1);
        expect(getShortSubjectNamesSpy).toHaveBeenCalledTimes(1);

        done();
      });
    });
  });
  describe('Effects.studentInfo', () => {
    it('should fire if studentInfo is null', done => {
      // set up the mock endpoint to return the studentInfo data
      const getStudentInfoSpy = spyOn(
        apiService,
        'getStudentInfo',
      ).and.callFake(() => {
        return of(studentInfoJson);
      });
      // trigger the action
      actions$ = of(student.fetch);
      // wait for the effect to fire
      effects.fetchStudent$.subscribe(res => {
        // was the studentInfo endpoint called?
        expect(getStudentInfoSpy).toHaveBeenCalledTimes(1);
        // did the subscription deliver the data returned by the mock endpoint?
        expect(res).toEqual(student.found({ info: studentInfoJson }));
        done();
      });
    });
    it('studentInfo should handle api errors', done => {
      // set up the API endpoint to fail
      const getStudentInfoSpy = spyOn(
        apiService,
        'getStudentInfo',
      ).and.callFake(() => {
        return throwError(new Error('Fake error'));
      });

      actions$ = of(student.fetch);
      effects.fetchStudent$.subscribe(res => {
        // did we try to call the endpoint?
        expect(getStudentInfoSpy).toHaveBeenCalledTimes(1);
        // did we get a failure action?
        expect(res).toEqual(student.failed());
        done();
      });
    });
  });
  describe('Effects.cart', () => {
    it('should fire if courses is null', done => {
      // set up the mock endpoints to return the appropriate data
      const getRoadmapCoursesSpy = spyOn(
        apiService,
        'getRoadmapCourses',
      ).and.callFake(() => {
        return of(roadmapJson);
      });
      const getCurrentCoursesSpy = spyOn(
        apiService,
        'getCurrentCourses',
      ).and.callFake(() => {
        return of(enrollmentsJson);
      });
      // trigger the action
      actions$ = of(courses.load({ termCode: TermCode.decodeOrThrow('1222') }));
      // wait for the effect to fire
      effects.both$.subscribe(res => {
        // did the action succeed?
        expect(res.type).toEqual(courses.done.type);

        // did the endpoints get called?
        expect(getRoadmapCoursesSpy).toHaveBeenCalledTimes(1);
        expect(getCurrentCoursesSpy).toHaveBeenCalledTimes(1);

        // were the calls successful?
        expect(res.current.success).toBeTruthy();
        expect(res.roadmap.success).toBeTruthy();

        // is the data we expect to be there actually there?
        expect(res.termCode).toEqual(TermCode.decodeOrThrow('1222'));

        // there was 1 enrolled course and 5 dropped courses in the mock API data
        expect((res.current as any)['value'].length).toEqual(5);

        // there were 5 courses in the cart for 1222 in the mock API data
        expect((res.roadmap as any)['value'].length).toEqual(5);
        done();
      });
    });
    it('should handle current enrollment errors and still load roadmap courses', done => {
      // set up the roadmap mock endpoint to return the appropriate data, and the current courses endpoint to fail
      const getRoadmapCoursesSpy = spyOn(
        apiService,
        'getRoadmapCourses',
      ).and.callFake(() => {
        return of(roadmapJson);
      });
      const getCurrentCoursesSpy = spyOn(
        apiService,
        'getCurrentCourses',
      ).and.callFake(() => {
        return throwError(new Error('Fake error'));
      });

      // trigger the action
      actions$ = of(courses.load({ termCode: TermCode.decodeOrThrow('1222') }));
      // wait for the effect to fire
      effects.both$.subscribe(res => {
        // did the overall action succeed?
        expect(res.type).toEqual(courses.done.type);

        // were the endpoints called?
        expect(getRoadmapCoursesSpy).toHaveBeenCalledTimes(1);
        expect(getCurrentCoursesSpy).toHaveBeenCalledTimes(1);

        // did the current endpoint fail and the roadmap endpoint succeed?
        expect(res.current.success).toBeFalsy();
        expect(res.roadmap.success).toBeTruthy();

        // is the roadmap data that we expect to be there actually there?
        expect(res.termCode).toEqual(TermCode.decodeOrThrow('1222'));
        expect((res.roadmap as any)['value'].length).toEqual(5);
        done();
      });
    });
    it('should handle roadmap errors and still load current courses', done => {
      // set up the current courses mock endpoint to return the appropriate data, and the roadmap endpoint to fail
      const getRoadmapCoursesSpy = spyOn(
        apiService,
        'getRoadmapCourses',
      ).and.callFake(() => {
        return throwError(new Error('Fake error'));
      });
      const getCurrentCoursesSpy = spyOn(
        apiService,
        'getCurrentCourses',
      ).and.callFake(() => {
        return of(enrollmentsJson);
      });

      // trigger the action
      actions$ = of(courses.load({ termCode: TermCode.decodeOrThrow('1222') }));
      // wait for the effect to fire
      effects.both$.subscribe(res => {
        // did the overall action succeed?
        expect(res.type).toEqual(courses.done.type);

        // were the API endpoints actually called?
        expect(getRoadmapCoursesSpy).toHaveBeenCalledTimes(1);
        expect(getCurrentCoursesSpy).toHaveBeenCalledTimes(1);

        // did the roadmap endpoint fail?
        expect(res.current.success).toBeTruthy();
        expect(res.roadmap.success).toBeFalsy();

        // is the data that we expect to be there actually there?
        expect(res.termCode).toEqual(TermCode.decodeOrThrow('1222'));
        expect((res.current as any)['value'].length).toEqual(5);
        done();
      });
    });
    it('should only load when needed', done => {
      // set up a mock selector to return all the required data from the store so that
      // loading from the network is not needed
      store.overrideSelector(selectors.courses.getForTerm, {
        isValidating: false,
        hintToRevalidate: false,
        roadmap: roadmapJson as any,
        current: enrollmentsJson as any,
      });
      // trigger the action
      actions$ = of(
        courses.loadIfNotAlready({ termCode: TermCode.decodeOrThrow('1222') }),
      );

      // if the data needs to be fetched, the actions for each endpoint will be delivered
      // separately, so we need to track them in a list
      let actionsSent: Array<any> = [];
      effects.bothIfNotAlready$.subscribe(
        res => {
          // Track the actions received in a list
          actionsSent.push(res);
        },
        () => {},
        () => {
          // wait until the subscription is done
          // no actions should be sent, since no data needs to be fetched
          expect(actionsSent.length).toEqual(0);

          done();
        },
      );
    });
    it('should only load roadmap when needed', done => {
      // set up a mock selector to return the current enrollments from the store
      // but not the roadmap
      store.overrideSelector(selectors.courses.getForTerm, {
        isValidating: false,
        hintToRevalidate: false,
        roadmap: null,
        current: enrollmentsJson as any,
      });

      // trigger the action
      actions$ = of(
        courses.loadIfNotAlready({ termCode: TermCode.decodeOrThrow('1222') }),
      );
      // if the data needs to be fetched, the actions for each endpoint will be delivered
      // separately, so we need to track them in a list
      let actionsSent: Array<any> = [];
      effects.bothIfNotAlready$.subscribe(
        res => {
          // Track the actions received in a list
          actionsSent.push(res);
        },
        () => {},
        () => {
          // we should have gotten one action to load the roadmap, but none to load current enrollments
          expect(actionsSent.length).toEqual(1);
          expect(
            actionsSent.find(
              action => action.type === courses.roadmap.load.type,
            ),
          ).toBeDefined();
          done();
        },
      );
    });
    it('should only load current when needed', done => {
      // set up a mock selector to return the roadmap data from the store, but not the
      // current enrollments
      store.overrideSelector(selectors.courses.getForTerm, {
        isValidating: false,
        hintToRevalidate: false,
        roadmap: roadmapJson as any,
        current: null,
      });

      // trigger the action
      actions$ = of(
        courses.loadIfNotAlready({ termCode: TermCode.decodeOrThrow('1222') }),
      );
      // if the data needs to be fetched, the actions for each endpoint will be delivered
      // separately, so we need to track them in a list
      let actionsSent: Array<any> = [];
      effects.bothIfNotAlready$.subscribe(
        res => {
          // Track the actions received in a list
          actionsSent.push(res);
        },
        () => {},
        () => {
          // we should have gotten one action back to load the current endpoint, and none to load the roadmap
          expect(actionsSent.length).toEqual(1);
          expect(
            actionsSent.find(
              action => action.type === courses.current.load.type,
            ),
          ).toBeDefined();
          done();
        },
      );
    });
    it('should load both when needed', done => {
      // set up a mock selector to return null for both
      // the roadmap and current enrollments from the store
      store.overrideSelector(selectors.courses.getForTerm, {
        isValidating: false,
        hintToRevalidate: false,
        roadmap: null,
        current: null,
      });
      // trigger the action
      actions$ = of(
        courses.loadIfNotAlready({ termCode: TermCode.decodeOrThrow('1222') }),
      );
      // if the data needs to be fetched, the actions for each endpoint will be delivered
      // separately, so we need to track them in a list
      let actionsSent: Array<any> = [];
      effects.bothIfNotAlready$.subscribe(
        res => {
          // Track the actions received in a list
          actionsSent.push(res);
        },
        () => {},
        () => {
          // we should have gotten actions back to load both the current and roadmap data
          // from the API endpoints
          expect(actionsSent.length).toEqual(2);
          expect(
            actionsSent.find(
              action => action.type === courses.current.load.type,
            ),
          ).toBeDefined();
          expect(
            actionsSent.find(
              action => action.type === courses.roadmap.load.type,
            ),
          ).toBeDefined();
          done();
        },
      );
    });
    it('should load roadmap', done => {
      // set up the mock endpoint to return the appropriate data
      const getRoadmapCoursesSpy = spyOn(
        apiService,
        'getRoadmapCourses',
      ).and.callFake(() => {
        return of(roadmapJson);
      });
      // set up a mock selector to return the reference data for the selected term.
      // The subject list below matches the subjects found in the roadmap courses,
      // since the roadmap action will fail if the subject for a course is not present
      store.overrideSelector(
        selectors.terms.getNonZeroTerm,
        success({
          termCode: TermCode.decodeOrThrow('1222'),
          subjects: [
            { code: '132', name: '', description: '' },
            { code: '448', name: '', description: '' },
            { code: '600', name: '', description: '' },
            { code: '242', name: '', description: '' },
            { code: '130', name: '', description: '' },
            { code: '224', name: '', description: '' },
          ],
          sessions: [],
          specialGroups: [],
        }),
      );
      // trigger the action
      actions$ = of(
        actions.courses.roadmap.load({
          termCode: TermCode.decodeOrThrow('1222'),
        }),
      );
      // wait for the effect to fire
      effects.roadmap$.subscribe(res => {
        // did we get the roadmap load completed action?
        expect(res.type).toEqual(courses.roadmap.done.type);
        // did the call to the API endpoint succeed?
        expect(res.roadmap.success).toEqual(true);
        // did we call the endpoint?
        expect(getRoadmapCoursesSpy).toHaveBeenCalledTimes(1);
        done();
      });
    });
  });
  describe('Effects.packages', () => {
    it('should load packages', done => {
      // set up the mock endpoint to return the appropriate data
      const getEnrollmentPackagesSpy = spyOn(
        apiService,
        'getEnrollmentPackages',
      ).and.callFake(() => {
        return of(packagesJson);
      });
      // dig the session list for 1222 out of the aggregate endpoint data
      // and set up a mock selector to deliver it
      let termData = aggregateJson.sessions.find(
        term => term.termCode === '1222',
      );
      // need to convert the JSON structure from the endpoint data to a list of Session instances
      let sessions: Session[] =
        termData?.sessions.map((rawSession: any) => {
          return Session.from(rawSession);
        }) || ([] as any);
      // set up a mock selector to return the session data we just built
      store.overrideSelector(
        selectors.terms.getSessionsForTerm,
        success(sessions),
      );

      // mock data for the action properties
      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        subject: {} as any,
        courseId: '000656',
        kind: 'roadmap',
        package: null,
      };
      // trigger the action
      actions$ = of(courses.roadmap.packs.load({ course: course }));
      // wait for the effect to fire
      effects.packs$.subscribe(
        res => {
          // did we get the load completed action?
          expect(res.type).toEqual(courses.roadmap.packs.done.type);
          // did the action succeed?
          expect(res.packs.success).toEqual(true);
        },
        () => {},
        () => {
          // was the endpoint called?
          expect(getEnrollmentPackagesSpy).toHaveBeenCalledTimes(1);
          done();
        },
      );
    });
  });
  describe('Effects.addToCart', () => {
    it('should add a course without a package to the cart', done => {
      // set up the mock endpoint
      const postRoadmapCourseWithoutPackageSpy = spyOn(
        apiService,
        'postRoadmapCourseWithoutPackage',
      ).and.callFake(() => {
        return of({});
      });
      // set up the mock pending endpoint to return Done
      const getCartStatusSpy = spyOn(apiService, 'getCartStatus').and.callFake(
        () => {
          return of({ status: 'Done' });
        },
      );
      const openSnackBarSpy = spyOn(snackBar, 'open').and.callThrough();

      // mock data for the action properties
      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: null,
      };
      // trigger the action
      actions$ = of(
        actions.courses.roadmap.addCourseWithoutPack({
          course: course,
          showToast: true,
        }),
      );
      // wait for the effec to fire
      effects.addCourseWithoutPack$.subscribe(res => {
        // did we get the roadmap load action?
        expect(res.type).toEqual(courses.roadmap.load.type);
        // did we get data back that makes sense?
        expect(res.termCode).toEqual(TermCode.decodeOrThrow('1222'));
        // did we call the endpoints with the correct parameters?
        expect(postRoadmapCourseWithoutPackageSpy).toHaveBeenCalledTimes(1);
        expect(
          postRoadmapCourseWithoutPackageSpy.calls.first().args[0],
        ).toEqual(course);

        expect(getCartStatusSpy).toHaveBeenCalledTimes(1);
        // did we open the snack bar with the correct parameters?
        expect(openSnackBarSpy).toHaveBeenCalledTimes(1);
        expect(openSnackBarSpy.calls.first().args[0]).toBeTruthy();
        expect(openSnackBarSpy.calls.first().args[1]).toEqual('View cart');
        done();
      });
    });
    it('should add a course with a package to the cart', done => {
      // set up the mock endpoint
      const postRoadmapCourseWithPackageSpy = spyOn(
        apiService,
        'postRoadmapCourseWithPackage',
      ).and.callFake(() => {
        return of({});
      });
      // set up the mock pending endpoint to return Done
      const getCartStatusSpy = spyOn(apiService, 'getCartStatus').and.callFake(
        () => {
          return of({ status: 'Done' });
        },
      );
      const openSnackBarSpy = spyOn(snackBar, 'open').and.callThrough();

      // mock data for the action properties
      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: null,
      };
      let pack: any = {
        classNumber: 12345,
        relatedClassNumbers: [],
      };
      // trigger the action
      actions$ = of(
        actions.courses.roadmap.addCourseWithPack({
          course: course,
          pack: pack,
          credits: 3,
          withHonors: false,
          classPermissionNumber: null,
          joinWaitlist: false,
          showToast: true,
        }),
      );
      // wait for the effect to fire
      effects.addCourseWithPack$.subscribe(res => {
        // did we get the roadmap load action?
        expect(res.type).toEqual(courses.roadmap.load.type);
        // did we get a term code?
        expect(res.termCode).toEqual(TermCode.decodeOrThrow('1222'));
        // did we call the endpoints?
        expect(postRoadmapCourseWithPackageSpy).toHaveBeenCalledTimes(1);
        expect(getCartStatusSpy).toHaveBeenCalledTimes(1);
        // did the action properties get passed through to the API service correctly?
        expect(postRoadmapCourseWithPackageSpy.calls.first().args[0]).toEqual(
          course,
        );
        expect(postRoadmapCourseWithPackageSpy.calls.first().args[1]).toEqual(
          pack,
        );
        expect(postRoadmapCourseWithPackageSpy.calls.first().args[2]).toEqual(
          3,
        ); // credits
        expect(postRoadmapCourseWithPackageSpy.calls.first().args[3]).toEqual(
          false,
        ); // joinWaitlist
        expect(postRoadmapCourseWithPackageSpy.calls.first().args[4]).toEqual(
          false,
        ); // withHonors
        expect(postRoadmapCourseWithPackageSpy.calls.first().args[5]).toEqual(
          null,
        ); // classPermissionNumber
        // did we open the snack bar with the correct arguments?
        expect(openSnackBarSpy).toHaveBeenCalledTimes(1);
        expect(openSnackBarSpy.calls.first().args[0]).toBeTruthy();
        expect(openSnackBarSpy.calls.first().args[1]).toEqual('View cart');

        done();
      });
    });
    it('should bulk add courses to the cart', done => {
      // set up the mock endpoint
      const postRoadmapCourseWithPackageSpy = spyOn(
        apiService,
        'postRoadmapCourseWithPackage',
      ).and.callFake(() => {
        return of({});
      });
      // set up the mock pending endpoint to return Done
      const getCartStatusSpy = spyOn(apiService, 'getCartStatus').and.callFake(
        () => {
          return of({ status: 'Done' });
        },
      );
      // mock data for the action properties
      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: null,
      };
      let course2: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 101',
        subject: { code: '132' } as any,
        courseId: '000657',
        kind: 'roadmap',
        package: null,
      };
      let pack: any = {
        classNumber: 12345,
        relatedClassNumbers: [],
      };
      let pack2: any = {
        classNumber: 22345,
        relatedClassNumbers: [],
      };
      // trigger the action
      actions$ = of(
        actions.courses.roadmap.addCourseWithPackBulk({
          courses: [
            {
              course: course,
              pack: pack,
              credits: 3,
              withHonors: true,
              classPermissionNumber: null,
              joinWaitlist: true,
            },
            {
              course: course2,
              pack: pack2,
              credits: 2,
              withHonors: false,
              classPermissionNumber: null,
              joinWaitlist: false,
            },
          ],
        }),
      );
      // wait for the effect to fire
      effects.addCourseWithPackBulk$.subscribe(res => {
        // did we get the roadmap load action?
        expect(res.type).toEqual(courses.roadmap.load.type);
        // did we get a term code?
        expect(res.termCode).toEqual(TermCode.decodeOrThrow('1222'));
        // did we call the post endpoint once per course?
        expect(postRoadmapCourseWithPackageSpy).toHaveBeenCalledTimes(2);
        expect(getCartStatusSpy).toHaveBeenCalledTimes(1);
        // did the action properties get passed through to the API service correctly?
        // Course 1
        expect(postRoadmapCourseWithPackageSpy.calls.first().args[0]).toEqual(
          course,
        );
        expect(postRoadmapCourseWithPackageSpy.calls.first().args[1]).toEqual(
          pack,
        );
        expect(postRoadmapCourseWithPackageSpy.calls.first().args[2]).toEqual(
          3,
        ); // credits
        expect(postRoadmapCourseWithPackageSpy.calls.first().args[3]).toEqual(
          true,
        ); // joinWaitlist
        expect(postRoadmapCourseWithPackageSpy.calls.first().args[4]).toEqual(
          true,
        ); // withHonors
        expect(postRoadmapCourseWithPackageSpy.calls.first().args[5]).toEqual(
          null,
        ); // classPermissionNumber
        // Course 2
        expect(postRoadmapCourseWithPackageSpy.calls.argsFor(1)[0]).toEqual(
          course2,
        );
        expect(postRoadmapCourseWithPackageSpy.calls.argsFor(1)[1]).toEqual(
          pack2,
        );
        expect(postRoadmapCourseWithPackageSpy.calls.argsFor(1)[2]).toEqual(2); // credits
        expect(postRoadmapCourseWithPackageSpy.calls.argsFor(1)[3]).toEqual(
          false,
        ); // joinWaitlist
        expect(postRoadmapCourseWithPackageSpy.calls.argsFor(1)[4]).toEqual(
          false,
        ); // withHonors
        expect(postRoadmapCourseWithPackageSpy.calls.argsFor(1)[5]).toEqual(
          null,
        ); // classPermissionNumber
        done();
      });
    });
  });
  describe('Effects.remove', () => {
    it('remove a locked package from a course in the cart', done => {
      // set up the mock endpoint
      const postClearRoadmapPackageOptionsSpy = spyOn(
        apiService,
        'postClearRoadmapPackageOptions',
      ).and.callFake(() => {
        return of({});
      });
      const getCartStatusSpy = spyOn(apiService, 'getCartStatus').and.callFake(
        () => {
          return of({ status: 'Done' });
        },
      );
      const openSnackBarSpy = spyOn(snackBar, 'open').and.callThrough();

      // mock data for the action properties
      let pack: any = {
        classNumber: 12345,
        relatedClassNumbers: [],
      };
      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: pack,
      };
      actions$ = of(
        actions.courses.roadmap.removePack({ course: course, showToast: true }),
      );
      effects.removePack$.subscribe(res => {
        expect(res).toBeTruthy();
        expect(postClearRoadmapPackageOptionsSpy).toHaveBeenCalledTimes(1);
        expect(getCartStatusSpy).toHaveBeenCalledTimes(1);
        // did we open the snack bar with the correct arguments?
        expect(openSnackBarSpy).toHaveBeenCalledTimes(1);
        expect(openSnackBarSpy.calls.first().args[0]).toBeTruthy();
        expect(openSnackBarSpy.calls.first().args[1]).toEqual('View cart');
        done();
      });
    });
    it('remove a list of courses from the cart successfully', done => {
      // set up the mock endpoint
      const deleteRoadmapCoursesSpy = spyOn(
        apiService,
        'deleteRoadmapCourses',
      ).and.callFake(() => {
        return of({});
      });
      const openSnackBarSpy = spyOn(snackBar, 'open').and.callThrough();
      // set up the mock pending endpoint to return Done
      const getCartStatusSpy = spyOn(apiService, 'getCartStatus').and.callFake(
        () => {
          return of({ status: 'Done' });
        },
      );
      // mock data for the action properties
      let courses: any[] = [
        {
          termCode: TermCode.decodeOrThrow('1222'),
          shortCatalog: 'ALS 100',
          subject: { code: '132' } as any,
          courseId: '000656',
          kind: 'roadmap',
          package: {
            classNumber: 12345,
            relatedClassNumbers: [],
          },
        },
        {
          termCode: TermCode.decodeOrThrow('1222'),
          shortCatalog: 'ALS 101',
          subject: { code: '132' } as any,
          courseId: '000657',
          kind: 'roadmap',
          package: null,
        },
      ];
      actions$ = of(
        actions.courses.roadmap.removeCoursesAndPacks({
          courses: courses,
          showToast: true,
        }),
      );
      effects.removeCoursesAndPacks$.subscribe(
        res => {
          expect(res).toBeTruthy();
          expect(res.type).toEqual(actions.courses.roadmap.load.type);
        },
        // have to catch this one in the error handler
        // nothing currently catches this error, so it fails silently in the browser
        err => {
          fail(`Uncaught error: ${err}`);
        },
        () => {
          expect(deleteRoadmapCoursesSpy).toHaveBeenCalledTimes(2);

          expect(getCartStatusSpy).toHaveBeenCalledTimes(1);
          // did we open the snack bar with the correct arguments?
          expect(openSnackBarSpy).toHaveBeenCalledTimes(1);
          expect(openSnackBarSpy.calls.first().args[0]).toEqual(
            `Removed 2 courses from Fall 2021`,
          );
          expect(openSnackBarSpy.calls.first().args[1]).toEqual('View cart');
          done();
        },
      );
    });
    it('remove a list of courses from the cart with some failures', done => {
      // set up the mock endpoint
      const deleteRoadmapCoursesSpy = spyOn(
        apiService,
        'deleteRoadmapCourses',
      ).and.callFake(course => {
        if (course.courseId == '000657') {
          return throwError('Could not remove course');
        }
        return of({});
      });

      // mock data for the action properties
      let courses: any[] = [
        {
          termCode: TermCode.decodeOrThrow('1222'),
          shortCatalog: 'ALS 100',
          subject: { code: '132' } as any,
          courseId: '000656',
          kind: 'roadmap',
          package: {
            classNumber: 12345,
            relatedClassNumbers: [],
          },
        },
        {
          termCode: TermCode.decodeOrThrow('1222'),
          shortCatalog: 'ALS 101',
          subject: { code: '132' } as any,
          courseId: '000657',
          kind: 'roadmap',
          package: null,
        },
      ];
      actions$ = of(
        actions.courses.roadmap.removeCoursesAndPacks({
          courses: courses,
          showToast: true,
        }),
      );
      effects.removeCoursesAndPacks$.subscribe(
        res => {
          expect(res).toBeTruthy();
          expect(res.type).toEqual(actions.courses.roadmap.load.type);
        },
        // have to catch this one in the error handler
        // nothing currently catches this error, so it fails silently in the browser
        err => fail(`uncaught error ${err}`),
        () => {
          expect(deleteRoadmapCoursesSpy).toHaveBeenCalledTimes(2);
          // All of these conditions currently fail, though it doesn't seem like they should fail.
          // If the API endpoint that calls deleteRoadmapCourses gets an error, the effect seems to just stop
          // without any notice to the user.
          // expect(getCartStatusSpy).toHaveBeenCalledTimes(1);
          // // did we open the snack bar with the correct arguments?
          // expect(openSnackBarSpy).toHaveBeenCalledTimes(1);
          // expect(openSnackBarSpy.calls.first().args[0]).toEqual(`Removed 1 course from Fall 2021`)
          // expect(openSnackBarSpy.calls.first().args[1]).toEqual('View cart');
          done();
        },
      );
    });
  });
  describe('Effects.validate', () => {
    it('validate a course in the cart', done => {
      // set up the mock endpoint
      const postValidationSpy = spyOn(
        apiService,
        'postValidation',
      ).and.callFake(() => {
        return of({});
      });
      // set up the mock pending endpoint to return Done
      const getCartStatusSpy = spyOn(apiService, 'getCartStatus').and.callFake(
        () => {
          return of({ status: 'Done' });
        },
      );

      // mock data for the action properties
      let pack: any = {
        classNumber: 12345,
        relatedClassNumbers: [],
        choices: {
          credits: {
            total: 3,
          },
          honors: 'chosen',
          waitlist: 'chosen',
        },
      };
      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: pack,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: 12334,
      };
      actions$ = of(
        actions.courses.roadmap.validate({
          termCode: TermCode.decodeOrThrow('1222'),
          courses: [course],
        }),
      );
      effects.validate$.subscribe(res => {
        expect(res).toBeTruthy();
        expect(res.type).toEqual(actions.courses.roadmap.load.type);
        expect(postValidationSpy).toHaveBeenCalledTimes(1);
        expect(getCartStatusSpy).toHaveBeenCalledTimes(1);
        expect(postValidationSpy.calls.first().args[0]).toEqual(
          TermCode.decodeOrThrow('1222'),
        );
        expect(postValidationSpy.calls.first().args[1][0]).toEqual({
          courseId: '000656',
          subjectCode: '132',
          classNumber: 12345,
          options: {
            credits: 3,
            honors: true,
            waitlist: true,
            passFail: false,
            classPermissionNumber: 12334,
            relatedClassNumber1: null,
            relatedClassNumber2: null,
          },
        });
        done();
      });
    });
  });
  describe('Effects.current', () => {
    it('should fire if courses is null', done => {
      // set up the mock endpoints to return the appropriate data
      const getCurrentCoursesSpy = spyOn(
        apiService,
        'getCurrentCourses',
      ).and.callFake(() => {
        return of(enrollmentsJson);
      });
      // trigger the action
      actions$ = of(
        actions.courses.current.load({
          termCode: TermCode.decodeOrThrow('1222'),
        }),
      );
      // wait for the effect to fire
      effects.current$.subscribe(res => {
        // did the action succeed?
        expect(res.type).toEqual(courses.current.done.type);

        // did the endpoints get called?
        expect(getCurrentCoursesSpy).toHaveBeenCalledTimes(1);

        // were the calls successful?
        expect(res.current.success).toBeTruthy();

        // is the data we expect to be there actually there?
        expect(res.termCode).toEqual(TermCode.decodeOrThrow('1222'));

        // there was 1 enrolled course and 5 dropped courses in the mock API data
        expect((res.current as any)['value'].length).toEqual(5);
        done();
      });
    });
    it('should handle current enrollment errors', done => {
      // set up the current courses endpoint to fail

      const getCurrentCoursesSpy = spyOn(
        apiService,
        'getCurrentCourses',
      ).and.callFake(() => {
        return throwError(new Error('Fake error'));
      });

      // trigger the action
      actions$ = of(
        actions.courses.current.load({
          termCode: TermCode.decodeOrThrow('1222'),
        }),
      );
      // wait for the effect to fire
      effects.current$.subscribe(res => {
        // did the overall action succeed?
        expect(res.type).toEqual(courses.current.done.type);

        // were the endpoints called?
        expect(getCurrentCoursesSpy).toHaveBeenCalledTimes(1);

        // did the current endpoint fail?
        expect(res.current.success).toBeFalsy();

        // is the roadmap data that we expect to be there actually there?
        expect(res.termCode).toEqual(TermCode.decodeOrThrow('1222'));
        done();
      });
    });
  });
  describe('Effects.package', () => {
    it('should load a specific enrollment package', done => {
      // set up the mock endpoint to return the appropriate data
      const getEnrollmentPackageByDocIdSpy = spyOn(
        apiService,
        'getEnrollmentPackageByDocId',
      ).and.callFake(() => {
        return of(packageByIdJson);
      });
      // dig the session list for 1222 out of the aggregate endpoint data
      // and set up a mock selector to deliver it
      let termData = aggregateJson.sessions.find(
        term => term.termCode === '1222',
      );
      // need to convert the JSON structure from the endpoint data to a list of Session instances
      let sessions: Session[] =
        termData?.sessions.map((rawSession: any) => {
          return Session.from(rawSession);
        }) || ([] as any);
      // set up a mock selector to return the session data we just built
      store.overrideSelector(
        selectors.terms.getSessionsForTerm,
        success(sessions),
      );

      // mock data for the action properties
      // mock data for the action properties
      let pack: any = {
        classNumber: 12345,
        relatedClassNumbers: [],
        choices: {
          credits: {
            total: 3,
          },
          honors: 'chosen',
          waitlist: 'chosen',
        },
        docId: '1222-A1-224-345-001-401',
      };
      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: pack,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: 12334,
      };
      // trigger the action
      actions$ = of(actions.courses.current.pack.load({ course }));
      // wait for the effect to fire
      effects.pack$.subscribe(
        res => {
          // did we get the load completed action?
          expect(res.type).toEqual(courses.current.pack.done.type);
          // did the action succeed?
          expect(res.pack.success).toEqual(true);
        },
        () => {},
        () => {
          // was the endpoint called?
          expect(getEnrollmentPackageByDocIdSpy).toHaveBeenCalledTimes(1);
          done();
        },
      );
    });
    it('should handle errors loading a specific enrollment package', done => {
      // set up the mock endpoint to return the appropriate data
      const getEnrollmentPackageByDocIdSpy = spyOn(
        apiService,
        'getEnrollmentPackageByDocId',
      ).and.callFake(() => {
        return throwError('Fake Error');
      });

      // dig the session list for 1222 out of the aggregate endpoint data
      // and set up a mock selector to deliver it
      let termData = aggregateJson.sessions.find(
        term => term.termCode === '1222',
      );
      // need to convert the JSON structure from the endpoint data to a list of Session instances
      let sessions: Session[] =
        termData?.sessions.map((rawSession: any) => {
          return Session.from(rawSession);
        }) || ([] as any);
      // set up a mock selector to return the session data we just built
      store.overrideSelector(
        selectors.terms.getSessionsForTerm,
        success(sessions),
      );

      // mock data for the action properties
      // mock data for the action properties
      let pack: any = {
        classNumber: 12345,
        relatedClassNumbers: [],
        choices: {
          credits: {
            total: 3,
          },
          honors: 'chosen',
          waitlist: 'chosen',
        },
        docId: '1222-A1-224-345-001-401',
      };
      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: pack,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: 12334,
      };
      // trigger the action
      actions$ = of(actions.courses.current.pack.load({ course }));
      // wait for the effect to fire
      effects.pack$.subscribe(
        res => {
          // did we get the load completed action?
          expect(res.type).toEqual(courses.current.pack.done.type);
          // did the action succeed? (should be false here)
          expect(res.pack.success).toEqual(false);
        },
        () => {},
        () => {
          // was the endpoint called?
          expect(getEnrollmentPackageByDocIdSpy).toHaveBeenCalledTimes(1);
          done();
        },
      );
    });
  });
  describe('Effects.enroll', () => {
    it('post an enrollment request', done => {
      // set up the mock endpoint
      const postEnrollSpy = spyOn(apiService, 'postEnroll').and.callFake(() => {
        return of({});
      });
      // set up the mock pending endpoint to return Done
      const getCartStatusSpy = spyOn(apiService, 'getCartStatus').and.callFake(
        () => {
          return of({ status: 'Done' });
        },
      );
      // mock getLastEnrollment
      const getLastEnrollmentSpy = spyOn(
        apiService,
        'getLastEnrollment',
      ).and.callFake(() => {
        return of({});
      });
      const keepOpenSpy = spyOn(dialog, 'keepOpen').and.callThrough();
      // mock data for the action properties
      let pack: any = {
        classNumber: 12345,
        relatedClassNumbers: [],
        choices: {
          credits: {
            total: 3,
          },
          honors: 'chosen',
          waitlist: 'chosen',
        },
      };
      let pack2: any = {
        classNumber: 52345,
        relatedClassNumbers: [],
        choices: {
          credits: {
            total: 3,
          },
          honors: 'chosen',
          waitlist: 'chosen',
        },
      };
      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: pack,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: 12334,
      };
      let course2: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 110',
        subject: { code: '132' } as any,
        courseId: '000657',
        kind: 'roadmap',
        package: pack2,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: null,
      };
      let enrollRequest = {
        subjectCode: '132',
        courseId: '000656',
        classNumber: '12334',
        options: {
          credits: 3,
          waitlist: null,
          honors: null,
          relatedClassNumber1: null,
          relatedClassNumber2: null,
          classPermissionNumber: 12334,
        },
      };
      let enrollRequest2 = {
        subjectCode: '132',
        courseId: '000657',
        classNumber: '52345',
        options: {
          credits: 2,
          waitlist: null,
          honors: null,
          relatedClassNumber1: '54321',
          relatedClassNumber2: null,
          classPermissionNumber: null,
        },
      };
      actions$ = of(
        actions.courses.current.enroll({
          termCode: TermCode.decodeOrThrow('1222'),
          requests: [
            [course, enrollRequest],
            [course2, enrollRequest2],
          ],
        }),
      );
      effects.enroll$.subscribe(
        res => {
          expect(res).toBeTruthy();
          expect(res.type).toEqual(actions.courses.load.type);
          expect(postEnrollSpy).toHaveBeenCalledTimes(1);
          expect(getCartStatusSpy).toHaveBeenCalledTimes(1);
          expect(getLastEnrollmentSpy).toHaveBeenCalledTimes(1);
          expect(postEnrollSpy.calls.first().args[0]).toEqual(
            TermCode.decodeOrThrow('1222'),
          );
          expect(postEnrollSpy.calls.first().args[1]).toEqual([
            {
              courseId: '000656',
              subjectCode: '132',
              classNumber: '12334',
              options: {
                credits: 3,
                honors: null,
                waitlist: null,
                classPermissionNumber: 12334,
                relatedClassNumber1: null,
                relatedClassNumber2: null,
              },
            },
            {
              subjectCode: '132',
              courseId: '000657',
              classNumber: '52345',
              options: {
                credits: 2,
                waitlist: null,
                honors: null,
                relatedClassNumber1: '54321',
                relatedClassNumber2: null,
                classPermissionNumber: null,
              },
            },
          ]);

          expect(keepOpenSpy).toHaveBeenCalledTimes(1);
          done();
        },
        err => fail(`uncaught error ${err}`),
      );
    });
  });
  describe('Effects.drop', () => {
    it('request to drop an enrollment successful', done => {
      // set up the mock endpoint
      const dropEnrollSpy = spyOn(apiService, 'postDrop').and.callFake(() => {
        return of([
          {
            messageSeverity: 'I',
            instanceId: 12345,
            description: 'description message',
          },
          {
            messageSeverity: 'I',
            instanceId: 52345,
            description: 'description message',
          },
        ]);
      });
      const openSnackBarSpy = spyOn(snackBar, 'open').and.callThrough();
      const dialogSmallSpy = spyOn(dialog, 'small').and.callThrough();

      // mock data for the action properties
      let pack: any = {
        classNumber: 12345,
        relatedClassNumbers: [],
        choices: {
          credits: {
            total: 3,
          },
          honors: 'chosen',
          waitlist: 'chosen',
        },
      };
      let pack2: any = {
        classNumber: 52345,
        relatedClassNumbers: [],
        choices: {
          credits: {
            total: 3,
          },
          honors: 'chosen',
          waitlist: 'chosen',
        },
      };
      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: pack,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: 12334,
      };
      let course2: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 110',
        subject: { code: '132' } as any,
        courseId: '000657',
        kind: 'roadmap',
        package: pack2,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: null,
      };
      actions$ = of(
        actions.courses.current.drop({
          termCode: TermCode.decodeOrThrow('1222'),
          courses: [course as any, course2 as any],
        }),
      );
      effects.drop$.subscribe(
        res => {
          expect(res).toBeTruthy();
          expect(res.type).toEqual(actions.courses.current.load.type);
          expect(dropEnrollSpy).toHaveBeenCalledTimes(1);
          expect(dropEnrollSpy.calls.first().args[0]).toEqual(
            TermCode.decodeOrThrow('1222'),
          );
          expect(dropEnrollSpy.calls.first().args[1]).toEqual([
            course.package,
            course2.package,
          ]);
          expect(openSnackBarSpy).toHaveBeenCalledTimes(1);
          expect(dialogSmallSpy).toHaveBeenCalledTimes(0);

          done();
        },
        err => fail(`uncaught error ${err}`),
      );
    });
    it('request to drop an enrollment with one failure', done => {
      // set up the mock endpoint
      const dropEnrollSpy = spyOn(apiService, 'postDrop').and.callFake(() => {
        return of([
          {
            messageSeverity: 'I',
            instanceId: '12345',
            description: 'description message',
          },
          {
            messageSeverity: 'E',
            instanceId: '52345',
            description: 'description message',
          },
        ]);
      });
      const openSnackBarSpy = spyOn(snackBar, 'open').and.callThrough();
      const dialogSmallSpy = spyOn(dialog, 'small').and.callThrough();

      // mock data for the action properties
      let pack: any = {
        classNumber: 12345,
        relatedClassNumbers: [],
        choices: {
          credits: {
            total: 3,
          },
          honors: 'chosen',
          waitlist: 'chosen',
        },
      };
      let pack2: any = {
        classNumber: 52345,
        relatedClassNumbers: [],
        choices: {
          credits: {
            total: 3,
          },
          honors: 'chosen',
          waitlist: 'chosen',
        },
      };
      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: pack,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: 12334,
      };
      let course2: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 110',
        subject: { code: '132' } as any,
        courseId: '000657',
        kind: 'roadmap',
        package: pack2,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: null,
      };
      actions$ = of(
        actions.courses.current.drop({
          termCode: TermCode.decodeOrThrow('1222'),
          courses: [course as any, course2 as any],
        }),
      );
      effects.drop$.subscribe(
        res => {
          expect(res).toBeTruthy();
          expect(res.type).toEqual(actions.courses.current.load.type);
          expect(dropEnrollSpy).toHaveBeenCalledTimes(1);
          expect(dropEnrollSpy.calls.first().args[0]).toEqual(
            TermCode.decodeOrThrow('1222'),
          );
          expect(dropEnrollSpy.calls.first().args[1]).toEqual([
            course.package,
            course2.package,
          ]);
          expect(openSnackBarSpy).toHaveBeenCalledTimes(0);
          expect(dialogSmallSpy).toHaveBeenCalledTimes(1);

          done();
        },
        err => fail(`uncaught error ${err}`),
      );
    });
  });
  describe('Effects.edit', () => {
    it('request to edit an enrollment successful', done => {
      // set up the mock endpoint
      const putEnrollSpy = spyOn(apiService, 'putEnroll').and.callFake(() => {
        return of([]);
      });
      const openSnackBarSpy = spyOn(snackBar, 'open').and.callThrough();
      const dialogSmallSpy = spyOn(dialog, 'small').and.callThrough();

      // mock data for the action properties
      let pack: any = {
        classNumber: 12345,
        relatedClassNumber1: 54321,
        relatedClassNumber2: 54322,
        choices: {
          credits: {
            total: 3,
          },
          honors: 'chosen',
          waitlist: 'chosen',
        },
      };
      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: pack,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: 12334,
        enrolledStatus: 'enrolled',
      };

      actions$ = of(
        actions.courses.current.edit({
          course: course,
          credits: 3,
          honors: false,
        }),
      );
      effects.edit$.subscribe(
        res => {
          expect(res).toBeTruthy();
          expect(res.type).toEqual(actions.courses.current.load.type);
          expect(putEnrollSpy).toHaveBeenCalledTimes(1);
          expect(putEnrollSpy.calls.first().args[0]).toEqual(
            TermCode.decodeOrThrow('1222'),
          );
          expect(putEnrollSpy.calls.first().args[1]).toEqual({
            subjectCode: '132',
            courseId: '000656',
            classNumber: '12345',
            options: {
              credits: 3,
              waitlist: null,
              honors: null,
              relatedClassNumber1: '54321',
              relatedClassNumber2: '54322',
              classPermissionNumber: 12334,
            },
          });
          expect(openSnackBarSpy).toHaveBeenCalledTimes(0);
          expect(dialogSmallSpy).toHaveBeenCalledTimes(0);

          done();
        },
        err => fail(`uncaught error ${err}`),
      );
    });
    it('request to edit a waitlisted enrollment successful', done => {
      // set up the mock endpoint
      const putEnrollSpy = spyOn(apiService, 'putEnroll').and.callFake(() => {
        return of([]);
      });
      const openSnackBarSpy = spyOn(snackBar, 'open').and.callThrough();
      const dialogSmallSpy = spyOn(dialog, 'small').and.callThrough();

      // mock data for the action properties
      let pack: any = {
        classNumber: 12345,
        relatedClassNumber1: 54321,
        relatedClassNumber2: 54322,
        choices: {
          credits: {
            total: 3,
          },
          honors: null,
          waitlist: 'chosen',
        },
      };
      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: pack,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: 12334,
        enrolledStatus: 'waitlisted',
      };

      actions$ = of(
        actions.courses.current.edit({
          course: course,
          credits: 3,
          honors: false,
        }),
      );
      effects.edit$.subscribe(
        res => {
          expect(res).toBeTruthy();
          expect(res.type).toEqual(actions.courses.current.load.type);
          expect(putEnrollSpy).toHaveBeenCalledTimes(1);
          expect(putEnrollSpy.calls.first().args[0]).toEqual(
            TermCode.decodeOrThrow('1222'),
          );
          expect(putEnrollSpy.calls.first().args[1]).toEqual({
            subjectCode: '132',
            courseId: '000656',
            classNumber: '12345',
            options: {
              credits: 3,
              waitlist: true,
              honors: null,
              relatedClassNumber1: '54321',
              relatedClassNumber2: '54322',
              classPermissionNumber: 12334,
            },
          });
          expect(openSnackBarSpy).toHaveBeenCalledTimes(0);
          expect(dialogSmallSpy).toHaveBeenCalledTimes(0);

          done();
        },
        err => fail(`uncaught error ${err}`),
      );
    });
    it('request to edit an enrollment failed', done => {
      // set up the mock endpoint
      const putEnrollSpy = spyOn(apiService, 'putEnroll').and.callFake(() => {
        return throwError('Backend error');
      });

      // mock data for the action properties
      let pack: any = {
        classNumber: 12345,
        relatedClassNumber1: 54321,
        relatedClassNumber2: 54322,
        choices: {
          credits: {
            total: 3,
          },
          honors: 'chosen',
          waitlist: 'chosen',
        },
      };
      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: pack,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: 12334,
        enrolledStatus: 'enrolled',
      };

      actions$ = of(
        actions.courses.current.edit({
          course: course,
          credits: 3,
          honors: false,
        }),
      );
      effects.edit$.subscribe(
        _res => {
          //we should never get here, so fail if we do
          expect(false).toBeTruthy();
        },
        () => {
          // have to catch this one in the error handler
          // Nothing currently catches this error, so it fails silently in the browser
          expect(putEnrollSpy).toHaveBeenCalledTimes(1);
          expect(putEnrollSpy.calls.first().args[0]).toEqual(
            TermCode.decodeOrThrow('1222'),
          );
          expect(putEnrollSpy.calls.first().args[1]).toEqual({
            subjectCode: '132',
            courseId: '000656',
            classNumber: '12345',
            options: {
              credits: 3,
              waitlist: null,
              honors: null,
              relatedClassNumber1: '54321',
              relatedClassNumber2: '54322',
              classPermissionNumber: 12334,
            },
          });
          done();
        },
      );
    });
  });
  describe('Effects.details', () => {
    it('should load course details for a specified course', done => {
      // set up the mock endpoint to return the appropriate data
      const getDetailsSpy = spyOn(apiService, 'getDetails').and.callFake(() => {
        return of(courseDetailsJson);
      });

      // mock data for the action properties

      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: null,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: 12334,
        ref: {
          termCode: TermCode.decodeOrThrow('1222'),
        },
      };
      // trigger the action
      actions$ = of(actions.details.load({ course }));
      // wait for the effect to fire
      effects.details$.subscribe(
        res => {
          // did we get the load completed action?
          expect(res.type).toEqual(actions.details.done.type);
          // did the action succeed?
          // was the endpoint called?
          expect(getDetailsSpy).toHaveBeenCalledTimes(1);
          expect(getDetailsSpy.calls.first().args[0]).toEqual(
            TermCode.decodeOrThrow('1222'),
          );
          expect(getDetailsSpy.calls.first().args[1]).toEqual(
            course.subject.code,
          );
          expect(getDetailsSpy.calls.first().args[2]).toEqual(course.courseId);
          done();
        },
        err => fail(`uncaught error ${err}`),
      );
    });
    it('should load course details for a specified course with error', done => {
      // set up the mock endpoint to return the appropriate data
      const getDetailsSpy = spyOn(apiService, 'getDetails').and.callFake(() => {
        return throwError('Backend error');
      });

      // mock data for the action properties

      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: null,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: 12334,
        ref: {
          termCode: TermCode.decodeOrThrow('1222'),
        },
      };
      // trigger the action
      actions$ = of(actions.details.load({ course }));
      // wait for the effect to fire
      effects.details$.subscribe(
        _res => {
          //we should never get here, so fail if we do
          expect(false).toBeTruthy();
        },
        () => {
          // have to catch this one in the error handler
          // nothing currently catches this error, so it fails silently in the browser
          expect(getDetailsSpy).toHaveBeenCalledTimes(1);
          expect(getDetailsSpy.calls.first().args[0]).toEqual(
            TermCode.decodeOrThrow('1222'),
          );
          expect(getDetailsSpy.calls.first().args[1]).toEqual(
            course.subject.code,
          );
          expect(getDetailsSpy.calls.first().args[2]).toEqual(course.courseId);
          done();
        },
      );
    });
  });
  describe('Effects.favorites', () => {
    it('should return favorited courses successfully', done => {
      // set up the mock endpoint to return the appropriate data
      const getFavoritesSpy = spyOn(apiService, 'getFavorites').and.callFake(
        () => {
          return of(favoritesJson);
        },
      );

      // set up a mock selector to return the reference data for the 0000 term.
      // The subject list below matches the subjects found in the favorited courses,
      // since the favorites action will fail if the subject for a course is not present
      store.overrideSelector(
        selectors.terms.getZeroTerm,
        success({
          termCode: Zero.decodeOrThrow('0000'),
          subjects: [
            { code: '528', name: '', description: '' },
            { code: '244', name: '', description: '' },
            { code: '660', name: '', description: '' },
            { code: '104', name: '', description: '' },
            { code: '244', name: '', description: '' },
          ],
          sessions: [],
          specialGroups: [],
        }),
      );
      // trigger the action
      actions$ = of(actions.saved.load());
      // wait for the effect to fire
      effects.saved$.subscribe(
        res => {
          // did we get the load completed action?
          expect(res.type).toEqual(actions.saved.done.type);
          expect(res.saved).toBeTruthy();
          expect(res.saved.success).toBeTruthy();

          // did the action succeed?
          // was the endpoint called?
          expect(getFavoritesSpy).toHaveBeenCalledTimes(1);

          done();
        },
        err => fail(`uncaught error ${err}`),
      );
    });
    it('should handle errors fetching favorited courses', done => {
      // set up the mock endpoint to return the appropriate data
      const getFavoritesSpy = spyOn(apiService, 'getFavorites').and.callFake(
        () => {
          return throwError('Backend error');
        },
      );

      // set up a mock selector to return the reference data for the 0000 term.
      // The subject list below matches the subjects found in the favorited courses,
      // since the favorites action will fail if the subject for a course is not present
      store.overrideSelector(
        selectors.terms.getZeroTerm,
        success({
          termCode: Zero.decodeOrThrow('0000'),
          subjects: [
            { code: '528', name: '', description: '' },
            { code: '244', name: '', description: '' },
            { code: '660', name: '', description: '' },
            { code: '104', name: '', description: '' },
            { code: '244', name: '', description: '' },
          ],
          sessions: [],
          specialGroups: [],
        }),
      );
      // trigger the action
      actions$ = of(actions.saved.load());
      // wait for the effect to fire
      effects.saved$.subscribe(
        res => {
          // did we get the load completed action?
          expect(res.type).toEqual(actions.saved.done.type);
          expect(res.saved).toBeTruthy();
          expect(res.saved.failed).toBeTruthy();
          expect(res.saved.success).toBeFalsy();

          // did the action succeed?
          // was the endpoint called?
          expect(getFavoritesSpy).toHaveBeenCalledTimes(1);

          done();
        },
        err => fail(`uncaught error ${err}`),
      );
    });
    it('should add favorited courses successfully', done => {
      // set up the mock endpoint to return the appropriate data
      const postFavoriteSpy = spyOn(apiService, 'postFavorite').and.callFake(
        () => {
          return of(favoritesJson);
        },
      );
      const openSnackBarSpy = spyOn(snackBar, 'open').and.callThrough();

      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: null,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: 12334,
        enrolledStatus: 'enrolled',
      };
      // trigger the action
      actions$ = of(actions.saved.add({ course: course }));
      // wait for the effect to fire
      effects.addSaved$.subscribe(
        res => {
          // did we get the load favorites action?
          expect(res.type).toEqual(actions.saved.load.type);

          // was the endpoint called?
          expect(postFavoriteSpy).toHaveBeenCalledTimes(1);
          // did the snackbar open?
          expect(openSnackBarSpy).toHaveBeenCalledTimes(1);

          done();
        },
        err => fail(`uncaught error ${err}`),
      );
    });
    it('should handle errors adding favorited courses', done => {
      // set up the mock endpoint to return the appropriate data
      const postFavoriteSpy = spyOn(apiService, 'postFavorite').and.callFake(
        () => {
          return throwError('Backend error');
        },
      );
      const openSnackBarSpy = spyOn(snackBar, 'open').and.callThrough();

      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: null,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: 12334,
        enrolledStatus: 'enrolled',
      };
      // trigger the action
      actions$ = of(actions.saved.add({ course: course }));
      // wait for the effect to fire
      effects.addSaved$.subscribe(
        _res => {
          // we don't get here if there's an error
          fail('Line should not be reached.');
        },
        () => {
          // have to catch this one in the error handler
          // Nothing currently catches this error, so it fails silently in the browser
          // was the endpoint called?
          expect(postFavoriteSpy).toHaveBeenCalledTimes(1);
          // did the snackbar open?
          expect(openSnackBarSpy).toHaveBeenCalledTimes(0);

          done();
        },
      );
    });
    it('should remove favorited courses successfully', done => {
      // set up the mock endpoint to return the appropriate data
      const deleteFavoriteSpy = spyOn(
        apiService,
        'deleteFavorite',
      ).and.callFake(() => {
        return of({});
      });
      const openSnackBarSpy = spyOn(snackBar, 'open').and.callThrough();

      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: null,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: 12334,
        enrolledStatus: 'enrolled',
      };
      // trigger the action
      actions$ = of(actions.saved.remove({ courses: [course] }));
      // wait for the effect to fire
      effects.removeSaved$.subscribe(
        res => {
          // did we get the load favorites action?
          expect(res.type).toEqual(actions.saved.load.type);

          // was the endpoint called?
          expect(deleteFavoriteSpy).toHaveBeenCalledTimes(1);
          // did the snackbar open?
          expect(openSnackBarSpy).toHaveBeenCalledTimes(1);
          expect(openSnackBarSpy.calls.first().args[0]).toBeTruthy();
          expect(openSnackBarSpy.calls.first().args[0]).toEqual(
            'Removed ALS 100',
          );
          done();
        },
        err => fail(`uncaught error ${err}`),
      );
    });
    it('should handle errors removing favorited courses', done => {
      // set up the mock endpoint to return the appropriate data
      const deleteFavoriteSpy = spyOn(
        apiService,
        'deleteFavorite',
      ).and.callFake(() => {
        return throwError('Backend error');
      });
      const openSnackBarSpy = spyOn(snackBar, 'open').and.callThrough();

      let course: any = {
        termCode: TermCode.decodeOrThrow('1222'),
        shortCatalog: 'ALS 100',
        subject: { code: '132' } as any,
        courseId: '000656',
        kind: 'roadmap',
        package: null,
        details: {
          isPassFail: false,
        },
        classPermissionNumber: 12334,
        enrolledStatus: 'enrolled',
      };
      // trigger the action
      actions$ = of(actions.saved.remove({ courses: [course] }));
      // wait for the effect to fire
      effects.removeSaved$.subscribe(
        res => {
          // did we get the load favorites action?
          expect(res.type).toEqual(actions.saved.load.type);

          // was the endpoint called?
          expect(deleteFavoriteSpy).toHaveBeenCalledTimes(1);
          // did the snackbar open?
          expect(openSnackBarSpy).toHaveBeenCalledTimes(1);
          expect(openSnackBarSpy.calls.first().args[0]).toBeTruthy();
          expect(openSnackBarSpy.calls.first().args[0]).toEqual(
            'Unable to remove ALS 100',
          );
          done();
        },
        err => fail(`uncaught error ${err}`),
      );
    });
  });
});
