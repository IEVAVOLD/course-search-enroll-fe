import { Pipe, PipeTransform } from '@angular/core';
import { NamedCourse } from '@app/types/courses';
import { IsPending } from '../objects/courses';

@Pipe({ name: 'isPendingCourse' })
export class IsPendingCoursePipe implements PipeTransform {
  transform(course: NamedCourse): boolean {
    return IsPending.matches(course);
  }
}
