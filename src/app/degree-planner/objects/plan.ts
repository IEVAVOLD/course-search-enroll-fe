import { YearCode } from '@app/types/terms';
import { WithEra } from '@app/types/terms';
import * as s from '@app/types/schema';
import {
  EnrolledCourse,
  PendingPlannedCourse,
  PlannedCourse,
  TransferredCourse,
} from './courses';

export interface Year {
  yearCode: YearCode;
  fall: PlannedTerm;
  spring: PlannedTerm;
  summer: PlannedTerm;
}

export interface YearMapping {
  [yearCode: string]: Year;
}

export type RawNote = s.FromSchema<typeof RawNote>;
export const RawNote = s.object({
  id: s.number,
  termCode: s.string,
  note: s.string,
});

export type PlannedTermNote =
  | { isLoaded: true; text: string; id: number }
  | { isLoaded: false; text: string };

export interface PlannedTerm {
  roadmapId: number;
  termCode: WithEra;
  note?: PlannedTermNote;
  transferred: TransferredCourse[];
  enrolled: EnrolledCourse[];
  planned: Array<PlannedCourse | PendingPlannedCourse>;
}
