import {
  CampusCourse,
  CourseId,
  HasTermCodeWithEra,
  NamedCourse,
  SubjectCode,
} from '@app/types/courses';
import '@app/types/credits';
import {
  CreditRange,
  CreditValue,
  HasCreditRange,
  HasCreditValue,
  MaybeHasCreditRange,
  MaybeHasCreditValue,
} from '@app/types/credits';
import { WithEra } from '@app/types/terms';
import * as s from '@app/types/schema';

const nextPendingId = (() => {
  let next = 1;
  return () => next++;
})();

export type IsPending = s.FromSchema<typeof IsPending>;
export type MaybeIsPending = Partial<IsPending>;
export const IsPending = s.object({ pendingId: s.number });

export class TransferredCourse implements NamedCourse, HasCreditValue {
  public readonly termCode: WithEra;
  public readonly name: string;
  public readonly title: string;
  public readonly creditValue: CreditValue;

  constructor(opts: NamedCourse & HasTermCodeWithEra & HasCreditValue) {
    this.termCode = opts.termCode;
    this.name = opts.name;
    this.title = opts.title;
    this.creditValue = opts.creditValue;
  }

  toString(): string {
    return this.name;
  }
}

export class EnrolledCourse implements CampusCourse, MaybeHasCreditValue {
  public readonly termCode: WithEra;
  public readonly name: string;
  public readonly title: string;
  public readonly subjectCode: SubjectCode;
  public readonly courseId: CourseId;
  public readonly creditValue: CreditValue | null;
  public readonly grade: string | null;
  public readonly isWaitlisted: boolean;

  constructor(
    opts: CampusCourse &
      HasTermCodeWithEra & {
        creditValue?: CreditValue;
        grade?: string;
        isWaitlisted: boolean;
      },
  ) {
    this.termCode = opts.termCode;
    this.name = opts.name;
    this.title = opts.title;
    this.subjectCode = opts.subjectCode;
    this.courseId = opts.courseId;
    this.creditValue = opts.creditValue ?? null;
    this.grade = opts.grade ?? null;
    this.isWaitlisted = opts.isWaitlisted;
  }

  toString(): string {
    return this.name;
  }
}

export type PlannedCourseId = number;

export class PlannedCourse
  implements CampusCourse, HasCreditRange, MaybeHasCreditValue
{
  public readonly termCode: WithEra;
  public readonly name: string;
  public readonly title: string;
  public readonly subjectCode: SubjectCode;
  public readonly courseId: CourseId;
  public readonly creditRange: CreditRange;
  public readonly creditValue: CreditValue | null;
  public readonly hasClassNumber: boolean;

  /**
   * All planned courses are given a unique ID by the backend. This ID is used
   * when making modifications to a planned course like changing the term it's
   * located in.
   */
  public readonly plannedCourseId: PlannedCourseId;

  /**
   * The backend may indicate that a course isn't reflected in a particular
   * term's catalog. In these cases we want to communicate that information to
   * the user and this field helps with that.
   */
  public readonly warning: 'doesNotExist' | 'notOffered' | null;

  constructor(
    opts: CampusCourse &
      HasCreditRange &
      MaybeHasCreditValue &
      HasTermCodeWithEra & {
        plannedCourseId: PlannedCourseId;
        warning?: PlannedCourse['warning'];
        hasClassNumber?: boolean;
      },
  ) {
    this.termCode = opts.termCode;
    this.name = opts.name;
    this.title = opts.title;
    this.subjectCode = opts.subjectCode;
    this.courseId = opts.courseId;
    this.creditRange = opts.creditRange;
    this.plannedCourseId = opts.plannedCourseId;
    this.warning = opts.warning ?? null;
    this.hasClassNumber = opts.hasClassNumber ?? false;

    if (WithEra.isActive(this.termCode)) {
      this.creditValue = opts.creditValue ?? null;
    } else {
      this.creditValue = null;
    }
  }

  toString(): string {
    return this.name;
  }

  public static warningFromStatus(status: unknown): PlannedCourse['warning'] {
    switch (status) {
      case 'NOTOFFERED':
        return 'notOffered';
      case 'DOESNOTEXIST':
        return 'doesNotExist';
      default:
        return null;
    }
  }
}

export class PendingPlannedCourse
  implements CampusCourse, MaybeHasCreditRange, MaybeHasCreditValue
{
  public readonly termCode: WithEra;
  public readonly name: string;
  public readonly title: string;
  public readonly subjectCode: SubjectCode;
  public readonly courseId: CourseId;
  public readonly creditRange: CreditRange | null;
  public readonly creditValue: CreditValue | null;

  /**
   * If the course was added to the term from search or from favorites, it will
   * lack an ID until the backend acknowledges that the course was added to the
   * term. If the course was moved from another term, it will already have an
   * ID and that ID will persist between terms.
   */
  public readonly plannedCourseId: PlannedCourseId | null;

  /**
   * This is just a auto-incremented integer added to each pending course to
   * help the app differentiate between pending courses when adding/removing
   * pending courses to a term.
   *
   * The value of this field is meaningless to all other parts of the app.
   */
  public readonly pendingId = nextPendingId();

  constructor(
    opts: CampusCourse &
      HasTermCodeWithEra & {
        creditRange?: CreditRange;
        creditValue?: CreditValue | null;
        plannedCourseId?: PlannedCourseId;
      },
  ) {
    this.termCode = opts.termCode;
    this.name = opts.name;
    this.title = opts.title;
    this.subjectCode = opts.subjectCode;
    this.courseId = opts.courseId;
    this.creditRange = opts.creditRange ?? null;
    this.plannedCourseId = opts.plannedCourseId ?? null;

    if (WithEra.isActive(this.termCode)) {
      this.creditValue = opts.creditValue ?? null;
    } else {
      this.creditValue = null;
    }
  }

  toString(): string {
    return this.name;
  }

  realize(
    plannedCourseId: PlannedCourseId,
    creditRange: CreditRange,
    status: unknown,
    creditValue: CreditValue | null = null,
  ): PlannedCourse {
    return new PlannedCourse({
      termCode: this.termCode,
      name: this.name,
      title: this.title,
      subjectCode: this.subjectCode,
      courseId: this.courseId,
      creditRange,
      creditValue,
      plannedCourseId,
      warning: PlannedCourse.warningFromStatus(status),
    });
  }
}

export class FavoriteCourse implements CampusCourse {
  public readonly name: string;
  public readonly title: string;
  public readonly subjectCode: SubjectCode;
  public readonly courseId: CourseId;

  constructor(opts: CampusCourse) {
    this.name = opts.name;
    this.title = opts.title;
    this.subjectCode = opts.subjectCode;
    this.courseId = opts.courseId;
  }

  toString(): string {
    return this.name;
  }
}

export class PendingFavoriteCourse implements CampusCourse {
  public readonly name: string;
  public readonly title: string;
  public readonly subjectCode: SubjectCode;
  public readonly courseId: CourseId;

  /**
   * This is just a auto-incremented integer added to each pending course to
   * help the app differentiate between pending courses when adding/removing
   * pending courses to a term.
   *
   * The value of this field is meaningless to all other parts of the app.
   */
  public readonly pendingId = nextPendingId();

  constructor(opts: CampusCourse) {
    this.name = opts.name;
    this.title = opts.title;
    this.subjectCode = opts.subjectCode;
    this.courseId = opts.courseId;
  }

  toString(): string {
    return this.name;
  }

  realize(): FavoriteCourse {
    return new FavoriteCourse({
      name: this.name,
      title: this.title,
      subjectCode: this.subjectCode,
      courseId: this.courseId,
    });
  }
}

export class SearchCourse implements CampusCourse {
  public readonly name: string;
  public readonly title: string;
  public readonly subjectCode: SubjectCode;
  public readonly courseId: CourseId;
  public readonly creditRange: CreditRange;

  constructor(opts: CampusCourse & HasCreditRange) {
    this.name = opts.name;
    this.title = opts.title;
    this.subjectCode = opts.subjectCode;
    this.courseId = opts.courseId;
    this.creditRange = opts.creditRange;
  }

  toString(): string {
    return this.name;
  }
}
