import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Component, Input, OnInit } from '@angular/core';
import { State } from '@app/degree-planner/store/state';
import * as globalSelectors from '@app/selectors';
import { YearCode, WithEra, Era } from '@app/types/terms';
import { map } from 'rxjs/operators';
import { ifSuccessThenUnwrap } from '@app/core/utils';

@Component({
  selector: 'cse-year-container',
  templateUrl: './year-container.component.html',
  styleUrls: ['./year-container.component.scss'],
})
export class YearContainerComponent implements OnInit {
  @Input() yearCode!: YearCode;

  public title$!: Observable<string>;
  public fallTermCode!: Observable<WithEra>;
  public springTermCode!: Observable<WithEra>;
  public summerTermCode!: Observable<WithEra>;
  public startsExpanded$!: Observable<boolean>;

  constructor(private store: Store<State>) {}

  public ngOnInit(): void {
    const activeTermCodes = this.store
      .select(globalSelectors.terms.getActiveTermCodes)
      .pipe(ifSuccessThenUnwrap());

    this.title$ = activeTermCodes.pipe(
      map(active => {
        const { fall, summer } = WithEra.fromYearCode(active, this.yearCode);
        const description = YearCode.describe(this.yearCode);

        if (summer.era === Era.Past) {
          return `Past year: ${description}`;
        }

        if (fall.era === Era.Future) {
          return `Future year: ${description}`;
        }

        return `Active year: ${description}`;
      }),
    );

    this.fallTermCode = activeTermCodes.pipe(
      map(active => WithEra.fromYearCode(active, this.yearCode).fall),
    );

    this.springTermCode = activeTermCodes.pipe(
      map(active => WithEra.fromYearCode(active, this.yearCode).spring),
    );

    this.summerTermCode = activeTermCodes.pipe(
      map(active => WithEra.fromYearCode(active, this.yearCode).summer),
    );

    this.startsExpanded$ = this.title$.pipe(
      map(title => title.startsWith('Past') === false),
    );
  }
}
