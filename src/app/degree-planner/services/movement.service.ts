import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { TermCode, WithEra } from '@app/types/terms';
import { BehaviorSubject } from 'rxjs';
import { delay, shareReplay } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class MovementService {
  private _connectedTermCodes$ = new BehaviorSubject<WithEra[]>([]);
  public connectedTermCodes$ = this._connectedTermCodes$.pipe(
    delay(0),
    shareReplay(1),
  );

  private _connectedIds$ = new BehaviorSubject<string[]>([]);
  public connectedIds$ = this._connectedIds$.pipe(delay(0), shareReplay(1));

  public static termCodeToDropZoneId(termCode: TermCode): string {
    return `term-drop-zone-${TermCode.encode(termCode)}`;
  }

  constructor(@Inject(DOCUMENT) private _document: Document) {}

  public register(id: string): string;
  public register(termCode: WithEra, connected: boolean): string;
  public register(ref: string | WithEra, connected = true): string {
    if (typeof ref === 'string') {
      appendIfNotPresent(this._connectedIds$, ref);
      return ref;
    }

    const id = MovementService.termCodeToDropZoneId(ref);

    if (connected) {
      // The "connection" parameter indicates whether the dropzone corresponding
      // to this term-code should allow courses to be dropped onto it. All
      // active and future term-codes *should* allow courses to be dropped. All
      // past term-codes *should not* allow courses to be dropped. So past terms
      // should not have their term-codes added to the list of connected
      // dropzones or to the list of connected term-codes.
      appendIfNotPresent(this._connectedIds$, id);
      appendIfNotPresent(this._connectedTermCodes$, ref, TermCode.equals);
    }

    return id;
  }

  public unregister(id: string): void;
  public unregister(termCode: WithEra): void;
  public unregister(ref: string | WithEra): void {
    removeIfPresent(
      this._connectedIds$,
      typeof ref === 'string' ? ref : MovementService.termCodeToDropZoneId(ref),
    );

    if (typeof ref !== 'string') {
      removeIfPresent(this._connectedTermCodes$, ref, TermCode.equals);
    }
  }

  public dragStarted(): void {
    this._document.body.classList.add('inherit-cursors');
  }

  public dragEnded(): void {
    this._document.body.classList.remove('inherit-cursors');
  }
}

export const appendIfNotPresent = <T>(
  s: BehaviorSubject<T[]>,
  a: T,
  eq = (a: T, b: T) => a === b,
): void => {
  const isPresent = s.value.some(b => eq(a, b));
  if (isPresent === false) {
    s.next([...s.value, a]);
  }
};

export const removeIfPresent = <T>(
  s: BehaviorSubject<T[]>,
  a: T,
  eq = (a: T, b: T) => a === b,
) => {
  const isPresent = s.value.some(b => eq(a, b));
  if (isPresent) {
    s.next(s.value.filter(b => eq(a, b) === false));
  }
};
