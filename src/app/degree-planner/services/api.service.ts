import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RawNote } from '@degree-planner/objects/plan';
import { CourseBase } from '@app/core/models/course';
import { RawPlan } from '@app/types/plans';
import { RawSearchResults } from '@app/types/courses';
import { TermCode } from '@app/types/terms';

const HTTP_OPTIONS = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

// const HTTP_OPTIONS = {
//   headers: new HttpHeaders({
//     'Content-Type': 'application/json',
//     'X-API-Key': '',
//     uid: '',
//     wiscEduISISEmplID: '',
//     wiscEduPVI: '',
//   }),
// };

@Injectable({ providedIn: 'root' })
export class DegreePlannerApiService {
  constructor(private http: HttpClient) {}

  public createDegreePlan(name: string, primary: boolean = false) {
    const url = `/api/planner/v1/degreePlan`;
    const payload = { name, primary };
    return this.http.post<RawPlan>(url, payload);
  }

  public deleteDegreePlan(roadmapId: number) {
    const url = `/api/planner/v1/degreePlan/${roadmapId}`;
    return this.http.delete<void>(url);
  }

  public getAllDegreePlans(): Observable<RawPlan[]> {
    return this.http.get<RawPlan[]>(`/api/planner/v1/degreePlan`, HTTP_OPTIONS);
  }

  public getAllNotes(roadmapId: number): Observable<RawNote[]> {
    return this.http.get<RawNote[]>(
      `/api/planner/v1/degreePlan/${roadmapId}/notes`,
      HTTP_OPTIONS,
    );
  }

  public getAllTermCourses(
    roadmapId: number,
  ): Observable<{ termCode: string; courses: CourseBase[] }[]> {
    return this.http.get<{ termCode: string; courses: CourseBase[] }[]>(
      `/api/planner/v1/degreePlan/${roadmapId}/termcourses`,
      HTTP_OPTIONS,
    );
  }

  public searchCourses(config: {
    subjectCode: string;
    searchText?: string;
    termCode?: string;
  }) {
    const { subjectCode, termCode, searchText } = config;

    const payload: any = {
      selectedTerm: termCode,
      queryString: searchText === '' ? '*' : searchText,

      // Filters are use to build the elastic search query
      filters: [],

      // These options control how much data we get back
      page: 1,
      pageSize: 10,
      sortOrder: 'SCORE',
    };

    // If we have a specific subject code, add a fitler for it
    if (subjectCode !== '-1') {
      payload.filters.push({ term: { 'subject.subjectCode': subjectCode } });
    }

    // 0000 is used to search all courses
    // Any other term code we can assuem is an active term
    if (termCode !== '0000') {
      // Used to search a specific term
      payload.filters.push({
        has_child: {
          type: 'enrollmentPackage',
          query: {
            // We want to make sure we search for ALL classes regardless of status
            match: {
              'packageEnrollmentStatus.status': 'OPEN WAITLISTED CLOSED',
            },
          },
        },
      });
    }

    return this.http.post<RawSearchResults>(
      '/api/search/v1',
      payload,
      HTTP_OPTIONS,
    );
  }

  public addCourse(
    planId: number,
    subjectCode: string,
    courseId: string,
    termCode: TermCode,
  ): Observable<CourseBase> {
    return this.http.post<CourseBase>(
      '/api/planner/v1/degreePlan/' + planId + '/courses',
      { subjectCode, courseId, termCode: TermCode.encode(termCode) },
      HTTP_OPTIONS,
    );
  }

  public addCourseToCart(
    subjectCode: string,
    courseId: string,
    termCode: TermCode,
  ): Observable<CourseBase> {
    const encoded = TermCode.encode(termCode);
    const url = `/api/planner/v1/roadmap/${encoded}/${subjectCode}/${courseId}`;
    return this.http.post<CourseBase>(url, {}, HTTP_OPTIONS);
  }

  public removeCourse(planId: number, recordId: number): Observable<void> {
    return this.http.delete<void>(
      '/api/planner/v1/degreePlan/' + planId + '/courses/' + recordId,
      HTTP_OPTIONS,
    );
  }

  public updateCourse(
    planId: number,
    recordId: number,
    termCode: string,
    options: { credits: number },
  ): Observable<void> {
    const url = `/api/planner/v1/degreePlan/${planId}/courses/${recordId}?termCode=${termCode}`;
    return this.http.put<void>(
      url,
      {
        credits: options.credits,
      },
      HTTP_OPTIONS,
    );
  }

  public createNote(
    planId: number,
    termCode: TermCode,
    noteText: string,
  ): Observable<RawNote> {
    const payload = {
      termCode: TermCode.encode(termCode),
      note: noteText,
    };

    return this.http.post<RawNote>(
      `/api/planner/v1/degreePlan/${planId}/notes`,
      payload,
      HTTP_OPTIONS,
    );
  }

  public updateNote(
    planId: number,
    termCode: TermCode,
    noteText: string,
    noteId: number,
  ): Observable<RawNote> {
    const payload = {
      termCode: TermCode.encode(termCode),
      note: noteText,
      id: noteId,
    };

    return this.http
      .put<null>(
        `/api/planner/v1/degreePlan/${planId}/notes/${noteId}`,
        payload,
        HTTP_OPTIONS,
      )
      .pipe(map(() => payload));
  }

  public deleteNote(planId: number, noteId: number): Observable<null> {
    return this.http.delete<null>(
      `/api/planner/v1/degreePlan/${planId}/notes/${noteId}`,
      HTTP_OPTIONS,
    );
  }

  public updatePlan(
    planId: number,
    name: string,
    primary: boolean,
  ): Observable<1> {
    return this.http.put<1>(
      `/api/planner/v1/degreePlan/${planId}`,
      { name, primary },
      HTTP_OPTIONS,
    );
  }
}
