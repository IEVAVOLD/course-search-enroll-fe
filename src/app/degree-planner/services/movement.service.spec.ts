import { BehaviorSubject } from 'rxjs';
import { appendIfNotPresent, removeIfPresent } from './movement.service';

describe('appendIfNotPresent', () => {
  it('should append element if not present', () => {
    const subA = new BehaviorSubject<string[]>(['a', 'b']);
    appendIfNotPresent(subA, 'x');
    expect(subA.value).toStrictEqual(['a', 'b', 'x']);

    type T = { a: number };
    const subB = new BehaviorSubject<T[]>([{ a: 123 }, { a: 456 }]);
    appendIfNotPresent(subB, { a: 789 }, (a, b) => a.a === b.a);
    expect(subB.value).toStrictEqual([{ a: 123 }, { a: 456 }, { a: 789 }]);
  });

  it('should not append if element is present', () => {
    const subA = new BehaviorSubject<string[]>(['a', 'b']);
    appendIfNotPresent(subA, 'b');
    expect(subA.value).toStrictEqual(['a', 'b']);

    type T = { a: number };
    const subB = new BehaviorSubject<T[]>([{ a: 123 }, { a: 456 }]);
    appendIfNotPresent(subB, { a: 123 }, (a, b) => a.a === b.a);
    expect(subB.value).toStrictEqual([{ a: 123 }, { a: 456 }]);
  });
});

describe('removeIfPresent', () => {
  it('should remove if element is present', () => {
    const subA = new BehaviorSubject<string[]>(['a', 'b']);
    removeIfPresent(subA, 'a');
    expect(subA.value).toStrictEqual(['b']);

    type T = { a: number };
    const subB = new BehaviorSubject<T[]>([{ a: 123 }, { a: 456 }]);
    removeIfPresent(subB, { a: 123 }, (a, b) => a.a === b.a);
    expect(subB.value).toStrictEqual([{ a: 456 }]);

    const subC = new BehaviorSubject<string[]>(['a', 'b', 'a']);
    removeIfPresent(subC, 'a');
    expect(subC.value).toStrictEqual(['b']);
  });

  it('should not remove element if not present', () => {
    const subA = new BehaviorSubject<string[]>(['a', 'b']);
    removeIfPresent(subA, 'x');
    expect(subA.value).toStrictEqual(['a', 'b']);

    type T = { a: number };
    const subB = new BehaviorSubject<T[]>([{ a: 123 }, { a: 456 }]);
    removeIfPresent(subB, { a: 789 }, (a, b) => a.a === b.a);
    expect(subB.value).toStrictEqual([{ a: 123 }, { a: 456 }]);
  });
});
