import { createAction, props } from '@ngrx/store';
import {
  FavoriteCourse,
  PendingFavoriteCourse,
  PendingPlannedCourse,
  PlannedCourse,
} from '@app/degree-planner/objects/courses';

/**
 * @deprecated
 */
export const moveCourseInsideTerm = createAction(
  'planner/moveCourseInsideTerm',
  props<{ course: PlannedCourse | PendingPlannedCourse; newIndex: number }>(),
);

/**
 * @deprecated
 */
export const moveCourseInsideSFL = createAction(
  'planner/moveCourseInsideSFL',
  props<{ course: FavoriteCourse | PendingFavoriteCourse; newIndex: number }>(),
);

//
// -> FAVORITES
//

export const addToFavorites = createAction(
  'planner/addToFavorites',
  props<{
    toAdd: PendingFavoriteCourse;
    index?: number;
  }>(),
);

export const addToFavoritesDone = createAction(
  'planner/addToFavoritesDone',
  props<{
    toRemove: PendingFavoriteCourse;
    toAdd: FavoriteCourse;
  }>(),
);

export const addToFavoritesCancel = createAction(
  'planner/addToFavoritesCancel',
  props<{ toRemove: PendingFavoriteCourse }>(),
);

//
// FAVORITES ->
//

export const removeFromFavorites = createAction(
  'planner/removeFromFavorites',
  props<{
    toAdd: PendingFavoriteCourse;
    toRemove: FavoriteCourse;
  }>(),
);

export const removeFromFavoritesDone = createAction(
  'planner/removeFromFavoritesDone',
  props<{ toRemove: PendingFavoriteCourse }>(),
);

export const removeFromFavoritesCancel = createAction(
  'planner/removeFromFavoritesCancel',
  props<{
    toRemove: PendingFavoriteCourse;
    toAdd: FavoriteCourse;
  }>(),
);

//
// -> TERM
//

export const addToTerm = createAction(
  'planner/addToTerm',
  props<{
    toAdd: PendingPlannedCourse;
    index?: number;
  }>(),
);

export const addToTermDone = createAction(
  'planner/addToTermDone',
  props<{ toRemove: PendingPlannedCourse; toAdd: PlannedCourse }>(),
);

export const addToTermCancel = createAction(
  'planner/addToTermCancel',
  props<{ toRemove: PendingPlannedCourse }>(),
);

//
// TERM ->
//

export const removeFromTerm = createAction(
  'planner/removeFromTerm',
  props<{
    toRemove: PlannedCourse;
    toAdd: PendingPlannedCourse;
  }>(),
);

export const removeFromTermDone = createAction(
  'planner/removeFromTermDone',
  props<{ toRemove: PendingPlannedCourse }>(),
);

export const removeFromTermCancel = createAction(
  'planner/removeFromTermCancel',
  props<{
    toRemove: PendingPlannedCourse;
    toAdd: PlannedCourse;
  }>(),
);

//
// TERM -> TERM
//

export const moveBetweenTerms = createAction(
  'planner/moveBetweenTerms',
  props<{
    toRemove: PlannedCourse;
    toAdd: PendingPlannedCourse;
    index?: number;
  }>(),
);

export const moveBetweenTermsDone = createAction(
  'planner/moveBetweenTermsDone',
  props<{
    toRemove: PendingPlannedCourse;
    toAdd: PlannedCourse;
  }>(),
);

export const moveBetweenTermsCancel = createAction(
  'planner/moveBetweenTermsCancel',
  props<{
    toRemove: PendingPlannedCourse;
    toAdd: PlannedCourse;
    index?: number;
  }>(),
);

//
// FAVORITES -> TERM
//

export const moveToTermFromFavorites = createAction(
  'planner/moveToTermFromFavorites',
  props<{
    toRemove: FavoriteCourse;
    toAdd: PendingPlannedCourse;
    index?: number;
  }>(),
);

export const moveToTermFromFavoritesDone = createAction(
  'planner/moveToTermFromFavoritesDone',
  props<{
    toRemove: PendingPlannedCourse;
    toAdd: PlannedCourse;
  }>(),
);

export const moveToTermFromFavoritesCancel = createAction(
  'planner/moveToTermFromFavoritesCancel',
  props<{
    toRemove: PendingPlannedCourse;
    toAdd: FavoriteCourse;
    index?: number;
  }>(),
);

//
// TERM -> FAVORITES
//

export const moveToFavoritesFromTerm = createAction(
  'planner/moveToFavoritesFromTerm',
  props<{
    toRemove: PlannedCourse;
    toAdd: PendingFavoriteCourse;
    index?: number;
  }>(),
);

export const moveToFavoritesFromTermDone = createAction(
  'planner/moveToFavoritesFromTermDone',
  props<{
    toRemove: PendingFavoriteCourse;
    toAdd: FavoriteCourse;
  }>(),
);

export const moveToFavoritesFromTermCancel = createAction(
  'planner/moveToFavoritesFromTermCancel',
  props<{
    toRemove: PendingFavoriteCourse;
    toAdd: PlannedCourse;
    index?: number;
  }>(),
);
