import { createAction, props } from '@ngrx/store';
import { RawPlan } from '@app/types/plans';
import { DegreePlannerState } from '@app/degree-planner/store/state';
import { YearMapping } from '@degree-planner/objects/plan';

export const initialLoadRequest = createAction('plan/initialLoadRequest');

export const initialLoadSuccess = createAction(
  'plan/initialLoadSuccess',
  props<{ payload: Partial<DegreePlannerState> }>(),
);

export const switchPlan = createAction(
  'plan/switchPlan',
  props<{ newVisibleRoadmapId: number }>(),
);

export const switchPlanSuccess = createAction(
  'plan/switchPlanSuccess',
  props<{ visibleDegreePlan: RawPlan; visibleYears: YearMapping }>(),
);

export const createPlan = createAction(
  'plan/createPlan',
  props<{ name: string; primary: boolean }>(),
);

export const createPlanSuccess = createAction(
  'plan/createPlanSuccess',
  props<{ newPlan: RawPlan; newYears: YearMapping }>(),
);

export const deletePlan = createAction(
  'plan/deletePlan',
  props<{ roadmapId: number }>(),
);

export const deletePlanSuccess = createAction(
  'plan/deletePlanSuccess',
  props<{ roadmapId: number }>(),
);

export const planError = createAction(
  'plan/planError',
  props<{ message: string; duration: number; error: any }>(),
);

export const makePrimary = createAction(
  'plan/makePrimary',
  props<{ roadmapId: number }>(),
);

export const changePlanName = createAction(
  'plan/changePlanName',
  props<{ roadmapId: number; newName: string }>(),
);

export const changePlanNameSuccess = createAction(
  'plan/changePlanNameSuccess',
  props<{ roadmapId: number; newName: string }>(),
);

export const changePlanNameFailure = createAction(
  'plan/changePlanNameFailure',
  props<{ roadmapId: number; oldName: string }>(),
);
