import { createAction, props } from '@ngrx/store';
import { RawNote } from '@app/degree-planner/objects/plan';
import { TermCode } from '@app/types/terms';

export const writeNote = createAction(
  'note/writeNote',
  props<{ termCode: TermCode; noteText: string }>(),
);

export const writeNoteSuccess = createAction(
  'note/writeNoteSuccess',
  props<{ termCode: TermCode; updatedNote: RawNote }>(),
);

export const deleteNote = createAction(
  'note/deleteNote',
  props<{ termCode: TermCode; noteId: number }>(),
);

export const deleteNoteSuccess = createAction(
  'note/deleteNoteSuccess',
  props<{ termCode: TermCode }>(),
);

export const noteError = createAction(
  'note/noteError',
  props<{ message: string; duration: number; error: any }>(),
);
