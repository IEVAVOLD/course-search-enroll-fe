import { createAction, props } from '@ngrx/store';
import { WithEra, YearCode } from '@app/types/terms';

export const openCourseSearch = createAction(
  'ui/openCourseSearch',
  props<{ termCode?: WithEra }>(),
);

export const closeCourseSearch = createAction('ui/closeCourseSearch');

export const toggleCourseSearch = createAction(
  'ui/toggleCourseSearch',
  props<{ termCode?: WithEra }>(),
);

export const updateSearchTermCode = createAction(
  'ui/updateSearchTermCode',
  props<{ termCode: WithEra }>(),
);

export const openSidenav = createAction('ui/openSidenav');

export const closeSidenav = createAction('ui/closeSidenav');

export const addAcademicYear = createAction(
  'ui/addAcademicYear',
  props<{ yearCode: YearCode }>(),
);
