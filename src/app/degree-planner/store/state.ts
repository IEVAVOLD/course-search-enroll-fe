import { YearMapping } from '@degree-planner/objects/plan';
import { RawPlan, PlanId } from '@app/types/plans';
import { WithEra } from '@app/types/terms';
import { GlobalState } from '@app/state';
import { FavoriteCourse, PendingFavoriteCourse } from '../objects/courses';

export interface DegreePlannerState {
  visibleDegreePlan: PlanId | undefined;
  visibleYears: YearMapping;
  savedForLaterCourses: Array<FavoriteCourse | PendingFavoriteCourse>;
  allDegreePlans: Array<RawPlan>;
  search: { visible: boolean; selectedTerm?: WithEra };
  isLoadingPlan: boolean;
  isSidenavOpen: 'defer' | boolean;
}

export interface State extends GlobalState {
  degreePlanner: DegreePlannerState;
}

export const INITIAL_DEGREE_PLANNER_STATE: DegreePlannerState = {
  visibleDegreePlan: undefined,
  visibleYears: {},
  savedForLaterCourses: [],
  allDegreePlans: [],
  search: { visible: false },
  isLoadingPlan: true,
  isSidenavOpen: 'defer',
};
