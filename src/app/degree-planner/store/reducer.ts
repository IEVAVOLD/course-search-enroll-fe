import { INITIAL_DEGREE_PLANNER_STATE } from '@app/degree-planner/store/state';
import { createReducer } from '@ngrx/store';
import * as courseReducers from './reducers/course.reducers';
import * as noteReducers from './reducers/note.reducers';
import * as planReducers from './reducers/plan.reducers';
import * as uiReducers from './reducers/ui.reducers';

export const degreePlannerReducer = createReducer(
  INITIAL_DEGREE_PLANNER_STATE,

  // Note reducer functions
  noteReducers.writeNote,
  noteReducers.writeNoteSuccess,
  noteReducers.deleteNote,

  // Plan reducer functions
  planReducers.planError,
  planReducers.createPlan,
  planReducers.initialLoadSuccess,
  planReducers.switchPlan,
  planReducers.switchPlanSuccess,
  planReducers.createPlanSuccess,
  planReducers.makePrimary,
  planReducers.changePlanNameSuccess,
  planReducers.changePlanNameFailure,
  planReducers.deletePlanSuccess,

  // Course reducer functions
  courseReducers.addToFavorites,
  courseReducers.addToFavoritesDone,
  courseReducers.addToFavoritesCancel,
  courseReducers.removeFromFavorites,
  courseReducers.removeFromFavoritesDone,
  courseReducers.removeFromFavoritesCancel,
  courseReducers.addToTerm,
  courseReducers.addToTermDone,
  courseReducers.addToTermCancel,
  courseReducers.removeFromTerm,
  courseReducers.removeFromTermDone,
  courseReducers.removeFromTermCancel,
  courseReducers.moveBetweenTerms,
  courseReducers.moveBetweenTermsDone,
  courseReducers.moveBetweenTermsCancel,
  courseReducers.moveToTermFromFavorites,
  courseReducers.moveToTermFromFavoritesDone,
  courseReducers.moveToTermFromFavoritesCancel,
  courseReducers.moveToFavoritesFromTerm,
  courseReducers.moveToFavoritesFromTermDone,
  courseReducers.moveToFavoritesFromTermCancel,
  courseReducers.moveCourseInsideTerm,
  courseReducers.moveCourseInsideSFL,

  // UI reducer functions
  uiReducers.addAcademicYear,
  uiReducers.toggleCourseSearch,
  uiReducers.openCourseSearch,
  uiReducers.closeCourseSearch,
  uiReducers.updateSearchTermCode,
  uiReducers.openSidenav,
  uiReducers.closeSidenav,
);
