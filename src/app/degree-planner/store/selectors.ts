import { createSelector, createFeatureSelector } from '@ngrx/store';
import { DegreePlannerState } from './state';
import { YearCode, TermCode, Era } from '@app/types/terms';
import { YearMapping, Year } from '@degree-planner/objects/plan';

export const getDegreePlannerState =
  createFeatureSelector<DegreePlannerState>('degreePlanner');

export const hasLoadedDegreePlan = createSelector(
  getDegreePlannerState,
  state => state.visibleDegreePlan !== undefined,
);

export const selectVisibleDegreePlan = createSelector(
  getDegreePlannerState,
  state => state.visibleDegreePlan,
);

export const selectAllDegreePlans = createSelector(
  getDegreePlannerState,
  state => state.allDegreePlans,
);

export const getSavedForLaterCourses = createSelector(
  getDegreePlannerState,
  state => state.savedForLaterCourses,
);

export const selectAllVisibleYears = createSelector(
  getDegreePlannerState,
  state => state.visibleYears,
);

export const selectVisibleTerm = createSelector(
  selectAllVisibleYears,
  (years: YearMapping, params: { termCode: TermCode }) => {
    const year = years[YearCode.encode(params.termCode)] as Year | undefined;
    if (year) {
      return year[TermCode.lowerTermName(params.termCode)];
    } else {
      return undefined;
    }
  },
);

export const isCourseSearchOpen = createSelector(
  getDegreePlannerState,
  (state: DegreePlannerState) => {
    return state.search.visible;
  },
);

export const getSelectedSearchTerm = createSelector(
  getDegreePlannerState,
  (state: DegreePlannerState) => {
    return state.search.selectedTerm;
  },
);

export const getActiveSelectedSearchTerm = createSelector(
  getDegreePlannerState,
  (state: DegreePlannerState) => {
    if (state.search.selectedTerm?.era === Era.Active) {
      return state.search.selectedTerm;
    } else {
      return undefined;
    }
  },
);

export const isLoadingPlan = createSelector(
  getDegreePlannerState,
  (state: DegreePlannerState) => state.isLoadingPlan,
);

export const isSidenavOpen = createSelector(
  getDegreePlannerState,
  (state: DegreePlannerState) => state.isSidenavOpen,
);
