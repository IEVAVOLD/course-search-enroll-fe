import { on } from '@ngrx/store';
import produce from 'immer';
import { DegreePlannerState } from '../state';
import * as actions from '../actions/plan.actions';

export const planError = on<DegreePlannerState, [typeof actions.planError]>(
  actions.planError,
  state => {
    return produce(state, draft => {
      draft.isLoadingPlan = false;
    });
  },
);

export const createPlan = on<DegreePlannerState, [typeof actions.createPlan]>(
  actions.createPlan,
  state => {
    return produce(state, draft => {
      draft.isLoadingPlan = true;
    });
  },
);

export const initialLoadSuccess = on<
  DegreePlannerState,
  [typeof actions.initialLoadSuccess]
>(actions.initialLoadSuccess, (state, action) => {
  return { ...state, ...action.payload };
});

export const switchPlan = on<DegreePlannerState, [typeof actions.switchPlan]>(
  actions.switchPlan,
  (state, {}) => {
    return produce(state, draft => {
      draft.isLoadingPlan = true;
    });
  },
);

export const switchPlanSuccess = on<
  DegreePlannerState,
  [typeof actions.switchPlanSuccess]
>(actions.switchPlanSuccess, (state, { visibleDegreePlan, visibleYears }) => {
  return produce(state, draft => {
    draft.isLoadingPlan = false;
    draft.visibleDegreePlan = visibleDegreePlan.roadmapId;
    (draft.visibleYears as any) = visibleYears;
  });
});

export const createPlanSuccess = on<
  DegreePlannerState,
  [typeof actions.createPlanSuccess]
>(actions.createPlanSuccess, (state, { newPlan, newYears }) => {
  return produce(state, draft => {
    draft.visibleDegreePlan = newPlan.roadmapId;
    (draft.visibleYears as any) = newYears;
    draft.allDegreePlans.push(newPlan);
    draft.isLoadingPlan = false;
  });
});

export const makePrimary = on<DegreePlannerState, [typeof actions.makePrimary]>(
  actions.makePrimary,
  (state, { roadmapId }) => {
    return produce(state, draft => {
      draft.allDegreePlans.forEach(plan => {
        plan.primary = plan.roadmapId === roadmapId;
      });
    });
  },
);

export const changePlanNameSuccess = on<
  DegreePlannerState,
  [typeof actions.changePlanNameSuccess]
>(actions.changePlanNameSuccess, (state, { newName }) => {
  return produce(state, draft => {
    draft.allDegreePlans.forEach(plan => {
      if (plan.roadmapId === draft.visibleDegreePlan) {
        plan.name = newName;
      }
    });
  });
});

export const changePlanNameFailure = on<
  DegreePlannerState,
  [typeof actions.changePlanNameFailure]
>(actions.changePlanNameFailure, (state, { oldName }) => {
  return produce(state, draft => {
    draft.allDegreePlans.forEach(plan => {
      if (plan.roadmapId === draft.visibleDegreePlan) {
        plan.name = oldName;
      }
    });
  });
});

export const deletePlanSuccess = on<
  DegreePlannerState,
  [typeof actions.deletePlanSuccess]
>(actions.deletePlanSuccess, (state, { roadmapId }) => {
  return produce(state, draft => {
    const planIndex = draft.allDegreePlans.findIndex(
      p => p.roadmapId === roadmapId,
    );
    draft.allDegreePlans.splice(planIndex, 1);
  });
});
