import { PlannedTerm } from '@degree-planner/objects/plan';
import { TermCode, YearCode } from '@app/types/terms';
import { on } from '@ngrx/store';
import produce from 'immer';
import * as actions from '../actions/note.actions';
import { DegreePlannerState } from '../state';

const findTerm = (
  state: DegreePlannerState,
  termCode: TermCode,
): PlannedTerm => {
  return state.visibleYears[YearCode.encode(termCode)][
    TermCode.lowerTermName(termCode)
  ];
};

export const writeNote = on<DegreePlannerState, [typeof actions.writeNote]>(
  actions.writeNote,
  (state, { termCode, noteText }) => {
    return produce(state, draft => {
      const term = findTerm(draft, termCode);

      if (term.note && term.note.isLoaded) {
        term.note.isLoaded = true;
        term.note.text = noteText;
      } else {
        term.note = {
          isLoaded: false,
          text: noteText,
        };
      }
    });
  },
);

export const writeNoteSuccess = on<
  DegreePlannerState,
  [typeof actions.writeNoteSuccess]
>(actions.writeNoteSuccess, (state, { termCode, updatedNote }) => {
  return produce(state, draft => {
    findTerm(draft, termCode).note = {
      isLoaded: true,
      text: updatedNote.note,
      id: updatedNote.id,
    };
  });
});

export const deleteNote = on<DegreePlannerState, [typeof actions.deleteNote]>(
  actions.deleteNote,
  (state, { termCode }) => {
    return produce(state, draft => {
      findTerm(draft, termCode).note = undefined;
    });
  },
);
