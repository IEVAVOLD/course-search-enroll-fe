import { PlannedTerm, YearMapping } from '@degree-planner/objects/plan';
import { TermCode, YearCode } from '@app/types/terms';
import { on } from '@ngrx/store';
import produce from 'immer';
import * as actions from '../actions/course.actions';
import { DegreePlannerState } from '../state';
import {
  FavoriteCourse,
  PendingFavoriteCourse,
  PendingPlannedCourse,
  PlannedCourse,
} from '@app/degree-planner/objects/courses';

//
// -> FAVORITES
//

export const addToFavorites = on<
  DegreePlannerState,
  [typeof actions.addToFavorites]
>(actions.addToFavorites, (state, { toAdd, index }) => {
  return produce(state, draft => {
    const favorites = draft.savedForLaterCourses;
    if (favoritesAlreadyContains(favorites, toAdd)) {
      // If favorites already contain the course, make no changes. The effect
      // will be responsible for alerting the user that the move failed.
      return;
    }

    favorites.splice(index ?? favorites.length, 0, toAdd);

    // TODO: sort favorites
  });
});

export const addToFavoritesDone = on<
  DegreePlannerState,
  [typeof actions.addToFavoritesDone]
>(actions.addToFavoritesDone, (state, { toRemove, toAdd }) => {
  return produce(state, draft => {
    const favorites = draft.savedForLaterCourses;
    const index = favorites.findIndex(matchingFavorite(toRemove));
    if (index >= 0) {
      favorites.splice(index, 1, toAdd);
    }
  });
});

export const addToFavoritesCancel = on<
  DegreePlannerState,
  [typeof actions.addToFavoritesCancel]
>(actions.addToFavoritesCancel, (state, { toRemove }) => {
  return produce(state, draft => {
    const favorites = draft.savedForLaterCourses;
    const index = favorites.findIndex(matchingFavorite(toRemove));
    if (index >= 0) {
      favorites.splice(index, 1);
    }
  });
});

//
// FAVORITES ->
//

export const removeFromFavorites = on<
  DegreePlannerState,
  [typeof actions.removeFromFavorites]
>(actions.removeFromFavorites, (state, { toRemove, toAdd }) => {
  return produce(state, draft => {
    const favorites = draft.savedForLaterCourses;
    const swapIndex = favorites.findIndex(matchingFavorite(toRemove));
    if (swapIndex >= 0) {
      favorites.splice(swapIndex, 1, toAdd);
    }
  });
});

export const removeFromFavoritesDone = on<
  DegreePlannerState,
  [typeof actions.removeFromFavoritesDone]
>(actions.removeFromFavoritesDone, (state, { toRemove }) => {
  return produce(state, draft => {
    const favorites = draft.savedForLaterCourses;
    const index = favorites.findIndex(matchingFavorite(toRemove));
    if (index >= 0) {
      favorites.splice(index, 1);
    }
  });
});

export const removeFromFavoritesCancel = on<
  DegreePlannerState,
  [typeof actions.removeFromFavoritesCancel]
>(actions.removeFromFavoritesCancel, (state, { toRemove, toAdd }) => {
  return produce(state, draft => {
    const favorites = draft.savedForLaterCourses;
    const swapIndex = favorites.findIndex(matchingFavorite(toRemove));
    if (swapIndex >= 0) {
      favorites.splice(swapIndex, 1, toAdd);
    }
  });
});

//
// -> TERM
//

export const addToTerm = on<DegreePlannerState, [typeof actions.addToTerm]>(
  actions.addToTerm,
  (state, { toAdd, index }) => {
    return produce(state, draft => {
      const term = findTerm(draft.visibleYears, toAdd.termCode);
      if (termAlreadyContains(term, toAdd)) {
        // If the term already contains the course, make no changes. The effect
        // will be responsible for alerting the user that the move failed.
        return;
      }

      if (term) {
        term.planned.splice(index ?? term.planned.length, 0, toAdd);

        // TODO: sort term
      }
    });
  },
);

export const addToTermDone = on<
  DegreePlannerState,
  [typeof actions.addToTermDone]
>(actions.addToTermDone, (state, { toRemove, toAdd }) => {
  return produce(state, draft => {
    const term = findTerm(draft.visibleYears, toAdd.termCode);
    if (term) {
      const swapIndex = term.planned.findIndex(matchingPlanned(toRemove));
      if (swapIndex >= 0) {
        term.planned.splice(swapIndex, 1, toAdd);
      } else {
        term.planned.push(toAdd);
      }

      // TODO: sort term
    }
  });
});

export const addToTermCancel = on<
  DegreePlannerState,
  [typeof actions.addToTermCancel]
>(actions.addToTermCancel, (state, { toRemove }) => {
  return produce(state, draft => {
    const term = findTerm(draft.visibleYears, toRemove.termCode);
    if (term) {
      const swapIndex = term.planned.findIndex(matchingPlanned(toRemove));
      if (swapIndex >= 0) {
        term.planned.splice(swapIndex, 1);
      }
    }
  });
});

//
// TERM ->
//

export const removeFromTerm = on<
  DegreePlannerState,
  [typeof actions.removeFromTerm]
>(actions.removeFromTerm, (state, { toRemove, toAdd }) => {
  return produce(state, draft => {
    const term = findTerm(draft.visibleYears, toRemove.termCode);
    if (term) {
      const swapIndex = term.planned.findIndex(matchingPlanned(toRemove));
      if (swapIndex >= 0) {
        term.planned.splice(swapIndex, 1, toAdd);
      }
    }
  });
});

export const removeFromTermDone = on<
  DegreePlannerState,
  [typeof actions.removeFromTermDone]
>(actions.removeFromTermDone, (state, { toRemove }) => {
  return produce(state, draft => {
    const term = findTerm(draft.visibleYears, toRemove.termCode);
    if (term) {
      const removeIndex = term.planned.findIndex(matchingPlanned(toRemove));
      if (removeIndex >= 0) {
        term.planned.splice(removeIndex, 1);
      }
    }
  });
});

export const removeFromTermCancel = on<
  DegreePlannerState,
  [typeof actions.removeFromTermCancel]
>(actions.removeFromTermCancel, (state, { toRemove, toAdd }) => {
  return produce(state, draft => {
    const term = findTerm(draft.visibleYears, toAdd.termCode);
    if (term) {
      const swapIndex = term.planned.findIndex(matchingPlanned(toRemove));
      if (swapIndex >= 0) {
        term.planned.splice(swapIndex, 1, toAdd);
      }
    }
  });
});

//
// TERM -> TERM
//

export const moveBetweenTerms = on<
  DegreePlannerState,
  [typeof actions.moveBetweenTerms]
>(actions.moveBetweenTerms, (state, { toRemove, toAdd, index }) => {
  return produce(state, draft => {
    const fromTerm = findTerm(draft.visibleYears, toRemove.termCode);
    const toTerm = findTerm(draft.visibleYears, toAdd.termCode);

    if (termAlreadyContains(toTerm, toAdd)) {
      // If the term already contains the course, make no changes. The effect
      // will be responsible for alerting the user that the move failed.
      return;
    }

    if (fromTerm) {
      const removeIndex = fromTerm.planned.findIndex(matchingPlanned(toRemove));
      if (removeIndex >= 0) {
        fromTerm.planned.splice(removeIndex, 1);
      }
    }

    if (toTerm) {
      toTerm.planned.splice(index ?? toTerm.planned.length, 0, toAdd);

      // TODO: sort term
    }
  });
});

export const moveBetweenTermsDone = on<
  DegreePlannerState,
  [typeof actions.moveBetweenTermsDone]
>(actions.moveBetweenTermsDone, (state, { toRemove, toAdd }) => {
  return produce(state, draft => {
    const term = findTerm(draft.visibleYears, toAdd.termCode);
    if (term) {
      const swapIndex = term.planned.findIndex(matchingPlanned(toRemove));
      if (swapIndex >= 0) {
        term.planned.splice(swapIndex, 1, toAdd);
      } else {
        term.planned.push(toAdd);
      }

      // TODO: sort term
    }
  });
});

export const moveBetweenTermsCancel = on<
  DegreePlannerState,
  [typeof actions.moveBetweenTermsCancel]
>(actions.moveBetweenTermsCancel, (state, { toRemove, toAdd, index }) => {
  return produce(state, draft => {
    const fromTerm = findTerm(draft.visibleYears, toRemove.termCode);
    const toTerm = findTerm(draft.visibleYears, toAdd.termCode);

    if (fromTerm) {
      const removeIndex = fromTerm.planned.findIndex(matchingPlanned(toRemove));
      if (removeIndex >= 0) {
        fromTerm.planned.splice(removeIndex, 1);
      }
    }

    if (toTerm) {
      toTerm.planned.splice(index ?? toTerm.planned.length, 0, toAdd);

      // TODO: sort term
    }
  });
});

//
// FAVORITES -> TERM
//

export const moveToTermFromFavorites = on<
  DegreePlannerState,
  [typeof actions.moveToTermFromFavorites]
>(actions.moveToTermFromFavorites, (state, { toRemove, toAdd, index }) => {
  return produce(state, draft => {
    const term = findTerm(draft.visibleYears, toAdd.termCode);
    const favorites = draft.savedForLaterCourses;

    if (termAlreadyContains(term, toAdd)) {
      // If the term already contains the course, make no changes. The effect
      // will be responsible for alerting the user that the move failed.
      return;
    }

    const removeIndex = favorites.findIndex(matchingFavorite(toRemove));
    if (removeIndex >= 0) {
      favorites.splice(removeIndex, 1);
    }

    if (term) {
      term.planned.splice(index ?? term.planned.length, 0, toAdd);

      // TODO: sort term
    }
  });
});

export const moveToTermFromFavoritesDone = on<
  DegreePlannerState,
  [typeof actions.moveToTermFromFavoritesDone]
>(actions.moveToTermFromFavoritesDone, (state, { toRemove, toAdd }) => {
  return produce(state, draft => {
    const term = findTerm(draft.visibleYears, toAdd.termCode);

    if (term) {
      const swapIndex = term.planned.findIndex(matchingPlanned(toRemove));
      if (swapIndex >= 0) {
        term.planned.splice(swapIndex, 1, toAdd);
      }
    }
  });
});

export const moveToTermFromFavoritesCancel = on<
  DegreePlannerState,
  [typeof actions.moveToTermFromFavoritesCancel]
>(
  actions.moveToTermFromFavoritesCancel,
  (state, { toRemove, toAdd, index }) => {
    return produce(state, draft => {
      const term = findTerm(draft.visibleYears, toRemove.termCode);

      if (term) {
        const removeIndex = term.planned.findIndex(matchingPlanned(toRemove));
        if (removeIndex >= 0) {
          term.planned.splice(removeIndex, 1);
        }
      }

      const favorites = draft.savedForLaterCourses;
      favorites.splice(index ?? favorites.length, 0, toAdd);

      // TODO: sort favorites
    });
  },
);

//
// TERM -> FAVORITES
//

export const moveToFavoritesFromTerm = on<
  DegreePlannerState,
  [typeof actions.moveToFavoritesFromTerm]
>(actions.moveToFavoritesFromTerm, (state, { toRemove, toAdd, index }) => {
  return produce(state, draft => {
    const favorites = draft.savedForLaterCourses;
    if (favoritesAlreadyContains(favorites, toAdd)) {
      console.log('(skip)');
      // If favorites already contain the course, make no changes. The effect
      // will be responsible for alerting the user that the move failed.
      return;
    }

    const term = findTerm(draft.visibleYears, toRemove.termCode);
    if (term) {
      const removeIndex = term.planned.findIndex(matchingPlanned(toRemove));
      if (removeIndex >= 0) {
        term.planned.splice(removeIndex, 1);
      }
    }

    favorites.splice(index ?? favorites.length, 0, toAdd);

    // TODO: sort favorites
  });
});

export const moveToFavoritesFromTermDone = on<
  DegreePlannerState,
  [typeof actions.moveToFavoritesFromTermDone]
>(actions.moveToFavoritesFromTermDone, (state, { toRemove, toAdd }) => {
  return produce(state, draft => {
    const favorites = draft.savedForLaterCourses;
    const swapIndex = favorites.findIndex(matchingFavorite(toRemove));
    if (swapIndex >= 0) {
      favorites.splice(swapIndex, 1, toAdd);
    } else {
      favorites.push(toAdd);
    }

    // TODO: sort favorites
  });
});

export const moveToFavoritesFromTermCancel = on<
  DegreePlannerState,
  [typeof actions.moveToFavoritesFromTermCancel]
>(
  actions.moveToFavoritesFromTermCancel,
  (state, { toRemove, toAdd, index }) => {
    return produce(state, draft => {
      const favorites = draft.savedForLaterCourses;
      const removeIndex = favorites.findIndex(matchingFavorite(toRemove));
      if (removeIndex >= 0) {
        favorites.splice(removeIndex, 1);
      }

      const term = findTerm(draft.visibleYears, toAdd.termCode);
      if (term) {
        term.planned.splice(index ?? term.planned.length, 0, toAdd);

        // TODO: sort term
      }
    });
  },
);

/**
 * @deprecated
 */
export const moveCourseInsideTerm = on<
  DegreePlannerState,
  [typeof actions.moveCourseInsideTerm]
>(actions.moveCourseInsideTerm, (state, { course, newIndex }) => {
  return produce(state, draft => {
    const term = findTerm(draft.visibleYears, course.termCode);

    if (term) {
      const oldIndex = term.planned.findIndex(matchingPlanned(course));
      if (oldIndex >= 0) {
        term.planned.splice(oldIndex, 1);
        term.planned.splice(newIndex, 0, course);
      }
    }
  });
});

/**
 * @deprecated
 */
export const moveCourseInsideSFL = on<
  DegreePlannerState,
  [typeof actions.moveCourseInsideSFL]
>(actions.moveCourseInsideSFL, (state, { course, newIndex }) => {
  return produce(state, draft => {
    const favorites = draft.savedForLaterCourses;
    const oldIndex = favorites.findIndex(matchingFavorite(course));
    favorites.splice(oldIndex, 1);
    favorites.splice(newIndex, 0, course);
  });
});

//
// UTILITIES
//

const findTerm = (years: YearMapping, termCode: TermCode) => {
  const yearKey = YearCode.encode(termCode);
  const year = years[yearKey];

  if (!year) {
    return null;
  }

  const termKey = TermCode.lowerTermName(termCode);
  return year[termKey];
};

const termAlreadyContains = (
  t: PlannedTerm | undefined | null,
  c: PlannedCourse | PendingPlannedCourse,
) => {
  if (!t) {
    return false;
  }

  return t.planned.some(({ courseId }) => {
    return c.courseId === courseId;
  });
};

const favoritesAlreadyContains = (
  f: DegreePlannerState['savedForLaterCourses'],
  c: FavoriteCourse | PendingFavoriteCourse,
) => {
  return f.some(({ courseId }) => {
    return c.courseId === courseId;
  });
};

const matchingPlanned = (a: PlannedCourse | PendingPlannedCourse) => {
  return (b: PlannedCourse | PendingPlannedCourse) => {
    if (a instanceof PlannedCourse && b instanceof PlannedCourse) {
      return a.plannedCourseId === b.plannedCourseId;
    } else if (
      a instanceof PendingPlannedCourse &&
      b instanceof PendingPlannedCourse
    ) {
      return a.pendingId === b.pendingId;
    } else {
      return false;
    }
  };
};

const matchingFavorite = (a: FavoriteCourse | PendingFavoriteCourse) => {
  return (b: FavoriteCourse | PendingFavoriteCourse) => {
    if (a instanceof FavoriteCourse && b instanceof FavoriteCourse) {
      return a.subjectCode === b.subjectCode && a.courseId === b.courseId;
    } else if (
      a instanceof PendingFavoriteCourse &&
      b instanceof PendingFavoriteCourse
    ) {
      return a.pendingId === b.pendingId;
    } else {
      return false;
    }
  };
};
