import { on } from '@ngrx/store';
import produce from 'immer';
import { DegreePlannerState } from '../state';
import * as actions from '../actions/ui.actions';
import { Era, WithEra, YearCode } from '@app/types/terms';
import { Year, PlannedTerm } from '@degree-planner/objects/plan';

const emptyTerm = (roadmapId: number, termCode: WithEra): PlannedTerm => {
  return {
    roadmapId,
    termCode,
    transferred: [],
    enrolled: [],
    planned: [],
  };
};

const emptyYear = (roadmapId: number, yearCode: YearCode): Year => {
  return {
    yearCode,
    fall: emptyTerm(
      roadmapId,
      WithEra.fromTermCodeAndEra(Era.Future, YearCode.toFall(yearCode)),
    ),
    spring: emptyTerm(
      roadmapId,
      WithEra.fromTermCodeAndEra(Era.Future, YearCode.toSpring(yearCode)),
    ),
    summer: emptyTerm(
      roadmapId,
      WithEra.fromTermCodeAndEra(Era.Future, YearCode.toSummer(yearCode)),
    ),
  };
};

export const addAcademicYear = on<
  DegreePlannerState,
  [typeof actions.addAcademicYear]
>(actions.addAcademicYear, (state, { yearCode }) => {
  return produce(state, draft => {
    if (
      draft.isLoadingPlan === false &&
      draft.visibleDegreePlan !== undefined
    ) {
      const nextYear = emptyYear(draft.visibleDegreePlan, yearCode);
      (draft.visibleYears[YearCode.encode(yearCode)] as any) = nextYear;
    }
  });
});

export const toggleCourseSearch = on<
  DegreePlannerState,
  [typeof actions.toggleCourseSearch]
>(actions.toggleCourseSearch, (state, { termCode }) => {
  return produce(state, draft => {
    draft.search.visible = !draft.search.visible;

    if (termCode) {
      draft.search.selectedTerm = termCode;
    }
  });
});

export const openCourseSearch = on<
  DegreePlannerState,
  [typeof actions.openCourseSearch]
>(actions.openCourseSearch, (state, { termCode }) => {
  return produce(state, draft => {
    draft.search.visible = true;

    if (termCode) {
      draft.search.selectedTerm = termCode;
    }
  });
});

export const closeCourseSearch = on<
  DegreePlannerState,
  [typeof actions.closeCourseSearch]
>(actions.closeCourseSearch, state => {
  return produce(state, draft => {
    draft.search.visible = false;
  });
});

export const updateSearchTermCode = on<
  DegreePlannerState,
  [typeof actions.updateSearchTermCode]
>(actions.updateSearchTermCode, (state, { termCode }) => {
  return produce(state, draft => {
    draft.search.selectedTerm = termCode;
  });
});

export const openSidenav = on<DegreePlannerState, [typeof actions.openSidenav]>(
  actions.openSidenav,
  state => {
    return produce(state, draft => {
      draft.isSidenavOpen = true;
    });
  },
);

export const closeSidenav = on<
  DegreePlannerState,
  [typeof actions.closeSidenav]
>(actions.closeSidenav, state => {
  return produce(state, draft => {
    draft.isSidenavOpen = false;
  });
});
