import * as planActions from '@app/degree-planner/store/actions/plan.actions';
import * as noteActions from '@app/degree-planner/store/actions/note.actions';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class ErrorEffects {
  constructor(private actions$: Actions, private snackBar: MatSnackBar) {}

  @Effect({ dispatch: false })
  error$ = this.actions$.pipe(
    ofType(planActions.planError, noteActions.noteError),

    tap(action => {
      const message = action.message;
      this.snackBar.open(message);
    }),
  );
}
