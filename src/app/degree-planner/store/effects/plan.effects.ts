import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import {
  tap,
  map,
  mergeMap,
  withLatestFrom,
  catchError,
  filter,
  first,
} from 'rxjs/operators';
import { State } from '@app/degree-planner/store/state';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DegreePlannerApiService } from '@app/degree-planner/services/api.service';
import * as selectors from '@app/degree-planner/store/selectors';
import * as planActions from '@app/degree-planner/store/actions/plan.actions';
import { RawPlan } from '@app/types/plans';
import {
  YearMapping,
  Year,
  PlannedTerm,
  PlannedTermNote,
} from '@degree-planner/objects/plan';
import { RawNote } from '@degree-planner/objects/plan';
import { CourseBase } from '@app/core/models/course';
import { YearCode, TermCode, WithEra, Era } from '@app/types/terms';
import { unique, forkJoinWithKeys, groupBy } from '@app/core/utils';
import { prefs as prefActions } from '@app/actions';
import {
  prefs as prefSelectors,
  terms as termsSelectors,
} from '@app/selectors';
import { isSuccess } from 'loadable.ts';
import { AnalyticsService } from '@app/services/analytics.service';
import * as globalSelectors from '@app/selectors';
import {
  EnrolledCourse,
  FavoriteCourse,
  PlannedCourse,
  TransferredCourse,
} from '@app/degree-planner/objects/courses';
import { ApiService } from '@app/services/api.service';
import { CreditRange } from '@app/types/credits';

const pickDegreePlanById = (roadmapId: number, plans: RawPlan[]): RawPlan => {
  const plan = plans.find(p => p.roadmapId === roadmapId);
  return plan ? plan : plans[0];
};

const pickPrimaryDegreePlan = (plans: RawPlan[]): RawPlan => {
  const primary = plans.find(plan => plan.primary);
  return primary ? primary : plans[0];
};

const matchesTermCode =
  (termCode: TermCode) => (thing: { termCode: string }) => {
    return thing.termCode === TermCode.encode(termCode);
  };

const buildTerm = (
  roadmapId: number,
  termCode: WithEra,
  notes: Array<RawNote>,
  courses: Array<{
    termCode: string;
    courses: Array<CourseBase>;
  }>,
): PlannedTerm => {
  const baseNote = notes.find(matchesTermCode(termCode));
  const note: PlannedTermNote | undefined = baseNote
    ? { isLoaded: true, text: baseNote.note, id: baseNote.id }
    : undefined;
  const group = courses.find(matchesTermCode(termCode));

  const groupCourses = (fn: CourseBase) => {
    switch (fn.studentEnrollmentStatus) {
      case 'Transfer':
        return 'transferred';
      case 'Enrolled':
      case 'Waitlisted':
        return 'enrolled';
      default:
        if (typeof fn.id === 'number') {
          return 'planned';
        } else {
          return 'unknown';
        }
    }
  };

  const grouped = groupBy(group?.courses ?? [], groupCourses);

  const transferred = (grouped.transferred ?? []).map(c => {
    return new TransferredCourse({
      termCode,
      name: `${c.subjectDescription} ${c.catalogNumber}`,
      title: c.title,
      creditValue: c.credits ?? 0,
    });
  });

  const enrolled = (grouped.enrolled ?? []).map(c => {
    return new EnrolledCourse({
      termCode,
      name: `${c.subjectDescription} ${c.catalogNumber}`,
      title: c.title,
      subjectCode: c.subjectCode,
      courseId: c.courseId,
      creditValue: c.credits ?? 0,
      grade: c.grade ?? null,
      isWaitlisted: c.studentEnrollmentStatus === 'Waitlisted',
    });
  });

  const toCreditValue = (range: CreditRange, credits: unknown) => {
    if (
      WithEra.isActive(termCode) &&
      typeof credits === 'number' &&
      credits >= range.min &&
      credits <= range.max
    ) {
      return credits;
    }

    return null;
  };

  const planned = (grouped.planned ?? []).map(c => {
    const creditRange = { min: c.creditMin ?? 0, max: c.creditMax ?? 0 };
    const creditValue = toCreditValue(creditRange, c.credits);
    return new PlannedCourse({
      termCode,
      name: `${c.subjectDescription} ${c.catalogNumber}`,
      title: c.title,
      subjectCode: c.subjectCode,
      courseId: c.courseId,
      creditRange,
      creditValue,
      plannedCourseId: c.id!,
      warning: PlannedCourse.warningFromStatus(c.studentEnrollmentStatus),
      hasClassNumber: typeof c.classNumber === 'string',
    });
  });

  return {
    roadmapId,
    termCode,
    note,
    transferred,
    enrolled,
    planned,
  };
};

const minYearCode = <T extends YearCode>(years: T[]): T => {
  let min = years[0];
  for (const year of years) {
    if (YearCode.comesBefore(year, min)) {
      min = year;
    }
  }
  return min;
};

const maxYearCode = <T extends YearCode>(years: T[]): T => {
  let max = years[0];
  for (const year of years) {
    if (YearCode.comesAfter(year, max)) {
      max = year;
    }
  }
  return max;
};

const yearCodeRange = (from: YearCode, to: YearCode): YearCode[] => {
  const years: YearCode[] = [];
  let year = from;
  while (YearCode.comesAfter(to, year) || YearCode.equals(to, year)) {
    years.push(year);
    year = YearCode.next(year);
  }

  return years;
};

const isEmptyPastTerm = (term: PlannedTerm): boolean => {
  const isPast = term.termCode.era === Era.Past;
  const isEmpty =
    !term.note && term.planned.length === 0 && term.enrolled.length === 0;
  return isPast && isEmpty;
};

const isEmptyPastYear = (year: Year): boolean => {
  return (
    isEmptyPastTerm(year.fall) &&
    isEmptyPastTerm(year.spring) &&
    isEmptyPastTerm(year.summer)
  );
};

const loadPlanYears = (
  api: DegreePlannerApiService,
  roadmapId: number,
  active: TermCode[],
): Observable<YearMapping> => {
  const notesAndCourses$ = forkJoinWithKeys({
    notes: api.getAllNotes(roadmapId),
    courses: api.getAllTermCourses(roadmapId),
  });

  const allYearCodes$ = notesAndCourses$.pipe(
    map(({ notes, courses }) => {
      const noteTermCodes = notes.map(note => note.termCode);
      const courseTermCodes = courses.map(course => course.termCode);
      const allTermCodes = [
        ...noteTermCodes.map(TermCode.decodeOrThrow),
        ...courseTermCodes.map(TermCode.decodeOrThrow),
        ...active,
      ].map(WithEra.fromTermCodeCurry(active));
      const uniqueYearCodes = unique(allTermCodes.map(YearCode.encode)).map(
        YearCode.decodeOrThrow,
      );

      const first = minYearCode(uniqueYearCodes);
      const last = maxYearCode(uniqueYearCodes);
      const allYearCodes = yearCodeRange(first, last);

      return {
        allYearCodes,
        notes,
        courses,
      };
    }),
  );

  const visibleYears$ = allYearCodes$.pipe(
    map(({ allYearCodes, notes, courses }) => {
      const mapping: YearMapping = {};
      allYearCodes
        .map(yearCode => {
          const { fall, spring, summer } = WithEra.fromYearCode(
            active,
            yearCode,
          );
          const isExpanded = !(
            fall.era === Era.Past &&
            spring.era === Era.Past &&
            summer.era === Era.Past
          );
          return {
            yearCode,
            isExpanded,
            fall: buildTerm(roadmapId, fall, notes, courses),
            spring: buildTerm(roadmapId, spring, notes, courses),
            summer: buildTerm(roadmapId, summer, notes, courses),
          } as Year;
        })
        .filter(year => !isEmptyPastYear(year))
        .forEach(year => (mapping[YearCode.encode(year.yearCode)] = year));

      return mapping as YearMapping;
    }),
  );

  return visibleYears$;
};

@Injectable()
export class DegreePlanEffects {
  constructor(
    private actions$: Actions,
    private api: DegreePlannerApiService,
    private appApi: ApiService,
    private store$: Store<State>,
    private snackBar: MatSnackBar,
    private analytics: AnalyticsService,
  ) {}

  @Effect()
  init$ = this.actions$.pipe(
    ofType(planActions.initialLoadRequest),
    // Load the list of degree plans and data used by all degree plans.
    mergeMap(() => {
      return forkJoinWithKeys({
        allDegreePlans: this.api.getAllDegreePlans(),
        activeTermCodes: this.store$
          .select(globalSelectors.terms.getActiveTermCodes)
          .pipe(
            filter(isSuccess),
            map(loadable => loadable.value),
            first(),
          ),
      });
    }),

    withLatestFrom(
      this.store$.select(prefSelectors.getKey, 'degreePlannerSelectedPlan'),
      this.store$.select(termsSelectors.getZeroTerm),
    ),
    mergeMap(
      ([
        { allDegreePlans, activeTermCodes },
        degreePlannerSelectedPlan,
        zeroTerm,
      ]) => {
        const savedForLaterCourses = this.appApi.getFavorites().pipe(
          map(courses => {
            return courses.map((saved): FavoriteCourse => {
              const subjects = isSuccess(zeroTerm)
                ? zeroTerm.value.subjects
                : [];
              const subject = subjects.find(s => s.code === saved.subjectCode);
              const subjectName = subject?.name ?? 'UNKNOWN';
              return new FavoriteCourse({
                name: `${subjectName} ${saved.catalogNumber}`,
                title: saved.title,
                subjectCode: saved.subjectCode,
                courseId: saved.courseId,
              });
            });
          }),
        );

        const visibleDegreePlan =
          typeof degreePlannerSelectedPlan === 'number'
            ? pickDegreePlanById(degreePlannerSelectedPlan, allDegreePlans)
            : pickPrimaryDegreePlan(allDegreePlans);
        const visibleYears = loadPlanYears(
          this.api,
          visibleDegreePlan.roadmapId,
          activeTermCodes,
        );

        return forkJoinWithKeys({
          visibleDegreePlan: of(visibleDegreePlan.roadmapId),
          visibleYears,
          savedForLaterCourses,
          allDegreePlans: of(allDegreePlans),
        }).pipe(
          map(payload => {
            return planActions.initialLoadSuccess({
              payload: {
                ...payload,
                isLoadingPlan: false,
              },
            });
          }),
        );
      },
    ),
    catchError(error => {
      console.error(error);
      return of(
        planActions.planError({
          message: 'Error loading data. Please reload to try again.',
          duration: 2000,
          error,
        }),
      );
    }),
  );

  @Effect()
  switch$ = this.actions$.pipe(
    ofType(planActions.switchPlan),
    withLatestFrom(this.store$.select(selectors.selectAllDegreePlans)),
    mergeMap(([action, allDegreePlans]) => {
      return this.store$.select(globalSelectors.terms.getActiveTermCodes).pipe(
        filter(isSuccess),
        map(loadable => [action, allDegreePlans, loadable.value] as const),
        first(),
      );
    }),
    mergeMap(([action, allDegreePlans, activeTermCodes]) => {
      const visibleDegreePlan = allDegreePlans.find(plan => {
        return plan.roadmapId === action.newVisibleRoadmapId;
      }) as RawPlan;

      const visibleYears = loadPlanYears(
        this.api,
        visibleDegreePlan.roadmapId,
        activeTermCodes,
      );

      return forkJoinWithKeys({
        visibleDegreePlan: of(visibleDegreePlan),
        visibleYears,
      });
    }),
    mergeMap(payload => [
      planActions.switchPlanSuccess(payload),
      prefActions.setKey({
        key: 'degreePlannerSelectedPlan',
        value: payload.visibleDegreePlan.roadmapId,
      }),
    ]),
    tap(() => this.analytics.event('Planner', 'switch-plan')),
    tap(state => {
      if (state.type === 'plan/switchPlanSuccess') {
        const touchedPlan = state.visibleDegreePlan.name;
        const message = `Switched to ${touchedPlan}`;
        this.snackBar.open(message, undefined, {});
      }
    }),
    catchError(error => {
      return of(
        planActions.planError({
          message: 'Unable to switch plan',
          duration: 2000,
          error,
        }),
      );
    }),
  );

  @Effect()
  ChangePlanName$ = this.actions$.pipe(
    ofType(planActions.changePlanName),
    withLatestFrom(this.store$.select(selectors.selectAllDegreePlans)),
    mergeMap(([{ roadmapId, newName }, allDegreePlans]) => {
      const oldDegreePlan = allDegreePlans.find(plan => {
        return plan.roadmapId === roadmapId;
      }) as RawPlan;
      const oldName = oldDegreePlan.name;

      return this.api
        .updatePlan(roadmapId, newName, oldDegreePlan.primary)
        .pipe(
          tap(() => this.analytics.event('Planner', 'rename-plan')),
          map(() => {
            return planActions.changePlanNameSuccess({ roadmapId, newName });
          }),
          tap(() => {
            const message = `Plan has been renamed to ${newName}`;
            this.snackBar.open(message, undefined, {});
          }),
          catchError(() => {
            return of(
              planActions.changePlanNameFailure({ roadmapId, oldName }),
            );
          }),
        );
    }),
  );

  @Effect()
  createPlan$ = this.actions$.pipe(
    ofType(planActions.createPlan),
    mergeMap(action => {
      return this.store$.select(globalSelectors.terms.getActiveTermCodes).pipe(
        filter(isSuccess),
        map(loadable => [action, loadable.value] as const),
        first(),
      );
    }),
    mergeMap(([{ name, primary }, activeTermCodes]) => {
      return this.api.createDegreePlan(name, primary).pipe(
        tap(() => this.analytics.event('Planner', 'create-plan')),
        mergeMap(newPlan => {
          const newYears = loadPlanYears(
            this.api,
            newPlan.roadmapId,
            activeTermCodes,
          );

          return forkJoinWithKeys({
            newPlan: of(newPlan),
            newYears,
          });
        }),
        mergeMap(({ newPlan, newYears }) => [
          planActions.createPlanSuccess({ newPlan, newYears }),
          prefActions.setKey({
            key: 'degreePlannerSelectedPlan',
            value: newPlan.roadmapId,
          }),
        ]),
        tap(() => {
          const message = `New plan has been created`;
          this.snackBar.open(message, undefined, {});
        }),
        catchError(error => {
          return of(
            planActions.planError({
              message: 'Unable to create new plan',
              duration: 2000,
              error,
            }),
          );
        }),
      );
    }),
  );

  @Effect()
  deletePlan$ = this.actions$.pipe(
    ofType(planActions.deletePlan),
    mergeMap(({ roadmapId }) => {
      return this.api.deleteDegreePlan(roadmapId).pipe(
        tap(() => this.analytics.event('Planner', 'remove-plan')),
        map(() => planActions.deletePlanSuccess({ roadmapId })),
        tap(() => {
          const message = `Deleting selected plan`;
          this.snackBar.open(message, undefined, { duration: 10000 });
        }),
        catchError(error => {
          return of(
            planActions.planError({
              message: 'Unable to delete plan',
              duration: 2000,
              error,
            }),
          );
        }),
      );
    }),
  );
}
