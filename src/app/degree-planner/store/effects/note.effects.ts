import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { combineLatest, of } from 'rxjs';
import {
  mergeMap,
  withLatestFrom,
  tap,
  map,
  filter,
  catchError,
} from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Year, YearMapping } from '@degree-planner/objects/plan';
import { DegreePlannerApiService } from '@app/degree-planner/services/api.service';
import * as noteActions from '@app/degree-planner/store/actions/note.actions';
import * as selectors from '@app/degree-planner/store/selectors';
import { State } from '@app/degree-planner/store/state';
import { TermCode, YearCode } from '@app/types/terms';
import { isntUndefined } from '@app/core/utils';

const getExistingNote = (years: YearMapping, termCode: TermCode) => {
  const year: Year | undefined = years[YearCode.encode(termCode)];

  if (year) {
    return year[TermCode.lowerTermName(termCode)].note;
  } else {
    return undefined;
  }
};

@Injectable()
export class NoteEffects {
  private currentPlan$ = combineLatest([
    this.store$.select(selectors.selectAllDegreePlans),
    this.store$.select(selectors.selectVisibleDegreePlan),
  ]).pipe(
    map(([plans, planId]) => plans.find(p => p.roadmapId === planId)),
    filter(isntUndefined),
  );

  constructor(
    private actions$: Actions,
    private store$: Store<State>,
    private api: DegreePlannerApiService,
    private snackBar: MatSnackBar,
  ) {}

  @Effect()
  write$ = this.actions$.pipe(
    ofType(noteActions.writeNote),

    // Get the most recent Degree Planner state object from the store. This is
    // used to decide to fire either the `updateNote` API or `createNote` API.
    withLatestFrom(
      this.store$.select(selectors.selectAllVisibleYears),
      this.currentPlan$,
    ),

    // Using the action and State objects, determine whether to fire the
    // `updateNote` or `createNote` API. Both of these API calls return
    // an observable wrapper around the modified/created Note object.
    mergeMap(([action, years, plan]) => {
      const planId = plan.roadmapId;
      const termCode = action.termCode;
      const noteText = action.noteText;
      const existingNote = getExistingNote(years, termCode);
      if (existingNote !== undefined && existingNote.isLoaded) {
        // Since the term DOES have a note, update the existing note
        const noteId = existingNote.id;
        return this.api
          .updateNote(planId, termCode, noteText, noteId)
          .pipe(
            map(updatedNote =>
              noteActions.writeNoteSuccess({ termCode, updatedNote }),
            ),
          );
      } else {
        // Since the term DOES NOT have a note, create a new note
        return this.api
          .createNote(planId, termCode, noteText)
          .pipe(
            map(updatedNote =>
              noteActions.writeNoteSuccess({ termCode, updatedNote }),
            ),
          );
      }
    }),

    tap(() => {
      const message = 'Note has been saved';
      this.snackBar.open(message, undefined, {});
    }),

    catchError(error => {
      return of(
        noteActions.noteError({
          message: 'Unable to save note',
          duration: 2000,
          error,
        }),
      );
    }),
  );

  @Effect()
  delete$ = this.actions$.pipe(
    ofType(noteActions.deleteNote),

    // Get the most recent Degree Planner state object.
    // This is used to lookup the Note ID.
    withLatestFrom(this.currentPlan$),

    // Using the action and State objects, fire the `deleteNote` API.
    mergeMap(([action, plan]) => {
      const planId = plan.roadmapId;
      const { termCode, noteId } = action;
      return this.api.deleteNote(planId, noteId).pipe(map(() => termCode));
    }),

    // Dispatch an `DeleteNoteSuccess` action so that the
    // State object can be updated with the note removed.
    map(termCode => noteActions.deleteNoteSuccess({ termCode })),

    tap(() => {
      const message = 'Note has been deleted';
      this.snackBar.open(message, undefined, {});
    }),

    catchError(error => {
      return of(
        noteActions.noteError({
          message: 'Unable to remove note',
          duration: 2000,
          error,
        }),
      );
    }),
  );
}
