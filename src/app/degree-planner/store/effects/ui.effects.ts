import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as actions from '@app/degree-planner/store/actions/ui.actions';
import { tap } from 'rxjs/operators';
import { YearCode } from '@app/types/terms';
import { AnalyticsService } from '@app/services/analytics.service';

@Injectable()
export class UIEffects {
  constructor(
    private actions$: Actions,
    private snackBar: MatSnackBar,
    private analytics: AnalyticsService,
  ) {}

  addNextYear$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.addAcademicYear),
        tap(() => this.analytics.event('Planner', 'add-year')),
        tap(({ yearCode }) => {
          this.snackBar.open(
            `Added ${YearCode.describe(yearCode)} to the degree plan`,
          );
        }),
      ),
    { dispatch: false },
  );
}
