import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of, forkJoin, combineLatest, from, Observable } from 'rxjs';
import {
  tap,
  map,
  mergeMap,
  withLatestFrom,
  filter,
  catchError,
} from 'rxjs/operators';
import { DegreePlannerState, State } from '@app/degree-planner/store/state';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as selectors from '@app/degree-planner/store/selectors';
import * as actions from '@degree-planner/store/actions/course.actions';
import { TermCode, Era, YearCode } from '@app/types/terms';
import { flatFilter, isntUndefined } from '@app/core/utils';
import { AnalyticsService, KnownEvents } from '@app/services/analytics.service';
import { ApiService } from '@app/services/api.service';
import { CampusCourse } from '@app/types/courses';
import { DialogService } from '@app/shared/services/dialog.service';
import { YearMapping } from '@app/degree-planner/objects/plan';
import {
  IsPending,
  MaybeIsPending,
  PendingFavoriteCourse,
  PendingPlannedCourse,
} from '@app/degree-planner/objects/courses';
import { AriaLivePoliteness } from '@angular/cdk/a11y';

@Injectable()
export class CourseEffects {
  private _favorites$ = this.store.select(selectors.getSavedForLaterCourses);

  private _years$ = this.store.select(selectors.selectAllVisibleYears);

  private _plan$ = combineLatest([
    this.store.select(selectors.selectAllDegreePlans),
    this.store.select(selectors.selectVisibleDegreePlan),
  ]).pipe(
    map(([plans, planId]) => plans.find(p => p.roadmapId === planId)),
    filter(isntUndefined),
  );

  /**
   * Abort the effect if the course being favorited for later has the same
   * `subjectCode` and `courseId` as a course already favorited. If this
   * condition is met, display a dialog so the user is aware of the reasoning.
   */
  private _preventDuplicateFavoriteCourses = <
    Action extends { toAdd: PendingFavoriteCourse },
  >() => {
    return filter<[Action, DegreePlannerState['savedForLaterCourses']]>(
      ([{ toAdd }, favorites]) => {
        if (favorites.some(sameCampusCourseButDifferentPendingId(toAdd))) {
          this.dialog.acknowledge({
            headline: `Duplicate course`,
            reasoning: [`${toAdd} is already saved for later.`],
          });
          return false;
        }

        return true;
      },
    );
  };

  /**
   * Abort the effect if the course is being added to a term that already has a
   * course with the same `subjectCode` and `courseId`. If this condition is
   * met, display a dialog so the user is aware of the reasoning.
   */
  private _preventDuplicateTermCourses = <
    Action extends { toAdd: PendingPlannedCourse },
  >() => {
    return filter<[Action, DegreePlannerState['visibleYears']]>(
      ([{ toAdd }, years]) => {
        const planned = findTerm(years, toAdd.termCode)?.planned ?? [];
        if (planned.some(sameCampusCourseButDifferentPendingId(toAdd))) {
          const where = TermCode.describe(toAdd.termCode);
          this.dialog.acknowledge({
            headline: `Duplicate course`,
            reasoning: [`${toAdd} is already planned for ${where}.`],
          });
          return false;
        }

        return true;
      },
    );
  };

  /**
   * A wrapper around the `AnalyticsService.event` method. If the `label`
   * argument is a term-code this function will encode the term-code before
   * emitting the analytics event.
   */
  private _analyticsEvent = <Category extends keyof KnownEvents, T>(
    category: Category,
    action: KnownEvents[Category],
    label?: string | TermCode,
  ) => {
    return tap<T>(() => {
      if (TermCode.schema.matches(label)) {
        this.analytics.event(category, action, TermCode.encode(label));
      } else {
        this.analytics.event(category, action, label);
      }
    });
  };

  /**
   * **Each successful effect should have a corresponding announcement.**
   *
   * Screen-readers should be told when course changes take effect. This
   * function wraps the Material `MatSnackBar`.
   */
  private _announce = <T>(
    message: string,
    politeness: AriaLivePoliteness = 'assertive',
  ) => tap<T>(() => this.snackBar.open(message, undefined, { politeness }));

  constructor(
    private actions$: Actions,
    private api: ApiService,
    private store: Store<State>,
    private snackBar: MatSnackBar,
    private analytics: AnalyticsService,
    private dialog: DialogService,
  ) {}

  addToFavorites$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.addToFavorites),
      withLatestFrom(this._favorites$),
      this._preventDuplicateFavoriteCourses(),
      mergeMap(([{ toAdd }]) => {
        const ifOk = actions.addToFavoritesDone({
          toRemove: toAdd,
          toAdd: toAdd.realize(),
        });

        const ifErr = actions.addToFavoritesCancel({ toRemove: toAdd });

        const { subjectCode, courseId } = toAdd;
        return this.api.postFavorite(subjectCode, courseId).pipe(
          this._analyticsEvent('Favorites', 'add-course'),
          this._announce(`${toAdd} has been saved for later`),
          map(() => ifOk),
          catchError(() => of(ifErr)),
        );
      }),
    ),
  );

  removeFromFavorites$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.removeFromFavorites),
      mergeMap(({ toRemove, toAdd }) => {
        const ifOk = actions.removeFromFavoritesDone({ toRemove: toAdd });

        const ifErr = actions.removeFromFavoritesCancel({
          toRemove: toAdd,
          toAdd: toRemove,
        });

        return from(
          this.dialog.confirm({
            headline: `Remove course?`,
            reasoning: [
              `This will remove ${toRemove} from your saved courses.`,
            ],
            affirmative: 'Remove',
          }),
        ).pipe(
          mergeMap(shouldRemove => {
            if (!shouldRemove) {
              return of(ifErr);
            }

            const payload = {
              subject: { code: toRemove.subjectCode },
              courseId: toRemove.courseId,
            };

            return this.api.deleteFavorite(payload).pipe(
              map(() => ifOk),
              catchError(() => of(ifErr)),
            );
          }),
        );
      }),
    ),
  );

  addToTerm$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.addToTerm),
      withLatestFrom(this._years$),
      this._preventDuplicateTermCourses(),
      withLatestFrom(this._plan$),
      mergeMap(([[{ toAdd }], plan]) => {
        const payload = {
          termCode: toAdd.termCode,
          subject: { code: toAdd.subjectCode },
          courseId: toAdd.courseId,
        };

        const addToCart =
          toAdd.termCode.era === Era.Active && plan.primary
            ? this.api.postRoadmapCourseWithoutPackage(payload)
            : this.api.postPlanCourse(plan.roadmapId, payload);

        const ifErr = actions.addToTermCancel({ toRemove: toAdd });

        return addToCart.pipe(
          this._analyticsEvent('Planner', 'add-course', toAdd.termCode),
          this._announce(
            `${toAdd} has been added to ${TermCode.describe(toAdd.termCode)}`,
          ),
          map(({ id, creditMin, creditMax, studentEnrollmentStatus }) => {
            const creditRange = { min: creditMin, max: creditMax };
            return actions.addToTermDone({
              toRemove: toAdd,
              toAdd: toAdd.realize(id, creditRange, studentEnrollmentStatus),
            });
          }),
          catchError(() => of(ifErr)),
        );
      }),
    ),
  );

  removeFromTerm$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.removeFromTerm),
      withLatestFrom(this._plan$),
      mergeMap(([{ toRemove, toAdd }, plan]) => {
        const ifOk = actions.removeFromTermDone({ toRemove: toAdd });

        const ifErr = actions.removeFromTermCancel({
          toRemove: toAdd,
          toAdd: toRemove,
        });

        const where = TermCode.describe(toRemove.termCode);
        return from(
          this.dialog.confirm({
            headline: `Remove course?`,
            reasoning: [`This will remove ${toRemove} from ${where}.`],
            affirmative: 'Remove',
          }),
        ).pipe(
          mergeMap(shouldRemove => {
            if (!shouldRemove) {
              return of(ifErr);
            }

            const where = TermCode.describe(toRemove.termCode);
            const announcement = `${toRemove} removed from ${where}`;

            return this.api.deletePlanCourse(plan.roadmapId, toRemove).pipe(
              this._analyticsEvent(
                'Planner',
                'remove-course',
                toRemove.termCode,
              ),
              this._announce(announcement),
              map(() => ifOk),
              catchError(() => of(ifErr)),
            );
          }),
        );
      }),
    ),
  );

  moveBetweenTerms$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.moveBetweenTerms),
      withLatestFrom(this._years$),
      this._preventDuplicateTermCourses(),
      withLatestFrom(this._plan$),
      mergeMap(([[{ toRemove, toAdd }], plan]) => {
        const moveToTerm = () => {
          if (toAdd.termCode.era === Era.Active && plan.primary) {
            // If the course is being added to an active term in the primary plan,
            // fire another API call to trigger a validation of the user's cart
            // for that term.
            return forkJoin([
              this.api.putPlanCourse(plan.roadmapId, toRemove, toAdd.termCode),
              this.api.postRoadmapCourseWithoutPackage({
                termCode: toAdd.termCode,
                subject: { code: toAdd.subjectCode },
                courseId: toAdd.courseId,
              }),
            ]).pipe(map(([{ studentEnrollmentStatus: status }]) => status));
          } else {
            // For terms that are non-active or not in the primary plan, updating
            // the course record is sufficient and no cart validation is needed.
            return this.api
              .putPlanCourse(plan.roadmapId, toRemove, toAdd.termCode)
              .pipe(map(({ studentEnrollmentStatus: status }) => status));
          }
        };

        const fromDescribed = TermCode.describe(toRemove.termCode);
        const toDescribed = TermCode.describe(toAdd.termCode);

        const confirmation = {
          headline: `Remove chosen section from ${toRemove}?`,
          reasoning: [
            `
              You have already chosen a section for ${toRemove} in ${fromDescribed}.
              Sections are term-specific so moving the course to ${toDescribed}
              will remove the chosen section from the course.
            `,
          ],
          affirmative: 'Move course',
        };
        const confirmSectionRemoval: Observable<boolean> =
          toRemove.hasClassNumber
            ? from(this.dialog.confirm(confirmation))
            : of(true);

        return confirmSectionRemoval.pipe(
          mergeMap(shouldRemove => {
            const ifOk = (status: string) =>
              actions.moveBetweenTermsDone({
                toRemove: toAdd,
                toAdd: toAdd.realize(
                  toRemove.plannedCourseId,
                  toRemove.creditRange,
                  status,
                ),
              });

            const ifErr = actions.moveBetweenTermsCancel({
              toRemove: toAdd,
              toAdd: toRemove,
            });

            if (shouldRemove === false) {
              return of(ifErr);
            }

            const fromEncoded = TermCode.encode(toRemove.termCode);
            const toEncoded = TermCode.encode(toAdd.termCode);
            const direction = `${fromEncoded} -> ${toEncoded}`;

            return moveToTerm().pipe(
              this._analyticsEvent('Planner', 'move-between-terms', direction),
              this._announce(`${toAdd} has been moved to ${toDescribed}`),
              map(ifOk),
              catchError(() => of(ifErr)),
            );
          }),
        );
      }),
    ),
  );

  moveToTermFromFavorites$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.moveToTermFromFavorites),
      withLatestFrom(this._years$),
      this._preventDuplicateTermCourses(),
      withLatestFrom(this._plan$),
      mergeMap(([[{ toAdd, toRemove }], plan]) => {
        const payload = {
          termCode: toAdd.termCode,
          subject: { code: toAdd.subjectCode },
          courseId: toAdd.courseId,
        };

        const shouldUpdateCart =
          toAdd.termCode.era === Era.Active && plan.primary;

        const transaction = forkJoin([
          shouldUpdateCart
            ? this.api.postRoadmapCourseWithoutPackage(payload)
            : this.api.postPlanCourse(plan.roadmapId, payload),
          this.api.deleteFavorite(payload),
        ]);

        const ifErr = actions.moveToTermFromFavoritesCancel({
          toRemove: toAdd,
          toAdd: toRemove,
        });

        return transaction.pipe(
          map(([{ id, creditMin, creditMax, studentEnrollmentStatus }]) => {
            return actions.moveToTermFromFavoritesDone({
              toRemove: toAdd,
              toAdd: toAdd.realize(
                id,
                { min: creditMin, max: creditMax },
                studentEnrollmentStatus,
              ),
            });
          }),
          catchError(() => of(ifErr)),
        );
      }),
    ),
  );

  moveToFavoritesFromTerm$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.moveToFavoritesFromTerm),
      withLatestFrom(this._favorites$),
      this._preventDuplicateFavoriteCourses(),
      flatFilter(([{ toRemove }]) => {
        if (toRemove.hasClassNumber) {
          return from(
            this.dialog.confirm({
              headline: `Remove selected section from ${toRemove}?`,
              reasoning: [
                `Moving ${toRemove} will remove your selected section.`,
              ],
              affirmative: 'Move course',
            }),
          );
        }

        return of(true);
      }),
      withLatestFrom(this._plan$),
      mergeMap(([[{ toAdd, toRemove }], plan]) => {
        const transaction = forkJoin([
          this.api.postFavorite(toAdd.subjectCode, toAdd.courseId),
          this.api.deletePlanCourse(plan.roadmapId, toRemove),
        ]);

        const ifOk = actions.moveToFavoritesFromTermDone({
          toRemove: toAdd,
          toAdd: toAdd.realize(),
        });

        const ifErr = actions.moveToFavoritesFromTermCancel({
          toRemove: toAdd,
          toAdd: toRemove,
        });

        return transaction.pipe(
          map(() => ifOk),
          catchError(() => of(ifErr)),
        );
      }),
    ),
  );
}

const sameCampusCourseButDifferentPendingId = (a: CampusCourse & IsPending) => {
  return (b: CampusCourse & MaybeIsPending): boolean => {
    // Because reducers run and change the state *before* the effect runs, if
    // the reducer thinks the course move should proceed, there may already be a
    // pending course in the course list with a matching subjectCode and
    // courseId.
    //
    // Therefore should only fail this check if courses a and b ALSO have
    // DIFFERENT pendingIds. This check will IGNORE the case where the matching
    // course item is just the pending record for this course.
    return (
      a.subjectCode === b.subjectCode &&
      a.courseId === b.courseId &&
      a.pendingId !== b.pendingId
    );
  };
};

const findTerm = (years: YearMapping, termCode: TermCode) => {
  const yearKey = YearCode.encode(termCode);
  const year = years[yearKey];

  if (!year) {
    return null;
  }

  const termKey = TermCode.lowerTermName(termCode);
  return year[termKey];
};
