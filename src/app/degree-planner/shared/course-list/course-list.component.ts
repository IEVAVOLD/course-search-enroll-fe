import {
  AfterContentInit,
  Component,
  ContentChildren,
  QueryList,
} from '@angular/core';
import {
  CreditRange,
  HasCreditRange,
  HasCreditValue,
} from '@app/types/credits';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, startWith } from 'rxjs/operators';
import { CourseItemComponent } from '@degree-planner/shared/course-item/course-item.component';

@Component({
  selector: 'ol[cse-course-list], ul[cse-course-list]',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss'],
})
export class CourseListComponent implements AfterContentInit {
  @ContentChildren(CourseItemComponent) items!: QueryList<CourseItemComponent>;

  public credits!: Observable<CreditRange>;

  ngAfterContentInit() {
    this.credits = this.items.changes.pipe(
      startWith(this.items),
      map((items: QueryList<CourseItemComponent>) => {
        const sum: CreditRange = { min: 0, max: 0 };
        for (const item of items.toArray()) {
          if (HasCreditValue.matches(item.course)) {
            sum.min += item.course.creditValue;
            sum.max += item.course.creditValue;
          } else if (HasCreditRange.matches(item.course)) {
            sum.min += item.course.creditRange.min;
            sum.max += item.course.creditRange.max;
          }
        }
        return sum;
      }),
      distinctUntilChanged((a, b) => a.min === b.min && a.max === b.max),
    );
  }
}
