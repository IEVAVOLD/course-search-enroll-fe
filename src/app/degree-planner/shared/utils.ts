import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { YearMapping } from '@degree-planner/objects/plan';
import { TermCode, Era } from '@app/types/terms';

export const yearsToDropZoneIds = () => (years$: Observable<YearMapping>) => {
  return years$.pipe(
    map(years => {
      const yearCodes = Object.keys(years);
      const termCodes = yearCodes.reduce<string[]>((acc, yearCode) => {
        if (years[yearCode].fall.termCode.era !== Era.Past) {
          acc = acc.concat(TermCode.encode(years[yearCode].fall.termCode));
        }

        if (years[yearCode].spring.termCode.era !== Era.Past) {
          acc = acc.concat(TermCode.encode(years[yearCode].spring.termCode));
        }

        if (years[yearCode].summer.termCode.era !== Era.Past) {
          acc = acc.concat(TermCode.encode(years[yearCode].summer.termCode));
        }

        return acc;
      }, []);

      const termIds = [
        'saved-courses',
        ...termCodes.map(termCode => `term-${termCode}`),
      ];
      return termIds;
    }),
  );
};

export const compareArrays = <T>(cmp: (a: T, b: T) => boolean) => {
  return (a: T[], b: T[]): boolean => {
    if (a.length === b.length) {
      return a.every((elem, i) => cmp(elem, b[i]));
    } else {
      return false;
    }
  };
};

export const compareStringArrays = compareArrays<string>((a, b) => a === b);

export const yearsToDroppableTermCodes =
  () => (years$: Observable<YearMapping>) => {
    return years$.pipe(
      map(years => {
        const yearCodes = Object.keys(years);
        return yearCodes.reduce<TermCode[]>((acc, yearCode) => {
          if (years[yearCode].fall.termCode.era !== Era.Past) {
            acc = acc.concat(years[yearCode].fall.termCode);
          }

          if (years[yearCode].spring.termCode.era !== Era.Past) {
            acc = acc.concat(years[yearCode].spring.termCode);
          }

          if (years[yearCode].summer.termCode.era !== Era.Past) {
            acc = acc.concat(years[yearCode].summer.termCode);
          }

          return acc;
        }, []);
      }),
    );
  };
