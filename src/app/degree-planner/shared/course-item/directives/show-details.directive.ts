import { Directive, HostListener, Inject } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CourseDetailsDialogComponent } from '@app/degree-planner/dialogs/course-details-dialog/course-details-dialog.component';
import { PlannedCourse } from '@app/degree-planner/objects/courses';
import { DialogService } from '@app/shared/services/dialog.service';
import { CourseItemComponent, COURSE_ITEM } from '../course-item.component';

@Directive({
  selector: '[cseShowDetails]',
  host: {
    '[style.cursor]': `'pointer'`,
  },
})
export class ShowDetailsForDirective {
  constructor(
    private dialog: DialogService,
    private snackbar: MatSnackBar,
    @Inject(COURSE_ITEM) private courseItem: CourseItemComponent,
  ) {}

  @HostListener('click')
  async handleClick() {
    const course = this.courseItem.course;

    if (course instanceof PlannedCourse && course.warning === 'doesNotExist') {
      this.snackbar.open('This course is no longer offered');
    } else {
      await this.dialog.large(CourseDetailsDialogComponent, course);

      // If this directive is inside of a <cse-course-item> component, focus on
      // that component after the dialog closes. This will help keep the DOM
      // focus nearby.
      this.courseItem.focus();
    }
  }
}
