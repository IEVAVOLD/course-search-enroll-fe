import { Directive, HostBinding, HostListener, Inject } from '@angular/core';
import { CourseItemComponent, COURSE_ITEM } from '../course-item.component';
import { GlobalState } from '@app/state';
import { Store } from '@ngrx/store';
import * as actions from '@degree-planner/store/actions/course.actions';
import {
  PendingPlannedCourse,
  PlannedCourse,
} from '@app/degree-planner/objects/courses';

@Directive({ selector: 'button[cseRemoveFromTerm]' })
export class RemoveFromTermDirective {
  @HostBinding('disabled')
  get disabled() {
    return !(this.courseItem.course instanceof PlannedCourse);
  }

  constructor(
    private store: Store<GlobalState>,
    @Inject(COURSE_ITEM) private courseItem: CourseItemComponent,
  ) {}

  @HostListener('click')
  handleClick() {
    if (this.courseItem.course instanceof PlannedCourse) {
      this.store.dispatch(
        actions.removeFromTerm({
          toRemove: this.courseItem.course,
          toAdd: new PendingPlannedCourse(this.courseItem.course),
        }),
      );
    }
  }
}
