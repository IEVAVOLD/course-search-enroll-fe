import { Directive, HostBinding, HostListener, Inject } from '@angular/core';
import { CourseItemComponent, COURSE_ITEM } from '../course-item.component';
import { GlobalState } from '@app/state';
import { Store } from '@ngrx/store';
import * as actions from '@degree-planner/store/actions/course.actions';
import {
  PendingFavoriteCourse,
  SearchCourse,
} from '@app/degree-planner/objects/courses';

@Directive({ selector: 'button[cseAddToFavorites]' })
export class AddToFavoritesDirective {
  @HostBinding('disabled')
  get disabled() {
    return !(this.courseItem.course instanceof SearchCourse);
  }

  constructor(
    private store: Store<GlobalState>,
    @Inject(COURSE_ITEM) private courseItem: CourseItemComponent,
  ) {}

  @HostListener('click')
  handleClick() {
    if (this.courseItem.course instanceof SearchCourse) {
      this.store.dispatch(
        actions.addToFavorites({
          toAdd: new PendingFavoriteCourse(this.courseItem.course),
        }),
      );
    }
  }
}
