import {
  Directive,
  HostBinding,
  HostListener,
  Inject,
  Input,
} from '@angular/core';
import { WithEra } from '@app/types/terms';
import { CourseItemComponent, COURSE_ITEM } from '../course-item.component';
import { GlobalState } from '@app/state';
import { Store } from '@ngrx/store';
import * as actions from '@degree-planner/store/actions/course.actions';
import { closeCourseSearch } from '@degree-planner/store/actions/ui.actions';
import { MediaMatcher } from '@angular/cdk/layout';
import {
  PendingPlannedCourse,
  SearchCourse,
} from '@app/degree-planner/objects/courses';

@Directive({ selector: 'button[cseAddToTerm]' })
export class AddToTermDirective {
  @Input('cseAddToTerm') public termCode!: WithEra;

  @HostBinding('disabled')
  get disabled() {
    return !(this.courseItem.course instanceof SearchCourse);
  }

  private isMobileView = this.mediaMatcher.matchMedia('(max-width: 959px)');

  constructor(
    private store: Store<GlobalState>,
    @Inject(COURSE_ITEM) private courseItem: CourseItemComponent,
    private mediaMatcher: MediaMatcher,
  ) {}

  @HostListener('click')
  async handleClick() {
    if (this.courseItem.course instanceof SearchCourse) {
      const pending = new PendingPlannedCourse({
        ...this.courseItem.course,
        termCode: this.termCode,
      });
      this.store.dispatch(actions.addToTerm({ toAdd: pending }));

      if (this.isMobileView.matches) {
        this.store.dispatch(closeCourseSearch());
      }
    }
  }
}
