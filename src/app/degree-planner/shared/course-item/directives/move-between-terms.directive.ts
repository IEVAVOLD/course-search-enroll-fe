import {
  Directive,
  HostBinding,
  HostListener,
  Inject,
  Input,
} from '@angular/core';
import { TermCode, WithEra } from '@app/types/terms';
import { CourseItemComponent, COURSE_ITEM } from '../course-item.component';
import { GlobalState } from '@app/state';
import { Store } from '@ngrx/store';
import * as actions from '@degree-planner/store/actions/course.actions';
import {
  PendingPlannedCourse,
  PlannedCourse,
} from '@app/degree-planner/objects/courses';

@Directive({ selector: 'button[cseMoveBetweenTerms]' })
export class MoveBetweenTermsDirective {
  @Input('cseMoveBetweenTerms') public termCode!: WithEra;

  @HostBinding('disabled')
  get disabled() {
    if (this.courseItem.course instanceof PlannedCourse) {
      return TermCode.equals(this.courseItem.course.termCode, this.termCode);
    }

    return true;
  }

  constructor(
    private store: Store<GlobalState>,
    @Inject(COURSE_ITEM) private courseItem: CourseItemComponent,
  ) {}

  @HostListener('click')
  handleClick() {
    if (this.courseItem.course instanceof PlannedCourse) {
      this.store.dispatch(
        actions.moveBetweenTerms({
          toRemove: this.courseItem.course,
          toAdd: new PendingPlannedCourse({
            ...this.courseItem.course,
            termCode: this.termCode,
          }),
        }),
      );
    }
  }
}
