import {
  Directive,
  HostBinding,
  HostListener,
  Inject,
  Input,
} from '@angular/core';
import { WithEra } from '@app/types/terms';
import { CourseItemComponent, COURSE_ITEM } from '../course-item.component';
import { GlobalState } from '@app/state';
import { Store } from '@ngrx/store';
import * as actions from '@degree-planner/store/actions/course.actions';
import {
  FavoriteCourse,
  PendingPlannedCourse,
} from '@app/degree-planner/objects/courses';

@Directive({ selector: 'button[cseMoveToTermFromFavorites]' })
export class MoveToTermFromFavoritesDirective {
  @Input('cseMoveToTermFromFavorites') public termCode!: WithEra;

  @HostBinding('disabled')
  get disabled() {
    return !(this.courseItem.course instanceof FavoriteCourse);
  }

  constructor(
    private store: Store<GlobalState>,
    @Inject(COURSE_ITEM) private courseItem: CourseItemComponent,
  ) {}

  @HostListener('click')
  handleClick() {
    if (this.courseItem.course instanceof FavoriteCourse) {
      this.store.dispatch(
        actions.moveToTermFromFavorites({
          toRemove: this.courseItem.course,
          toAdd: new PendingPlannedCourse({
            ...this.courseItem.course,
            termCode: this.termCode,
          }),
        }),
      );
    }
  }
}
