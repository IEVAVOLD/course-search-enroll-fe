import { Directive, HostBinding, HostListener, Inject } from '@angular/core';
import { CourseItemComponent, COURSE_ITEM } from '../course-item.component';
import { GlobalState } from '@app/state';
import { Store } from '@ngrx/store';
import * as actions from '@degree-planner/store/actions/course.actions';
import {
  FavoriteCourse,
  PendingFavoriteCourse,
} from '@app/degree-planner/objects/courses';

@Directive({ selector: 'button[cseRemoveFromFavorites]' })
export class RemoveFromFavoritesDirective {
  @HostBinding('disabled')
  get disabled() {
    return !(this.courseItem.course instanceof FavoriteCourse);
  }

  constructor(
    private store: Store<GlobalState>,
    @Inject(COURSE_ITEM) private courseItem: CourseItemComponent,
  ) {}

  @HostListener('click')
  handleClick() {
    if (this.courseItem.course instanceof FavoriteCourse) {
      this.store.dispatch(
        actions.removeFromFavorites({
          toRemove: this.courseItem.course,
          toAdd: new PendingFavoriteCourse(this.courseItem.course),
        }),
      );
    }
  }
}
