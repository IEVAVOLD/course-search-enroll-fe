import {
  Component,
  ContentChild,
  ElementRef,
  HostBinding,
  InjectionToken,
  Input,
} from '@angular/core';
import { Era } from '@app/types/terms';
import { MatMenu } from '@angular/material/menu';
import { NamedCourse } from '@app/types/courses';
import {
  EnrolledCourse,
  PlannedCourse,
  TransferredCourse,
} from '@app/degree-planner/objects/courses';
import { HasCreditRange, HasCreditValue } from '@app/types/credits';

export const COURSE_ITEM = new InjectionToken<CourseItemComponent>(
  'COURSE_ITEM',
);

type CourseStatus =
  | 'AdvancedStanding'
  | 'InProgress'
  | 'Waitlisted'
  | 'NotOfferedInTerm'
  | 'DoesNotExist'
  | 'Normal';

@Component({
  selector: '[cse-course-item]',
  templateUrl: './course-item.component.html',
  styleUrls: ['./course-item.component.scss'],
  providers: [{ provide: COURSE_ITEM, useExisting: CourseItemComponent }],
  host: {
    class: 'course-item',
    '[class.course-advanced-standing]': `status === 'AdvancedStanding'`,
    '[class.course-in-progress]': `status === 'InProgress'`,
    '[class.course-waitlisted]': `status === 'Waitlisted'`,
    '[class.course-not-offered-in-term]': `status === 'NotOfferedInTerm'`,
    '[class.course-does-not-exist]': `status === 'DoesNotExist'`,
    '[class.course-normal]': `status === 'Normal'`,
    '[tabindex]': '-1',
  },
})
export class CourseItemComponent {
  @Input('cse-course-item') public course!: NamedCourse;
  @ContentChild(MatMenu) public menu?: MatMenu;

  @HostBinding('class.is-transferred-course')
  get isTransferredCourse() {
    return this.course instanceof TransferredCourse;
  }

  @HostBinding('class.is-enrolled-course')
  get isEnrolledCourse() {
    return this.course instanceof EnrolledCourse;
  }

  get credits() {
    if (HasCreditValue.matches(this.course)) {
      return this.course.creditValue;
    }

    if (HasCreditRange.matches(this.course)) {
      return this.course.creditRange;
    }

    return null;
  }

  get status(): CourseStatus {
    if (this.course instanceof PlannedCourse) {
      switch (this.course.warning) {
        case 'notOffered':
          return 'NotOfferedInTerm';
        case 'doesNotExist':
          return 'DoesNotExist';
      }
    }

    if (this.course instanceof EnrolledCourse) {
      if (this.course.isWaitlisted) {
        return 'Waitlisted';
      } else if (this.course.termCode.era === Era.Active) {
        return 'InProgress';
      }
    }

    if (this.course instanceof TransferredCourse) {
      return 'AdvancedStanding';
    }

    return 'Normal';
  }

  get statusIcon(): string | null {
    switch (this.status) {
      case 'InProgress':
        return 'check_circle';
      case 'Waitlisted':
        return 'report_problem';
      case 'NotOfferedInTerm':
        return 'error';
      case 'DoesNotExist':
        return 'not_interested';
      default:
        return null;
    }
  }

  get statusLabel(): string {
    switch (this.status) {
      case 'InProgress':
        return 'Course is in progress';
      case 'Waitlisted':
        return 'Course is waitlisted';
      case 'NotOfferedInTerm':
        return 'Course is not offered in term';
      case 'DoesNotExist':
        return 'Course is no longer offered';
      default:
        return '';
    }
  }

  constructor(private elementRef: ElementRef) {}

  focus() {
    this.elementRef.nativeElement.focus();
  }
}
