import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'cse-course-item-grade',
  template: `<ng-content></ng-content>`,
  encapsulation: ViewEncapsulation.None,
})
export class CourseItemGradeComponent {}
