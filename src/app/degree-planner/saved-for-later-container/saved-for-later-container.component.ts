import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { DegreePlannerState } from '@app/degree-planner/store/state';
import * as courseActions from '@app/degree-planner/store/actions/course.actions';
import * as selectors from '@app/degree-planner/store/selectors';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { MovementService } from '@degree-planner/services/movement.service';
import {
  FavoriteCourse,
  PendingFavoriteCourse,
  PlannedCourse,
} from '../objects/courses';
import { CdkDragDrop } from '@angular/cdk/drag-drop';

@Component({
  selector: 'cse-saved-for-later-container',
  templateUrl: './saved-for-later-container.component.html',
  styleUrls: ['./saved-for-later-container.component.scss'],
})
export class SavedForLaterContainerComponent implements OnInit, OnDestroy {
  public SAVED_DROP_ZONE_ID = 'saved-courses';

  public courses$ = this.store.pipe(select(selectors.getSavedForLaterCourses));
  public hasItemDraggedOver!: boolean;

  constructor(
    public movement: MovementService,
    private store: Store<{ degreePlanner: DegreePlannerState }>,
    private announcer: LiveAnnouncer,
  ) {}

  public ngOnInit() {
    this.movement.register(this.SAVED_DROP_ZONE_ID);
    this.hasItemDraggedOver = false;
  }

  ngOnDestroy() {
    this.movement.unregister(this.SAVED_DROP_ZONE_ID);
  }

  startDrag(event: any) {
    const touchedCourse =
      event.source.data.title + ' ' + event.source.data.catalogNumber;
    this.announcer.announce(`Dragging ${touchedCourse} course`, 'assertive');

    this.movement.dragStarted();
  }

  drop(event: CdkDragDrop<unknown, unknown, unknown>) {
    this.movement.dragEnded();

    this.hasItemDraggedOver = false;

    const droppedCourse = event.item.data;

    if (droppedCourse instanceof PlannedCourse) {
      this.store.dispatch(
        courseActions.moveToFavoritesFromTerm({
          toAdd: new PendingFavoriteCourse(droppedCourse),
          toRemove: droppedCourse,
          index: event.currentIndex,
        }),
      );
    } else if (
      droppedCourse instanceof FavoriteCourse ||
      droppedCourse instanceof PendingFavoriteCourse
    ) {
      this.store.dispatch(
        courseActions.moveCourseInsideSFL({
          course: droppedCourse,
          newIndex: event.currentIndex,
        }),
      );
    }
  }

  dragEnter() {
    this.hasItemDraggedOver = true;
  }

  dragExit() {
    this.hasItemDraggedOver = false;
  }
}
