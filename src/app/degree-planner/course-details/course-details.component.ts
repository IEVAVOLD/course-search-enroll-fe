import { MediaMatcher } from '@angular/cdk/layout';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import {
  distinctUntilChanged,
  take,
  withLatestFrom,
  map,
} from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import * as selectors from '@app/degree-planner/store/selectors';
import * as globalSelectors from '@app/selectors';
import * as utils from '@app/degree-planner/shared/utils';
import { TermCode, WithEra } from '@app/types/terms';
import { State } from '@app/degree-planner/store/state';
import { ifSuccessThenUnwrap } from '@app/core/utils';
import * as courseActions from '@app/degree-planner/store/actions/course.actions';
import {
  FavoriteCourse,
  PendingPlannedCourse,
  SearchCourse,
} from '../objects/courses';
import { CampusCourse, RawDetails } from '@app/types/courses';

@Component({
  selector: 'cse-old-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.scss'],
})
export class CourseDetailsComponent implements OnInit, OnDestroy {
  @Input() public courseDetails!: RawDetails;
  @Input() public course!: CampusCourse;

  public selectedSearchTerm: TermCode | undefined;
  public termSelector!: UntypedFormGroup;
  public mobileView: MediaQueryList;

  public selectedSearchTerm$!: Observable<TermCode | undefined>;
  public droppableTermCodes$!: Observable<WithEra[]>;
  public searchTermSubscription!: Subscription;
  public termSubscription!: Subscription;

  get isCourseFromSearchOrFavorites() {
    return (
      this.course instanceof FavoriteCourse ||
      this.course instanceof SearchCourse
    );
  }

  constructor(
    private store: Store<State>,
    private fb: UntypedFormBuilder,
    mediaMatcher: MediaMatcher,
  ) {
    this.mobileView = mediaMatcher.matchMedia('(max-width: 959px)');
  }

  ngOnInit() {
    this.searchTermSubscription = this.store
      .select(selectors.getSelectedSearchTerm)
      .pipe(take(1))
      .subscribe(term => (this.termSelector = this.fb.group({ term })));

    this.droppableTermCodes$ = this.store.pipe(
      select(selectors.selectAllVisibleYears),
      utils.yearsToDroppableTermCodes(),
      distinctUntilChanged(utils.compareArrays(TermCode.equals)),
      withLatestFrom(
        this.store
          .select(globalSelectors.terms.getActiveTermCodes)
          .pipe(ifSuccessThenUnwrap()),
      ),
      map(([termCodes, active]) =>
        termCodes.map(WithEra.fromTermCodeCurry(active)),
      ),
    );
  }

  ngOnDestroy() {
    this.searchTermSubscription.unsubscribe();
  }

  addCourseToPlan($event: any) {
    $event.preventDefault();

    const termCode: WithEra | undefined = this.termSelector.value.term;
    if (!termCode) {
      return;
    }

    const {
      courseId,
      catalogNumber,
      title,
      subject: { subjectCode, shortDescription: subjectName },
      minimumCredits,
      maximumCredits,
    } = this.courseDetails;

    const toAdd = new PendingPlannedCourse({
      termCode,
      name: `${subjectName} ${catalogNumber}`,
      title: title,
      subjectCode,
      courseId,
      creditRange: { min: minimumCredits, max: maximumCredits },
    });

    if (this.course instanceof SearchCourse) {
      this.store.dispatch(courseActions.addToTerm({ toAdd }));
    } else if (this.course instanceof FavoriteCourse) {
      this.store.dispatch(
        courseActions.moveToTermFromFavorites({
          toAdd,
          toRemove: this.course,
        }),
      );
    }
  }

  public sameTermCodes(a: TermCode | undefined, b: TermCode | undefined) {
    if (a === undefined && b === undefined) {
      return true;
    } else if (!a || !b) {
      return false;
    } else {
      return TermCode.equals(a, b);
    }
  }

  get breadth(): string | null {
    if (this.courseDetails && this.courseDetails.breadths.length > 0) {
      return this.courseDetails.breadths
        .map(breadth => breadth.description)
        .join(' or ');
    } else {
      return null;
    }
  }

  get level(): string | null {
    if (this.courseDetails && this.courseDetails.levels.length > 0) {
      return this.courseDetails.levels
        .map(level => level.description)
        .join(', ');
    } else {
      return null;
    }
  }
}
