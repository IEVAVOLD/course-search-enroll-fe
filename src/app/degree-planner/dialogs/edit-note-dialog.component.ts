import { Component, Inject } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

const MAX_NOTE_LENGTH = 512;

@Component({
  selector: 'cse-edit-plan-dialog',
  template: ` <cse-dialog headline="Edit note">
    <cse-dialog-body>
      <mat-form-field style="display:block">
        <mat-label>Note contents</mat-label>
        <textarea
          [formControl]="note"
          matInput
          [cdkTextareaAutosize]="true"
          resizeToFitContent
          placeholder="i.e. Studying abroad"
        >
        </textarea>
        <mat-hint align="end">
          {{ note.value.length }} / {{ maxLength }}
        </mat-hint>
      </mat-form-field>
    </cse-dialog-body>
    <cse-dialog-actions>
      <button mat-button mat-dialog-close>Cancel</button>
      <div class="spacer"></div>
      <button
        mat-button
        mat-raised-button
        style="color:#fff;background:#b5261e"
        [mat-dialog-close]="{ delete: true }"
      >
        Delete
      </button>
      <button
        mat-raised-button
        color="primary"
        [mat-dialog-close]="{ save: note.value }"
        [disabled]="note.invalid"
      >
        Save
      </button>
    </cse-dialog-actions>
  </cse-dialog>`,
})
export class EditNoteDialogComponent {
  public maxLength = MAX_NOTE_LENGTH;
  public note = new UntypedFormControl('', [
    Validators.required,
    Validators.maxLength(MAX_NOTE_LENGTH),
  ]);

  constructor(@Inject(MAT_DIALOG_DATA) note: string) {
    this.note.setValue(note);
  }
}
