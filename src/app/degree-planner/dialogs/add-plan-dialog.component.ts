import { Component } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';

const MAX_PLAN_NAME_LENGTH = 100;

@Component({
  selector: 'cse-add-plan-dialog',
  template: ` <cse-dialog headline="Add degree plan">
    <cse-dialog-body>
      <mat-form-field style="display:block">
        <input [formControl]="name" matInput placeholder="Plan name" />
        <mat-hint align="end">
          {{ name.value.length }} / {{ maxLength }}
        </mat-hint>
      </mat-form-field>
    </cse-dialog-body>
    <cse-dialog-actions>
      <button mat-button mat-dialog-close>Cancel</button>
      <div class="spacer"></div>
      <button
        mat-raised-button
        color="primary"
        [mat-dialog-close]="name.value"
        [disabled]="name.invalid"
      >
        Create plan
      </button>
    </cse-dialog-actions>
  </cse-dialog>`,
})
export class AddPlanDialogComponent {
  public maxLength = MAX_PLAN_NAME_LENGTH;
  public name = new UntypedFormControl('', [
    Validators.required,
    Validators.maxLength(MAX_PLAN_NAME_LENGTH),
  ]);
}
