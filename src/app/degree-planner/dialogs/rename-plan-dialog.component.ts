import { Component, Inject } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

const MAX_PLAN_NAME_LENGTH = 100;

@Component({
  selector: 'cse-rename-plan-dialog',
  template: ` <cse-dialog headline="Rename plan">
    <cse-dialog-body>
      <mat-form-field style="display:block">
        <input [formControl]="name" matInput placeholder="Plan name" />
        <mat-hint align="end">
          {{ name.value.length }} / {{ maxLength }}
        </mat-hint>
      </mat-form-field>
    </cse-dialog-body>
    <cse-dialog-actions>
      <button mat-button mat-dialog-close>Cancel</button>
      <div class="spacer"></div>
      <button
        mat-raised-button
        color="primary"
        [mat-dialog-close]="name.value"
        [disabled]="name.invalid"
      >
        Rename plan
      </button>
    </cse-dialog-actions>
  </cse-dialog>`,
})
export class RenamePlanDialogComponent {
  public maxLength = MAX_PLAN_NAME_LENGTH;
  public name = new UntypedFormControl('', [
    Validators.required,
    Validators.maxLength(MAX_PLAN_NAME_LENGTH),
  ]);

  constructor(@Inject(MAT_DIALOG_DATA) oldName: string) {
    this.name.setValue(oldName);
  }
}
