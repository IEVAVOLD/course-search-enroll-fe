import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RawPlan } from '@app/types/plans';
import { TermCode } from '@app/types/terms';
import { BehaviorSubject } from 'rxjs';
import * as actions from '@degree-planner/store/actions/plan.actions';
import { Store } from '@ngrx/store';
import { GlobalState } from '@app/state';
import { DegreePlannerApiService } from '../services/api.service';
import { finalize, tap } from 'rxjs/operators';
import { AnalyticsService } from '@app/services/analytics.service';

@Component({
  selector: 'cse-make-primary-dialog',
  template: `
    <cse-dialog headline="Are you sure?">
      <cse-dialog-body>
        <p>
          This will change your primary plan and replace the current courses in
          your cart with the courses in this plan&rsquo;s
          {{ termCode | getTermDescription }} term.
        </p>

        <p *ngIf="error$ | async" class="error" role="alert">
          An error occured while attempting to make this plan your primary plan.
          Please try again.
        </p>
      </cse-dialog-body>

      <cse-dialog-actions>
        <button mat-button mat-dialog-close>Cancel</button>

        <div class="spacer"></div>

        <mat-spinner *ngIf="waiting$ | async"></mat-spinner>
        <button mat-raised-button color="primary" (click)="confirm()">
          Set as primary plan
        </button>
      </cse-dialog-actions>
    </cse-dialog>
  `,
  styles: [
    `
      .error {
        color: #e20000;
        font-weight: 500;
      }

      cse-dialog-actions {
        align-items: center;
      }

      cse-dialog-actions mat-spinner {
        margin-right: 1rem;
      }
    `,
  ],
})
export class MakePrimaryDialogComponent {
  public plan: RawPlan;
  public termCode: TermCode;

  public waiting$ = new BehaviorSubject(false);
  public error$ = new BehaviorSubject(false);

  constructor(
    @Inject(MAT_DIALOG_DATA) data: { plan: RawPlan; termCode: TermCode },
    private ref: MatDialogRef<any>,
    private store: Store<GlobalState>,
    private api: DegreePlannerApiService,
    private analytics: AnalyticsService,
  ) {
    this.plan = data.plan;
    this.termCode = data.termCode;
  }

  confirm() {
    if (this.waiting$.value) {
      // If the dialog is already doing work, ignore duplicate clicks
      return;
    }

    this.waiting$.next(true);
    this.error$.next(false);

    const onSuccess = () => {
      this.ref.close();

      const action = actions.makePrimary({ roadmapId: this.plan.roadmapId });
      this.store.dispatch(action);
    };

    const onError = (_err: unknown) => {
      this.error$.next(true);
    };

    const { roadmapId, name } = this.plan;
    this.api
      .updatePlan(roadmapId, name, true)
      .pipe(
        tap(() => this.analytics.event('Planner', 'change-primary-plan')),
        finalize(() => this.waiting$.next(false)),
      )
      .subscribe(onSuccess, onError);
  }
}
