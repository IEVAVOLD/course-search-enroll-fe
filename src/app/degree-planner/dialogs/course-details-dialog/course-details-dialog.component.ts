import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, of } from 'rxjs';
import { ApiService } from '@app/services/api.service';
import { catchError, map } from 'rxjs/operators';
import {
  CampusCourse,
  HasTermCodeWithEra,
  RawDetails,
} from '@app/types/courses';

@Component({
  selector: 'cse-course-details-dialog',
  templateUrl: './course-details-dialog.component.html',
})
export class CourseDetailsDialogComponent implements OnInit {
  public data$!: Observable<{ ok: true; details: RawDetails } | { ok: false }>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public course: CampusCourse & HasTermCodeWithEra,
    private api: ApiService,
  ) {}

  public ngOnInit() {
    this.data$ = this.api
      .getCourseDetails(
        this.course.termCode,
        this.course.subjectCode,
        this.course.courseId,
      )
      .pipe(
        map(details => ({ ok: true, details })),
        catchError(() => of({ ok: false as const })),
      );
  }
}
