import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import {
  BehaviorSubject,
  combineLatest,
  Observable,
  ReplaySubject,
  Subscription,
} from 'rxjs';
import {
  filter,
  map,
  distinctUntilChanged,
  pairwise,
  startWith,
} from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { DegreePlannerState } from '@app/degree-planner/store/state';
import * as uiActions from '@app/degree-planner/store/actions/ui.actions';
import * as actions from '@app/degree-planner/store/actions/course.actions';
import * as selectors from '@app/degree-planner/store/selectors';
import { PlannedTerm, PlannedTermNote } from '@degree-planner/objects/plan';
import { TermCode, Era, SUMMER, WithEra } from '@app/types/terms';
import { MediaMatcher } from '@angular/cdk/layout';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import {
  flatFilter,
  ifSuccessThenUnwrap,
  isntUndefined,
  unsubscribeAll,
} from '@app/core/utils';
import { DialogService } from '@app/shared/services/dialog.service';
import { AddNoteDialogComponent } from '../dialogs/add-note-dialog.component';
import * as noteActions from '../store/actions/note.actions';
import { EditNoteDialogComponent } from '../dialogs/edit-note-dialog.component';
import * as globalSelectors from '@app/selectors';
import * as s from '@app/types/schema';
import { MovementService } from '@degree-planner/services/movement.service';
import {
  FavoriteCourse,
  PendingPlannedCourse,
  PlannedCourse,
  SearchCourse,
} from '../objects/courses';
import { CreditRange, CreditValue } from '@app/types/credits';
import { formatCredits } from '@app/shared/pipes/credits.pipe';
import { MatTabChangeEvent } from '@angular/material/tabs';

// Both the summer and fall/spring undergrad credit limits are INCLUSIVE.
const SUMMER_CREDIT_LIMIT = 12;
const FALL_SPRING_CREDIT_LIMIT = 18;

// If the user is an undergrad, their student info
// object should satisfy these constraints:
const undergradSchema = s.object({
  primaryCareer: s.object({
    careerCode: s.constant('UGRD'),
  }),
});

type WhichTab = 'transferred' | 'enrolled' | 'planned';

@Component({
  selector: 'cse-term-container',
  templateUrl: './term-container.component.html',
  styleUrls: ['./term-container.component.scss'],
})
export class TermContainerComponent implements OnInit, OnDestroy {
  @Input() termCode!: WithEra;

  public dropZoneId!: string;

  public term$!: Observable<PlannedTerm>;
  public note$!: Observable<PlannedTermNote | undefined>;
  public tooManyCredits$!: Observable<boolean>;
  public visibleTab$ = new ReplaySubject<WhichTab>();
  public hasItemDraggedOver$ = new BehaviorSubject(false);
  public showCreditOverload$!: Observable<boolean>;

  public showGrades$ = this.store.select(globalSelectors.prefs.isShowingGrades);

  private isUndergrad$ = this.store
    .select(globalSelectors.student.getAll)
    .pipe(ifSuccessThenUnwrap(), map(undergradSchema.matches));

  private isPrimary$ = combineLatest([
    this.store.select(selectors.selectAllDegreePlans),
    this.store.select(selectors.selectVisibleDegreePlan),
  ]).pipe(
    map(([plans, planId]) => plans.find(p => p.roadmapId === planId)),
    map(plan => plan?.primary ?? false),
  );

  public transferred$!: Observable<PlannedTerm['transferred']>;
  public enrolled$!: Observable<PlannedTerm['enrolled']>;
  public planned$!: Observable<PlannedTerm['planned']>;

  public visibleCredits$!: Observable<string>;

  public mobileView: MediaQueryList;

  public transferredTabLabel$!: Observable<string>;
  public enrolledTabLabel$!: Observable<string>;
  public plannedTabLabel$!: Observable<string>;
  public firstVisibleTabIndex$!: Observable<number>;

  public hasTransferred$!: Observable<boolean>;

  public noTransferredCoursesMessage$!: Observable<string | null>;
  public noEnrolledCoursesMessage$!: Observable<string | null>;
  public noPlannedCoursesMessage$!: Observable<string | null>;

  private subscriptions!: Subscription[];

  get editNoteLabel(): string {
    return `Edit note for ${TermCode.describe(this.termCode)}`;
  }

  get addNoteLabel(): string {
    return `Add note for ${TermCode.describe(this.termCode)}`;
  }

  constructor(
    public movement: MovementService,
    private dialog: DialogService,
    private store: Store<{ degreePlanner: DegreePlannerState }>,
    private announcer: LiveAnnouncer,
    mediaMatcher: MediaMatcher,
  ) {
    this.mobileView = mediaMatcher.matchMedia('(max-width: 900px)');
  }

  public ngOnInit() {
    const isConnected = this.termCode.era !== Era.Past;
    this.dropZoneId = this.movement.register(this.termCode, isConnected);

    this.hasItemDraggedOver$.next(false);

    this.term$ = this.store.pipe(
      select(selectors.selectVisibleTerm, { termCode: this.termCode }),
      filter(isntUndefined),
      distinctUntilChanged(),
    );

    this.transferredTabLabel$ = this.term$.pipe(
      map(term => `Transferred (${term.transferred.length})`),
    );

    this.enrolledTabLabel$ = this.term$.pipe(
      map(term => {
        if (term.termCode.era === Era.Past) {
          return `Completed (${term.enrolled.length})`;
        } else {
          return `In Progress (${term.enrolled.length})`;
        }
      }),
    );

    this.plannedTabLabel$ = combineLatest([this.term$, this.isPrimary$]).pipe(
      map(([term, isPrimary]) => {
        if (isPrimary) {
          return `Cart (${term.planned.length})`;
        } else {
          return `Planned (${term.planned.length})`;
        }
      }),
    );

    this.firstVisibleTabIndex$ = this.term$.pipe(
      map(t => (t.enrolled.length ? 0 : 1)),
    );
    this.hasTransferred$ = this.term$.pipe(map(t => t.transferred.length > 0));

    this.transferred$ = this.term$.pipe(
      map(t => t.transferred),
      startWith([]),
    );

    this.enrolled$ = this.term$.pipe(
      map(t => t.enrolled),
      startWith([]),
    );

    this.planned$ = this.term$.pipe(
      map(t => t.planned),
      startWith([]),
    );

    this.visibleCredits$ = combineLatest([this.term$, this.visibleTab$]).pipe(
      map(([term, visibleTab]) => formatCredits(sumCredits(term[visibleTab]))),
    );

    this.showCreditOverload$ = combineLatest([
      this.planned$.pipe(map(courses => sumCredits(courses).min)),
      this.isUndergrad$,
      this.visibleTab$,
    ]).pipe(
      map(([minCredits, isUndergrad, visibleTab]) => {
        if (!isUndergrad) {
          // The app doesn't regulate non-undergrad credit totals
          return false;
        } else if (visibleTab !== 'planned') {
          // Don't display the warning if the user isn't looking at planned courses.
          return false;
        }

        const maxAllowed = maxAllowedCredits(this.termCode);
        return minCredits > maxAllowed;
      }),
    );

    this.noEnrolledCoursesMessage$ = this.term$.pipe(
      map(term => {
        if (term.enrolled.length > 0) {
          return null;
        }

        if (term.termCode.era === Era.Past) {
          return 'No courses taken';
        } else {
          return 'Not enrolled in any courses';
        }
      }),
    );

    this.noPlannedCoursesMessage$ = combineLatest([
      this.term$,
      this.hasItemDraggedOver$,
    ]).pipe(
      map(([term, hasItemDraggedOver]) => {
        if (term.planned.length > 0 || hasItemDraggedOver) {
          return null;
        }

        if (term.termCode.era === Era.Active) {
          return 'No courses in cart';
        } else {
          return 'No courses planned';
        }
      }),
    );

    this.noTransferredCoursesMessage$ = this.term$.pipe(
      map(term => {
        if (term.transferred.length === 0 && term.termCode.era === Era.Past) {
          return 'No transferred courses';
        }

        return null;
      }),
    );

    this.subscriptions = [
      this.term$.subscribe(term => {
        if (term.termCode.era === Era.Past) {
          this.visibleTab$.next('enrolled');
        } else if (term.termCode.era === Era.Active && term.enrolled.length) {
          this.visibleTab$.next('enrolled');
        } else {
          this.visibleTab$.next('planned');
        }
      }),

      this.term$
        .pipe(
          // Condition #1
          flatFilter(() => this.isUndergrad$),

          // Don't compare two terms if they are different terms
          pairwise(),
          filter(([prev, curr]) => {
            return (
              prev.roadmapId === curr.roadmapId &&
              TermCode.equals(prev.termCode, curr.termCode)
            );
          }),
        )
        .subscribe(([prev, curr]) => {
          const prevCredits = sumCredits(prev.planned);
          const currCredits = sumCredits(curr.planned);
          const maxAllowedCredits = maximumAllowedCreditsForTerm(curr.termCode);
          const prevWasOverLimit = prevCredits.min > maxAllowedCredits;
          const currIsUnderLimit = currCredits.min <= maxAllowedCredits;
          const currHasFewerCreditsThanPrev = currCredits.min < prevCredits.min;

          if (prevWasOverLimit || currHasFewerCreditsThanPrev) {
            // Failed condition #2
            return;
          }

          if (currIsUnderLimit) {
            // Failed condition #3
            return;
          }

          this.dialog.acknowledge({
            headline: 'Credit overload',
            reasoning: [
              `Undergraduate students who wish to take more than ${maxAllowedCredits} ` +
                `during the ${TermCode.lowerTermName(
                  curr.termCode,
                )} semester ` +
                `must receive approval from their School or College.`,
            ],
            moreInfo: {
              label: 'More information',
              href: 'https://registrar.wisc.edu/enrollment-related-info',
            },
          });
        }),
    ];

    this.note$ = this.term$.pipe(
      map(term => term.note),
      distinctUntilChanged(),
    );
  }

  ngOnDestroy() {
    this.movement.unregister(this.termCode);
    unsubscribeAll(this.subscriptions);
  }

  openNotesDialog(note?: PlannedTermNote) {
    if (note === undefined || note.isLoaded) {
      const termCode = this.termCode;
      if (note) {
        this.dialog.small(EditNoteDialogComponent, note.text).then(response => {
          if (response && typeof response === 'object') {
            if ((response as any)['delete'] === true) {
              this.store.dispatch(
                noteActions.deleteNote({ termCode, noteId: note.id }),
              );
            } else if (typeof (response as any)['save'] === 'string') {
              const noteText: string = (response as any)['save'];
              this.store.dispatch(
                noteActions.writeNote({ termCode, noteText }),
              );
            }
          }
        });
      } else {
        this.dialog.small(AddNoteDialogComponent).then(noteText => {
          if (typeof noteText === 'string' && noteText.length > 0) {
            this.store.dispatch(noteActions.writeNote({ termCode, noteText }));
          }
        });
      }
    }
  }

  openCourseSearch() {
    this.store.dispatch(
      uiActions.openCourseSearch({ termCode: this.termCode }),
    );
  }

  changeVisibleCredits(event: MatTabChangeEvent) {
    const names: WhichTab[] = ['enrolled', 'planned', 'transferred'];
    this.visibleTab$.next(names[event.index] ?? 'planned');
  }

  startDrag(event: any) {
    const touchedCourse =
      event.source.data.subjectDescription +
      ' ' +
      event.source.data.catalogNumber;
    this.announcer.announce(`Dragging ${touchedCourse} course`, 'assertive');

    this.movement.dragStarted();
  }

  drop(event: CdkDragDrop<WithEra, unknown, unknown>) {
    const newContainer = event.container.id;
    const previousContainer = event.previousContainer.id;

    this.hasItemDraggedOver$.next(false);
    this.announcer.announce('Dropped course', 'assertive');

    const course = event.item.data;

    if (course instanceof PlannedCourse) {
      if (newContainer === previousContainer) {
        this.store.dispatch(
          actions.moveCourseInsideTerm({
            course,
            newIndex: event.currentIndex,
          }),
        );
        return;
      }

      this.store.dispatch(
        actions.moveBetweenTerms({
          toRemove: course,
          toAdd: new PendingPlannedCourse({
            ...course,
            termCode: event.container.data,
          }),
          index: event.currentIndex,
        }),
      );
      return;
    }

    if (course instanceof FavoriteCourse) {
      this.store.dispatch(
        actions.moveToTermFromFavorites({
          toRemove: course,
          toAdd: new PendingPlannedCourse({
            ...course,
            termCode: event.container.data,
          }),
          index: event.currentIndex,
        }),
      );
      return;
    }

    if (course instanceof SearchCourse) {
      this.store.dispatch(
        actions.addToTerm({
          toAdd: new PendingPlannedCourse({
            ...course,
            termCode: event.container.data,
          }),
          index: event.currentIndex,
        }),
      );

      if (this.mobileView.matches) {
        this.store.dispatch(uiActions.closeCourseSearch());
      }

      return;
    }
  }

  dragEnter() {
    this.hasItemDraggedOver$.next(true);
  }

  dragExit() {
    this.hasItemDraggedOver$.next(false);
  }
}

const sumCredits = (
  courses: {
    creditRange?: CreditRange | null;
    creditValue?: CreditValue | null;
  }[],
) => {
  const min = courses.reduce((sum, course) => {
    if (typeof course.creditValue === 'number') {
      return sum + course.creditValue;
    } else if (course.creditRange) {
      return sum + course.creditRange.min;
    } else {
      return sum;
    }
  }, 0);

  const max = courses.reduce((sum, course) => {
    if (typeof course.creditValue === 'number') {
      return sum + course.creditValue;
    } else if (course.creditRange) {
      return sum + course.creditRange.max;
    } else {
      return sum;
    }
  }, 0);

  return { min, max };
};

const maximumAllowedCreditsForTerm = (termCode: TermCode) => {
  switch (termCode.term) {
    case SUMMER:
      return SUMMER_CREDIT_LIMIT;
    default:
      return FALL_SPRING_CREDIT_LIMIT;
  }
};

const UNDERGRAD_PER_TERM_CREDIT_MAX = {
  fall: 18,
  spring: 18,
  summer: 12,
};

export const maxAllowedCredits = (termCode: TermCode): number => {
  const termName = TermCode.lowerTermName(termCode);
  return UNDERGRAD_PER_TERM_CREDIT_MAX[termName];
};
