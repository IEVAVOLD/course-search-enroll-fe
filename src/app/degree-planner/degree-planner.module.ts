import { NgModule } from '@angular/core';
import { MatListModule } from '@angular/material/list';
import { DegreePlannerViewComponent } from './degree-planner-view/degree-planner-view.component';
import { SharedModule } from '@app/shared/shared.module';
import { TermContainerComponent } from './term-container/term-container.component';
import { SidenavMenuItemComponent } from './sidenav-menu-item/sidenav-menu-item.component';
import { SavedForLaterContainerComponent } from './saved-for-later-container/saved-for-later-container.component';
import { CourseListComponent } from './shared/course-list/course-list.component';
import { CourseItemComponent } from './shared/course-item/course-item.component';
import { CourseItemGradeComponent } from './shared/course-item/course-item-grade.component';
import { CDK_DRAG_CONFIG, DragDropModule } from '@angular/cdk/drag-drop';
import { YearContainerComponent } from '@app/degree-planner/year-container/year-container.component';
import { CourseSearchComponent } from '@app/degree-planner/course-search/course-search.component';
import { EffectsModule } from '@ngrx/effects';
import { DegreePlanEffects } from './store/effects/plan.effects';
import { NoteEffects } from './store/effects/note.effects';
import { CourseEffects } from './store/effects/course.effects';
import { ErrorEffects } from './store/effects/error.effects';
import { UIEffects } from './store/effects/ui.effects';
import { StoreModule } from '@ngrx/store';
import { degreePlannerReducer } from './store/reducer';
import { DegreePlannerRoutingModule } from './degree-planner-routing.module';
import { CourseDetailsDialogComponent } from './dialogs/course-details-dialog/course-details-dialog.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { AddPlanDialogComponent } from './dialogs/add-plan-dialog.component';
import { RenamePlanDialogComponent } from './dialogs/rename-plan-dialog.component';
import { AddNoteDialogComponent } from './dialogs/add-note-dialog.component';
import { EditNoteDialogComponent } from './dialogs/edit-note-dialog.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { LoadableDirectivesModule } from 'loadable.ts';
import { MakePrimaryDialogComponent } from './dialogs/make-primary.dialog';
import { AddYearToPlanDirective } from './directives/add-year-to-plan.directive';
import { IsPendingCoursePipe } from './pipes/is-pending-course.pipe';

import { AddToFavoritesDirective } from './shared/course-item/directives/add-to-favorites.directive';
import { AddToTermDirective } from './shared/course-item/directives/add-to-term.directive';
import { MoveBetweenTermsDirective } from './shared/course-item/directives/move-between-terms.directive';
import { MoveToFavoritesFromTermDirective } from './shared/course-item/directives/move-to-favorites-from-term.directive';
import { MoveToTermFromFavoritesDirective } from './shared/course-item/directives/move-to-term-from-favorites.directive';
import { RemoveFromFavoritesDirective } from './shared/course-item/directives/remove-from-favorites.directive';
import { RemoveFromTermDirective } from './shared/course-item/directives/remove-from-term.directive';
import { ShowDetailsForDirective } from './shared/course-item/directives/show-details.directive';

@NgModule({
  imports: [
    SharedModule,
    MatListModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    MatCardModule,
    MatTabsModule,
    DragDropModule,
    StoreModule.forFeature('degreePlanner', degreePlannerReducer),
    EffectsModule.forFeature([
      DegreePlanEffects,
      NoteEffects,
      CourseEffects,
      ErrorEffects,
      UIEffects,
    ]),
    DegreePlannerRoutingModule,
    NgxMatSelectSearchModule,
    LoadableDirectivesModule,
  ],
  exports: [DragDropModule],
  declarations: [
    DegreePlannerViewComponent,
    TermContainerComponent,
    CourseListComponent,
    CourseItemComponent,
    CourseItemGradeComponent,
    SidenavMenuItemComponent,
    SavedForLaterContainerComponent,
    YearContainerComponent,
    CourseSearchComponent,
    CourseDetailsDialogComponent,
    CourseDetailsComponent,
    AddPlanDialogComponent,
    RenamePlanDialogComponent,
    AddNoteDialogComponent,
    EditNoteDialogComponent,
    MakePrimaryDialogComponent,
    AddYearToPlanDirective,
    AddToFavoritesDirective,
    AddToTermDirective,
    MoveBetweenTermsDirective,
    MoveToFavoritesFromTermDirective,
    MoveToTermFromFavoritesDirective,
    RemoveFromFavoritesDirective,
    RemoveFromTermDirective,
    ShowDetailsForDirective,
    IsPendingCoursePipe,
  ],
  providers: [
    {
      provide: CDK_DRAG_CONFIG,
      useValue: {
        dragStartDelay: { touch: 100, mouse: 0 },
      },
    },
  ],
})
export class DegreePlannerModule {}
