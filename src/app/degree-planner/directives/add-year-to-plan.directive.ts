import {
  Directive,
  HostBinding,
  HostListener,
  Input,
  OnDestroy,
} from '@angular/core';
import { GlobalState } from '@app/state';
import { Store } from '@ngrx/store';
import * as globalSelectors from '@app/selectors';
import * as viewSelectors from '@app/degree-planner/store/selectors';
import * as actions from '@app/degree-planner/store/actions/ui.actions';
import { ifSuccessThenUnwrap } from '@app/core/utils';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import { YearCode } from '@app/types/terms';

@Directive({
  selector: '[cseAddYearToPlan]',
})
export class AddYearToPlanDirective implements OnDestroy {
  public _nextYearCode: YearCode | null = null;

  @Input('disabled') @HostBinding('disabled') public _isDisabled = true;

  private _activeYearCodes$ = this.store
    .select(globalSelectors.terms.getActiveTermCodes)
    .pipe(ifSuccessThenUnwrap<YearCode[]>());

  private _planYearCodes$ = this.store
    .select(viewSelectors.selectAllVisibleYears)
    .pipe(map(mapping => Object.values(mapping).map(year => year.yearCode)));

  private _nextYearCode$ = combineLatest([
    this._activeYearCodes$,
    this._planYearCodes$,
  ]).pipe(
    map(([active, visible]): YearCode | null => {
      const sorted = [...active, ...visible];
      sorted.sort(YearCode.sort);

      if (sorted.length < 1) {
        // If there are no year codes something else probably went wrong.
        // Don't guess the next year because the guess would be nonsense anyway.
        return null;
      }

      const last = sorted[sorted.length - 1];
      const next = YearCode.next(last);
      return next;
    }),
    distinctUntilChanged((a, b) => {
      if (a === b) {
        return true;
      } else if (!a || !b) {
        return false;
      } else {
        return YearCode.equals(a, b);
      }
    }),
  );

  private _nextYearCodeSub = this._nextYearCode$.subscribe(next => {
    this._isDisabled = next === null;
    this._nextYearCode = next;
  });

  constructor(private store: Store<GlobalState>) {}

  @HostListener('click')
  click() {
    if (this._nextYearCode) {
      this.store.dispatch(
        actions.addAcademicYear({ yearCode: this._nextYearCode }),
      );
    }
  }

  ngOnDestroy() {
    this._nextYearCodeSub.unsubscribe();
  }
}
