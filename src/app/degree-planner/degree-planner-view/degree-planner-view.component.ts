import {
  filter,
  map,
  distinctUntilChanged,
  tap,
  delay,
  first,
} from 'rxjs/operators';
import { OnInit, ViewChild, OnDestroy, Component } from '@angular/core';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { MatSelectChange } from '@angular/material/select';
import { MatSidenavContent } from '@angular/material/sidenav';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatAccordion } from '@angular/material/expansion';
import { RawPlan } from '@app/types/plans';
import { Year } from '@degree-planner/objects/plan';
import * as selectors from '@app/degree-planner/store/selectors';
import * as globalSelectors from '@app/selectors';
import * as utils from '@app/degree-planner/shared/utils';
import * as planActions from '@app/degree-planner/store/actions/plan.actions';
import * as uiActions from '../store/actions/ui.actions';
import { YearCode } from '@app/types/terms';
import { Actions, ofType } from '@ngrx/effects';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { ifSuccessThenUnwrap, isntUndefined } from '@app/core/utils';
import { prefs as prefActions } from '@app/actions';
import { prefs as prefSelectors } from '@app/selectors';
import { State } from '@app/degree-planner/store/state';
import { DialogService } from '@app/shared/services/dialog.service';
import { AddPlanDialogComponent } from '../dialogs/add-plan-dialog.component';
import { RenamePlanDialogComponent } from '../dialogs/rename-plan-dialog.component';
import { environment } from '@env/environment';
import { Router } from '@angular/router';
import { MakePrimaryDialogComponent } from '../dialogs/make-primary.dialog';
import { AnalyticsService } from '@app/services/analytics.service';

@Component({
  selector: 'cse-degree-planner',
  templateUrl: './degree-planner-view.component.html',
  styleUrls: ['./degree-planner-view.component.scss'],
})
export class DegreePlannerViewComponent implements OnInit, OnDestroy {
  @ViewChild(MatAccordion)
  public planYearAccordion!: MatAccordion;
  @ViewChild('degreePlanWrapper')
  public degreePlanWrapper!: MatSidenavContent | null;

  public mobileView: MediaQueryList;
  public coursesData$: any;
  public showGrades$!: Observable<boolean>;

  public allDegreePlans$: Observable<RawPlan[]> = this.store.select(
    selectors.selectAllDegreePlans,
  );

  public degreePlan$: Observable<RawPlan> = combineLatest([
    this.store.select(selectors.selectAllDegreePlans),
    this.store.select(selectors.selectVisibleDegreePlan),
  ]).pipe(
    map(([plans, planId]) => plans.find(p => p.roadmapId === planId)),
    filter(isntUndefined),
  );

  public termsByYear$!: Observable<ReadonlyArray<Year>>;
  public yearCodes$!: Observable<ReadonlyArray<YearCode>>;
  public isCourseSearchOpen$!: Observable<boolean>;
  public isLoadingPlan$!: Observable<boolean>;
  public isSidenavOpen$!: Observable<boolean>;
  public version: string;

  private addAcademicYearSubscription!: Subscription;

  constructor(
    private actions$: Actions,
    private store: Store<State>,
    public mediaMatcher: MediaMatcher,
    private dialog: DialogService,
    private snackBar: MatSnackBar,
    private announcer: LiveAnnouncer,
    router: Router,
    private analytics: AnalyticsService,
  ) {
    this.mobileView = mediaMatcher.matchMedia('(max-width: 959px)');
    this.version = environment.version;

    const navState = router.getCurrentNavigation()?.extras?.state;
    const showPlan: unknown = navState?.showPlan;
    if (typeof showPlan === 'number') {
      this.store.dispatch(
        planActions.switchPlan({
          newVisibleRoadmapId: showPlan,
        }),
      );
    }
  }

  public ngOnInit() {
    this.store.dispatch(planActions.initialLoadRequest());

    this.showGrades$ = this.store
      .select(prefSelectors.getKey, 'degreePlannerGradesVisibility')
      .pipe(map(val => (val === false ? false : true)));

    // Get observable for the search open state
    this.isCourseSearchOpen$ = this.store.pipe(
      select(selectors.isCourseSearchOpen),
    );

    this.isLoadingPlan$ = this.store.pipe(select(selectors.isLoadingPlan));

    this.isSidenavOpen$ = this.store.pipe(select(selectors.isSidenavOpen)).pipe(
      map(isSidenavOpen => {
        if (isSidenavOpen === 'defer') {
          return !this.mobileView.matches;
        }

        return isSidenavOpen;
      }),
    );

    this.yearCodes$ = this.store.pipe(
      select(selectors.selectAllVisibleYears),
      map(years => Object.keys(years).sort()),
      distinctUntilChanged(utils.compareStringArrays),
      map(ycs => ycs.map(YearCode.decodeOrThrow)),
    );

    this.addAcademicYearSubscription = this.actions$
      .pipe(
        ofType(uiActions.addAcademicYear),
        delay(20), // The delay makes sure the scrolling takes place *after* the academic year has been rendered
        tap(() => {
          if (this.degreePlanWrapper) {
            this.degreePlanWrapper.scrollTo({ bottom: 0 });
          }
        }),
      )
      .subscribe();
  }

  public ngOnDestroy() {
    this.addAcademicYearSubscription.unsubscribe();
  }

  public openSidenav() {
    this.store.dispatch(uiActions.openSidenav());
  }

  public closeSidenav() {
    this.store.dispatch(uiActions.closeSidenav());
  }

  public handleDegreePlanChange(event: MatSelectChange): void {
    if (typeof event.value === 'number') {
      this.store.dispatch(
        planActions.switchPlan({ newVisibleRoadmapId: event.value }),
      );
    }
  }

  public onCreatePlanClick() {
    this.dialog.small(AddPlanDialogComponent).then(name => {
      if (typeof name === 'string' && name.length > 0) {
        this.store.dispatch(planActions.createPlan({ name, primary: false }));
      }
    });
  }

  public onRenamePlanClick(currentPlan: RawPlan) {
    this.dialog
      .small(RenamePlanDialogComponent, currentPlan.name)
      .then(newName => {
        if (typeof newName === 'string' && newName.length > 0) {
          this.store.dispatch(
            planActions.changePlanName({
              roadmapId: currentPlan.roadmapId,
              newName,
            }),
          );
        }
      });
  }

  public async onMakePrimayClick(currentPlan: RawPlan) {
    const active = await this.store
      .select(globalSelectors.terms.getActiveTermCodes)
      .pipe(ifSuccessThenUnwrap(), first())
      .toPromise();

    this.dialog.small(MakePrimaryDialogComponent, {
      plan: currentPlan,
      termCode: active[0],
    });
  }

  public async onDeletePlanClick(currentPlan: RawPlan) {
    if (currentPlan.primary) {
      this.snackBar.open('The primary degree plan cannot be deleted');
      return;
    }

    const shouldDelete = await this.dialog.confirm({
      headline: 'Are you sure?',
      reasoning: [
        'This will delete this plan and course information related to this plan.',
      ],
      affirmative: 'Delete',
    });

    if (shouldDelete) {
      const primaryPlan = (
        await this.store
          .select(selectors.selectAllDegreePlans)
          .pipe(first())
          .toPromise()
      ).find(plan => plan.primary);

      this.store.dispatch(
        planActions.deletePlan({
          roadmapId: currentPlan.roadmapId,
        }),
      );
      this.store.dispatch(
        planActions.switchPlan({
          newVisibleRoadmapId: primaryPlan!.roadmapId,
        }),
      );
    }
  }

  public changeGradeVisibility(event: MatSlideToggleChange) {
    const label = event.checked ? 'show-grades' : 'hide-grades';
    this.analytics.event('Planner', label);

    this.store.dispatch(
      prefActions.setKey({
        key: 'degreePlannerGradesVisibility',
        value: event.checked,
      }),
    );
  }

  public closeCourseSearch() {
    this.store.dispatch(uiActions.closeCourseSearch());
  }

  public trackYearCodes(_index: number, yearCode: YearCode) {
    return YearCode.encode(yearCode);
  }

  public expandAllYears() {
    this.planYearAccordion.openAll();
    this.announcer.announce('Expanded All Academic Years', 'assertive');
  }

  public collapseAllYears() {
    this.planYearAccordion.closeAll();
    this.announcer.announce('Collapsed All Academic Years', 'assertive');
  }
}
