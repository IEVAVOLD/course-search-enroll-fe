import { Loadable, LOADING } from 'loadable.ts';
import { Json } from '@app/types/json';
import { TermCode, Terms } from '@app/types/terms';
import { StudentInfo } from './types/student';
import { CurrentCourse, RoadmapCourse, SavedCourse } from '@app/types/courses';

export interface Prefs {
  [key: string]: Json;
}

interface Courses {
  isValidating: boolean;
  hintToRevalidate: boolean;
  roadmap: Loadable<RoadmapCourse[]> | null;
  current: Loadable<CurrentCourse[]> | null;
}

export interface GlobalState {
  prefs: Loadable<Prefs>;
  terms: Loadable<Terms>;
  currentTermCode: Loadable<TermCode>;
  student: Loadable<StudentInfo>;
  courses: Record<string, Courses>;
  saved: Loadable<SavedCourse[]> | null;
}

export namespace GlobalState {
  export const INIT: GlobalState = {
    prefs: LOADING,
    terms: LOADING,
    currentTermCode: LOADING,
    student: LOADING,
    courses: {},
    saved: null,
  };
}
