import { WindowRef } from './window.service';
import { Inject, Injectable } from '@angular/core';
import Sockette from 'sockette';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Seconds, Minutes } from '@app/types';
import { hasSchema } from '@app/core/utils';
import Schema, * as s from '@app/types/schema';
import { IS_PRIVATE } from './is-private.token';

type ConnectionState = 'connecting' | 'connected' | 'rateLimited';

const CONNECTION_TIMEOUT: Seconds = 5;
const PING_INTERVAL: Minutes = 5;
const MAX_CONNECTION_ATTEMPTS = 5;

@Injectable({ providedIn: 'root' })
export class WebsocketService {
  private jsonWebToken?: string;
  private state = new BehaviorSubject<ConnectionState>('connecting');
  private wrapper?: Sockette;
  private listenersByType: Record<string, Subject<unknown>> = {};

  constructor(
    private http: HttpClient,
    private winRef: WindowRef,
    @Inject(IS_PRIVATE) isPrivate: boolean,
  ) {
    if (isPrivate) {
      const reqSub = this.http
        .post('/wsregister', {})
        .pipe(hasSchema(s.object({ token: s.string })))
        .subscribe(({ token }) => {
          this.jsonWebToken = token;
          this.log('got jwt token');

          // Init websocket wrapper
          this.startConnection();

          // Clean-up single use subscription
          reqSub.unsubscribe();
        });
    } else {
      this.log('public site, cancel websocket connection');
    }
  }

  private setState(newState: ConnectionState): void {
    const oldState = this.state.getValue();

    if (newState !== oldState) {
      this.state.next(newState);
      this.log(`${oldState} -> ${newState}`);
    }
  }

  private log(message: string, payload?: unknown): void {
    if (payload) {
      console.log(`[WEBSOCKET] ${message}`, payload);
    } else {
      console.log(`[WEBSOCKET] ${message}`);
    }
  }

  private startConnection() {
    const configSchema: Schema<{ config: { wsendpoint: string } }> = s.object({
      config: s.object({
        wsendpoint: s.string,
      }),
    });

    configSchema.asserts(this.winRef.nativeWindow);
    const endpoint = this.winRef.nativeWindow.config.wsendpoint;
    const url = `${endpoint}?token=${this.jsonWebToken}`;

    this.wrapper = new Sockette(url, {
      timeout: CONNECTION_TIMEOUT * 1000,
      maxAttempts: MAX_CONNECTION_ATTEMPTS,
      onopen: () => {
        this.setState('connected');
        window.setInterval(() => {
          this.wrapper!.json({ type: 'ping' });
        }, PING_INTERVAL * 1000 * 60);
      },
      onmessage: event => this.delegateMessage(`${event.data}`),
      onreconnect: () => this.log('did reconnect'),
      onmaximum: () => this.setState('rateLimited'),
      onclose: () => this.wrapper!.reconnect(),
      onerror: err => {
        this.log('error');
        console.error(err);
      },
    });
  }

  /**
   * Determine which of the subscriptions are receptive to the incoming message.
   */
  private delegateMessage(data: string): void {
    const message = JSON.parse(data);
    this.log('got message', message);

    if (s.object({ type: s.string }).matches(message)) {
      if (this.listenersByType.hasOwnProperty(message.type)) {
        this.listenersByType[message.type].next(message);
      }
    }
  }

  /**
   * Returns an observable stream containing all WebSocket messages with a
   * `type` field equal to the first parameter.
   */
  public listenByType(type: string): Observable<unknown> {
    if (this.listenersByType.hasOwnProperty(type) === false) {
      this.listenersByType[type] = new Subject();
    }

    return this.listenersByType[type];
  }
}
