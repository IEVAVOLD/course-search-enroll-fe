import { ApiService } from '@app/services/api.service';
import { TestBed } from '@angular/core/testing';

import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TermCode } from '@app/types/terms';

import enrollmentsJson from '../../../testdata/current-enrollments.json';
import studentInfoJson from '../../../testdata/studentInfo.json';

import { SchemaAssertionError } from '@app/types/schema';
import { AnalyticsService } from '@app/services/analytics.service';

class MockAnalyticsService {
  schemaException(_label: string, _err: SchemaAssertionError) {}
}
// API tests using the HttpTestingController
describe('API', () => {
  let apiService: ApiService;
  let httpMock: HttpTestingController;
  let errorResponse: any;
  let analyticsService: AnalyticsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ApiService,
        { provide: AnalyticsService, useClass: MockAnalyticsService },
      ],
    }).compileComponents();

    apiService = TestBed.inject(ApiService);
    httpMock = TestBed.inject(HttpTestingController);
    analyticsService = TestBed.inject(AnalyticsService);
  });
  describe('getCurrentCourses', () => {
    it('should return an Observable with courses', () => {
      let termCode = TermCode.decodeOrThrow('1222');
      // set up the subscription to the endpoint method with the validation checks on the result
      apiService.getCurrentCourses(termCode).subscribe(currentCourses => {
        expect(currentCourses.length).toBe(5);
        expect(currentCourses).toEqual(enrollmentsJson);
      });

      // Tell the mock endpoint to expect a specific endpoint request
      const request = httpMock.expectOne(
        `/api/enroll/v1/current/${TermCode.encode(termCode)}`,
      );
      expect(request.request.method).toBe('GET');

      // flush the results to trigger the subscription above to fire
      request.flush(enrollmentsJson);

      httpMock.verify();
    });
    it('should return an Error if an error occurs', () => {
      let termCode = TermCode.decodeOrThrow('1222');

      apiService.getCurrentCourses(termCode).subscribe(
        _currentCourses => {
          // we should never get here if there's an error,
          fail('Line should not be reached.');
        },
        err => (errorResponse = err),
      );

      const mockErrorResponse = { status: 400, statusText: 'Bad Request' };
      const data = 'Invalid request parameters';

      const request = httpMock.expectOne(
        `/api/enroll/v1/current/${TermCode.encode(termCode)}`,
      );
      expect(request.request.method).toBe('GET');
      // flush the error response to the subscription above
      request.flush(data, mockErrorResponse);

      httpMock.verify();

      expect(errorResponse).toBeTruthy();
    });
    it('should handle a schema error', () => {
      let schemaExceptionSpy = spyOn(
        analyticsService,
        'schemaException',
      ).and.callThrough();
      let termCode = TermCode.decodeOrThrow('1222');

      apiService.getCurrentCourses(termCode).subscribe(
        () => {
          // we should never get here if there's an error,
          // so I'm booby trapping this line with a test that will fail.
          fail('Line should not be reached.');
        },
        err => (errorResponse = err),
      );

      const request = httpMock.expectOne(
        `/api/enroll/v1/current/${TermCode.encode(termCode)}`,
      );
      expect(request.request.method).toBe('GET');
      request.flush({ invalidKey: 'Not here' });

      httpMock.verify();

      // check that we called the analytics service with the schema error
      expect(schemaExceptionSpy).toHaveBeenCalledTimes(1);
      expect(schemaExceptionSpy.calls.first().args[0]).toEqual(
        'getCurrentCourses',
      );
      expect(schemaExceptionSpy.calls.first().args[1].found).toEqual({
        invalidKey: 'Not here',
      });

      expect(errorResponse).toBeTruthy();
    });
  });

  describe('getStudentInfo', () => {
    it('should return an Observable with studentInfo', () => {
      // set up the subscription to the endpoint method with the validation checks on the result
      apiService.getStudentInfo().subscribe(info => {
        expect(info).toEqual(studentInfoJson);
      });

      // Tell the mock endpoint to expect a specific endpoint request
      const request = httpMock.expectOne(`/api/enroll/v1/studentInfo`);
      expect(request.request.method).toBe('GET');

      // flush the results to trigger the subscription above to fire
      request.flush(studentInfoJson);

      httpMock.verify();
    });
    it('should handle an error fetching studentInfo', () => {
      let errorResponse: any;

      // set up the subscription to the endpoint method with the validation checks on the result
      apiService.getStudentInfo().subscribe(
        _info => {
          // we should never get here if there's an error,
          fail('Line should not be reached.');
        },
        err => (errorResponse = err),
      );
      // Tell the mock endpoint to expect a specific endpoint request
      const request = httpMock.expectOne(`/api/enroll/v1/studentInfo`);
      expect(request.request.method).toBe('GET');

      // flush the results to trigger the subscription above to fire
      const mockErrorResponse = {
        status: 500,
        statusText: 'Internal Server Error',
      };
      const data = 'Invalid request parameters';
      request.flush(data, mockErrorResponse);
      expect(errorResponse).toBeTruthy();

      httpMock.verify();
    });
  });
});
