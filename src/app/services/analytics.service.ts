import { Injectable } from '@angular/core';
import { SchemaAssertionError } from '@app/types/schema';
import { environment } from '@env/environment';

interface ExceptionHit {
  hitType: 'exception';
  exDescription: string;
  exFatal: boolean;
}

interface PageViewHit {
  hitType: 'pageview';
  page: string;
}

interface EventHit {
  hitType: 'event';
  eventCategory: string;
  eventAction?: string;
  eventLabel?: string;
  eventValue?: number;
}

type AnyHit = ExceptionHit | PageViewHit | EventHit;

/**
 * We assume that this function exists because the Google Analytics library is
 * loaded within the index.html file. We also assume it's signature based on the
 * Google Analytics documentation.
 *
 * @link https://developers.google.com/analytics/devguides/collection/analyticsjs/command-queue-reference
 */
declare function ga(cmd: 'send', hit: AnyHit): void;

/**
 * To reduce the risk of typos we've created a type to capture all the event
 * categories and event actions we want to record. By having the names of events
 * captured in a type like this we make it easier to rename or remove events in
 * the future because any existing usages of those events will then trigger a
 * compile-time error.
 *
 * As we find more events that we want to track we'll add those to this
 * lookup-table.
 */
export interface KnownEvents {
  Filter: string;
  Audit: 'generate-audit';
  Cart:
    | 'add-course-no-pack'
    | 'add-course-with-pack'
    | 'remove-pack'
    | 'remove-course'
    | 'revalidate'
    | 'take-revalidate-hint'
    | 'move-to-favorites';
  Enrolled:
    | 'edit'
    | 'swap-courses'
    | `enrollment-${string}`
    | 'dropped-success'
    | 'dropped-failure';
  Favorites: 'add-course' | 'remove-course' | 'move-to-cart';
  Planner:
    | 'create-plan'
    | 'rename-plan'
    | 'switch-plan'
    | 'change-primary-plan'
    | 'remove-plan'
    | 'add-year'
    | 'show-grades'
    | 'hide-grades'
    | 'move-between-terms'
    | 'add-course'
    | 'remove-course';
}

@Injectable({ providedIn: 'root' })
export class AnalyticsService {
  pageview() {
    const PATHNAME_ALIASES: Array<[RegExp, string]> = [
      [/^\/search/, '/search'],
      [/^\/my-courses/, '/my-courses'],
      [/^\/scheduler/, '/scheduler'],
      [/^\/degree-planner/, '/degree-planner'],
      [/^\/dars\/\d+/, '/dars/<audit>'],
      [/^\/dars/, '/dars'],
    ];

    const alias = PATHNAME_ALIASES.find(([p]) => p.test(location.pathname));
    const pathname = alias?.[1] ?? location.pathname;
    const page = `${pathname}${location.search}`;
    const payload = { hitType: 'pageview' as const, page };

    if (environment.production) {
      ga('send', payload);
    } else {
      console.log(`ga('send', ${JSON.stringify(payload)})`);
    }
  }

  schemaException(label: string, err: SchemaAssertionError, showValue = false) {
    const exDescription = showValue
      ? `${label}: ${err.message} (${JSON.stringify(err.found)})`
      : `${label}: ${err.message}`;

    if (environment.production) {
      console.warn('sent exception to Google Analytics', err);
      ga('send', {
        hitType: 'exception',
        exDescription,
        exFatal: false,
      });
    } else {
      console.warn('did NOT send exception to Google Analytics (not prod env)');
      console.error(exDescription);
    }
  }

  event<Category extends keyof KnownEvents>(
    category: Category,
    action: KnownEvents[Category],
    label?: string,
  ): void {
    if (environment.production) {
      ga('send', {
        hitType: 'event',
        eventCategory: category,
        eventAction: action,
        eventLabel: label,
      });
    }
  }
}
