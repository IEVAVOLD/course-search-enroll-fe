import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { hasSchema } from '@app/core/utils';
import { Prefs } from '@app/state';
import * as s from '@app/types/schema';
import {
  Era,
  RawTerms,
  TermCode,
  TermCodeOrZero,
  WithEra,
  ZERO,
  Zero,
} from '@app/types/terms';
import { PlanId, RawPlan } from '@app/types/plans';
import { StudentInfo } from '@app/types/student';
import {
  CurrentCourse,
  DayOfTheWeek,
  HasClassNumber,
  HasCourseId,
  HasRelatedClassNumbers,
  HasSubject,
  HasSubjectCode,
  HasTermCode,
  RawCurrentCourse,
  RawDetails,
  RawEnrollmentPackage,
  RawRoadmapCourse,
  RawSavedCourse,
  RawSearchResults,
  RoadmapCourse,
  RoadmapCourseReady,
} from '@app/types/courses';
import { map } from 'rxjs/operators';
import {
  ExistingScheduleBreak,
  GenerateRequest,
  NewRawSchedules,
  NewScheduleBreak,
} from '@app/types/schedules';
import { EnrollRequest } from '@app/types/enroll';
import { fromUtcSecondsToMadisonSecondsWithoutDST } from '@app/shared/date-formats';
import { AnalyticsService } from '@app/services/analytics.service';
import { CreditRange, CreditValue } from '@app/types/credits';
import { DegreePrograms } from '@app/dars/models/degree-program';
import { CourseBase } from '@app/core/models/course';
import { StudentDegreeProgram } from '@app/dars/models/student-degree-program';
import { AuditMetadata } from '@app/dars/models/audit-metadata';
import { Audit } from '@app/dars/models/audit/audit';

export interface SearchParameters {
  selectedTerm: string;
  queryString: string;
  filters: unknown[];
  page: number;
  pageSize: number;
  sortOrder: 'SUBJECT' | 'CATALOG_NUMBER' | 'SCORE';
}

export interface EditEnrolledCourse {
  credits: number;
  honors: 'Y' | 'N';
  waitlist: boolean;
  relatedClassNumber1?: number;
  relatedClassNumber2?: number;
  classPermissionNumber: string | null;
}

@Injectable({ providedIn: 'root' })
export class ApiService {
  // prettier-ignore
  private reportSchemaError: (label: string, showValue?: boolean) => (err: s.SchemaAssertionError, value?: string) => void;

  constructor(private http: HttpClient, private analytics: AnalyticsService) {
    this.reportSchemaError = (label, showValue = false) => {
      return err => {
        this.analytics.schemaException(label, err, showValue);
      };
    };
  }

  public getPrefs() {
    return this.http
      .get<unknown>(`/api/planner/v1/preferences`)
      .pipe(hasSchema(s.object<Prefs>({}), this.reportSchemaError('getPrefs')));
  }

  public postPrefs(prefs: Prefs) {
    return this.http.post<Prefs>(`/api/planner/v1/preferences`, prefs);
  }

  public getAggregate() {
    return this.http.get<RawTerms>(`/api/search/v1/aggregate`);
  }

  public getShortSubjectNames() {
    return this.http
      .get(`/api/search/v1/subjectsMap/0000`)
      .pipe(hasSchema(s.record(s.string)));
  }

  public getDegreePlans() {
    return this.http
      .get(`/api/planner/v1/degreePlan`)
      .pipe(
        hasSchema(s.array(RawPlan), this.reportSchemaError('getDegreePlans')),
      );
  }

  public getStudentInfo() {
    return this.http
      .get<unknown>(`/api/enroll/v1/studentInfo`)
      .pipe(
        hasSchema(StudentInfo.schema, this.reportSchemaError('getStudentInfo')),
      );
  }

  public getRoadmapCourses(termCode: TermCode) {
    return this.http
      .get<unknown>(`/api/planner/v1/roadmap/${TermCode.encode(termCode)}`)
      .pipe(
        hasSchema(
          s.array(RawRoadmapCourse.schema),
          this.reportSchemaError('getRoadmapCourses'),
        ),
      );
  }

  public postRoadmapCourseWithoutPackage(
    course: HasTermCode & HasSubjectCode & HasCourseId,
  ) {
    const encoded = TermCode.encode(course.termCode);
    const subjectCode = course.subject.code;
    const courseId = course.courseId;
    const url = `/api/planner/v1/roadmap/${encoded}/${subjectCode}/${courseId}`;
    return this.http.post<void>(url, {}).pipe(
      hasSchema(
        s.object({
          id: s.number,
          studentEnrollmentStatus: s.string,
          creditMin: s.number,
          creditMax: s.number,
        }),
      ),
    );
  }

  public getPlanCourses(planId: PlanId) {
    const url = `/api/planner/v1/degreePlan/${planId}/courses`;
    // TODO: replace use of `CourseBase` type
    return this.http.get<CourseBase[]>(url);
  }

  public postPlanCourse(
    planId: PlanId,
    course: HasTermCode & HasSubjectCode & HasCourseId,
  ) {
    const payload = {
      subjectCode: course.subject.code,
      courseId: course.courseId,
      termCode: TermCode.encode(course.termCode),
    };

    const schema = s.object({
      id: s.number,
      studentEnrollmentStatus: s.string,
      creditMin: s.number,
      creditMax: s.number,
    });

    return this.http
      .post(`/api/planner/v1/degreePlan/${planId}/courses`, payload)
      .pipe(hasSchema(schema));
  }

  public putPlanCourse(
    planId: PlanId,
    course: { plannedCourseId: number; creditRange: CreditRange },
    termCode: TermCode,
  ) {
    const id = course.plannedCourseId;
    const url = `/api/planner/v1/degreePlan/${planId}/courses/${id}`;

    // If the course is non-variable credits, make sure the credit value written
    // is non-zero so that any DARS reports created using this planned course
    // work correctly.
    //
    // If the course *is* variable credit, don't set pass a credits value. The
    // backend will set the credit value to zero.
    const credits: CreditValue | undefined =
      course.creditRange.min === course.creditRange.max
        ? course.creditRange.min
        : undefined;

    const params = { termCode: TermCode.encode(termCode) };
    const schema = s.object({ studentEnrollmentStatus: s.string });
    return this.http.put(url, { credits }, { params }).pipe(hasSchema(schema));
  }

  public deletePlanCourse(planId: PlanId, course: { plannedCourseId: number }) {
    const id = course.plannedCourseId;
    const url = `/api/planner/v1/degreePlan/${planId}/courses/${id}`;
    return this.http.delete<unknown>(url);
  }

  public postRoadmapCourseWithPackage(
    course: HasTermCode & HasSubjectCode & HasCourseId,
    pack: HasClassNumber & HasRelatedClassNumbers,
    credits: number,
    joinWaitlist: boolean,
    withHonors: boolean,
    classPermissionNumber: number | null,
  ) {
    const payload: any = {
      credits,

      // omit property from JSON payload if not true
      waitlist: joinWaitlist ? true : undefined,

      // omit property from JSON payload if not true
      honors: withHonors ? true : undefined,

      classPermissionNumber,
    };

    pack.relatedClassNumbers.forEach((classNumber, index) => {
      payload[`relatedClassNumber${index + 1}`] = classNumber;
    });

    const encoded = TermCode.encode(course.termCode);
    const subjectCode = course.subject.code;
    const courseId = course.courseId;
    const classNumber = pack.classNumber;
    const url = `/api/planner/v1/roadmap/${encoded}/${subjectCode}/${courseId}/${classNumber}`;
    return this.http.post<void>(url, payload);
  }

  public deleteRoadmapCourses(course: HasTermCode & HasSubject & HasCourseId) {
    const termCode = TermCode.encode(course.termCode);
    const subjectCode = course.subject.code;
    const courseId = course.courseId;
    const url = `/api/planner/v1/roadmap/${termCode}/${subjectCode}/${courseId}`;
    return this.http.delete<void>(url);
  }

  public postClearRoadmapPackageOptions(
    termCode: TermCode,
    subjectCode: string,
    courseId: string,
  ) {
    const encoded = TermCode.encode(termCode);
    const url = `/api/planner/v1/roadmap/${encoded}/${subjectCode}/${courseId}`;
    return this.http.post<void>(url, {});
  }

  public postValidation(termCode: TermCode, choices: ValidationChoice[]) {
    const encoded = TermCode.encode(termCode);
    const url = `/api/enroll/v1/validate/${encoded}`;
    return this.http.post<unknown>(url, choices);
  }

  public getCurrentCourses(termCode: TermCode) {
    return this.http
      .get<unknown>(`/api/enroll/v1/current/${TermCode.encode(termCode)}`)
      .pipe(
        hasSchema(
          s.array(RawCurrentCourse.schema),
          this.reportSchemaError('getCurrentCourses'),
        ),
      );
  }

  public postEnroll(termCode: TermCode, requests: EnrollRequest[]) {
    const url = `/api/enroll/v1/enroll/${TermCode.encode(termCode)}`;
    return this.http.post<void>(url, requests);
  }

  public putEnroll(termCode: TermCode, request: EnrollRequest) {
    const payload: EditEnrolledCourse = {
      credits: request.options.credits,
      waitlist: request.options.waitlist === true,
      honors: request.options.honors ? 'Y' : 'N',
      classPermissionNumber: request.options.classPermissionNumber
        ? `${request.options.classPermissionNumber}`
        : null,
    };

    if (request.options.relatedClassNumber1 !== null) {
      payload.relatedClassNumber1 = parseInt(
        request.options.relatedClassNumber1,
        10,
      );
    }

    if (request.options.relatedClassNumber2 !== null) {
      payload.relatedClassNumber2 = parseInt(
        request.options.relatedClassNumber2,
        10,
      );
    }

    const encoded = TermCode.encode(termCode);
    const url = `/api/enroll/v1/update/${encoded}/${request.classNumber}`;
    return this.http.put<void>(url, payload);
  }

  public postDrop(termCode: TermCode, packs: HasClassNumber[]) {
    const encoded = TermCode.encode(termCode);
    const payload = packs.map(pack => pack.classNumber);

    const messages = s.array(
      s.object({
        messageSeverity: s.string,
        instanceId: s.string, // string form of classNumber
        description: s.string,
      }),
    );

    return this.http
      .post<unknown>(`/api/enroll/v1/drop/${encoded}`, payload)
      .pipe(hasSchema(messages, this.reportSchemaError('postDrop', true)));
  }

  /**
   * The method uses 'favorites' intead of 'saved-for-later' because that's what
   * word the endpoint URL uses.
   */
  public getFavorites() {
    return this.http
      .get<unknown>(`/api/planner/v1/favorites`)
      .pipe(
        hasSchema(
          s.array(RawSavedCourse.schema),
          this.reportSchemaError('getFavorites'),
        ),
      );
  }

  /**
   * The method uses 'favorites' intead of 'saved-for-later' because that's what
   * word the endpoint URL uses.
   */
  public postFavorite(subjectCode: string, courseId: string) {
    const url = `/api/planner/v1/favorites/${subjectCode}/${courseId}`;
    return this.http.post<void>(url, {});
  }

  /**
   * The method uses 'favorites' intead of 'saved-for-later' because that's what
   * word the endpoint URL uses.
   */
  public deleteFavorite(course: HasSubjectCode & HasCourseId) {
    const subject = course.subject.code;
    const courseId = course.courseId;
    const url = `/api/planner/v1/favorites/${subject}/${courseId}`;
    return this.http.delete<void>(url);
  }

  public getCourseDetails(
    termCode: WithEra | Zero,
    subjectCode: string,
    courseId: string,
  ) {
    const isActive = (termCode as any)?.era === Era.Active;
    const encoded = TermCodeOrZero.encode(isActive ? termCode : ZERO);
    const url = `/api/search/v1/course/${encoded}/${subjectCode}/${courseId}`;
    return this.http
      .get<unknown>(url)
      .pipe(
        hasSchema(
          RawDetails.schema,
          this.reportSchemaError('getCourseDetails'),
        ),
      );
  }

  public getDetails(
    termCode: TermCodeOrZero,
    subjectCode: string,
    courseId: string,
  ) {
    const encoded = TermCodeOrZero.encode(termCode);
    const url = `/api/search/v1/course/${encoded}/${subjectCode}/${courseId}`;
    return this.http
      .get<unknown>(url)
      .pipe(hasSchema(RawDetails.schema, this.reportSchemaError('getDetails')));
  }

  public postSearch(params: SearchParameters) {
    return this.http.post<RawSearchResults>(`/api/search/v1`, params);
  }

  public getEnrollmentPackages(course: HasTermCode & HasSubject & HasCourseId) {
    const termCode = TermCode.encode(course.termCode);
    const subject = course.subject.code;
    const courseId = course.courseId;
    const url = `/api/search/v1/enrollmentPackages/${termCode}/${subject}/${courseId}`;
    return this.http
      .get<unknown>(url)
      .pipe(
        hasSchema(
          s.array(RawEnrollmentPackage.schema),
          this.reportSchemaError('getEnrollmentPackages'),
        ),
      );
  }

  public getEnrollmentPackageByDocId(termCode: TermCode, docId: string) {
    const query = {
      size: '1000',
      query: {
        filtered: {
          filter: {
            bool: { must: [{ term: { _id: docId } }] },
          },
        },
      },
    };

    const foundOneMatch = s.object({
      hits: s.object({
        hits: s.one(s.object({ _source: s.any })),
      }),
    });

    const url = `/api/search/v1/enrollmentPackage/${TermCode.encode(termCode)}`;
    return this.http.post<unknown>(url, query).pipe(
      hasSchema(foundOneMatch),
      map(response => response.hits.hits[0]._source),

      // For some reason, the objects returned by the search API are missing the
      // `docId` property so manually add it here before checking the object's
      // schema.
      map(match => ({ ...match, docId })),

      // Make sure the object matches all the frontend's assumptions about what
      // properties and types enrollment packages have.
      hasSchema(
        RawEnrollmentPackage.schema,
        this.reportSchemaError('getEnrollmentPackageByDocId'),
      ),
    );
  }

  public getPackageByClassNumber(termCode: TermCode, classNumber: number) {
    const query = {
      size: 200,
      query: {
        match: { enrollmentClassNumber: classNumber },
      },
    };

    const expected = s.object({
      hits: s.object({
        hits: s.array(
          s.object({
            _id: s.string, // the docId
            _source: s.any, // the package JSON without a docId property
          }),
        ),
      }),
    });

    const url = `/api/search/v1/enrollmentPackage/${TermCode.encode(termCode)}`;
    return this.http.post<unknown>(url, query).pipe(
      hasSchema(expected, this.reportSchemaError('getPackageByClassNumber')),
      map(({ hits }) =>
        hits.hits.map(({ _id, _source }) => ({ ..._source, docId: _id })),
      ),
      hasSchema(
        s.array(RawEnrollmentPackage.schema),
        this.reportSchemaError('getPackageByClassNumber'),
      ),
    );
  }

  public postSwap(enroll: RoadmapCourseReady, drop: CurrentCourse) {
    const termCode = TermCode.encode(enroll.termCode);
    const courseId = enroll.courseId;
    const classNumber = enroll.package.classNumber;
    const payload = {
      dropCourseId: drop.courseId,
      dropPackageNumber: drop.package.classNumber,
      relatedClassNumber1: drop.package.relatedClassNumber1,
      relatedClassNumber2: drop.package.relatedClassNumber2,
    };

    const repsonse = s.array(
      s.object({
        messageSeverity: s.string,
        description: s.string,
      }),
    );

    const url = `/api/enroll/v1/swap/${termCode}/${courseId}/${classNumber}`;
    return this.http.post<unknown>(url, payload).pipe(hasSchema(repsonse));
  }

  public getLastEnrollment() {
    return this.http
      .get<unknown>(`/api/lastenrollment`)
      .pipe(
        hasSchema(
          s.nullable(s.array(s.unknown)),
          this.reportSchemaError('getLastEnrollment'),
        ),
      );
  }

  public getCartStatus() {
    return this.http.get<unknown>(`/api/pending`);
  }

  /**
   * @deprecated
   */
  public getPending() {
    const inProgress = s.object({ status: s.constant('InProgress') });

    return this.http.get<unknown>(`/api/pending`).pipe(
      map(raw => {
        if (inProgress.matches(raw)) {
          return 'pending' as const;
        } else {
          return 'done' as const;
        }
      }),
    );
  }

  public postSchedules(payload: GenerateRequest) {
    const url = `/api/scheduler/v1/schedules/${payload.termCode}`;
    return this.http.post<unknown>(url, payload, {
      responseType: 'text' as 'json', // Angular, why is this necessary? Be better
    });
  }

  public getSchedulerStatus() {
    const url = `/scheduling/status`;
    return this.http.get<unknown>(url);
  }

  /**
   * Can return `null` if there are no cached schedules. This could indicate
   * that the user had generated schedules for the term but those cached
   * schedules have since been cleared. Or it could indicate that the user has
   * never generated schedules for the term.
   */
  public getScheduleResults(termCode: TermCode) {
    const url = `/scheduling/results?termCode=${TermCode.encode(termCode)}`;
    return this.http
      .get<unknown>(url)
      .pipe(
        hasSchema(
          s.nullable(NewRawSchedules.schema),
          this.reportSchemaError('getScheduleResults'),
        ),
      );
  }

  public postScheduleBreak(termCode: TermCode, brk: NewScheduleBreak) {
    let storedDaysOfWeek = '';
    storedDaysOfWeek += brk.days.sun ? DayOfTheWeek.longToShort.SUNDAY : '';
    storedDaysOfWeek += brk.days.mon ? DayOfTheWeek.longToShort.MONDAY : '';
    storedDaysOfWeek += brk.days.tue ? DayOfTheWeek.longToShort.TUESDAY : '';
    storedDaysOfWeek += brk.days.wed ? DayOfTheWeek.longToShort.WEDNESDAY : '';
    storedDaysOfWeek += brk.days.thu ? DayOfTheWeek.longToShort.THURSDAY : '';
    storedDaysOfWeek += brk.days.fri ? DayOfTheWeek.longToShort.FRIDAY : '';
    storedDaysOfWeek += brk.days.sat ? DayOfTheWeek.longToShort.SATURDAY : '';

    /**
     * The backend wants the times as milliseconds after midnight in the Madison
     * timezone. Since the times are initially encoded as seconds after midnight
     * in UTC, add 6 hrs (timezone offset) then multiply each timestamp by 1000
     * (to convert from seconds to milliseconds).
     */
    const startTimeInMs =
      1000 * fromUtcSecondsToMadisonSecondsWithoutDST(brk.times.startSeconds);
    const endTimeInMs =
      1000 * fromUtcSecondsToMadisonSecondsWithoutDST(brk.times.endSeconds);

    const payload = {
      termCode: TermCode.encode(termCode),
      name: brk.label,
      storedDaysOfWeek,
      startTime: startTimeInMs,
      endTime: endTimeInMs,

      /**
       * This is some real nightmare fuel here. If the `startDate`/`endDate`
       * fields aren't provided, the backend (a Spring Java app) will try and
       * interpret the start/end integers using the `LocalDate.MIN` and
       * `LocalDate.MAX` constants which seems to cause all kinds of issues like
       * the request being rejected with a 500 error code and a message like
       * "java.lang.ArithmeticException: long overflow".
       *
       * - Isaac
       */
      startDate: 21600000, // 1/1/1970 00:00
      endDate: 17514165600000, // 1/1/2525 00:00
    };

    return this.http.post<void>(`/api/scheduler/v1/blocks`, payload);
  }

  public putScheduleBreak(termCode: TermCode, brk: ExistingScheduleBreak) {
    let storedDaysOfWeek = '';
    storedDaysOfWeek += brk.days.sun ? DayOfTheWeek.longToShort.SUNDAY : '';
    storedDaysOfWeek += brk.days.mon ? DayOfTheWeek.longToShort.MONDAY : '';
    storedDaysOfWeek += brk.days.tue ? DayOfTheWeek.longToShort.TUESDAY : '';
    storedDaysOfWeek += brk.days.wed ? DayOfTheWeek.longToShort.WEDNESDAY : '';
    storedDaysOfWeek += brk.days.thu ? DayOfTheWeek.longToShort.THURSDAY : '';
    storedDaysOfWeek += brk.days.fri ? DayOfTheWeek.longToShort.FRIDAY : '';
    storedDaysOfWeek += brk.days.sat ? DayOfTheWeek.longToShort.SATURDAY : '';

    /**
     * The backend wants the times as milliseconds after midnight in the Madison
     * timezone. Since the times are initially encoded as seconds after midnight
     * in UTC, add 6 hrs (timezone offset) then multiply each timestamp by 1000
     * (to convert from seconds to milliseconds).
     */
    const startTimeInMs =
      1000 * fromUtcSecondsToMadisonSecondsWithoutDST(brk.times.startSeconds);
    const endTimeInMs =
      1000 * fromUtcSecondsToMadisonSecondsWithoutDST(brk.times.endSeconds);

    const payload = {
      termCode: TermCode.encode(termCode),
      name: brk.label,
      storedDaysOfWeek,
      startTime: startTimeInMs,
      endTime: endTimeInMs,

      /**
       * This is some real nightmare fuel here. If the `startDate`/`endDate`
       * fields aren't provided, the backend (a Spring Java app) will try and
       * interpret the start/end integers using the `LocalDate.MIN` and
       * `LocalDate.MAX` constants which seems to cause all kinds of issues like
       * the request being rejected with a 500 error code and a message like
       * "java.lang.ArithmeticException: long overflow".
       *
       * - Isaac
       */
      startDate: 21600000, // 1/1/1970 00:00
      endDate: 17514165600000, // 1/1/2525 00:00
    };

    return this.http.put<void>(`/api/scheduler/v1/blocks/${brk.id}`, payload);
  }

  public deleteScheduleBreak(id: number) {
    return this.http.delete<void>(`/api/scheduler/v1/blocks/${id}`);
  }

  public getAllDegreePrograms() {
    const url = `/api/data/degreeprograms.json`;
    return this.http.get<DegreePrograms>(url);
  }

  public getStudentDegreePrograms() {
    const url = `/api/dars/studentplans`;
    return this.http.get<StudentDegreeProgram[]>(url);
  }

  public getAuditMetadata() {
    const url = `/api/dars/auditmetadata`;
    return this.http.get<AuditMetadata[]>(url);
  }

  public getAudit(reportId: number) {
    const url = `/api/dars/auditresults`;
    return this.http.get<Audit>(url, { params: { reportId } });
  }

  public postAudit(params: {
    darsInstitutionCode: string;
    darsDegreeProgramCode: string;
    degreePlannerPlanName?: string;
    whichEnrolledCoursesIncluded?: string;
    darsHonorsOptionCode: string;
  }) {
    const url = `/api/dars/auditrequest`;
    return this.http.post<{ darsJobId: string }>(url, { ...params });
  }
}

export interface ValidationChoice {
  courseId: string;
  subjectCode: string;
  classNumber: number | null;
  options: {
    credits: number;
    honors: boolean;
    waitlist: boolean;
    passFail: boolean;
    classPermissionNumber: number | null;
    relatedClassNumber1: number | null;
    relatedClassNumber2: number | null;
  };
}

export namespace ValidationChoice {
  export const fromRoadmapCourse = (
    course: RoadmapCourse | RoadmapCourseReady,
  ): ValidationChoice => {
    return {
      courseId: course.courseId,
      subjectCode: course.subject.code,
      classNumber: course.package?.classNumber ?? null,
      options: {
        // Yes, when the package hasn't been chosen or the credit value
        // hasn't been set, use 0 for the credits. At least this is how the
        // AngularJS app did it. - Isaac
        credits: course.package?.choices?.credits?.total ?? 0,

        // Set to true iff the user has opted to take with honors.
        honors: course.package?.choices?.honors === 'chosen',

        // Set to true iff the user has opted to waitlist.
        waitlist: course.package?.choices?.waitlist === 'chosen',

        // Set to true iff the course gradingBasis == "CNC"
        passFail: course.details.isPassFail,

        // Values copied directly from the RawRoadmapCourse object
        classPermissionNumber: course.classPermissionNumber,
        relatedClassNumber1: course.package?.relatedClassNumbers?.[0] ?? null,
        relatedClassNumber2: course.package?.relatedClassNumbers?.[1] ?? null,
      },
    };
  };
}
