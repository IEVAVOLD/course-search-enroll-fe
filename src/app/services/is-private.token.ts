import { InjectionToken } from '@angular/core';
import * as s from '@app/types/schema';

const isPrivate = s.object({
  config: s.object({
    public: s.constant(false),
  }),
});

export const IS_PRIVATE = new InjectionToken<boolean>('IsPrivate', {
  providedIn: 'root',
  factory: () => isPrivate.matches(window),
});
