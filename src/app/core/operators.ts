/**
 * # Custom RxJS operators
 *
 * Some reactive patterns have cropped up often enough that it makes sense for
 * code-reuse and readability reasons to package the pattern into its own
 * operator.
 */

import { interval, Observable, OperatorFunction, pipe } from 'rxjs';
import { mergeMap, takeWhile, last, filter, first, map } from 'rxjs/operators';
import * as s from '@app/types/schema';
import { Failed, isFailed, isSuccess, Loadable, Success } from 'loadable.ts';

/**
 * Calls the `poll` parameter repeatedly as long as the response from that
 * function is an object with a `"status"` property that is a string contained
 * in the `statuses` paraemter. This operator returns an observable that will
 * emit once with the last result from calling the `poll` function.
 *
 * Typically this is used to poll an API endpoint until some asynchronous job
 * has completed.
 */
export const pollWhileStatusIsOneOf = <Input, Output>(
  statuses: string[],
  poll: (input: Input) => Observable<Output>,
  delay = 1000,
): OperatorFunction<Input, Output> => {
  const hasStatus = s.object({
    status: s.string,
  });

  return mergeMap(input => {
    // Call the `poll` function every cycle (usually once per second)
    // with whatever object was passed into this operator.
    return interval(delay).pipe(
      mergeMap(() => poll(input)),

      // Keep calling the `poll` function as long as the response
      // is some JSON object like `{ "status": "InProgress" }`.
      takeWhile(response => {
        if (hasStatus.matches(response)) {
          return statuses.includes(response.status);
        } else {
          return false;
        }
      }, true),

      // Wait until polling is done before emitting
      // anything to the next operator.
      last(),
    );
  });
};

/**
 * Given an observable over a loadable value, emit the first value that is
 * either a success or a failure. The observable completes after the first
 * matching value is emitted.
 */
export const firstNonLoading = <T>(): OperatorFunction<
  Loadable<T>,
  Success<T> | Failed<T>
> => {
  return pipe(
    filter((loadable): loadable is Success<T> | Failed<T> => {
      return isSuccess(loadable) || isFailed(loadable);
    }),
    first(),
  );
};

/**
 * Emit only the first `Success<T>`. Completes after the first value is emitted.
 */
export const waitUntilFirstSuccessThenUnwrap = <T>(): OperatorFunction<
  Loadable<T>,
  T
> => {
  return pipe(
    filter((loadable): loadable is Success<T> => isSuccess(loadable)),
    first(),
    map(success => success.value),
  );
};

export const takeFirstSuccess = <T>(
  { skipErrors } = { skipErrors: false },
): OperatorFunction<Loadable<T> | null, T> => {
  return src => {
    return new Observable(subscriber => {
      src.subscribe({
        next(loadable) {
          if (loadable && isFailed(loadable) && !skipErrors) {
            subscriber.error(loadable.error);
          } else if (loadable && isSuccess(loadable)) {
            subscriber.next(loadable.value);
            subscriber.complete();
          }
        },
        error(err) {
          subscriber.error(err);
        },
        complete() {
          subscriber.complete();
        },
      });
    });
  };
};
