import { groupBy } from './utils';

test('empty array', () => {
  expect(groupBy([], (_: any) => 'x')).toStrictEqual({});
});

test('even odd', () => {
  const arr = [0, 1, 2, 3, 4, 5];
  const fn = (num: number) => (num % 2 === 0 ? 'even' : 'odd');

  expect(groupBy(arr, fn)).toStrictEqual({
    even: [0, 2, 4],
    odd: [1, 3, 5],
  });
});
