import { from, of, throwError } from 'rxjs';
import {
  firstNonLoading,
  pollWhileStatusIsOneOf,
  takeFirstSuccess,
  waitUntilFirstSuccessThenUnwrap,
} from '@app/core/operators';
import { failed, loading, success } from 'loadable.ts';

describe('Operators', () => {
  it('pollWhileStatusIsOneOf with success', done => {
    let called = 0;
    let statusesToReturn = ['InProgress', 'InProgress', 'Done'];
    let pollFunc = () => {
      return of({ status: statusesToReturn[called++] });
    };
    of({})
      .pipe(pollWhileStatusIsOneOf(['InProgress'], pollFunc, 100))
      .subscribe(result => {
        expect(called).toEqual(statusesToReturn.length);
        expect(result.status).toEqual(
          statusesToReturn[statusesToReturn.length - 1],
        );
        done();
      });
  });
  it('pollWhileStatusIsOneOf with failure', done => {
    let called = 0;
    let statusesToReturn = ['InProgress', 'InProgress', 'Error'];
    let pollFunc = () => {
      return of({ status: statusesToReturn[called++] });
    };
    of({})
      .pipe(pollWhileStatusIsOneOf(['InProgress'], pollFunc, 100))
      .subscribe(result => {
        expect(called).toEqual(statusesToReturn.length);
        expect(result.status).toEqual(
          statusesToReturn[statusesToReturn.length - 1],
        );
        done();
      });
  });
  it('pollWhileStatusIsOneOf with exception', done => {
    let pollFunc = () => {
      return throwError('ERROR');
    };
    of({})
      .pipe(pollWhileStatusIsOneOf(['InProgress'], pollFunc, 100))
      .subscribe(
        () => {
          //we will never reach this line if there is a network error
          fail('Line should not be reached.');
        },
        () => {
          done();
        },
        () => done(),
      );
  });
  it('firstNonLoading with success', done => {
    from([loading(), success('Value3'), failed('Value2')])
      .pipe(firstNonLoading())
      .subscribe(result => {
        expect(result).toEqual(success('Value3'));
        done();
      });
  });
  it('firstNonLoading with failure', done => {
    from([loading(), failed('Value2'), success('Value3')])
      .pipe(firstNonLoading())
      .subscribe(result => {
        expect(result).toEqual(failed('Value2'));
        done();
      });
  });
  it('waitUntilFirstSuccessThenUnwrap', done => {
    from([loading(), failed('Value2'), success('Value3')])
      .pipe(waitUntilFirstSuccessThenUnwrap())
      .subscribe(_result => {
        expect(_result).toEqual('Value3');
        done();
      });
  });
  it('takeFirstSuccess and skip errors', done => {
    from([loading(), failed('Value2'), success('Value3')])
      .pipe(takeFirstSuccess({ skipErrors: true }))
      .subscribe(
        result => {
          expect(result).toEqual('Value3');
          done();
        },
        err => fail(err),
      );
  });
  it('takeFirstSuccess and fail errors', done => {
    from([loading(), failed('Value2'), success('Value3')])
      .pipe(takeFirstSuccess({ skipErrors: false }))
      .subscribe(
        () => {
          //we will never reach this line if there is an error
          fail('Line should not be reached.');
        },
        err => {
          expect(err).toEqual('Value2');
          done();
        },
      );
  });
});
