/**
 * These interfaces are a bit sparse, the real objects will have more fields but
 * these are the only fields needed to convert a flat list of enrollment packages
 * into a nested list where enrollment packages with the same lecture are grouped
 * together.
 *
 * Some enrollment packages don't have a lecture. Any enrollment packages WITHOUT
 * a lecture aren't grouped and are left alone.
 *
 * Call the `groupPackages` function to run the grouping algorithm.
 */

import {
  EnrollmentPackage,
  EnrollmentSection,
  RawEnrollmentPackage,
  RawSection,
} from '@app/types/courses';

export type PackageGroup = NestedPackageGroup | ShallowPackageGroup;

export interface NestedPackageGroup {
  kind: 'nested';
  lecture: EnrollmentSection;
  packages: NestedPackage[];
}

export interface NestedPackage extends EnrollmentPackage {
  isNested: true;
  sectionsWithoutLecture: EnrollmentSection[];
}

export interface ShallowPackageGroup {
  kind: 'shallow';
  package: ShallowPackage;
}

export interface ShallowPackage extends EnrollmentPackage {
  isNested: false;
}

/** Takes in a list of enrollment packages and groups them by lecture section without
 *  disturbing the enrollment package order.
 *
 * @param packs An array of enrollment packages that will be grouped together
 * @returns An array of package groups, possibly containing both nested and shallow package groups
 */
export function groupPackages(packs: EnrollmentPackage[]): PackageGroup[] {
  const groupedPackages: { [index: string]: PackageGroup } = {};

  for (let index = 0; index < packs.length; index++) {
    const pack = formatPackage(packs[index]);
    if (pack.kind === 'nested') {
      //Create a key composed of 'LEC' and the LEC section number. If this key exists,
      // that indicates a nested group. Merge the package into the existing package group.
      const key = `${pack.lecture.type} ${pack.lecture.sectionNumber}`;
      groupedPackages[key] = groupedPackages[key]
        ? mergeNested(groupedPackages[key] as NestedPackageGroup, pack)
        : pack;
    } else {
      //Add a property with a unique-per-package ID and a value of a shallow package
      const key = pack.package.docId;
      groupedPackages[key] = pack;
    }
  }
  //Strip out the keys and get an array of nested and shallow packages
  const processedPackages = Object.values(groupedPackages);
  return processedPackages;
}
/**
 * Sorts the enrollment packages raw data according to logic specified by the RO.
 * @param incomingPackages An array of raw enrollment packages to be sorted
 * @returns A sorted array of raw enrollment packages
 */
export function sortPackages(incomingPackages: RawEnrollmentPackage[]) {
  const outgoingPackages = incomingPackages.slice();
  outgoingPackages.sort(sortPackagesHelper);
  return outgoingPackages;
}

/**
 * Helper sort function for sortPackages.
 * Sorts by a package's lecture section number if it has one (indicated by LEC),
 *  then by by count of sections in package,
 *  then by all other section numbers as listed in CAOS.
 *
 * Quirks: It is theoretically possible (but hasn't been observed) that a package may have
 *  multiples of the same section type. If that happens, the sort order will depend on the
 *  order in which sections were placed in the enrollment package sections array.
 *
 * @param a First comparison argument
 * @param b Second comparision argument
 */
function sortPackagesHelper(
  a: RawEnrollmentPackage,
  b: RawEnrollmentPackage,
): number {
  const SORT_A_FIRST = -1;
  const SORT_B_FIRST = +1;
  const NO_SORTING_PREFERENCE = 0;

  let aSections: { [index: string]: number } = {};
  let bSections: { [index: string]: number } = {};

  //Populate dictionaries of section numbers for each package.
  for (let index = 0; index < a.sections.length; index++) {
    let key = a.sections[index].type;
    let val = parseInt(a.sections[index].sectionNumber);
    aSections[key] = val;
  }
  for (let index = 0; index < b.sections.length; index++) {
    let key = b.sections[index].type;
    let val = parseInt(b.sections[index].sectionNumber);
    bSections[key] = val;
  }

  const aHasLEC = aSections.hasOwnProperty('LEC');
  const bHasLEC = bSections.hasOwnProperty('LEC');

  // If only one of the two sections has a LEC, prefer the package with a LEC
  if (aHasLEC && bHasLEC === false) {
    return SORT_A_FIRST;
  } else if (aHasLEC === false && bHasLEC) {
    return SORT_B_FIRST;
  }

  // Both packages have lectures with different section numbers
  // Can rank according to lecture number
  if (aHasLEC && bHasLEC && aSections['LEC'] !== bSections['LEC']) {
    return aSections['LEC'] - bSections['LEC'];
  }

  for (let i = 0; i < Math.max(a.sections.length, b.sections.length); i++) {
    const aSection = a.sections[i] ?? null;
    const bSection = b.sections[i] ?? null;

    // Handle the case where both packs were equivalent up until this point. If
    // the packs have different numbers of sections but all existing sections
    // are equivalent, rank the pack with more sections higher.
    if (!aSection) {
      return SORT_B_FIRST;
    } else if (!bSection) {
      return SORT_A_FIRST;
    }

    if (aSection.sectionNumber < bSection.sectionNumber) {
      return SORT_A_FIRST;
    } else if (aSection.sectionNumber > bSection.sectionNumber) {
      return SORT_B_FIRST;
    }
  }

  // If we get to this point, the packages may as well be equal as far as sorting goes.
  return NO_SORTING_PREFERENCE;
}

export function groupWithoutNesting(
  packs: EnrollmentPackage[],
): ShallowPackageGroup[] {
  return packs.map(pack => ({
    kind: 'shallow',
    package: { ...pack, isNested: false },
  }));
}

/** Merges two or more nested package groups together.
 *
 * @param a A nested package group to which the new package group will be added
 * @param b The nested package group to add
 * @returns A merged nested package group that contains all of 'a' plus 'b'
 */
function mergeNested(a: NestedPackageGroup, b: NestedPackageGroup) {
  return {
    ...a,
    packages: [...a.packages, ...b.packages],
  } as NestedPackageGroup;
}

const isLecture = (s: RawSection): boolean => s.type === 'LEC';
const isNotLecture = (s: RawSection): boolean => s.type !== 'LEC';

/** Formats an enrollment package based on whether it has exactly one lecture section
 *  and at least one other non-lecture section. Based on this, the return type is either a nested
 *  package group or a shallow package group.
 *
 * @param pack The enrollment package to be formatted
 * @returns The enrollment package formatted according to whether it follows a nested or shallow structure
 */
function formatPackage(pack: EnrollmentPackage): PackageGroup {
  const areLectures = pack.sections.filter(isLecture);
  const areNotLectures = pack.sections.filter(isNotLecture);

  if (areLectures.length === 1 && areNotLectures.length > 0) {
    return {
      kind: 'nested',
      lecture: areLectures[0],
      packages: [
        {
          ...pack,
          isNested: true,
          sectionsWithoutLecture: areNotLectures,
        },
      ],
    };
  } else {
    return {
      kind: 'shallow',
      package: { ...pack, isNested: false },
    };
  }
}
