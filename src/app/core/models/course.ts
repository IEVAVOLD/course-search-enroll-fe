export interface CourseBase {
  id: number | null;
  courseId: string;
  termCode: string | null;
  topicId: number;
  title: string;
  subjectCode: string;
  subject: any;
  catalogNumber: string;
  credits?: number;
  creditMin?: number;
  creditMax?: number;
  grade?: any;
  classNumber: string | null;
  courseOrder: number;
  honors: string;
  waitlist: string;
  relatedClassNumber1?: any;
  relatedClassNumber2?: any;
  classPermissionNumber?: any;
  sessionCode?: any;
  validationResults: any[];
  enrollmentResults: any[];
  pendingEnrollments: any[];
  details?: any;
  classMeetings?: any;
  enrollmentOptions?: any;
  packageEnrollmentStatus?: any;
  creditRange?: any;
  subjectDescription?: string;
  studentEnrollmentStatus:
    | null
    | 'Transfer'
    | 'Enrolled'
    | 'Waitlisted'
    | 'cart'
    | 'NOTOFFERED'
    | 'DOESNOTEXIST';
}

export interface Course extends CourseBase {
  termCode: string;
}

export function pickCreditAmountFromCourse(course: CourseBase): number {
  if (typeof course.creditMin === 'number') {
    if (
      typeof course.credits === 'number' &&
      course.credits >= course.creditMin
    ) {
      return course.credits;
    } else {
      return course.creditMin;
    }
  }

  if (typeof course.creditRange === 'string') {
    const matches = course.creditRange.match(/^(\d+)/);
    if (matches !== null && matches.length >= 1) {
      return parseInt(matches[1], 10);
    }
  }

  console.warn(
    `course ${course.courseId} has no credits value and no creditsMin value`,
  );
  return 0;
}
