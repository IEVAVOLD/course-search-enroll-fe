import { EnrollmentPackage, RawEnrollmentPackage } from '@app/types/courses';
import { groupPackages, sortPackages } from './packageGroup';

describe('packageGroup', () => {
  describe('sortPackages', () => {
    /* Declare a bunch of test data. The below packages consist of the bare minimum fields needed by the
    sorting function.*/
    const noLecLab1 = {
      sections: [
        {
          type: 'LAB',
          sectionNumber: '001',
        },
      ],
    } as RawEnrollmentPackage;
    const noLecLab2 = {
      sections: [
        {
          type: 'LAB',
          sectionNumber: '002',
        },
      ],
    } as RawEnrollmentPackage;
    const lec1Lab301 = {
      sections: [
        {
          type: 'LEC',
          sectionNumber: '001',
        },
        {
          type: 'LAB',
          sectionNumber: '301',
        },
        {
          type: 'DIS',
          sectionNumber: '601',
        },
      ],
    } as RawEnrollmentPackage;
    const lec1Lab302 = {
      sections: [
        {
          type: 'LEC',
          sectionNumber: '001',
        },
        {
          type: 'LAB',
          sectionNumber: '302',
        },
        {
          type: 'DIS',
          sectionNumber: '601',
        },
      ],
    } as RawEnrollmentPackage;
    const lec2Lab301 = {
      sections: [
        {
          type: 'LEC',
          sectionNumber: '002',
        },
        {
          type: 'LAB',
          sectionNumber: '301',
        },
        {
          type: 'DIS',
          sectionNumber: '601',
        },
      ],
    } as RawEnrollmentPackage;
    const lec1With3Sections = {
      sections: [
        {
          type: 'LEC',
          sectionNumber: '001',
        },
        {
          type: 'LAB',
          sectionNumber: '301',
        },
        {
          type: 'DIS',
          sectionNumber: '601',
        },
      ],
    } as RawEnrollmentPackage;
    const lec1With2Sections = {
      sections: [
        {
          type: 'LEC',
          sectionNumber: '001',
        },
        {
          type: 'DIS',
          sectionNumber: '601',
        },
      ],
    } as RawEnrollmentPackage;
    const justLec1 = {
      sections: [
        {
          type: 'LEC',
          sectionNumber: '001',
        },
      ],
    } as RawEnrollmentPackage;
    const justLec2 = {
      sections: [
        {
          type: 'LEC',
          sectionNumber: '002',
        },
      ],
    } as RawEnrollmentPackage;
    let arrayToBeSorted: RawEnrollmentPackage[];
    let expectedArray: RawEnrollmentPackage[];

    beforeEach(() => {
      //Since we're sharing arrays to avoid duplicate code, ensure that they're empty at the start of each test
      arrayToBeSorted = [];
      expectedArray = [];
    });

    it('should place a package with a lecture in front of a package without a lecture', () => {
      //Arrange
      arrayToBeSorted = [noLecLab1, lec1Lab301];
      expectedArray = [lec1Lab301, noLecLab1];

      //Act
      arrayToBeSorted = sortPackages(arrayToBeSorted);

      //Assert
      expect(arrayToBeSorted).toEqual(expectedArray);
    });

    it('should place a package with a higher lecture section number after a package with a lower one', () => {
      //Arrange
      arrayToBeSorted = [lec2Lab301, lec1Lab301];
      expectedArray = [lec1Lab301, lec2Lab301];

      //Act
      arrayToBeSorted = sortPackages(arrayToBeSorted);

      //Assert
      expect(arrayToBeSorted).toEqual(expectedArray);
    });

    it('should place a package with fewer sections after a package with more sections', () => {
      //Arrange
      arrayToBeSorted = [lec1With2Sections, lec1With3Sections];
      expectedArray = [lec1With3Sections, lec1With2Sections];

      //Act
      arrayToBeSorted = sortPackages(arrayToBeSorted);

      //Assert
      expect(arrayToBeSorted).toEqual(expectedArray);
    });

    it('should rank otherwise equivalent packages (with LECs) by non-LEC section number', () => {
      //Arrange
      arrayToBeSorted = [lec1Lab302, lec1Lab301];
      expectedArray = [lec1Lab301, lec1Lab302];

      //Act
      arrayToBeSorted = sortPackages(arrayToBeSorted);

      //Assert
      expect(arrayToBeSorted).toEqual(expectedArray);
    });

    it('should rank otherwise equivalent packages (without LECs) by non-LEC section number', () => {
      //Arrange
      arrayToBeSorted = [noLecLab2, noLecLab1];
      expectedArray = [noLecLab1, noLecLab2];

      //Act
      arrayToBeSorted = sortPackages(arrayToBeSorted);

      //Assert
      expect(arrayToBeSorted).toEqual(expectedArray);
    });

    it('should rank equivallent packs with more sections higher', () => {
      const lec001_lab002_dis302 = {
        sections: [
          { type: 'LEC', sectionNumber: '001' },
          { type: 'LAB', sectionNumber: '002' },
          { type: 'DIS', sectionNumber: '302' },
        ],
      } as RawEnrollmentPackage;

      const lec001_lab002_dis303 = {
        sections: [
          { type: 'LEC', sectionNumber: '001' },
          { type: 'LAB', sectionNumber: '002' },
          { type: 'DIS', sectionNumber: '303' },
        ],
      } as RawEnrollmentPackage;

      expect(
        sortPackages([lec001_lab002_dis303, lec001_lab002_dis302]),
      ).toEqual([lec001_lab002_dis302, lec001_lab002_dis303]);

      const lec001_lab001 = {
        sections: [
          { type: 'LEC', sectionNumber: '001' },
          { type: 'LAB', sectionNumber: '001' },
        ],
      } as RawEnrollmentPackage;

      expect(sortPackages([lec001_lab002_dis303, lec001_lab001])).toEqual([
        lec001_lab001,
        lec001_lab002_dis303,
      ]);
    });

    it('should appropriately rank packages that just consist of a single lecture seciton', () => {
      //Arrange
      arrayToBeSorted = [justLec2, justLec1];
      expectedArray = [justLec1, justLec2];

      //Act
      arrayToBeSorted = sortPackages(arrayToBeSorted);

      //Assert
      expect(arrayToBeSorted).toEqual(expectedArray);
    });

    it('should properly rank packages that consist of a single section where only one is a LEC', () => {
      //Arrange
      arrayToBeSorted = [noLecLab1, justLec1];
      expectedArray = [justLec1, noLecLab1];

      //Act
      arrayToBeSorted = sortPackages(arrayToBeSorted);

      //Assert
      expect(arrayToBeSorted).toEqual(expectedArray);
    });

    it('should handle a package with just a single lecture section', () => {
      //Arrange
      arrayToBeSorted = [justLec1];
      expectedArray = [justLec1];

      //Act
      arrayToBeSorted = sortPackages(arrayToBeSorted);

      //Assert
      expect(arrayToBeSorted).toEqual(expectedArray);
    });

    it('should handle a package with just a single non-lecture section', () => {
      //Arrange
      arrayToBeSorted = [noLecLab1];
      expectedArray = [noLecLab1];

      //Act
      arrayToBeSorted = sortPackages(arrayToBeSorted);

      //Assert
      expect(arrayToBeSorted).toEqual(expectedArray);
    });

    it('should not change the sort order of an already correctly sorted list', () => {
      //Arrange
      arrayToBeSorted = [lec1Lab301, lec1Lab302, noLecLab1];
      expectedArray = [lec1Lab301, lec1Lab302, noLecLab1];

      //Act
      arrayToBeSorted = sortPackages(arrayToBeSorted);

      //Assert
      expect(arrayToBeSorted).toEqual(expectedArray);
    });
  });

  describe('groupPackages', () => {
    it('should group all packs in a course that share the same lecture and format the group correctly', () => {
      //Arrange
      //Mock array of two enrollment packages
      const sameLec2Pacs = [
        {
          docId: '123',
          sections: [
            { type: 'LEC', sectionNumber: '001' },
            { type: 'LAB', sectionNumber: '301' },
            { type: 'DIS', sectionNumber: '601' },
          ],
        },
        {
          docId: '234',
          sections: [
            { type: 'LEC', sectionNumber: '001' },
            { type: 'LAB', sectionNumber: '302' },
            { type: 'DIS', sectionNumber: '602' },
          ],
        },
      ] as EnrollmentPackage[];
      //Mock grouped course package
      const expectedGrouping = [
        {
          kind: 'nested',
          lecture: {
            type: 'LEC',
            sectionNumber: '001',
          },
          packages: [
            {
              docId: '123',
              sections: [
                { type: 'LEC', sectionNumber: '001' },
                { type: 'LAB', sectionNumber: '301' },
                { type: 'DIS', sectionNumber: '601' },
              ],
              isNested: true,
              sectionsWithoutLecture: [
                { type: 'LAB', sectionNumber: '301' },
                { type: 'DIS', sectionNumber: '601' },
              ],
            },
            {
              docId: '234',
              sections: [
                { type: 'LEC', sectionNumber: '001' },
                { type: 'LAB', sectionNumber: '302' },
                { type: 'DIS', sectionNumber: '602' },
              ],
              isNested: true,
              sectionsWithoutLecture: [
                { type: 'LAB', sectionNumber: '302' },
                { type: 'DIS', sectionNumber: '602' },
              ],
            },
          ],
        },
      ];

      //Action
      let processedGroup = groupPackages(sameLec2Pacs);

      //Assert
      expect(processedGroup).toEqual(expectedGrouping);
    });

    it('should not group packages without lectures', () => {
      //Arrange
      const noLec2Pacs = [
        {
          docId: '123',
          sections: [
            { type: 'LAB', sectionNumber: '301' },
            { type: 'DIS', sectionNumber: '601' },
          ],
        },
        {
          docId: '234',
          sections: [
            { type: 'LAB', sectionNumber: '302' },
            { type: 'DIS', sectionNumber: '602' },
          ],
        },
      ] as EnrollmentPackage[];
      const expectedGrouping = [
        {
          kind: 'shallow',
          package: {
            docId: '123',
            sections: [
              { sectionNumber: '301', type: 'LAB' },
              { sectionNumber: '601', type: 'DIS' },
            ],
            isNested: false,
          },
        },
        {
          kind: 'shallow',
          package: {
            docId: '234',
            sections: [
              { sectionNumber: '302', type: 'LAB' },
              { sectionNumber: '602', type: 'DIS' },
            ],
            isNested: false,
          },
        },
      ];

      //Action
      let processedGroup = groupPackages(noLec2Pacs);

      //Assert
      expect(processedGroup).toEqual(expectedGrouping);
    });

    it('should group packages that share a LEC together even when packages without a LEC are present', () => {
      //Arrange
      const twoLecTwoOther = [
        {
          docId: '123',
          sections: [
            { type: 'LAB', sectionNumber: '301' },
            { type: 'DIS', sectionNumber: '601' },
          ],
        },
        {
          docId: '234',
          sections: [
            { type: 'LAB', sectionNumber: '302' },
            { type: 'DIS', sectionNumber: '602' },
          ],
        },
        {
          docId: '345',
          sections: [
            { type: 'LEC', sectionNumber: '001' },
            { type: 'LAB', sectionNumber: '303' },
            { type: 'DIS', sectionNumber: '603' },
          ],
        },
        {
          docId: '456',
          sections: [
            { type: 'LEC', sectionNumber: '001' },
            { type: 'LAB', sectionNumber: '304' },
            { type: 'DIS', sectionNumber: '604' },
          ],
        },
      ] as EnrollmentPackage[];
      const expectedGrouping = [
        {
          kind: 'shallow',
          package: {
            docId: '123',
            sections: [
              { sectionNumber: '301', type: 'LAB' },
              { sectionNumber: '601', type: 'DIS' },
            ],
            isNested: false,
          },
        },
        {
          kind: 'shallow',
          package: {
            docId: '234',
            sections: [
              { sectionNumber: '302', type: 'LAB' },
              { sectionNumber: '602', type: 'DIS' },
            ],
            isNested: false,
          },
        },
        {
          kind: 'nested',
          lecture: {
            type: 'LEC',
            sectionNumber: '001',
          },
          packages: [
            {
              docId: '345',
              sections: [
                { type: 'LEC', sectionNumber: '001' },
                { type: 'LAB', sectionNumber: '303' },
                { type: 'DIS', sectionNumber: '603' },
              ],
              sectionsWithoutLecture: [
                { type: 'LAB', sectionNumber: '303' },
                { type: 'DIS', sectionNumber: '603' },
              ],
              isNested: true,
            },
            {
              docId: '456',
              sections: [
                { type: 'LEC', sectionNumber: '001' },
                { type: 'LAB', sectionNumber: '304' },
                { type: 'DIS', sectionNumber: '604' },
              ],
              sectionsWithoutLecture: [
                { type: 'LAB', sectionNumber: '304' },
                { type: 'DIS', sectionNumber: '604' },
              ],
              isNested: true,
            },
          ],
        },
      ];

      //Action
      let processedGroup = groupPackages(twoLecTwoOther);

      //Assert
      expect(processedGroup).toEqual(expectedGrouping);
    });

    it('should not group packages just a single lecture section', () => {
      //Arrange
      const noLec2Pacs = [
        {
          docId: '123',
          sections: [{ type: 'LEC', sectionNumber: '001' }],
        },
      ] as EnrollmentPackage[];
      const expectedGrouping = [
        {
          kind: 'shallow',
          package: {
            docId: '123',
            sections: [{ type: 'LEC', sectionNumber: '001' }],
            isNested: false,
          },
        },
      ];

      //Action
      let processedGroup = groupPackages(noLec2Pacs);

      //Assert
      expect(processedGroup).toEqual(expectedGrouping);
    });

    it('should not group packages just a single non-lecture section', () => {
      //Arrange
      const noLec2Pacs = [
        {
          docId: '123',
          sections: [{ type: 'DIS', sectionNumber: '601' }],
        },
      ] as EnrollmentPackage[];
      const expectedGrouping = [
        {
          kind: 'shallow',
          package: {
            docId: '123',
            sections: [{ type: 'DIS', sectionNumber: '601' }],
            isNested: false,
          },
        },
      ];

      //Action
      let processedGroup = groupPackages(noLec2Pacs);

      //Assert
      expect(processedGroup).toEqual(expectedGrouping);
    });
  });
});
