import {
  Observable,
  forkJoin,
  Subscription,
  MonoTypeOperatorFunction,
  pipe,
  from,
  OperatorFunction,
  of,
} from 'rxjs';
import { map, mergeMap, filter } from 'rxjs/operators';
import Schema, { SchemaAssertionError } from '@app/types/schema';
import { Json } from '@app/types/json';
import { isSuccess, Loadable, Failed, isFailed, Success } from 'loadable.ts';
import { Course } from '@app/types/courses';

export const notNull = <T>(t: T | null): t is T => t !== null;

export const notUndefined = <T>(t: T | undefined): t is T => t !== undefined;

export const isntUndefined = <T>(thing: T | undefined): thing is T => {
  return thing !== undefined;
};

export const isNullOrIsFailed = <T, E>(
  thing: Loadable<T, E> | null,
): thing is null | Failed<E> => {
  return thing === null || isFailed(thing);
};

export const unique = <T>(things: T[]): T[] => {
  return things.filter((thing, index, all) => all.indexOf(thing) === index);
};

interface SimpleMap {
  [name: string]: any;
}

type ObservableMap<T = SimpleMap> = { [K in keyof T]: Observable<T[K]> };

export const forkJoinWithKeys = <T = SimpleMap>(pairs: ObservableMap<T>) => {
  const keys = Object.keys(pairs) as Array<keyof T>;
  const observables = keys.map(key => pairs[key]);

  if (observables.length === 0) {
    return of({}) as Observable<T>;
  }

  return forkJoin(observables).pipe(
    map<any[], T>(values => {
      const valueMapping = {} as T;

      keys.forEach((key, index) => {
        (valueMapping as any)[key] = values[index];
      });

      return valueMapping;
    }),
  );
};

export const hasSchema =
  <T>(schema: Schema<T>, reportError?: (err: SchemaAssertionError) => void) =>
  (src: Observable<unknown>): Observable<T> => {
    return new Observable(subscriber => {
      src.subscribe({
        next(val) {
          try {
            schema.asserts(val);
            subscriber.next(val);
          } catch (err) {
            if (err instanceof SchemaAssertionError && reportError) {
              reportError(err);
            }

            subscriber.error(err);
          }
        },
        error(err) {
          subscriber.error(err);
        },
        complete() {
          subscriber.complete();
        },
      });
    });
  };

/**
 * An async filter operator taken from here:
 *
 * https://gist.github.com/bjesuiter/288326f9822e0bc82389976f8da66dd8
 */
export const flatFilter = <T>(
  predicate: (input: T) => Observable<boolean>,
): MonoTypeOperatorFunction<T> => {
  return pipe(
    mergeMap((data: T) => {
      return from(predicate(data)).pipe(
        map(isValid => ({ filterResult: isValid, entry: data })),
      );
    }),
    filter(data => data.filterResult === true),
    map(data => data.entry),
  );
};

export const ifSuccessThenUnwrap = <T>(): OperatorFunction<
  Loadable<T> | null,
  T
> => {
  return pipe(
    filter((l): l is Success<T> => l !== null && isSuccess(l)),
    map(thing => thing.value),
  );
};

export const negate: MonoTypeOperatorFunction<boolean> = map(bool => !bool);

export const unsubscribeAll = (subscriptions: Subscription[]) => {
  subscriptions.forEach(sub => sub.unsubscribe());
};

export const curry2 =
  <A, B, C>(fn: (a: A, b: B) => C) =>
  (a: A) =>
  (b: B) => {
    return fn(a, b);
  };

export const titlecase = (str: string): string => {
  return str.replace(/\w\S*/g, word => {
    return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
  });
};

export const parseJsonWithoutThrowing = (
  str: string,
  fallback: Json = null,
): Json => {
  try {
    return JSON.parse(str);
  } catch {
    return fallback;
  }
};

export const pluralize = (courses: Course[]): string => {
  if (courses.length === 1) {
    return 'course';
  } else {
    return 'courses';
  }
};

type Sortable = number | string;

export const sortBy = <T, V extends Sortable>(fn: (t: T) => V) => {
  return (a: T, b: T): number => {
    const av = fn(a);
    const bv = fn(b);
    if (av < bv) {
      return -1;
    } else if (av > bv) {
      return 1;
    } else {
      return 0;
    }
  };
};

export const parseCreditRange = (range: string) => {
  if (/^\d+-\d+$/.test(range)) {
    const [min, max] = range.split('-');
    return {
      min: parseInt(min, 10),
      max: parseInt(max, 10),
    };
  } else {
    const val = parseInt(range, 10);
    return { min: val, max: val };
  }
};

export const groupBy = <T, K extends keyof any>(arr: T[], fn: (t: T) => K) => {
  const table = {} as { [P in K]?: T[] };
  for (const t of arr) {
    const key = fn(t);
    if (table[key]) {
      table[key]!.push(t);
    } else {
      table[key] = [t];
    }
  }
  return table;
};
