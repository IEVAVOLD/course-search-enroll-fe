import { createAction, props } from '@ngrx/store';
import { Json } from '@app/types/json';
import { Prefs } from './state';
import { TermCode, Terms } from './types/terms';
import { StudentInfo } from './types/student';
import { Loadable } from 'loadable.ts';
import {
  Course,
  CurrentCourse,
  Details,
  EnrollmentPackage,
  HasClassNumber,
  HasCourseId,
  HasCourseRef,
  HasCurrentId,
  HasPackage,
  HasRelatedClassNumbers,
  HasShortCatalog,
  HasSubject,
  HasSubjectCode,
  HasTermCode,
  MaybeHasPackage,
  RoadmapCourse,
  RoadmapCourseRef,
  SavedCourse,
} from './types/courses';
import { EnrollRequest } from './types/enroll';

export namespace prefs {
  export const fetch = createAction('app/prefs/fetch');

  export const found = createAction(
    'app/prefs/found',
    props<{ prefs: Prefs }>(),
  );

  export const failed = createAction('app/prefs/failed');

  export const setKey = createAction(
    'app/prefs/setKey',
    props<{ key: string; value: Json }>(),
  );

  export const setNestedKey = createAction(
    'app/prefs/setNestedKey',
    props<{ pref: string; key: string; value: Json }>(),
  );
}

export namespace currentTermCode {
  export const set = createAction(
    'app/currentTermCode/set',
    props<{ currentTermCode: TermCode }>(),
  );
}

/**
 * Actions responsible for loading data about the active term set, subjects,
 * sessions, and other global curricular data.
 *
 * If there's an error while loading this data the app won't be able to fully
 * function and the user should be presented with a prominent error message.
 */
export namespace terms {
  export const fetch = createAction('app/terms/fetch');

  export const found = createAction(
    'app/terms/found',
    props<{ terms: Terms; currentTermCode: TermCode }>(),
  );

  export const failed = createAction('app/terms/failed');
}

/**
 * Actions for loading info about the currently logged-in user. This includes
 * details about their degree plans and enrollment deadlines.
 */
export namespace student {
  export const fetch = createAction('app/student/fetch');

  export const found = createAction(
    'app/student/found',
    props<{ info: StudentInfo }>(),
  );

  export const failed = createAction('app/student/failed');
}

export namespace lifecycle {
  /**
   * Signal to child views that the term data has been successfully loaded.
   */
  export const ready = createAction('app/lifecycle/ready');
}

/**
 * Actions for loading and changing user course data. The `roadmap` namespace
 * deals with cart courses. The `current` namespace deals with enrolled,
 * waitlisted, and dropped courses. The names come from the respective
 * endpoints for both kinds of courses.
 *
 * These actions DO NOT deal with saved-for-later courses. Those are handled
 * by actions in the `saved` namespace.
 */
export namespace courses {
  /**
   * Starts loading roadmap and current courses associated with a term-code.
   */
  export const load = createAction(
    'app/courses/load',
    props<{
      termCode: TermCode;
    }>(),
  );

  /**
   * Similar to `app.courses.load` but will not fire an API request if it
   * detects that either the roadmap or the current courses are set to `null`.
   * If the loadable is in the loading, success, or failed states this action
   * will NOT trigger a reload.
   *
   * If the roadmap courses are unloaded but the current courses aren't (or
   * vice-versa), this action will only fire the API request for the unloaded
   * courses.
   */
  export const loadIfNotAlready = createAction(
    'app/courses/loadIfNotAlready',
    props<{
      termCode: TermCode;
    }>(),
  );

  export const done = createAction(
    'app/courses/done',
    props<{
      termCode: TermCode;
      roadmap: Loadable<RoadmapCourse[]>;
      current: Loadable<CurrentCourse[]>;
    }>(),
  );

  /**
   * Actions for loading and changing the user's roadmap courses. The name
   * "roadmap" and "cart" can generally mean the same thing although all user
   * facing text should use "cart". Roadmap is used internally because that's
   * the name of the REST endpoint that is used to manage these courses.
   *
   * Most modifications to the roadmap (adding courses, removing courses, adding
   * enrollment packages, removing enrollment packages, etc.) trigger a re
   * validation of the entire roadmap. The validation is an asynchronous job
   * that runs on the backend. For the actions that trigger a roadmap
   * validation, the NgRX effect will wait for the validation to complete before
   * loading an updated roadmap and displaying it to the user. This ensures that
   * any validation messages that the user sees in their roadmap reflect the
   * current state.
   */
  export namespace roadmap {
    export const load = createAction(
      'app/courses/roadmap/load',
      props<{
        termCode: TermCode;
      }>(),
    );

    /**
     * Similar to `app.courses.roadmap.load` but will not fire an API request if
     * it detects that the roadmap courses are set to `null`. If the loadable is
     * in the loading, success, or failed states this action will NOT trigger a
     * reload.
     */
    export const loadIfNotAlready = createAction(
      'app/courses/roadmap/loadIfNotAlready',
      props<{ termCode: TermCode }>(),
    );

    export const done = createAction(
      'app/courses/roadmap/done',
      props<{
        termCode: TermCode;
        roadmap: Loadable<RoadmapCourse[]>;
      }>(),
    );

    export const addCourseWithoutPack = createAction(
      'app/courses/roadmap/addCourseWithoutPack',
      props<{
        course: HasTermCode & HasSubject & HasCourseId & HasShortCatalog;
        showToast?: boolean;
      }>(),
    );

    /**
     * Used to assign an enrollment package to a roadmap course. This will
     * trigger a re-validation of the cart. When the validation finishes, the
     * cart will be reloaded for the term and any validation messages will be
     * attached to the relevant roadmap courses.
     *
     * If the waitlist or honors options are NOT specified, they will be set
     * to false when adding the package.
     */
    export const addCourseWithPack = createAction(
      'app/courses/roadmap/addCourseWithPack',
      props<{
        course: HasTermCode & HasSubjectCode & HasCourseId & HasShortCatalog;
        pack: HasClassNumber & HasRelatedClassNumbers;
        credits: number;
        joinWaitlist: boolean;
        withHonors: boolean;
        classPermissionNumber: number | null;
        showToast?: boolean;
      }>(),
    );

    export const addCourseWithPackBulk = createAction(
      'app/courses/roadmap/addCourseWithPackBulk',
      props<{
        courses: {
          course: HasTermCode & HasSubjectCode & HasCourseId & HasShortCatalog;
          pack: HasClassNumber & HasRelatedClassNumbers;
          credits: number;
          joinWaitlist: boolean;
          withHonors: boolean;
          classPermissionNumber: number | null;
        }[];
      }>(),
    );

    /**
     * Used to remove a previously chosen enrollment package from a roadmap
     * course WITHOUT removing the course from the cart. This will trigger a
     * re-validation of the cart but that validation should not take very long.
     * When the validation finishes, the cart will be reloaded for the term.
     */
    export const removePack = createAction(
      'app/courses/roadmap/removePack',
      props<{
        course: HasTermCode & HasSubjectCode & HasCourseId & HasShortCatalog;
        showToast?: boolean;
      }>(),
    );

    /**
     * Removes a course and any associated enrollment package from the roadmap.
     */
    export const removeCoursesAndPacks = createAction(
      'app/courses/roadmap/removeCoursesAndPacks',
      props<{
        courses: Array<
          HasTermCode & HasSubject & HasCourseId & HasShortCatalog
        >;
        showToast?: boolean;
      }>(),
    );

    /**
     * Begins validation of one or more roadmap courses. While validation is being
     * performed, the roadmap for the given term will be in a loading state.
     * When validation is done, validation messages for the given courses will
     * be updated.
     */
    export const validate = createAction(
      'app/courses/roadmap/validate',
      props<{
        termCode: TermCode;
        courses: RoadmapCourse[];
      }>(),
    );

    export const validationFailed = createAction(
      'app/courses/roadmap/validate/failed',
      props<{ termCode: TermCode; message: string }>(),
    );

    export const hintToRevalidate = createAction(
      'app/courses/roadmap/hintToRevalidate',
      props<{ eventId: string; termCodes: TermCode[] }>(),
    );

    /**
     * Actions for loading all enrollment packages associated with a roadmap
     * course. If the course already has a chosen enrollment package, that
     * individual package is loaded via its unique `docId`. The list of all
     * packages are loaded using the termCode/subjectCode/courseId of the course.
     */
    export namespace packs {
      export const load = createAction(
        'app/courses/roadmap/packs/load',
        props<{
          course: HasTermCode &
            HasSubject &
            HasCourseId &
            HasCourseRef<RoadmapCourseRef> &
            MaybeHasPackage;
        }>(),
      );

      export const done = createAction(
        'app/courses/roadmap/packs/done',
        props<{
          course: HasTermCode &
            HasSubject &
            HasCourseId &
            HasCourseRef<RoadmapCourseRef>;
          pack: Loadable<EnrollmentPackage> | null;
          packs: Loadable<EnrollmentPackage[]>;
        }>(),
      );
    }
  }

  /**
   * Actions for loading and changing the user current courses. Current courses
   * is an umbrella for all of the courses that the user has enrolled-in,
   * waitlisted-in, or dropped for a particular term-code. Like "roadmap", the
   * name "current" comes from the name of the REST endpoint that handles these
   * actions.
   *
   * Since the current endpoint returns enrolled/waitlisted/dropped courses as
   * a single list, the `studentEnrollmentStatus` property is used to
   * disambiguate the state of each course in the list.
   *
   * Internally all current courses are kept in a single list. Since all current
   * courses are loaded and fail/succeed as one list, it's easier to manage the
   * data when they stay together. Only when the enrolled/waitlisted/dropped
   * courses have to be presented to the user are the courses grouped by status.
   */
  export namespace current {
    export const load = createAction(
      'app/courses/current/load',
      props<{
        termCode: TermCode;
      }>(),
    );

    /**
     * Similar to `app.courses.current.load` but will not fire an API request if
     * it detects that the current courses are set to `null`. If the loadable is
     * in the loading, success, or failed states this action will NOT trigger a
     * reload.
     */
    export const loadIfNotAlready = createAction(
      'app/courses/current/loadIfNotAlready',
      props<{ termCode: TermCode }>(),
    );

    export const done = createAction(
      'app/courses/current/done',
      props<{
        termCode: TermCode;
        current: Loadable<CurrentCourse[]>;
      }>(),
    );

    export const enroll = createAction(
      'app/courses/current/enroll',
      props<{
        termCode: TermCode;
        requests: Array<[Course, EnrollRequest]>;
      }>(),
    );

    /**
     * Drops one or more enrolled or waitlisted courses. **DOES NOT** ask the
     * user for confirmation before dropping. Confirmation has to happen
     * **BEFORE** this action is dispatched.
     */
    export const drop = createAction(
      'app/courses/current/drop',
      props<{
        termCode: TermCode;
        courses: CurrentCourse[];
      }>(),
    );

    /**
     * Changes the enrollment options for a one of the current enrollments.
     */
    export const edit = createAction(
      'app/courses/current/edit',
      props<{
        course: CurrentCourse;
        credits: number;
        honors: boolean;
      }>(),
    );

    /**
     * Actions for loading the package associated with the current course.
     */
    export namespace pack {
      export const load = createAction(
        'app/courses/current/pack/load',
        props<{
          course: HasTermCode &
            HasSubject &
            HasCourseId &
            HasCurrentId &
            HasPackage;
        }>(),
      );

      export const done = createAction(
        'app/courses/current/pack/done',
        props<{
          course: HasTermCode &
            HasSubject &
            HasCourseId &
            HasCurrentId &
            HasPackage;
          pack: Loadable<EnrollmentPackage>;
        }>(),
      );
    }
  }
}

export namespace details {
  export const load = createAction(
    'app/details/load',
    props<{
      course: HasCourseRef & HasSubject & HasCourseId;
    }>(),
  );

  export const done = createAction(
    'app/details/done',
    props<{
      course: HasCourseRef & HasSubject & HasCourseId;
      details: Loadable<Details>;
    }>(),
  );
}

/**
 * Actions for loading and changing the user's saved-for-later courses.
 */
export namespace saved {
  export const load = createAction('app/saved/load');

  export const loadIfNotAlready = createAction('app/saved/loadIfNotAlready');

  export const done = createAction(
    'app/saved/done',
    props<{
      saved: Loadable<SavedCourse[]>;
    }>(),
  );

  /**
   * Adds a new course to the saved list. Afterward it will trigger a re-load of
   * the entire saved list.
   */
  export const add = createAction(
    'app/saved/add',
    props<{
      course: HasSubject & HasCourseId & HasShortCatalog;
    }>(),
  );

  /**
   * Removes a course from the saved list. Afterward it will trigger a re-load
   * of the entire saved list.
   */
  export const remove = createAction(
    'app/saved/remove',
    props<{
      courses: Array<HasSubject & HasCourseId & HasShortCatalog>;
    }>(),
  );
}
