import { DateTime } from 'luxon';
import { UnixMilliseconds } from '@app/types';

const UTC = 'utc';

// Madison, WI is 6 (or 5 hours during daylight-savings) hours after GMT
export const CST_OFFSET_IN_SECONDS_WITHOUT_DST =
  DateTime.local().setZone('UTC-6', {
    keepLocalTime: true,
  }).offset * 60;

export const CST_OFFSET_IN_SECONDS_WITH_DST =
  DateTime.local().setZone('America/Chicago', {
    keepLocalTime: true,
  }).offset * 60;

export const fromUtcSecondsToMadisonSecondsWithoutDST = (
  utc: number,
): number => {
  return utc + Math.abs(CST_OFFSET_IN_SECONDS_WITHOUT_DST);
};
export const fromUtcSecondsToMadisonSecondsWithDST = (utc: number): number => {
  return utc + Math.abs(CST_OFFSET_IN_SECONDS_WITH_DST);
};

export const fromMadisonSecondsToUtcSeconds = (madison: number): number => {
  return madison - Math.abs(CST_OFFSET_IN_SECONDS_WITHOUT_DST);
};

const to = (fmt: string, zone: string) => {
  return (millis: UnixMilliseconds): string => {
    return DateTime.fromMillis(millis, { zone }).toFormat(fmt);
  };
};

export const WeekdayMonthDayYear = 'EEEE, DD'; // Wed, Aug 6, 2014
export const MonthDayYear = 'DD';
export const MonthDay = 'LLL d';
export const HourMinuteMeridian = 't';
export const HourMeridiem = 'h a';
export const DateAndTime = `DD 'at' t`;

export const toWeekdayMonthDayYearWithDST = to(
  WeekdayMonthDayYear,
  'America/Chicago',
);
export const toMonthDayYearWithDST = to(MonthDayYear, 'America/Chicago');
export const toMonthDayWithDST = to(MonthDay, 'America/Chicago');
export const toHourMinuteMeridianWithDST = to(
  HourMinuteMeridian,
  'America/Chicago',
);
export const toDateAndTimeWithDST = to(DateAndTime, 'America/Chicago');

export const toWeekdayMonthDayYearWithoutDST = to(WeekdayMonthDayYear, 'UTC-6');
export const toMonthDayYearWithoutDST = to(MonthDayYear, 'UTC-6');
export const toMonthDayWithoutDST = to(MonthDay, 'UTC-6');
export const toHourMinuteMeridianWithoutDST = to(HourMinuteMeridian, 'UTC-6');
export const toDateAndTimeWithoutDST = to(DateAndTime, 'UTC-6');

export const toWeekdayMonthDayYearUtc = to(WeekdayMonthDayYear, UTC);
export const toMonthDayYearUtc = to(MonthDayYear, UTC);
export const toMonthDayUtc = to(MonthDay, UTC);
export const toHourMinuteMeridianUtc = to(HourMinuteMeridian, UTC);
export const toHourMeridiemUtc = to(HourMeridiem, UTC);
export const toDateAndTimeUtc = to(DateAndTime, UTC);
