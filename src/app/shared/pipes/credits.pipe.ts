import { Pipe, PipeTransform } from '@angular/core';
import * as s from '@app/types/schema';
import { Course } from '@app/types/courses';

type Credits = Course['credits'];

namespace Credits {
  export const schema = s.object<Credits>({ min: s.number, max: s.number });
}

interface HasCredits {
  credits: Credits;
}

namespace HasCredits {
  export const schema = s.object<HasCredits>({
    credits: Credits.schema,
  });
}

interface HasChosenCredits {
  package: { choices: { credits: { total: number } } };
}

namespace HasChosenCredits {
  export const schema = s.object<HasChosenCredits>({
    package: s.object({
      choices: s.object({
        credits: s.object({
          total: s.number,
        }),
      }),
    }),
  });
}

namespace HasPackageWithCredits {
  export const schema = s.object<{ package: { credits: number } }>({
    package: s.object({
      credits: s.number,
    }),
  });
}

type AcceptsSingular = number | Credits | HasCredits | HasChosenCredits;

type Accepts = AcceptsSingular | AcceptsSingular[];

const normalize = (accepts: Accepts): Credits[] => {
  if (typeof accepts === 'number') {
    return [{ min: accepts, max: accepts }];
  }

  if (Array.isArray(accepts)) {
    return accepts.flatMap(normalize);
  }

  if (HasPackageWithCredits.schema.matches(accepts)) {
    const value = accepts.package.credits;
    return [{ min: value, max: value }];
  } else if (HasChosenCredits.schema.matches(accepts)) {
    const value = accepts.package.choices.credits.total;
    return [{ min: value, max: value }];
  } else if (Credits.schema.matches(accepts)) {
    return [accepts];
  } else {
    return [accepts.credits];
  }
};

export const formatCredits = (
  hasCredits: Accepts,
  label: 'short' | 'long' = 'long',
): string => {
  const inputs = normalize(hasCredits);
  const min = inputs.reduce((acc, next) => acc + next.min, 0);
  const max = inputs.reduce((acc, next) => acc + next.max, 0);

  if (min === max) {
    if (min === 1) {
      return `1 ${label === 'short' ? 'Cr' : 'credit'}`;
    } else {
      return `${min} ${label === 'short' ? 'Cr' : 'credits'}`;
    }
  } else {
    return `${min}-${max} ${label === 'short' ? 'Cr' : 'credits'}`;
  }
};

@Pipe({ name: 'credits', pure: true })
export class CreditsPipe implements PipeTransform {
  transform(hasCredits: Accepts, label: 'short' | 'long' = 'long'): string {
    return formatCredits(hasCredits, label);
  }
}

@Pipe({ name: 'enumerateCredits', pure: true })
export class EnumerateCreditsPipe implements PipeTransform {
  transform(hasCredits: AcceptsSingular): number[] {
    const { min, max } = normalize(hasCredits)[0];
    const total = max - min + 1;
    return Array.from({ length: total }).map((_, index) => index + min);
  }
}
