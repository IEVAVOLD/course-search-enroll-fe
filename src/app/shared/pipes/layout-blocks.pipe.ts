import { Pipe, PipeTransform } from '@angular/core';
import { Viewport } from '@app/shared/components/schedule/schedule.component';
import {
  CurrentCourse,
  DayOfTheWeek,
  MeetingSchedule,
  RoadmapCourse,
} from '@app/types/courses';
import { ColorQueue } from '@app/types/colors';

export interface Block {
  day: DayOfTheWeek;

  /**
   * Seconds after midnight
   */
  start: number;

  /**
   * Seconds since start
   */
  duration: number;

  /**
   * CSS rules for changing the appearance of the block.
   */
  appearance: Styles;

  /**
   * Arbitrary data that will be passed to whatever template renders the block.
   */
  data: unknown;
}

// should probably move this to a different file - Isaac
export namespace Block {
  export const fromSchedule = (schedule: MeetingSchedule): Block[] => {
    return schedule.daysList.map(day => {
      return {
        day,
        start: schedule.startSeconds,
        duration: schedule.durationSeconds,
        appearance: {},
        data: null,
      };
    });
  };

  export const fromRoadmap = (course: RoadmapCourse): Block[] => {
    if (!course.package) {
      return [];
    }

    return course.package.classMeetings.flatMap(meeting => {
      if (meeting.schedule) {
        return meeting.schedule.daysList.map(day => {
          return {
            day,
            start: meeting.schedule!.startSeconds,
            duration: meeting.schedule!.durationSeconds,
            appearance: {},
            data: {
              ...meeting,
              shortCatalog: course.shortCatalog,
              section: `${meeting.sectionType} ${meeting.sectionNumber}`,
            },
          };
        });
      }

      return [];
    });
  };

  export const fromCurrent = (colors: ColorQueue) => {
    return (course: CurrentCourse): Block[] => {
      return course.package.classMeetings.flatMap(meeting => {
        if (meeting.schedule) {
          const appearance = colors.pick();
          return meeting.schedule.daysList.map(day => {
            return {
              day,
              start: meeting.schedule!.startSeconds,
              duration: meeting.schedule!.durationSeconds,
              appearance,
              data: {
                course,
                meeting,
              },
            };
          });
        }

        return [];
      });
    };
  };
}

type Styles = Record<string, string | number>;

interface BlockWithPosition {
  block: Block;
  position: Styles;
}

@Pipe({ name: 'layoutBlocks', pure: true })
export class LayoutBlocksPipe implements PipeTransform {
  transform(blocks: Block[], viewport: Viewport): BlockWithPosition[] {
    blocks = blocks.filter(removeMeetingsFromNonVisibleDays(viewport));
    blocks = blocks.slice().sort(sortBlocks(viewport));
    return addPosition(viewport, blocks);
  }
}

@Pipe({ name: 'groupByDay', pure: true })
export class GroupByDayPipe implements PipeTransform {
  transform(
    blocks: BlockWithPosition[],
    viewport: Viewport,
  ): Array<Array<BlockWithPosition>> {
    const days: Partial<Record<DayOfTheWeek, BlockWithPosition[]>> = {};

    viewport.days.forEach(day => {
      return (days[day] = blocks.filter(block => block.block.day === day));
    });

    return Object.values(days);
  }
}

const removeMeetingsFromNonVisibleDays = (viewport: Viewport) => {
  return (b: Block) => viewport.days.includes(b.day);
};

const sortBlocks = (viewport: Viewport) => {
  const daySorter = (a: DayOfTheWeek, b: DayOfTheWeek): number => {
    return viewport.days.indexOf(a) - viewport.days.indexOf(b);
  };

  return (a: Block, b: Block): number => {
    // First sort by the meeting's day-of-the-week
    const daySort = daySorter(a.day, b.day);
    if (daySort !== 0) {
      return daySort;
    }

    // If the days are the same, sort by the meeting start time
    return a.start - b.start;
  };
};

/**
 * Time complexity is at most O(N^3) for the three levels of while loops,
 * I think it is acceptable for we have a small N of at most 20 -ish
 */
const findConflicts = (blocks: Block[]): number[][][] => {
  const conflicts: number[][][] = [];
  let i = 0;
  const n = blocks.length;
  while (i < n - 1) {
    let j = i + 1;
    // find the last one that conflicts with blocks i through j-1
    while (j < n) {
      let hasNoConflict = true;
      for (let x = i; x < j; x++) {
        if (hasConflict(blocks, x, j)) {
          hasNoConflict = false;
        }
      }
      if (hasNoConflict) {
        break;
      }
      j++;
    }
    // now (j-1) conflicts with blocks i through (j-2), but j does not,
    // so we have blocks i through (j-1) as one group

    // construct the set of conflicts
    if (j > i + 1) {
      const group: number[][] = []; // each group consists of one or more columns
      const visited: number[] = []; // helper array so we only visit each meeting once
      let k = i;
      while (k < j) {
        if (visited.includes(k)) {
          k++;
          continue;
        }
        const column: number[] = [k];
        let p = k + 1;
        while (p < j) {
          if (!visited.includes(p)) {
            let hasNoConflict = true;
            column.forEach(q => {
              if (hasConflict(blocks, q, p)) {
                hasNoConflict = false;
              }
            });
            if (hasNoConflict) {
              column.push(p);
              visited.push(p);
            }
          }
          p++;
        }
        group.push(column);
        k++;
      }
      conflicts.push(group);
    }
    i = j;
  }
  return conflicts;
};

const hasConflict = (blocks: Block[], a: number, b: number) => {
  const aDay = blocks[a].day;
  const bDay = blocks[b].day;
  const aStart = blocks[a].start;
  const aEnd = aStart + blocks[a].duration;
  const bStart = blocks[b].start;

  return (
    aDay === bDay && // assume no overnight meetings
    aEnd > bStart
  );
};

const conflictsLayout = (
  conflicts: number[][][],
): Record<number, { width: number; 'margin-left': number }> => {
  return Object.fromEntries(
    conflicts.flatMap((group: number[][]) => {
      const width = 100 / group.length;
      return group.flatMap((meetingIdxs: number[], idx: number) => {
        const marginLeft = idx * width;
        return meetingIdxs.map(meetingIdx => [
          meetingIdx,
          { width, 'margin-left': marginLeft },
        ]);
      });
    }),
  );
};

const addPosition = (
  viewport: Viewport,
  blocks: Block[],
): BlockWithPosition[] => {
  const conflictHints = conflictsLayout(findConflicts(blocks));
  return blocks.map((block, index) => {
    const totalHeight = 100;
    const totalWidth = 100;
    const dailyWidth = 100;

    const hasConflict = index in conflictHints;
    const conflictStyles = hasConflict ? conflictHints[index] : null;

    // Within the range [0, 1]
    const verticalOffsetNormalized =
      (block.start - viewport.start) / viewport.secondsInWindow;

    // Math.min() is to prevent overflow if block is too late
    // A decimal on the range [0, 1]
    const heightNormalized = Math.min(
      block.duration / viewport.secondsInWindow,
      1 - verticalOffsetNormalized,
    );

    const horizontalOffsetPercentage = 0;

    const position: Styles = {};

    position['width'] = percent(conflictStyles?.['width'] ?? dailyWidth);
    position['height'] = percent(heightNormalized * totalHeight);
    position['top'] = percent(
      Math.max(0, verticalOffsetNormalized * totalHeight),
    );
    position['margin-left'] = percent(
      horizontalOffsetPercentage * totalWidth +
        (conflictStyles?.['margin-left'] ?? 0),
    );

    return { position, block };
  });
};

const percent = (n: number) => `${n}%`;
