import { Pipe, PipeTransform } from '@angular/core';

const has = (thing: unknown, key: string): boolean => {
  if (
    typeof thing === 'object' &&
    thing !== null &&
    thing.hasOwnProperty(key)
  ) {
    if (Array.isArray((thing as any)[key])) {
      return (thing as any)[key].length > 0;
    } else {
      return !!(thing as any)[key];
    }
  }
  return false;
};

@Pipe({ name: 'has', pure: true })
export class HasPipe implements PipeTransform {
  transform(thing: unknown, key: string): boolean {
    return has(thing, key);
  }
}

@Pipe({ name: 'hasSomeOf', pure: true })
export class HasSomeOfPipe implements PipeTransform {
  transform(thing: unknown, keys: string[]): boolean {
    return keys.some(key => {
      return has(thing, key);
    });
  }
}

@Pipe({ name: 'hasAllOf', pure: true })
export class HasAllOf implements PipeTransform {
  transform(thing: unknown, keys: string[]): boolean {
    return keys.every(key => {
      return has(thing, key);
    });
  }
}
