import { Pipe, PipeTransform } from '@angular/core';
import { ExamSchedule, MeetingSchedule } from '@app/types/courses';

type AnySchedule = MeetingSchedule | ExamSchedule;

const DAYS = {
  Su: ['Sun', 'Sunday'],
  M: ['Mon', 'Monday'],
  T: ['Tue', 'Tuesday'],
  W: ['Wed', 'Wednesday'],
  R: ['Thu', 'Thursday'],
  F: ['Fri', 'Friday'],
  Sa: ['Sat', 'Saturday'],
};

const fmtDays = (schedule: AnySchedule, form: 'short' | 'long') => {
  if (schedule.type === 'exam') {
    return schedule.startMonthDayYear;
  }

  const found = Object.keys(DAYS).filter(
    abbr => schedule.days.indexOf(abbr) > -1,
  );

  if (found.length > 0) {
    type D = keyof typeof DAYS;
    return found.map(k => DAYS[k as D][form === 'short' ? 0 : 1]).join(', ');
  } else {
    return schedule.days;
  }
};

const fmtTime = (schedule: AnySchedule): string => {
  if (schedule.type === 'exam') {
    return `${schedule.startHourMinuteMeridian}-${schedule.endHourMinuteMeridian}`;
  }

  return `${schedule.start}-${schedule.end}`;
};

@Pipe({ name: 'schedule' })
export class SchedulePipe implements PipeTransform {
  transform(schedule: AnySchedule | null, part?: 'days' | 'times') {
    if (schedule === null) {
      return 'No times provided';
    }

    switch (part) {
      case 'days':
        return fmtDays(schedule, 'short');
      case 'times':
        return fmtTime(schedule);
      default:
        return `${fmtDays(schedule, 'short')} ${fmtTime(schedule)}`;
    }
  }
}
