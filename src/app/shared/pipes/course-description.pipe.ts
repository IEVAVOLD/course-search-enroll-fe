import { OnDestroy, Pipe, PipeTransform } from '@angular/core';
import { CourseBase } from '@app/core/models/course';
import { GlobalState } from '@app/state';
import * as selectors from '@app/selectors';
import { Store } from '@ngrx/store';
import { isSuccess } from 'loadable.ts';
import { Subscription } from 'rxjs';
import { Subject } from '@app/types/terms';

@Pipe({ name: 'courseDescription' })
export class CourseDescriptionPipe implements PipeTransform, OnDestroy {
  private _subjects: Subject[] = [];
  private _subscription: Subscription;

  constructor(store: Store<GlobalState>) {
    this._subscription = store
      .select(selectors.terms.getZeroTerm)
      .subscribe(zero => {
        if (isSuccess(zero)) {
          this._subjects = zero.value.subjects;
        } else {
          this._subjects = [];
        }
      });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  transform(arg: CourseBase) {
    if (arg.studentEnrollmentStatus === 'Transfer') {
      return `${arg.subjectDescription} ${arg.catalogNumber || ''}`;
    } else {
      if (arg.hasOwnProperty('subject')) {
        return `${(arg as any).subject.shortDescription} ${arg.catalogNumber}`;
      } else if (arg.hasOwnProperty('subjectDescription')) {
        return `${(arg as any).subjectDescription} ${arg.catalogNumber}`;
      } else if (arg.hasOwnProperty('shortCatalog')) {
        return `${(arg as any).shortCatalog}`;
      } else {
        const subjectName =
          this._subjects.find(({ code }) => code === arg.subjectCode)?.name ??
          'UNKNOWN';
        return `${subjectName} ${arg.catalogNumber}`;
      }
    }
  }
}
