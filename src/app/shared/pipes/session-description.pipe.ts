import { Pipe, PipeTransform } from '@angular/core';
import { Session } from '@app/types/terms';
import { toMonthDayWithDST, toMonthDayYearWithDST } from '../date-formats';

@Pipe({ name: 'sessionDescription' })
export class SessionDescriptionPipe implements PipeTransform {
  transform(session: Session): string {
    const begin = toMonthDayWithDST(session.beginDate);
    const end = toMonthDayYearWithDST(session.endDate);
    const desc = session.description;
    return `${begin} - ${end} (${desc})`;
  }
}
