import { Pipe, PipeTransform } from '@angular/core';

const patterns = [
  / in position number \d+/,

  // a more relaxed pattern to catch any that miss the first pattern
  /position number \d+/,
];

@Pipe({ name: 'removePositionNumber' })
export class RemovePositionNumberPipe implements PipeTransform {
  transform(message: string): string {
    for (const p of patterns) {
      if (p.test(message)) {
        return message.replace(p, '');
      }
    }

    return message;
  }
}
