import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import {
  MatSnackBarModule,
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
} from '@angular/material/snack-bar';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { A11yModule } from '@angular/cdk/a11y';
import {
  GetTermDescriptionPipe,
  EncodeTermPipe,
  IsInactivePipe,
  IsActivePipe,
  IsPastPipe,
  IsFuturePipe,
} from './pipes/term.pipes';
import {
  IsUnloadedPipe,
  IsLoadingPipe,
  IsErrorPipe,
  IsLoadedPipe,
} from './pipes/loadable.pipe';
import { SessionDescriptionPipe } from './pipes/session-description.pipe';
import { AcademicYearRangePipe } from './pipes/academic-year-range.pipe';
import { SectionDetailsComponent } from './components/section-details/section-details.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import {
  MatRadioModule,
  MAT_RADIO_DEFAULT_OPTIONS,
} from '@angular/material/radio';
import {
  MatProgressSpinnerModule,
  MAT_PROGRESS_SPINNER_DEFAULT_OPTIONS,
} from '@angular/material/progress-spinner';
import { ClickStopPropagationDirective } from '@app/shared/directives/click-stop-propagation';
import { CourseDescriptionPipe } from './pipes/course-description.pipe';
import { CourseListItemComponent } from './components/course-list-item/course-list-item.component';
import { CourseCreditsComponent } from './components/course-list-item/course-credits/course-credits.component';
import { CourseMessageComponent } from './components/course-list-item/course-message/course-message.component';
import {
  GroupByLecturePipe,
  GroupOneWithoutNesting,
} from './pipes/group-by-lecture.pipe';
import { DetailTopicComponent } from './components/detail-topic/detail-topic.component';
import {
  MatCheckboxModule,
  MAT_CHECKBOX_DEFAULT_OPTIONS,
} from '@angular/material/checkbox';
import { GenericSmallDialogComponent } from './components/dialogs/generic-small-dialog/generic-small-dialog.component';
import { BuckyEmoteComponent } from './components/bucky-emote/bucky-emote.component';
import { HasPipe, HasSomeOfPipe, HasAllOf } from './pipes/has.pipe';
import { CreditsPipe, EnumerateCreditsPipe } from './pipes/credits.pipe';
import {
  PaneComponent,
  PaneActionsComponent,
} from './components/pane/pane.component';
import { SeatIndicatorComponent } from './components/seat-indicator/seat-indicator.component';
import { CourseDetailsComponent } from './components/course-details/course-details.component';
import { PackageGroupComponent } from './components/package-group/package-group.component';
import { ParentHeaderComponent } from './components/package-group/parent-header/parent-header.component';
import { ChildHeaderComponent } from './components/package-group/child-header/child-header.component';
import { ScheduleComponent } from './components/schedule/schedule.component';
import { MeetingComponent } from './components/schedule/meeting/meeting.component';
import { CourseMeetingsComponent } from './components/course-list-item/course-meetings/course-meetings.component';
import { InstructorContentDialogComponent } from './components/section-details/instructor-content-dialog/instructor-content-dialog.component';
import { SessionDeadlinesDialogComponent } from './components/section-details/session-deadlines-dialog/session-deadlines-dialog.component';
import { SectionTextbooksDialogComponent } from './components/section-details/section-textbooks-dialog/section-textbooks-dialog.component';
import { BookCoverLoaderComponent } from './components/section-details/book-cover-loader/book-cover-loader.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { DialogBodyComponent } from './components/dialog-body/dialog-body.component';
import { DialogActionsComponent } from './components/dialog-actions/dialog-actions.component';
import { LinkifyPipe } from './pipes/linkify.pipe';
import { environment } from '@env/environment';
import { LoadableDirectivesModule } from 'loadable.ts';
import { CoursePackagesComponent } from './components/course-packages/course-packages.component';
import { LayoutBlocksPipe, GroupByDayPipe } from './pipes/layout-blocks.pipe';
import { LocationPipe } from './pipes/location.pipe';
import { ExamScheduleComponent } from './components/exam-schedule/exam-schedule.component';
import { SchedulePipe } from './pipes/schedule.pipe';
import { StatusMessageComponent } from './components/status-message/status-message.component';
import { FinishedEnrollDialogComponent } from './components/finished-enroll-dialog/finished-enroll-dialog.component';
import { FailedEnrollDialogComponent } from './components/failed-enroll-dialog/failed-enroll-dialog.component';
import { KeepOpenDialogComponent } from './components/dialogs/keep-open-dialog/keep-open-dialog.component';
import { FinishedDropDialogComponent } from './components/dialogs/finished-drop-dialog/finished-drop-dialog.component';
import { RemovePositionNumberPipe } from './pipes/remove-position-number.pipe';
import { AlertComponent } from './components/alert/alert.component';
import { PackHeaderComponent } from './components/package-group/pack-header/pack-header.component';

const modules = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  MatButtonModule,
  MatMenuModule,
  MatIconModule,
  MatExpansionModule,
  MatSelectModule,
  MatRadioModule,
  MatSidenavModule,
  MatToolbarModule,
  MatDialogModule,
  MatInputModule,
  MatTooltipModule,
  MatFormFieldModule,
  MatSnackBarModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  A11yModule,
  MatCheckboxModule,
  LoadableDirectivesModule,
];

const pipes = [
  GetTermDescriptionPipe,
  SessionDescriptionPipe,
  AcademicYearRangePipe,
  CourseDescriptionPipe,
  GroupByLecturePipe,
  GroupOneWithoutNesting,
  EncodeTermPipe,
  IsInactivePipe,
  IsActivePipe,
  IsPastPipe,
  IsFuturePipe,
  HasPipe,
  HasSomeOfPipe,
  HasAllOf,
  CreditsPipe,
  EnumerateCreditsPipe,
  IsUnloadedPipe,
  IsLoadingPipe,
  IsErrorPipe,
  IsLoadedPipe,
  LinkifyPipe,
  LayoutBlocksPipe,
  GroupByDayPipe,
  LocationPipe,
  SchedulePipe,
  RemovePositionNumberPipe,
];

const components = [
  SectionDetailsComponent,
  CourseListItemComponent,
  CourseCreditsComponent,
  CourseMessageComponent,
  CourseMeetingsComponent,
  DetailTopicComponent,
  SectionTextbooksDialogComponent,
  BuckyEmoteComponent,
  PaneComponent,
  PaneActionsComponent,
  SeatIndicatorComponent,
  CourseDetailsComponent,
  PackageGroupComponent,
  ParentHeaderComponent,
  ChildHeaderComponent,
  PackHeaderComponent,
  ScheduleComponent,
  MeetingComponent,
  CoursePackagesComponent,
  ExamScheduleComponent,
  StatusMessageComponent,
  AlertComponent,

  // Dialog-related components
  DialogBodyComponent,
  DialogActionsComponent,
  DialogComponent,
];

const directives = [ClickStopPropagationDirective];

@NgModule({
  imports: [modules],
  exports: [modules, pipes, directives, components],
  declarations: [
    pipes,
    directives,
    components,
    InstructorContentDialogComponent,
    SessionDeadlinesDialogComponent,
    SectionTextbooksDialogComponent,
    GenericSmallDialogComponent,
    BookCoverLoaderComponent,
    FinishedEnrollDialogComponent,
    FailedEnrollDialogComponent,
    KeepOpenDialogComponent,
    FinishedDropDialogComponent,
  ],
  providers: [
    { provide: MAT_RADIO_DEFAULT_OPTIONS, useValue: { color: 'primary' } },
    { provide: MAT_CHECKBOX_DEFAULT_OPTIONS, useValue: { color: 'primary' } },
    {
      provide: MAT_PROGRESS_SPINNER_DEFAULT_OPTIONS,
      useValue: { diameter: 20 },
    },
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
      useValue: { duration: environment.snackbarDuration },
    },
  ],
})
export class SharedModule {}
