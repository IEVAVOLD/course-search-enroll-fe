import { Component, Input } from '@angular/core';

@Component({
  selector: 'cse-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent {
  @Input() public headline!: string;
}
