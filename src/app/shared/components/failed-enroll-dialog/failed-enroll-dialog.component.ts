import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'cse-failed-enroll-dialog',
  templateUrl: './failed-enroll-dialog.component.html',
  styleUrls: ['./failed-enroll-dialog.component.scss'],
})
export class FailedEnrollDialogComponent {
  public message: string | null;

  constructor(@Inject(MAT_DIALOG_DATA) data: { message: string | null }) {
    this.message = data.message;
  }
}
