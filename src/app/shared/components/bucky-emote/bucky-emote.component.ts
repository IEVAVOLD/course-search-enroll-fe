import { Component, OnInit, Input } from '@angular/core';

const KNOWN_EMOTES = [
  // 'dead',
  // 'grimace',
  'happy',
  // 'meh',
  'sad',
  'shrug',
  // 'whistle',
] as const;

type Emote = typeof KNOWN_EMOTES[number];

const EMOTE_ALTS: Record<Emote, string> = {
  // dead: 'Bucky Badger knocked-out',
  // grimace: 'Bucky Badger grimacing',
  happy: 'Bucky Badger smiling',
  // meh: 'Bucky Badger with a skewed frown',
  sad: 'Bucky Badger frowning with sad eyes',
  shrug: 'Bucky Badger shrugging',
  // whistle: 'Bucky Badger whistling',
};

@Component({
  selector: 'cse-bucky-emote',
  templateUrl: './bucky-emote.component.html',
  styles: [
    `
      :host {
        display: block;
      }

      .emote::ng-deep svg {
        height: 100px;
        display: block;
        margin-left: auto;
        margin-right: auto;
      }

      .message {
        margin-left: auto;
        margin-right: auto;
        text-align: center;
        max-width: 100ex;
      }

      .message:empty {
        display: none;
      }

      .action {
        text-align: center;
      }

      .action:empty {
        display: none;
      }
    `,
  ],
})
export class BuckyEmoteComponent implements OnInit {
  @Input() public emote!: Emote;
  public alt!: string;

  ngOnInit(): void {
    if (!KNOWN_EMOTES.includes(this.emote)) {
      this.emote = 'shrug';
    }

    this.alt = EMOTE_ALTS[this.emote];
  }
}
