import {
  Component,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { GlobalState } from '@app/state';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import * as globalSelectors from '@app/selectors';
import * as globalActions from '@app/actions';
import { isSuccess } from 'loadable.ts';

@Component({
  selector: 'cse-alert, [cse-alert]',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
})
export class AlertComponent implements OnInit, OnDestroy {
  @HostBinding('attr.role') public role = 'banner';
  @HostBinding('class.alert-shown') public shown = false;
  @Input('cse-alert') public dismissedKey!: string;

  private _sub = Subscription.EMPTY;

  constructor(private store: Store<GlobalState>) {}

  ngOnInit() {
    this._sub = this.store
      .select(globalSelectors.prefs.getAll)
      .subscribe(loadable => {
        this.shown =
          isSuccess(loadable) && loadable.value[this.dismissedKey] !== true;
      });
  }

  dismiss() {
    this.store.dispatch(
      globalActions.prefs.setKey({ key: this.dismissedKey, value: true }),
    );
  }

  ngOnDestroy() {
    this._sub.unsubscribe();
  }
}
