import {
  Component,
  Input,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy,
  ContentChild,
  TemplateRef,
} from '@angular/core';
import { PackageGroup, NestedPackage } from '@app/core/models/packageGroup';
import { Subscription, BehaviorSubject, fromEvent } from 'rxjs';
import { unsubscribeAll } from '@app/core/utils';
import { map, debounceTime } from 'rxjs/operators';

/**
 * Computed by looking at how tall the parent summary element is when there is
 * only one row of text and the screen is wide enough for there to be no text-wrapping.
 *
 * Will need to be updated if the package group styles change dramatically.
 */
const MIN_PARENT_SUMMARY_HEIGHT = 41; // pixels

/**
 * Since the window resize handler can fire _a lot_ in a short period of time,
 * this constant is the time to wait after the last resize event to perform any
 * layout computations. This tries to prevent layout thrashing until the width
 * of the screen has settled.
 */
const RESIZE_DEBOUNCE_THRESHOLD = 100; // milliseconds

@Component({
  selector: 'cse-package-group',
  templateUrl: './package-group.component.html',
  styleUrls: ['./package-group.component.scss'],
})
export class PackageGroupComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() public group!: PackageGroup;
  @Input() public openGroupsByDefault = false;
  @Input() public showLockIcon!: boolean;
  @Input() public sectionIsLocked!: boolean;
  @ViewChild('parentSummary') public parentSummary: ElementRef | undefined;
  @ViewChild('childSummary') public childSummary: ElementRef | undefined;
  @ContentChild('auxHeader') public auxHeaderRef!: TemplateRef<any>;
  @ContentChild('auxSpacer') public auxSpacerRef!: TemplateRef<any>;
  @ContentChild('packageActions') public packageActionsRef!: TemplateRef<any>;

  public isNested!: boolean;
  private subscriptions: Subscription[] = [];
  public parentSummaryHeight = new BehaviorSubject(MIN_PARENT_SUMMARY_HEIGHT);
  public childSummaryStyles = this.parentSummaryHeight.pipe(
    map(offset => ({ top: `${offset}px` })),
  );

  ngOnInit(): void {
    this.isNested = this.group.kind === 'nested';
    this.openGroupsByDefault = this.isNested; // only expand the top level if nested
  }

  ngAfterViewInit() {
    if (this.parentSummary) {
      this.updateParentSummaryHeight();
      this.subscriptions.push(
        fromEvent(window, 'resize')
          .pipe(debounceTime(RESIZE_DEBOUNCE_THRESHOLD))
          .subscribe(() => this.updateParentSummaryHeight()),
      );
    }
  }

  ngOnDestroy() {
    unsubscribeAll(this.subscriptions);
  }

  private updateParentSummaryHeight(): void {
    if (this.parentSummary?.nativeElement) {
      const newOffset = this.parentSummary.nativeElement.offsetHeight;
      this.parentSummaryHeight.next(newOffset);
    }
  }

  public trackPackage(_index: number, pack: NestedPackage): string {
    return pack.id;
  }
}
