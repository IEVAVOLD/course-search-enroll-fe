import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, Input } from '@angular/core';
import { notNull, titlecase, unique } from '@app/core/utils';
import { Breakpoints } from '@app/shared/breakpoints';
import {
  EnrollmentPackage,
  EnrollmentSection,
  isReservedSection,
} from '@app/types/courses';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RawSection, RawMeeting } from '@app/types/courses';
import { toHourMinuteMeridianWithoutDST } from '@app/shared/date-formats';

@Component({
  selector: 'cse-pack-header',
  styleUrls: ['./pack-header.component.scss'],
  templateUrl: './pack-header.component.html',
  preserveWhitespaces: true,
})
export class PackHeaderComponent {
  _rows: Columns[] = [];
  _topics: string[] | null = null;
  _isDesktop$!: Observable<boolean>;

  @Input() isSelected = false;
  @Input() seats!: EnrollmentPackage['status'];
  @Input() creditRange!: EnrollmentPackage['creditRange'];
  @Input() set sections(sections: EnrollmentSection[]) {
    this._rows = sections.map(sec => {
      // Simplify meeting structure, remove duplicate meeting entries
      const dedupedMeetingAndLocation = processMeetings(
        sec.sessionDescription,
        sec,
      );

      return {
        catalogRef: `${sec.type} ${sec.sectionNumber}`,
        topic: sec.topic ? sec.topic.shortDescription : null,
        session: sec.sessionDescription,
        meetings: dedupedMeetingAndLocation.map(({ meeting }) => meeting),
        locations: dedupedMeetingAndLocation.map(({ location }) => location),
        instructors: sec.instructors
          .map(i => {
            const first = titlecase(i.name.first ?? '');
            const last = titlecase(i.name.last ?? '');
            return `${first} ${last}`.trim();
          })
          .filter(name => name.trim() !== ''),
        reservedSection: isReservedSection(sec),
        honors: formatHonors(sec.honors),
        commB: sec.comB,
      };
    });

    const topics = unique(this._rows.map(r => r.topic).filter(notNull));
    if (topics.length > 0) {
      this._topics = topics;
    } else {
      this._topics = null;
    }
  }

  constructor(breakpoints: BreakpointObserver) {
    this._isDesktop$ = breakpoints
      .observe(Breakpoints.Mobile)
      .pipe(map(({ matches }) => !matches));
  }
}

const NOT_REAL_BUILDING = ['PENDING', 'ONLINE', 'OFF CAMPUS'];

type Meeting =
  | { dayTime: string; session: string | null }
  | { dayTime: null; session: string };

interface Columns {
  catalogRef: string;
  topic: string | null;
  session: string;
  meetings: Meeting[];
  locations: (string | null)[];
  instructors: string[];
  reservedSection: boolean;
  honors: string | null;
  commB: boolean;
}

/**
 * - If there are no meetings, show session begin/end date
 * - If there are meetings:
 *   - Show the meeting days if applicable
 *   - Show session begin/end date if the meeting has no meeting
 *     days OR if the session code is NOT 'A1'
 */
const toMeeting = (session: string, sec: RawSection, m: RawMeeting) => {
  if (m.meetingDays && m.meetingTimeStart && m.meetingTimeEnd) {
    return {
      session: sec.sessionCode !== 'A1' ? session : null,
      dayTime: `${m.meetingDays} ${toHourMinuteMeridianWithoutDST(
        m.meetingTimeStart,
      )} - ${toHourMinuteMeridianWithoutDST(m.meetingTimeEnd)}`,
    };
  } else if (m.meetingDays && m.meetingTimeStart) {
    return {
      session: sec.sessionCode !== 'A1' ? session : null,
      dayTime: `${m.meetingDays} ${toHourMinuteMeridianWithoutDST(
        m.meetingTimeStart,
      )}`,
    };
  } else if (m.meetingDays) {
    return {
      session: sec.sessionCode !== 'A1' ? session : null,
      dayTime: `${m.meetingDays}`,
    };
  } else {
    return { session, dayTime: null };
  }
};

const toLocation = (m: RawMeeting) => {
  if (
    m.building &&
    NOT_REAL_BUILDING.includes(m.building.buildingName) === false
  ) {
    return m.room
      ? `${m.room} ${m.building.buildingName}`
      : m.building.buildingName;
  } else if (
    m.building &&
    (m.building as { latitude?: number }).latitude !== undefined
  ) {
    // Not exactly sure what this check achieves but it's
    // copied from the AngularJS template - Isaac
    return `${m.building.buildingName}`;
  } else {
    return null;
  }
};

const processMeetings = (session: string, sec: RawSection) => {
  return sec.classMeetings
    .filter(m => m.meetingType === 'CLASS')
    .map(m => ({
      meeting: toMeeting(session, sec, m),
      location: toLocation(m),
    }))
    .filter((next, index, all) => {
      return !all.slice(0, index).some(other => {
        return JSON.stringify(next) === JSON.stringify(other);
      });
    });
};

const formatHonors = (raw: RawSection['honors']): string | null => {
  switch (raw) {
    case 'HONORS_ONLY':
      return 'Honors only';
    case 'HONORS_LEVEL':
      return 'Accelerated honors';
    case 'INSTRUCTOR_APPROVED':
      return 'Honors optional';
    default:
      return raw;
  }
};
