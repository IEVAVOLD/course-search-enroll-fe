import { Component, OnInit, Input } from '@angular/core';
import { EnrollmentPackage, EnrollmentSection } from '@app/types/courses';

@Component({
  selector: 'cse-parent-header',
  template: `
    <cse-pack-header
      [isSelected]="isSelected"
      [seats]="package?.status ?? null"
      [creditRange]="package?.creditRange ?? ''"
      [sections]="sections"
    >
      <ng-content></ng-content>
    </cse-pack-header>
  `,
  styles: [
    `
      :host {
        display: block;
      }
    `,
  ],
})
export class ParentHeaderComponent implements OnInit {
  @Input() public lecture?: EnrollmentSection;
  @Input() public package?: EnrollmentPackage;
  @Input() public isSelected!: boolean;

  get sections(): EnrollmentSection[] {
    return this.lecture ? [this.lecture] : this.package!.sections;
  }

  ngOnInit() {
    console.assert(
      (this.lecture && !this.package) || (!this.lecture && this.package),
      'component expects `lecture` OR `package` to be set but NOT BOTH',
    );

    console.assert(
      Array.isArray(this.sections) && this.sections.length > 0,
      'found an enrollment package with 0 sections',
    );
  }
}
