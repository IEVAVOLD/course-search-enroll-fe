import { Component, Input } from '@angular/core';
import { NestedPackage } from '@app/core/models/packageGroup';

@Component({
  selector: 'cse-child-header',
  template: `
    <cse-pack-header
      [isSelected]="isSelected"
      [seats]="package.status"
      [creditRange]="package.creditRange"
      [sections]="package.sectionsWithoutLecture"
    >
      <ng-content></ng-content>
    </cse-pack-header>
  `,
  styles: [
    `
      :host {
        border-bottom: 1px solid #ccc;
        background: white;
        cursor: pointer;
      }
    `,
  ],
})
export class ChildHeaderComponent {
  @Input() public package!: NestedPackage;
  @Input() public isSelected!: boolean;
}
