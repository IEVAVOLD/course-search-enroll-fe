import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Course } from '@app/types/courses';
import { EnrollRequest } from '@app/types/enroll';
import * as s from '@app/types/schema';

export interface FinishedEnrollmentData {
  lastEnrollment: unknown[];
  requests: Array<[Course, EnrollRequest]>;
}

interface EnrollResult {
  isOkay: boolean;
  shortCatalog: string;
  messages: string[];
}

@Component({
  selector: 'cse-finished-enroll-dialog',
  templateUrl: './finished-enroll-dialog.component.html',
  styleUrls: ['./finished-enroll-dialog.component.scss'],
})
export class FinishedEnrollDialogComponent {
  public results: EnrollResult[];
  public buckyEmote: 'happy' | 'shrug' | 'sad';

  constructor(@Inject(MAT_DIALOG_DATA) data: FinishedEnrollmentData) {
    this.results = data.requests.map(([course], index): EnrollResult => {
      const result = data.lastEnrollment[index] as unknown;

      const isCompleted = s.object({
        enrollmentState: s.constant('Completed'),
      });

      let messages: string[] = [];
      const hasMessages = s.object({ enrollmentMessages: s.string });
      if (hasMessages.matches(result)) {
        try {
          const parsedMessages = JSON.parse(result.enrollmentMessages);
          if (s.array(s.unknown).matches(parsedMessages)) {
            messages = parsedMessages
              .filter(s.object({ description: s.string }).matches)
              .map(message => message.description);
          }
        } catch {
          messages = ['Incomplete enrollment message'];
        }
      } else {
        messages = ['No enrollment messages'];
      }

      return {
        isOkay: isCompleted.matches(result),
        shortCatalog: course.shortCatalog,
        messages,
      };
    });

    const allSuccessful = this.results.every(({ isOkay }) => isOkay);
    const someSuccessful = this.results.some(({ isOkay }) => isOkay);

    this.buckyEmote = allSuccessful
      ? 'happy'
      : someSuccessful
      ? 'shrug'
      : 'sad';
  }
}
