import { Component, Input } from '@angular/core';

@Component({
  selector: 'cse-detail-topic',
  templateUrl: './detail-topic.component.html',
  styleUrls: ['./detail-topic.component.scss'],
})
export class DetailTopicComponent {
  @Input() public topic!: string;
  @Input() public level = 3;
}
