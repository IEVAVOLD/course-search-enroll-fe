import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: '[cse-status-message]',
  templateUrl: './status-message.component.html',
  styleUrls: ['./status-message.component.scss'],
})
export class StatusMessageComponent {
  @HostBinding('class')
  get classes(): string {
    return `cse-status-message--${this.status}`;
  }

  get icon(): string | null {
    switch (this.status) {
      case 'okay':
        return 'check_circle';
      case 'error':
        return 'cancel';
      default:
        return this.status;
    }
  }

  @Input() public status!: 'okay' | 'warning' | 'error';
  @Input('cse-status-message') public headline!: string;
}
