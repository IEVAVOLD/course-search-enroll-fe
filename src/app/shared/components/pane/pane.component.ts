import { Component, Input } from '@angular/core';

@Component({
  selector: 'cse-pane',
  templateUrl: './pane.component.html',
  styleUrls: ['./pane.component.scss'],
})
export class PaneComponent {
  @Input() public label!: string;
  @Input() public scroll: boolean | 'hidden' | 'true' | 'false' = false;
}

@Component({
  selector: 'cse-pane-actions',
  template: '<ng-content></ng-content>',
  styles: [
    `
      :host {
        font-size: 1rem;
        text-align: right;
        flex-grow: 1;
      }
    `,
  ],
})
export class PaneActionsComponent {}
