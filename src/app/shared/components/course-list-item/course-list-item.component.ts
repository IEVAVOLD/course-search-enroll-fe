import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { CourseRef } from '@app/types/courses';

@Component({
  selector: 'cse-course-list-item',
  templateUrl: './course-list-item.component.html',
  styleUrls: ['./course-list-item.component.scss'],
})
export class CourseListItemComponent {
  @ViewChild('btn') private btnRef?: ElementRef;

  @Input() public ref?: CourseRef;
  @Input() public catalog!: string;
  @Input() public title!: string;
  @Input() public interactive = true;

  focus() {
    if (this.btnRef) {
      this.btnRef.nativeElement.focus();
    }
  }
}
