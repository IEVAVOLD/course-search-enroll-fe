import { Component, Input } from '@angular/core';

@Component({
  selector: 'cse-course-credits',
  template: `{{ value }}`,
})
export class CourseCreditsComponent {
  @Input() public value!: string;
}
