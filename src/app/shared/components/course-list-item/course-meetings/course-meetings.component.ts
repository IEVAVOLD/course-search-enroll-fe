import { Component, Input, OnChanges } from '@angular/core';
import { RoadmapMeeting, CurrentMeeting } from '@app/types/courses';

@Component({
  selector: 'cse-course-meetings',
  template: `
    <div class="meeting-grid" *ngIf="meetings.length > 0; else noMeetings">
      <ng-container *ngFor="let meeting of meetings">
        <span class="section-type">{{ meeting.sectionType }}</span>

        <span class="empty" *ngIf="meeting.location.type !== 'online'"></span>
        <span class="online" *ngIf="meeting.location.type === 'online'">
          Online
        </span>

        <span class="empty" *ngIf="!meeting.schedule"></span>
        <span class="days" *ngIf="meeting.schedule">
          {{ meeting.schedule.days }}
        </span>

        <span class="empty" *ngIf="!meeting.schedule"></span>
        <span class="times" *ngIf="meeting.schedule">
          {{ meeting.schedule.start }}&ndash;{{ meeting.schedule.end }}
        </span>
      </ng-container>
    </div>

    <ng-template #noMeetings>
      <span class="no-meetings">No class meetings</span>
    </ng-template>
  `,
  styles: [
    `
      :host {
        display: block;
        font-size: 0.9rem;
      }

      .meeting-grid {
        display: grid;
        grid-template-columns: auto auto 1fr auto;
        row-gap: 0.3rem;
      }

      .section-type {
        font-weight: 500;
        white-space: nowrap;
      }

      .days,
      .times,
      .online {
        margin-left: 0.5rem;
      }
    `,
  ],
})
export class CourseMeetingsComponent implements OnChanges {
  @Input() public meetings!: (RoadmapMeeting | CurrentMeeting)[];

  ngOnChanges() {
    /**
     * Since only the sectionType, schedule, and isOnline values are displayed
     * by this component, de-duplicate any meetings that have the same values
     * for all of those fields.
     */
    this.meetings = this.meetings.reduce((acc, next) => {
      const areDupes = (other: RoadmapMeeting | CurrentMeeting) =>
        next.sectionType === other.sectionType &&
        next.location.type === other.location.type &&
        JSON.stringify(next.schedule) === JSON.stringify(other.schedule);

      if (acc.some(areDupes)) {
        // Remove this meeting from this list since another meeting already
        // has been found with the same values.
        return acc;
      } else {
        // This meeting has a new combo of values so keep it in the list.
        return acc.concat(next);
      }
    }, [] as (RoadmapMeeting | CurrentMeeting)[]);
  }
}
