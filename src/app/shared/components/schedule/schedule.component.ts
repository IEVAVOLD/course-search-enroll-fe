import {
  AfterViewInit,
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { toHourMeridiemUtc } from '@app/shared/date-formats';
import { Block } from '@app/shared/pipes/layout-blocks.pipe';
import { DayOfTheWeek } from '@app/types/courses';

type Fit = 'optimize' | 'workday' | '24hrs';

const SECONDS_PER_HOUR = 60 * 60;

@Component({
  selector: 'cse-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
})
export class ScheduleComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() public blocks!: Block[];
  @Input() public interactive = true;
  @Input() public height: 'fixed' | 'short' | 'flex' = 'flex';
  @Input() public fit: Fit = 'workday';
  @Output() public block = new EventEmitter<unknown>();
  @Output() public rendered = new EventEmitter<void>();
  @ContentChild('scheduleBlock') public scheduleBlockRef!: TemplateRef<any>;

  public viewport!: Viewport;
  public hourHeightClass!: {
    'height-fixed': boolean;
    'height-short': boolean;
    'height-flex': boolean;
  };

  ngOnInit() {
    this.viewport = new Viewport(this.blocks, this.fit);
    this.hourHeightClass = {
      'height-fixed': this.height === 'fixed',
      'height-short': this.height === 'short',
      'height-flex': this.height === 'flex',
    };
  }

  ngAfterViewInit() {
    this.rendered.next();
  }

  ngOnChanges() {
    this.viewport = new Viewport(this.blocks, this.fit);
  }
}

const fmtHour = (hour: number): string => {
  hour = Math.floor(hour) % 24;
  return toHourMeridiemUtc(hour * SECONDS_PER_HOUR * 1000);
};

const WEEKEND: DayOfTheWeek[] = ['SUNDAY', 'SATURDAY'];

const WORK_WEEK: DayOfTheWeek[] = [
  'MONDAY',
  'TUESDAY',
  'WEDNESDAY',
  'THURSDAY',
  'FRIDAY',
];

const FULL_WEEK: DayOfTheWeek[] = ['SUNDAY', ...WORK_WEEK, 'SATURDAY'];

export class Viewport {
  days: DayOfTheWeek[];
  start: number; // seconds since midnight
  end: number; // seconds since midnight
  dayLabels: string[];
  hourLabels: string[];

  get secondsInWindow(): number {
    return this.end - this.start;
  }

  get hoursInWindow(): number {
    return this.secondsInWindow / SECONDS_PER_HOUR;
  }

  get startHour(): number {
    return Math.floor(this.start / SECONDS_PER_HOUR);
  }

  constructor(blocks: Block[], fit: Fit) {
    this.days = blocks.some(block => WEEKEND.includes(block.day))
      ? FULL_WEEK
      : WORK_WEEK;
    this.start = Viewport.calcStartHour(blocks, fit) * SECONDS_PER_HOUR;
    this.end = Viewport.calcEndHour(blocks, fit) * SECONDS_PER_HOUR;
    this.dayLabels = this.days.map(DayOfTheWeek.short);
    this.hourLabels = Array.from({
      length: Math.ceil(this.hoursInWindow),
    }).map((_, i) => fmtHour(this.startHour + i));
  }

  private static calcStartHour(blocks: Block[], fit: Fit): number {
    const MIDNIGHT = 0;
    const NINE_AM = 9;

    if (!blocks.length) {
      if (fit === '24hrs') {
        return MIDNIGHT;
      } else {
        return NINE_AM;
      }
    }

    const firstHour = Math.min(
      ...blocks.map(({ start }) => Math.floor(start / SECONDS_PER_HOUR)),
    );

    switch (fit) {
      case 'optimize':
        return firstHour;
      case 'workday':
        return Math.min(firstHour, NINE_AM);
      case '24hrs':
        return MIDNIGHT;
    }
  }

  private static calcEndHour(blocks: Block[], fit: Fit): number {
    const MIDNIGHT = 24;
    const SIX_PM = 12 + 6;

    if (!blocks.length) {
      if (fit === '24hrs') {
        return MIDNIGHT;
      } else {
        return SIX_PM;
      }
    }

    const lastHour = Math.max(
      ...blocks.map(block =>
        Math.ceil((block.start + block.duration) / SECONDS_PER_HOUR),
      ),
    );

    switch (fit) {
      case 'optimize':
        return lastHour;
      case 'workday':
        return Math.max(lastHour, SIX_PM);
      case '24hrs':
        return MIDNIGHT;
    }
  }
}
