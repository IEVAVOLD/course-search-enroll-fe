import { Component, ElementRef, Input } from '@angular/core';

@Component({
  selector: 'cse-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.scss'],
})
export class MeetingComponent {
  @Input() day!: string;
  @Input() start!: number;
  @Input() duration!: number;
  public time = '';
  public title = '';
  public location = '';
  public color = '';
  constructor(public elem: ElementRef) {}
}
