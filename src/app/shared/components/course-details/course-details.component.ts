import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Course } from '@app/types/courses';

@Component({
  selector: 'cse-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.scss'],
})
export class CourseDetailsComponent {
  @Input() public course!: Course;
  @Input() public showSectionsButton = true;
  @Output() public showPackages = new EventEmitter<void>();
}
