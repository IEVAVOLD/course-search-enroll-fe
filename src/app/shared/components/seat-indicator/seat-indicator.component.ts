import { Component, Input } from '@angular/core';
import { EnrollmentPackage } from '@app/types/courses';

@Component({
  selector: 'cse-seat-indicator',
  styles: [
    `
      :host {
        display: inline-block;
        width: 18px;
        height: 18px;
      }

      mat-icon {
        width: 18px;
        height: 18px;
        font-size: 18px;
      }

      .open {
        color: #457a3b;
      }

      .waitlisted {
        color: #ef6c00;
      }

      .closed {
        color: #c5050c;
      }
    `,
  ],
  template: `
    <ng-container [ngSwitch]="status">
      <mat-icon
        *ngSwitchCase="'OPEN'"
        class="open"
        alt="Has open seats"
        aria-label="Has open seats"
        matTooltip="Has open seats"
        matTooltipPosition="above"
        role="img"
      >
        check_circle
      </mat-icon>

      <mat-icon
        *ngSwitchCase="'WAITLISTED'"
        class="waitlisted"
        alt="Wait list seats available"
        aria-label="Wait list seats available"
        matTooltip="Wait list seats available"
        matTooltipPosition="above"
        role="img"
      >
        warning
      </mat-icon>

      <mat-icon
        *ngSwitchCase="'CLOSED'"
        class="closed"
        alt="Section closed"
        aria-label="Section closed"
        matTooltip="Section closed"
        matTooltipPosition="above"
        role="img"
      >
        report
      </mat-icon>
    </ng-container>
  `,
})
export class SeatIndicatorComponent {
  @Input() public status!: EnrollmentPackage['status'];
}
