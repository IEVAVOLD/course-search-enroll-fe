import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'cse-generic-small-dialog',
  template: `
    <cse-dialog [headline]="data.headline">
      <cse-dialog-body class="cse-typeset">
        <ng-container *ngIf="data | has: 'reasoning'">
          <p *ngFor="let line of data.reasoning">{{ line }}</p>
        </ng-container>
        <ul *ngIf="data | has: 'courses'">
          <li *ngFor="let course of data.courses">
            <strong>{{ course.shortCatalog }}</strong>
          </li>
        </ul>
      </cse-dialog-body>
      <cse-dialog-actions>
        <button *ngIf="!data.hideNegativeAction" mat-button mat-dialog-close>
          {{ data.negative || 'Cancel' }}
        </button>
        <a
          mat-button
          *ngIf="data.moreInfo"
          [href]="data.moreInfo.href"
          target="_blank"
        >
          {{ data.moreInfo.label }}
        </a>
        <div class="spacer"></div>
        <button mat-raised-button color="primary" [mat-dialog-close]="true">
          {{ data.affirmative || 'Okay' }}
        </button>
      </cse-dialog-actions>
    </cse-dialog>
  `,
})
export class GenericSmallDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}
}
