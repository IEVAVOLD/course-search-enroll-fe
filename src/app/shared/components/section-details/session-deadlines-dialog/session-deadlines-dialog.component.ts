import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Session } from '@app/types/terms';
import { toMonthDayYearWithDST } from '@app/shared/date-formats';

@Component({
  selector: 'cse-session-deadlines-dialog',
  templateUrl: './session-deadlines-dialog.component.html',
  styleUrls: ['./session-deadlines-dialog.component.scss'],
})
export class SessionDeadlinesDialogComponent implements OnInit {
  public addDate!: string;
  public withdrawDate!: string;
  public dropDate!: string;

  constructor(@Inject(MAT_DIALOG_DATA) public session: Session) {}

  ngOnInit(): void {
    this.addDate =
      this.session.addDate !== null
        ? toMonthDayYearWithDST(this.session.addDate)
        : 'Not specified';
    this.withdrawDate =
      this.session.drwDate !== null
        ? toMonthDayYearWithDST(this.session.drwDate)
        : 'Not specified';
    this.dropDate =
      this.session.dropDate !== null
        ? toMonthDayYearWithDST(this.session.dropDate)
        : 'Not specified';
  }
}
