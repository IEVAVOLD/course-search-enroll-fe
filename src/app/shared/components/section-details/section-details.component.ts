import { Component, Input } from '@angular/core';
import { EnrollmentPackage } from '@app/types/courses';
import { DialogService } from '@app/shared/services/dialog.service';
import { SessionDeadlinesDialogComponent } from './session-deadlines-dialog/session-deadlines-dialog.component';
import { InstructorContentDialogComponent } from './instructor-content-dialog/instructor-content-dialog.component';
import { SectionTextbooksDialogComponent } from './section-textbooks-dialog/section-textbooks-dialog.component';

@Component({
  selector: 'cse-section-details',
  templateUrl: './section-details.component.html',
  styleUrls: ['./section-details.component.scss'],
})
export class SectionDetailsComponent {
  @Input() public package!: EnrollmentPackage;
  @Input() public isSelected!: boolean;

  constructor(private dialog: DialogService) {}

  public showInstructorProvidedContentDialog() {
    this.dialog.large(
      InstructorContentDialogComponent,
      this.package.instructorProvidedContent,
    );
  }

  public showSessionDeadlinesDialog() {
    this.dialog.small(SessionDeadlinesDialogComponent, this.package.session);
  }

  public showTextbooksDialog() {
    this.dialog.large(SectionTextbooksDialogComponent, this.package);
  }
}
