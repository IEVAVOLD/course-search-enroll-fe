import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'cse-book-cover-loader',
  templateUrl: './book-cover-loader.component.html',
  styleUrls: ['./book-cover-loader.component.scss'],
})
export class BookCoverLoaderComponent implements OnInit {
  @Input() public isbn!: string;
  public src!: string;
  public state: 'loading' | 'loaded' | 'error' = 'loading';

  ngOnInit() {
    this.src = `/book/${this.isbn}`;
  }

  onLoad() {
    this.state = 'loaded';
  }

  onError() {
    this.state = 'error';
  }
}
