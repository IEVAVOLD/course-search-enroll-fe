import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { environment } from '@env/environment';

type DebugCallback = () => void;

export class DebugAction {
  constructor(
    public readonly label: string,
    public readonly callback: DebugCallback,
  ) {
    this.callback = this.callback.bind(this);
  }
}

@Injectable({ providedIn: 'root' })
export class DebugService {
  private _actions$ = new BehaviorSubject<DebugAction[]>([]);

  public actions$: Observable<DebugAction[] | null> = this._actions$.pipe(
    map(actions => {
      // Only expose debug actions if the current environment is NOT production
      // and if there is at least one action to expose.
      if (environment.production === false && actions.length > 0) {
        return actions;
      } else {
        return null;
      }
    }),

    // Add a zero millisecond delay so that these changes to the action list
    // take place during the next event-loop task and avoid an Angular
    // "change after checked" warning.
    delay(0),
  );

  register(label: string, callback: DebugCallback): DebugAction {
    const action = new DebugAction(label, callback);
    this._actions$.next([...this._actions$.value, action]);
    return action;
  }

  unregister(action: DebugAction): void {
    const newActions = this._actions$.value.filter(a => a !== action);
    if (newActions.length < this._actions$.value.length) {
      this._actions$.next(newActions);
    }
  }
}
