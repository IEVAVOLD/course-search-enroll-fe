import { Type } from '@angular/core';
import { AppModule } from '@app/app.module';
import { DebugAction, DebugService } from '@app/shared/debug/debug.service';

type DebugOptions = string | { label?: string };

type RegisterDebugMethodCallback = (thisAtRuntime: unknown) => DebugAction;

/**
 * There are a few behaviors of the `@Debug(...)` decorator that we need to
 * support:
 *
 * - Components need to be able to register multiple `@Debug(...)` methods
 * - Each `@Debug(...)` method needs to have the correct object bound to `this`
 *   when the method is called at runtime.
 * - `@Debug(...)` methods are only registered in the global `DebugService`
 *   while the component that houses the method is mounted in the DOM.
 *
 * To facilitate the third behavior we need to know when components with
 * `@Debug(...)` methods are added to the DOM and when those components are
 * removed from the DOM. Angular provides two lifecycle hooks that signal when
 * these events happen:
 *
 * - `ngOnInit` indicates the component is being added to the DOM
 * - `ngOnDestroy` indicates the component is being removed from the DOM
 *
 * Components can choose to implement or not implement one or both of these
 * hooks. It would be annoying and error-prone to ask any component that wants
 * to use `@Debug(...)` methods to manually add additional code in these hooks
 * just to support this decorator. Instead we want the decorator itself to
 * transparently detect when the Angular runtime calls these hooks without the
 * component itself having to do any manual work.
 *
 * We can achieve this by replacing any `ngOnInit` or `ngOnDestory` hooks on the
 * component's initial prototype with our own custom functions. These custom
 * functions will do any bookkeeping needed for any `@Debug(...)` methods before
 * calling the component's original `ngOnInit` or `ngOnDestory` hooks like
 * nothing out of the ordinary happened. Any custom behavior that the component
 * implemented for the `ngOnInit` or `ngOnDestory` hooks is preserved *and* the
 * debug service gets to know when the component is added or removed from the
 * DOM. We'll refer to this process as "patching" the lifecycle hooks.
 *
 * However if the component has multiple `@Debug(...)` methods we don't want to
 * patch the lifecycle hooks again and again for each method. We want to patch
 * the lifecycle hooks once and do all the bookkeeping needed by all
 * `@Debug(...)` methods at once. To facilitate this we'll need to keep track of
 * which component prototypes have been patched already and which have not been
 * patched yet. That's where the `patchedPrototype` WeakMap becomes useful.
 *
 * WeakMaps are a built-in JS data structure that maps arbitrary object keys to
 * arbitrary object values. The difference between a WeakMap and a regular Map
 * is that using an object as a key in a WeakMap does not prevent that object
 * from being garbage-collected. The practical application of this is that
 * WeakMaps help track metadata about objects that can't store the metadata
 * themselves. In this case the WeakMap key will be the prototype of any
 * component that has `@Debug(...)` methods and the WeakMap value will be an
 * array of callbacks that will register each `@Debug(...)` method with the
 * global debug service.
 */
const patchedPrototypes = new WeakMap<object, RegisterDebugMethodCallback[]>();

export function Debug(options?: DebugOptions): MethodDecorator {
  return function (target, key) {
    const label = toLabel(key, options);

    // Get a reference to the global debug service. This service is what
    // displays the debug buttons. In order for a debug button to be visible it
    // has to be registered when its component is initialized. The debug button
    // will also need to be un-registered when the component is destroyed.
    const service = AppModule.injector.get<DebugService>(
      DebugService as Type<DebugService>,
    );

    // Create a closure that will be called each time the component is initialized.
    const ngOnInitClosure = (thisAtRuntime: unknown): DebugAction => {
      // This closure will be called any time the debug button is clicked.
      const debugClickClosure = () => {
        const method = (target as any)[key];
        if (typeof method === 'function') {
          method.call(thisAtRuntime);
        }
      };

      return service.register(label, debugClickClosure);
    };

    // If this component's prototype has already been patched, the only
    // bookkeeping needed for this `@Debug(...)` method is to add its
    // initialization closure to the list of all initialization closures.
    const priorNgOnInitClosures = patchedPrototypes.get(
      target.constructor.prototype,
    );
    if (priorNgOnInitClosures) {
      priorNgOnInitClosures.push(ngOnInitClosure);
      return;
    }

    //
    // ANGULAR COMPONENT HOOK PATCHING & BOOKKEEPING SETUP
    //

    const newNgOnInitClosures = [ngOnInitClosure];
    const actions: DebugAction[] = [];

    // In the WeakMap, associate this component's prototype with the array of
    // initialization closures we have started building. Any subsequent
    // `@Debug(...)` methods will append their own initialization closure to
    // the end of this array.
    patchedPrototypes.set(target.constructor.prototype, newNgOnInitClosures);

    const originalNgOnInit: Function | undefined =
      target.constructor.prototype.ngOnInit;

    // Patch the `ngOnInit` hook to register any `@Debug(...)` actions with the
    // global debug service when the component is initialized.
    target.constructor.prototype.ngOnInit = function (...args: any[]) {
      newNgOnInitClosures.forEach(callback => {
        actions.push(callback(this));
      });

      // Call the original `ngOnInit` function if the prototype defines one.
      if (originalNgOnInit) {
        originalNgOnInit.call(this, ...args);
      }
    };

    const originalNgOnDestroy: Function | undefined =
      target.constructor.prototype.ngOnDestroy;

    // Patch the `ngOnDestroy` hook to un-register all `@Debug(...)` actions
    // from the global debug service when the component is destroyed.
    target.constructor.prototype.ngOnDestroy = function (...args: any[]) {
      actions.forEach(action => {
        service.unregister(action);
      });

      // Call the original `ngOnDestroy` function if the prototype defines one.
      if (originalNgOnDestroy) {
        originalNgOnDestroy.call(this, ...args);
      }
    };
  };
}

const toLabel = (key: string | symbol, opts?: DebugOptions): string => {
  if (typeof opts === 'string') {
    return opts;
  } else if (typeof opts?.label === 'string') {
    return opts.label;
  } else {
    return String(key);
  }
};
