import { createSelector, createFeatureSelector } from '@ngrx/store';
import {
  failed,
  isFailed,
  isLoading,
  isSuccess,
  Loadable,
  LOADING,
  monad,
  success,
} from 'loadable.ts';
import { GlobalState, Prefs } from './state';
import {
  Session,
  SpecialGroups,
  Subject,
  Term,
  TermCode,
  TermCodeOrZero,
  Terms,
  Zero,
} from '@app/types/terms';
import { StudentInfo } from './types/student';
import {
  Course,
  CourseRef,
  CurrentCourseRef,
  HasCurrentId,
  RoadmapCourseRef,
} from './types/courses';
import * as s from '@app/types/schema';

export namespace prefs {
  export const getAll = createFeatureSelector<Loadable<Prefs>>('prefs');

  export const getKey = createSelector(
    getAll,
    (prefs: Loadable<Prefs>, key: string) => {
      if (isSuccess(prefs)) {
        return prefs.value[key];
      } else {
        return null;
      }
    },
  );

  export const isShowingGrades = createSelector(getAll, prefs => {
    if (isSuccess(prefs)) {
      return prefs.value.degreePlannerGradesVisibility === true;
    } else {
      return false;
    }
  });
}

export namespace currentTermCode {
  export const get =
    createFeatureSelector<Loadable<TermCode>>('currentTermCode');
}

export namespace terms {
  const getFeature = createFeatureSelector<Loadable<Terms>>('terms');

  export const getAll = getFeature;

  export const getActiveTermCodes = createSelector(
    getFeature,
    (terms): Loadable<TermCode[]> =>
      monad(terms).map(terms => terms.activeTermCodes),
  );

  export const getTerm = createSelector(
    getFeature,
    (terms: Loadable<Terms>, termCode: TermCodeOrZero): Loadable<Term> => {
      if (isLoading(terms)) {
        return LOADING;
      } else if (isFailed(terms)) {
        return failed(null);
      }

      if (Zero.is(termCode)) {
        return success(terms.value.zero);
      }

      const encoded = TermCode.encode(termCode);
      const term: Term<TermCode> | undefined = terms.value.nonZero[encoded];

      if (!term) {
        return LOADING;
      }

      return success(term);
    },
  );

  export const getZeroTerm = createSelector(
    getFeature,
    (terms: Loadable<Terms>): Loadable<Term<Zero>> => {
      if (isLoading(terms)) {
        return LOADING;
      } else if (isFailed(terms)) {
        return failed(null);
      }

      return success(terms.value.zero);
    },
  );

  export const getNonZeroTerm = createSelector(
    getFeature,
    (terms: Loadable<Terms>, termCode: TermCode): Loadable<Term<TermCode>> => {
      if (isLoading(terms)) {
        return LOADING;
      } else if (isFailed(terms)) {
        return failed(null);
      }

      const encoded = TermCode.encode(termCode);
      const term: Term<TermCode> | undefined = terms.value.nonZero[encoded];

      if (!term) {
        return LOADING;
      }

      return success(term);
    },
  );

  export const getSubjectsForTerm = createSelector(getTerm, term => {
    return monad(term).map(term => term.subjects) as Loadable<Subject[]>;
  });

  export const getSessionsForTerm = createSelector(getTerm, term => {
    return monad(term).map(term => term.sessions) as Loadable<Session[]>;
  });

  export const getSpecialGroupsForTerm = createSelector(getTerm, term => {
    return monad(term).map(
      term => term.specialGroups,
    ) as Loadable<SpecialGroups>;
  });
}

export namespace student {
  export const getAll = createFeatureSelector<Loadable<StudentInfo>>('student');

  export const getLetter = createSelector(
    getAll,
    (student: Loadable<StudentInfo>): Loadable<string> => {
      return monad(student).map(student => {
        const schema = s.object({
          personAttributes: s.object({
            name: s.object({ first: s.string }),
          }),
        });

        if (schema.matches(student)) {
          return student.personAttributes.name.first[0] ?? '?';
        } else {
          return '?';
        }
      });
    },
  );
}

export namespace courses {
  export type Feature = GlobalState['courses'];

  const getFeature = createFeatureSelector<Feature>('courses');

  export const getAll = getFeature;

  export const getForTerm = createSelector(
    getFeature,
    (courses: Feature, termCode: TermCode): Feature[string] | null => {
      const key = TermCode.encode(termCode);
      return courses[key] ?? null;
    },
  );

  export const getRoadmapForTerm = createSelector(
    getFeature,
    (courses: Feature, termCode: TermCode): Feature[string]['roadmap'] => {
      const key = TermCode.encode(termCode);
      return courses[key]?.roadmap ?? null;
    },
  );

  export const getRoadmapForTermIsUnloaded = createSelector(
    getFeature,
    (courses: Feature, termCode: TermCode): boolean => {
      const key = TermCode.encode(termCode);
      const roadmap = courses[key]?.roadmap ?? null;
      return roadmap === null;
    },
  );

  export const getCurrentForTerm = createSelector(
    getFeature,
    (courses: Feature, termCode: TermCode): Feature[string]['current'] => {
      const key = TermCode.encode(termCode);
      return courses[key]?.current ?? null;
    },
  );

  export const getCurrentForTermIsUnloaded = createSelector(
    getFeature,
    (courses: Feature, termCode: TermCode): boolean => {
      const key = TermCode.encode(termCode);
      const current = courses[key]?.current ?? null;
      return current === null;
    },
  );
}

export namespace saved {
  export type Feature = GlobalState['saved'];

  const getFeature = createFeatureSelector<Feature>('saved');

  export const getAll = getFeature;
}

export const getCourseByRef = createSelector(
  courses.getAll,
  saved.getAll,
  (
    courses: courses.Feature,
    saved: saved.Feature,
    ref: CourseRef,
  ): Loadable<Course> | null => {
    if (RoadmapCourseRef.is(ref)) {
      const key = TermCode.encode(ref.termCode);
      const roadmap = courses[key]?.roadmap ?? null;
      if (roadmap && isSuccess(roadmap)) {
        const found = roadmap.value.find(CourseRef.matches(ref)) ?? null;
        if (found) {
          return success(found);
        } else {
          return null;
        }
      } else {
        return roadmap;
      }
    } else if (CurrentCourseRef.is(ref)) {
      const key = TermCode.encode(ref.termCode);
      const current = courses[key]?.current ?? null;
      if (current && isSuccess(current)) {
        const found = current.value.find(HasCurrentId.equalsCurry(ref)) ?? null;
        if (found) {
          return success(found);
        } else {
          return null;
        }
      } else {
        return current;
      }
    } else {
      if (saved && isSuccess(saved)) {
        const found = saved.value.find(CourseRef.matches(ref)) ?? null;
        if (found) {
          return success(found);
        } else {
          return null;
        }
      } else {
        return saved;
      }
    }
  },
);
