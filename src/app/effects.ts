import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ApiService, ValidationChoice } from './services/api.service';
import * as actions from './actions';
import {
  catchError,
  filter,
  map,
  mergeMap,
  withLatestFrom,
  tap,
  switchMap,
  first,
  finalize,
} from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { GlobalState, Prefs } from './state';
import * as selectors from './selectors';
import { forkJoin, of, pipe } from 'rxjs';
import { failed, isSuccess, success, Success } from 'loadable.ts';
import { TermCode, TermCodeOrZero, Terms, toTerms, ZERO } from './types/terms';
import { DialogService } from './shared/services/dialog.service';
import { ErrorLoadingTermsDialogComponent } from './components/error-loading-terms-dialog.component';
import {
  pollWhileStatusIsOneOf,
  waitUntilFirstSuccessThenUnwrap,
} from '@app/core/operators';
import {
  CurrentCourse,
  Details,
  EnrollmentPackage,
  HasClassNumber,
  RoadmapCourse,
  SavedCourse,
} from './types/courses';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { notNull, notUndefined, unsubscribeAll } from './core/utils';
import {
  FinishedEnrollDialogComponent,
  FinishedEnrollmentData,
} from './shared/components/finished-enroll-dialog/finished-enroll-dialog.component';
import { FailedEnrollDialogComponent } from './shared/components/failed-enroll-dialog/failed-enroll-dialog.component';
import {
  FinishedDropDialogComponent,
  FinishedDropDialogData,
} from './shared/components/dialogs/finished-drop-dialog/finished-drop-dialog.component';
import { HttpErrorResponse } from '@angular/common/http';
import { EnrollRequest } from './types/enroll';
import { AnalyticsService } from './services/analytics.service';
import * as s from '@app/types/schema';
import { sortPackages } from './core/models/packageGroup';

@Injectable()
export class Effects {
  constructor(
    private actions$: Actions,
    private store: Store<GlobalState>,
    private api: ApiService,
    private dialog: DialogService,
    private snackbar: MatSnackBar,
    private router: Router,
    private active: ActivatedRoute,
    private analytics: AnalyticsService,
  ) {}

  fetchPrefs$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.prefs.fetch),
      mergeMap(() => this.api.getPrefs()),
      map(prefs => actions.prefs.found({ prefs })),
      catchError(() => of(actions.prefs.failed())),
    ),
  );

  setPref$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.prefs.setKey, actions.prefs.setNestedKey),
        withLatestFrom(this.store.select(selectors.prefs.getAll)),
        map(([, prefs]) => prefs),
        filter((prefs): prefs is Success<Prefs> => isSuccess(prefs)),
        mergeMap(prefs => this.api.postPrefs(prefs.value)),
      ),
    { dispatch: false },
  );

  fetchTerms$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.terms.fetch),
      mergeMap(() => {
        return forkJoin([
          this.api.getAggregate(),
          this.api.getShortSubjectNames(),
        ]);
      }),
      map(([terms, subjects]) => toTerms(terms, subjects)),
      withLatestFrom(
        this.active.queryParams.pipe(
          map(params => params['term']),
          map(TermCode.decodeOrNull),
        ),
      ),
      map(([terms, termCodeOrNull]): [Terms, TermCode] => {
        if (
          termCodeOrNull !== null &&
          TermCode.isContainedInArray(termCodeOrNull, terms.activeTermCodes)
        ) {
          return [terms, termCodeOrNull];
        } else {
          return [terms, terms.activeTermCodes[0]];
        }
      }),
      map(([terms, currentTermCode]) =>
        actions.terms.found({ terms, currentTermCode }),
      ),
      catchError(() => of(actions.terms.failed())),
    ),
  );

  termsFailed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.terms.failed),
        tap(() => this.dialog.cannotClose(ErrorLoadingTermsDialogComponent)),
      ),
    { dispatch: false },
  );

  fetchStudent$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.student.fetch),
      mergeMap(() => this.api.getStudentInfo()),
      map(info => actions.student.found({ info: info ?? {} })),
      catchError(() => of(actions.student.failed())),
    ),
  );

  both$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.courses.load),
      switchMap(({ termCode }) => {
        return this.store
          .select(selectors.terms.getNonZeroTerm, termCode)
          .pipe(waitUntilFirstSuccessThenUnwrap());
      }),
      mergeMap(term => {
        return forkJoin([
          this.api.getRoadmapCourses(term.termCode).pipe(
            map(courses => courses.map(RoadmapCourse.fromCurry(term))),
            map(courses => success(courses)),
            catchError(() => of(failed(null))),
          ),
          this.api.getCurrentCourses(term.termCode).pipe(
            map(courses => courses.map(CurrentCourse.fromCurry(term))),
            map(courses => success(courses)),
            catchError(() => of(failed(null))),
          ),
        ]).pipe(
          map(([roadmap, current]) => {
            return actions.courses.done({
              termCode: term.termCode,
              roadmap,
              current,
            });
          }),
        );
      }),
    ),
  );

  bothIfNotAlready$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.courses.loadIfNotAlready),
      mergeMap(({ termCode }) => {
        return this.store.select(selectors.courses.getForTerm, termCode).pipe(
          map(courses => [termCode, courses] as const),
          first(),
        );
      }),
      switchMap(([termCode, courses]) => {
        const shouldLoadRoadmap = !courses || !courses.roadmap;
        const shouldLoadCurrent = !courses || !courses.current;

        const roadmapAction = shouldLoadRoadmap
          ? actions.courses.roadmap.load({ termCode })
          : null;

        const currentAction = shouldLoadCurrent
          ? actions.courses.current.load({ termCode })
          : null;

        return [roadmapAction, currentAction].filter(notNull);
      }),
    ),
  );

  roadmap$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.courses.roadmap.load),
      switchMap(({ termCode }) => {
        return this.store
          .select(selectors.terms.getNonZeroTerm, termCode)
          .pipe(waitUntilFirstSuccessThenUnwrap());
      }),
      mergeMap(term => {
        const ifOk = (courses: RoadmapCourse[]) =>
          actions.courses.roadmap.done({
            termCode: term.termCode,
            roadmap: success(courses),
          });

        const ifErr = () =>
          of(
            actions.courses.roadmap.done({
              termCode: term.termCode,
              roadmap: failed(null),
            }),
          );

        return this.api.getRoadmapCourses(term.termCode).pipe(
          map(courses => courses.map(RoadmapCourse.fromCurry(term))),
          map(ifOk),
          catchError(ifErr),
        );
      }),
    ),
  );

  packs$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.courses.roadmap.packs.load),
      mergeMap(({ course }) => {
        return this.store
          .select(selectors.terms.getSessionsForTerm, course.termCode)
          .pipe(
            first(),
            filter(isSuccess),
            map(sessions => [course, sessions.value] as const),
          );
      }),
      mergeMap(([course, sessions]) => {
        const getAllPacks = this.api.getEnrollmentPackages(course).pipe(
          map(packs => sortPackages(packs)),
          map(raw => raw.map(EnrollmentPackage.fromCurry(sessions))),
          map(packs => success(packs)),
          catchError(() => of(failed(null))),
        );

        const getChosenPack =
          course.package === null
            ? of(null)
            : this.api
                .getEnrollmentPackageByDocId(
                  course.termCode,
                  course.package.docId,
                )
                .pipe(
                  map(EnrollmentPackage.fromCurry(sessions)),
                  map(pack => success(pack)),
                  catchError(() => of(failed(null))),
                );

        return forkJoin([getAllPacks, getChosenPack]).pipe(
          map(([packs, pack]) => {
            return actions.courses.roadmap.packs.done({
              course,
              packs,
              pack,
            });
          }),
        );
      }),
    ),
  );

  addCourseWithoutPack$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.courses.roadmap.addCourseWithoutPack),
      mergeMap(({ course, showToast }) => {
        return this.api.postRoadmapCourseWithoutPackage(course).pipe(
          tap(() =>
            this.analytics.event(
              'Cart',
              'add-course-no-pack',
              course.shortCatalog,
            ),
          ),
          tap(() => {
            if (showToast === true) {
              const what = course.shortCatalog;
              const when = TermCode.describe(course.termCode);
              const message = `Added ${what} in ${when}`;
              this.openNaviationSnackbarToCart(message, course.termCode);
            }
          }),
          catchError(() => {
            if (showToast === true) {
              const what = course.shortCatalog;
              const when = TermCodeOrZero.describe(course.termCode);
              const message = `Unable to add ${what} in ${when}`;
              this.snackbar.open(message);
            }

            // Emitting a message to the user is the extent of error handling
            // for this case. Return an empty observable to keep going with
            // the rest of the pipeline and eventually reload the cart.
            return of();
          }),
          this.waitForValidationThenReloadCart(course.termCode),
        );
      }),
    ),
  );

  addCourseWithPack$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.courses.roadmap.addCourseWithPack),
      mergeMap(({ course, pack, ...action }) => {
        return this.api
          .postRoadmapCourseWithPackage(
            course,
            pack,
            action.credits,
            action.joinWaitlist,
            action.withHonors,
            action.classPermissionNumber,
          )
          .pipe(
            tap(() =>
              this.analytics.event(
                'Cart',
                'add-course-with-pack',
                course.shortCatalog,
              ),
            ),
            tap(() => {
              if (action.showToast === true) {
                const what = course.shortCatalog;
                const when = TermCode.describe(course.termCode);
                const message = `Added section to ${what} in ${when}`;
                this.openNaviationSnackbarToCart(message, course.termCode);
              }
            }),
            catchError(() => {
              if (action.showToast === true) {
                const what = course.shortCatalog;
                const when = TermCodeOrZero.describe(course.termCode);
                const message = `Unable to add section to ${what} in ${when}`;
                this.snackbar.open(message);
              }

              // Emitting a message to the user is the extent of error handling
              // for this case. Return an empty observable to keep going with
              // the rest of the pipeline and eventually reload the cart.
              return of();
            }),
            this.waitForValidationThenReloadCart(course.termCode),
          );
      }),
    ),
  );

  addCourseWithPackBulk$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.courses.roadmap.addCourseWithPackBulk),
      mergeMap(({ courses }) => {
        const calls =
          courses.length < 1
            ? of([])
            : forkJoin(
                courses.map(
                  action =>
                    this.api
                      .postRoadmapCourseWithPackage(
                        action.course,
                        action.pack,
                        action.credits,
                        action.joinWaitlist,
                        action.withHonors,
                        action.classPermissionNumber,
                      )
                      .pipe(
                        tap(() =>
                          this.analytics.event(
                            'Cart',
                            'add-course-with-pack',
                            action.course.shortCatalog,
                          ),
                        ),
                        catchError(e => of(e)),
                      ), // pass along errors
                ),
              );

        return calls.pipe(
          map(results => {
            if (results.some(e => e instanceof HttpErrorResponse)) {
              // TODO: alert users of the error
              console.log('One or more responses have errors', results);
            }
          }),
          this.waitForValidationThenReloadCart(courses[0].course.termCode),
        );
      }),
    ),
  );

  removePack$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.courses.roadmap.removePack),
      mergeMap(({ course, showToast }) => {
        return this.api
          .postClearRoadmapPackageOptions(
            course.termCode,
            course.subject.code,
            course.courseId,
          )
          .pipe(
            tap(() =>
              this.analytics.event('Cart', 'remove-pack', course.shortCatalog),
            ),
            tap(() => {
              if (showToast === true) {
                const what = course.shortCatalog;
                const when = TermCode.describe(course.termCode);
                const message = `Removed section from ${what} in ${when}`;
                this.openNaviationSnackbarToCart(message, course.termCode);
              }
            }),
            catchError(() => {
              if (showToast === true) {
                const what = course.shortCatalog;
                const when = TermCodeOrZero.describe(course.termCode);
                const message = `Unable to remove section from ${what} in ${when}`;
                this.snackbar.open(message);
              }

              // Emitting a message to the user is the extent of error handling
              // for this case. Return an empty observable to keep going with
              // the rest of the pipeline and eventually reload the cart.
              return of();
            }),
            this.waitForValidationThenReloadCart(course.termCode),
          );
      }),
    ),
  );

  removeCoursesAndPacks$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.courses.roadmap.removeCoursesAndPacks),
      filter(({ courses }) => courses.length > 0),
      mergeMap(({ courses, showToast }) => {
        const termCode = courses[0].termCode;
        const ok: string[] = [];
        const err: string[] = [];

        const removeAll = forkJoin(
          courses.map(course => {
            return this.api.deleteRoadmapCourses(course).pipe(
              tap(() =>
                this.analytics.event(
                  'Cart',
                  'remove-course',
                  course.shortCatalog,
                ),
              ),
              tap(() => ok.push(course.shortCatalog)),
              catchError(() => {
                err.push(course.shortCatalog);
                return of<void>();
              }),
            );
          }),
        );

        return removeAll.pipe(
          tap(() => {
            if (showToast === true) {
              const what = ok.length === 1 ? ok[0] : `${ok.length} courses`;
              const when = TermCode.describe(termCode);
              const message = `Removed ${what} from ${when}`;
              this.openNaviationSnackbarToCart(message, termCode);
            }
          }),
          this.waitForValidationThenReloadCart(termCode),
        );
      }),
    ),
  );

  validate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.courses.roadmap.validate),
      mergeMap(({ termCode, courses }) => {
        const payload = courses.map(ValidationChoice.fromRoadmapCourse);
        return this.api.postValidation(termCode, payload).pipe(
          tap(() => this.analytics.event('Cart', 'revalidate')),
          this.waitForValidationThenReloadCart(termCode),
          catchError((err: unknown) => {
            const message =
              err instanceof HttpErrorResponse &&
              s.object({ message: s.string }).matches(err.error)
                ? err.error.message
                : 'Please try again.';
            return of(
              actions.courses.roadmap.validationFailed({ termCode, message }),
            );
          }),
        );
      }),
    ),
  );

  validationFailed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.courses.roadmap.validationFailed),
      tap(({ message }) => {
        this.dialog.acknowledge({
          headline: 'Validation failed',
          reasoning: [message],
        });
      }),
      map(({ termCode }) => actions.courses.roadmap.load({ termCode })),
    ),
  );

  current$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.courses.current.load),
      switchMap(({ termCode }) => {
        return this.store
          .select(selectors.terms.getNonZeroTerm, termCode)
          .pipe(waitUntilFirstSuccessThenUnwrap());
      }),
      mergeMap(term => {
        const ifOk = (courses: CurrentCourse[]) =>
          actions.courses.current.done({
            termCode: term.termCode,
            current: success(courses),
          });

        const ifErr = () =>
          of(
            actions.courses.current.done({
              termCode: term.termCode,
              current: failed(null),
            }),
          );

        return this.api.getCurrentCourses(term.termCode).pipe(
          map(courses => courses.map(CurrentCourse.fromCurry(term))),
          map(ifOk),
          catchError(ifErr),
        );
      }),
    ),
  );

  pack$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.courses.current.pack.load),
      mergeMap(({ course }) => {
        return this.store
          .select(selectors.terms.getSessionsForTerm, course.termCode)
          .pipe(
            first(),
            filter(isSuccess),
            map(sessions => [course, sessions.value] as const),
          );
      }),
      mergeMap(([course, sessions]) => {
        return this.api
          .getEnrollmentPackageByDocId(course.termCode, course.package.docId)
          .pipe(
            map(EnrollmentPackage.fromCurry(sessions)),
            map(pack => success(pack)),
            catchError(() => of(failed(null))),
            map(pack => {
              return actions.courses.current.pack.done({
                course,
                pack,
              });
            }),
          );
      }),
    ),
  );

  enroll$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.courses.current.enroll),
      mergeMap(({ termCode, requests }) => {
        const closePendingDialog = this.dialog.keepOpen(
          'Enrolling',
          'Your enrollment is in progress.',
        );

        const statuses = ['New', 'InProgress'];
        const pollFn = () => this.api.getCartStatus();
        return this.api
          .postEnroll(
            termCode,
            requests.map(([, request]) => request),
          )
          .pipe(
            pollWhileStatusIsOneOf(statuses, pollFn),
            mergeMap(() => this.api.getLastEnrollment()),

            // Even if there was an error in the observable, the pending dialog
            // needs to be closed when the source observable completes.
            finalize(closePendingDialog),

            // If the last enrollment endpoint responded with a JSON array,
            // interpret that data as a finished enrollment and present it to the
            // user via a dialog.
            tap(lastEnrollment => {
              const data: FinishedEnrollmentData = {
                lastEnrollment: lastEnrollment ?? [],
                requests,
              };

              // For each enrollment record, decide whether it counts as a
              // successful enrollment or as a failed enrollment. Report those
              // statuses to the analytics backend.
              data.lastEnrollment.forEach((record, index) => {
                const [{ shortCatalog }] = requests[index];
                const eventLabel: `enrollment-${string}` = s
                  .object({ enrollmentState: s.string })
                  .matches(record)
                  ? `enrollment-${record.enrollmentState}`
                  : `enrollment-UNKNOWN`;
                this.analytics.event('Enrolled', eventLabel, shortCatalog);
              });

              this.dialog.large(FinishedEnrollDialogComponent, data);
            }),

            // If the last enrollment endpoint failed to respond, tell the user
            // that something went wrong with their enrollment and that they
            // should try again.
            catchError((err: unknown) => {
              const message =
                err instanceof HttpErrorResponse &&
                s.object({ message: s.string }).matches(err.error)
                  ? err.error.message
                  : null;

              this.analytics.event('Enrolled', 'enrollment-failure');
              this.dialog.large(FailedEnrollDialogComponent, { message });
              return of(null);
            }),

            // Reload the roadmap and current courses whether there was an error or not.
            map(() => actions.courses.load({ termCode })),
            catchError(() => of(actions.courses.load({ termCode }))),
          );
      }),
    ),
  );

  drop$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.courses.current.drop),
      mergeMap(({ termCode, courses }) => {
        const sameClassNumber = (match: string) => (course: CurrentCourse) => {
          return `${course.package.classNumber}` === match;
        };

        const packs: HasClassNumber[] = courses.map(course => course.package);

        interface Tuple {
          termCode: TermCode;
          success: CurrentCourse[];
          failed: Array<[CurrentCourse, string]>;
        }

        return this.api.postDrop(termCode, packs).pipe(
          map((messages): Tuple => {
            // Build a list of classes that were successfully dropped.
            const success = messages
              .filter(msg => msg.messageSeverity !== 'E')
              .map(msg => courses.find(sameClassNumber(msg.instanceId)))
              .filter(notUndefined);

            // Build a list of classes that were not dropped and the reason why
            // the class couldn't be dropped.
            const failed = messages
              .filter(msg => msg.messageSeverity === 'E')
              .map(msg => {
                const course = courses.find(sameClassNumber(msg.instanceId));
                return [course, msg.description] as const;
              })
              .filter((tuple): tuple is [CurrentCourse, string] => !!tuple[0]);

            return { termCode, success, failed };
          }),
          tap(({ success, failed }) => {
            success.forEach(({ shortCatalog }) => {
              this.analytics.event('Enrolled', 'dropped-success', shortCatalog);
            });

            failed.forEach(([{ shortCatalog }]) => {
              this.analytics.event('Enrolled', 'dropped-failure', shortCatalog);
            });
          }),
          catchError(err => {
            // If the entire drop request fails, report the errors to the user.
            const reasoning = s
              .array(s.object({ description: s.string }))
              .matches(err)
              ? err[0].description
              : 'Unknown error';

            return of<Tuple>({
              termCode,
              success: [],
              failed: courses.map(c => [c, reasoning]),
            });
          }),
        );
      }),
      tap(({ success, failed }) => {
        if (failed.length === 0) {
          const message =
            success.length === 1
              ? `Dropped ${success[0].shortCatalog}`
              : `Dropped ${success.length} courses`;

          this.snackbar.open(message);
        } else {
          const data: FinishedDropDialogData = {
            failed,
          };

          this.dialog.small(FinishedDropDialogComponent, data);
        }
      }),
      map(({ termCode }) => actions.courses.current.load({ termCode })),
    ),
  );

  edit$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.courses.current.edit),
      mergeMap(({ course, credits, honors }) => {
        const termCode = course.termCode;

        const request: EnrollRequest = {
          subjectCode: course.subject.code,
          courseId: course.courseId,
          classNumber: `${course.package.classNumber}`,
          options: {
            credits,
            waitlist: course.enrolledStatus === 'waitlisted' || null,
            honors: honors || null,
            relatedClassNumber1: toStringOrNull(
              course.package.relatedClassNumber1,
            ),
            relatedClassNumber2: toStringOrNull(
              course.package.relatedClassNumber2,
            ),
            classPermissionNumber: course.classPermissionNumber,
          },
        };

        return this.api.putEnroll(termCode, request).pipe(
          tap(() =>
            this.analytics.event('Enrolled', 'edit', course.shortCatalog),
          ),
          map(() => actions.courses.current.load({ termCode })),
        );
      }),
    ),
  );

  details$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.details.load),
      mergeMap(({ course }) => {
        const termCode: TermCodeOrZero =
          course.ref.kind === 'saved' ? ZERO : course.ref.termCode;
        const subjectCode = course.subject.code;
        const courseId = course.courseId;
        return this.api.getDetails(termCode, subjectCode, courseId).pipe(
          map(Details.from),
          map(details =>
            actions.details.done({
              course,
              details: success(details),
            }),
          ),
        );
      }),
    ),
  );

  saved$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.saved.load),
      switchMap(() => {
        return this.store
          .select(selectors.terms.getZeroTerm)
          .pipe(waitUntilFirstSuccessThenUnwrap());
      }),
      mergeMap(term => {
        const ifOk = (courses: SavedCourse[]) =>
          actions.saved.done({ saved: success(courses) });

        const ifErr = () => of(actions.saved.done({ saved: failed(null) }));

        return this.api.getFavorites().pipe(
          map(raws => raws.map(raw => SavedCourse.from(term, raw))),
          map(ifOk),
          catchError(ifErr),
        );
      }),
    ),
  );

  addSaved$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.saved.add),
      mergeMap(({ course }) => {
        return this.api.postFavorite(course.subject.code, course.courseId).pipe(
          tap(() =>
            this.analytics.event(
              'Favorites',
              'add-course',
              course.shortCatalog,
            ),
          ),
          tap(() => {
            const msg = `${course.shortCatalog} saved for later`;
            this.snackbar.open(msg);
          }),
          map(() => actions.saved.load()),
        );
      }),
    ),
  );

  removeSaved$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.saved.remove),
      filter(({ courses }) => courses.length > 0),
      mergeMap(({ courses }) => {
        if (courses.length === 0) {
          return of(actions.saved.load());
        }

        return forkJoin(
          courses.map(course => {
            return this.api.deleteFavorite(course).pipe(
              tap(() =>
                this.analytics.event(
                  'Favorites',
                  'remove-course',
                  course.shortCatalog,
                ),
              ),
              map(() => ({ course, wasRemoved: true })),
              catchError(() => of({ course, wasRemoved: false })),
            );
          }),
        ).pipe(
          tap(results => {
            const success = results.filter(({ wasRemoved }) => wasRemoved);
            const failed = results.filter(({ wasRemoved }) => !wasRemoved);

            if (failed.length === 0) {
              const msg =
                success.length === 1
                  ? `Removed ${success[0].course.shortCatalog}`
                  : `Removed ${success.length} courses`;
              this.snackbar.open(msg);
            } else {
              const msg =
                failed.length === 1
                  ? `Unable to remove ${failed[0].course.shortCatalog}`
                  : `Unable to remove ${failed.length} courses`;
              this.snackbar.open(msg);
            }
          }),
          map(() => actions.saved.load()),
        );
      }),
    ),
  );

  private waitForValidationThenReloadCart(termCode: TermCode) {
    const statuses = ['New', 'InProgress'];
    const pollFn = () => this.api.getCartStatus();
    return pipe(
      pollWhileStatusIsOneOf(statuses, pollFn),
      map(() => actions.courses.roadmap.load({ termCode })),
    );
  }

  private openNaviationSnackbar(
    message: string,
    action: string,
    commands: any[],
    state: any,
  ) {
    const ref = this.snackbar.open(message, action);
    const subs = [
      ref.afterDismissed().subscribe(() => unsubscribeAll(subs)),
      ref.onAction().subscribe(() => {
        this.router.navigate(commands, { state });
      }),
    ];
  }

  private openNaviationSnackbarToCart(message: string, termCode: TermCode) {
    this.openNaviationSnackbar(message, 'View cart', ['my-courses'], {
      showCart: termCode,
    });
  }
}

const toStringOrNull = (num: number | null): string | null => {
  if (typeof num === 'number') {
    return `${num}`;
  } else {
    return null;
  }
};
