import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '@app/services/api.service';
import { DegreePrograms, DegreeProgram } from '../models/degree-program';
import { Observable, combineLatest, Subject, Subscription } from 'rxjs';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
  UntypedFormArray,
} from '@angular/forms';
import { HonorsOption } from '../models/honors-option';
import {
  map,
  mergeMap,
  shareReplay,
  distinctUntilChanged,
  tap,
  filter,
  withLatestFrom,
} from 'rxjs/operators';
import { CourseBase } from '@app/core/models/course';
import { Store } from '@ngrx/store';
import { State } from '@app/dars/store/state';
import { RawPlan } from '@app/types/plans';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TermCode } from '@app/types/terms';
import { getActiveTermCodes } from '@app/dars/store/selectors';

const inclusiveRange = (from: number, to: number) => {
  const range: number[] = [];
  for (let i = from; i <= to; i++) {
    range.push(i);
  }
  return range;
};

type CourseWithCreditRange = CourseBase & { range: number[] };

export interface NewWhatIfAuditFields {
  roadmapId: number | null;
  darsInstitutionCode: string;
  darsDegreeProgramCode: string;
  degreePlannerPlanName: string;
  darsHonorsOptionCode: string;
  whichEnrolledCoursesIncluded: string;
  fixedCredits: {
    termCode: string;
    subjectCode: string;
    recordId: number;
    credits: number;
  }[];
}

@Component({
  selector: 'cse-new-what-if-audit-dialog',
  templateUrl: './new-what-if-audit-dialog.component.html',
  styleUrls: ['./new-what-if-audit-dialog.component.scss'],
})
export class NewWhatIfAuditDialogComponent implements OnInit, OnDestroy {
  // Form-builder objects
  public chosenProgram: UntypedFormGroup;
  public chosenAuditSettings: UntypedFormGroup;
  public chosenCreditSettings: UntypedFormArray;

  // API observables
  public institutions$!: Observable<DegreePrograms>;
  public programOrPlanOptions$!: Observable<readonly DegreeProgram[]>;
  public degreePlans$!: Observable<readonly RawPlan[]>;
  public chosenRoadmapId$ = new Subject<number>();
  public honorsOptions$!: Observable<readonly HonorsOption[]>;
  public variableCreditCourses$!: Observable<readonly CourseWithCreditRange[]>;
  public variableCreditCoursesSubscription!: Subscription;

  constructor(
    private fb: UntypedFormBuilder,
    private api: ApiService,
    private store: Store<State>,
    private dialogRef: MatDialogRef<
      NewWhatIfAuditDialogComponent,
      NewWhatIfAuditFields
    >,
    private snackBar: MatSnackBar,
  ) {
    this.chosenProgram = fb.group({
      institution: fb.control('', Validators.required),
      planOrProgram: fb.control(
        { value: '', disabled: true },
        Validators.required,
      ),
    });

    this.chosenAuditSettings = fb.group({
      honorsOptions: fb.control(' ', Validators.required),
      includeCoursesFrom: fb.control('future', Validators.required),
      degreePlan: fb.control('', Validators.required),
    });

    this.chosenCreditSettings = fb.array([]);
  }

  private initInstitutions() {
    return this.api.getAllDegreePrograms().pipe(shareReplay());
  }

  private initProgramOrPlanOptions() {
    // prettier-ignore
    const chosenInstitution = this.chosenProgram.controls.institution.valueChanges;

    return combineLatest([this.institutions$, chosenInstitution]).pipe(
      map(([insitutions, darsInstitutionCode]) => {
        if (insitutions.hasOwnProperty(darsInstitutionCode)) {
          return insitutions[darsInstitutionCode].programs;
        } else {
          return [];
        }
      }),
    );
  }

  private initHonorsOptions(): Observable<readonly HonorsOption[]> {
    /**
     * Get the honors options based on the parent insitution of the degree
     * program chosen in step 1 of the audit dialog. Since different
     * insitutions can have different honors options, this subscription needs
     * to be re-run whenever the degree program is switched.
     */

    // prettier-ignore
    const chosenInstitution = this.chosenProgram.controls.institution.valueChanges;

    return combineLatest([this.institutions$, chosenInstitution]).pipe(
      map(([institutions, darsInstitutionCode]) => {
        if (institutions.hasOwnProperty(darsInstitutionCode)) {
          return institutions[darsInstitutionCode].honorsOptions;
        } else {
          return [];
        }
      }),
    );
  }

  private initVariableCreditCourses(): Observable<
    readonly CourseWithCreditRange[]
  > {
    /**
     * When the user chooses a degree plan:
     *
     * 1. Load all of the courses from that degree plan
     * 2. Find all the courses that support a range of credits and that occur
     *    in an active or future term
     * 3. Compute the range of credit values supported by those courses and add
     *    that range to the course objects
     * 4. Populate the `chosenCreditSettings` form input with these courses
     *    using the credit range to populate each course's dropdown.
     */

    // prettier-ignore
    const chosenDegreePlan = this.chosenAuditSettings.controls.degreePlan.valueChanges;

    return chosenDegreePlan.pipe(
      filter((maybeDegreePlan): maybeDegreePlan is { roadmapId: number } => {
        return (
          !!maybeDegreePlan &&
          maybeDegreePlan.hasOwnProperty('roadmapId') &&
          typeof maybeDegreePlan['roadmapId'] === 'number'
        );
      }),
      map(maybeDegreePlan => maybeDegreePlan.roadmapId),
      distinctUntilChanged(),
      mergeMap(roadmapId => this.api.getPlanCourses(roadmapId)),
      withLatestFrom(this.store.select(getActiveTermCodes)),
      map(([courses, activeTermCodes]) => {
        return courses.filter(course => {
          return (
            !!course.creditMin &&
            !!course.creditMax &&
            course.creditMax > course.creditMin &&
            this.courseIsFromAnActiveOrFutureTerm(course, activeTermCodes)
          );
        });
      }),
      map(courses => {
        return courses.map((course): CourseWithCreditRange => {
          return {
            ...course,
            range: inclusiveRange(course.creditMin || 0, course.creditMax || 0),
          };
        });
      }),
      shareReplay(),
    );
  }

  public ngOnInit() {
    this.institutions$ = this.initInstitutions().pipe(
      tap(() => {
        return this.chosenProgram.controls.planOrProgram.enable();
      }),
    );

    this.programOrPlanOptions$ = this.initProgramOrPlanOptions();

    this.degreePlans$ = this.api.getDegreePlans().pipe(
      shareReplay(),
      tap(plans => {
        const primaryPlan = plans.find(plan => plan.primary);
        this.chosenAuditSettings.controls.degreePlan.setValue(primaryPlan);
      }),
    );

    this.honorsOptions$ = this.initHonorsOptions();

    this.variableCreditCourses$ = this.initVariableCreditCourses().pipe(
      tap(courses => {
        /**
         * This feels hacky and hopefully there's a better way to remove the old
         * course credit inputs and replace them with new course credit inputs.
         */

        // Remove the old credit settings form inputs
        while (this.chosenCreditSettings.length > 0) {
          this.chosenCreditSettings.removeAt(0);
        }

        // Add the new credit settings form inputs
        courses.forEach(course => {
          const initialCreditValue = course.credits || '';
          const creditsDropdown = this.fb.control(
            initialCreditValue,
            Validators.required,
          );
          const newCreditSetting = this.fb.group({ course, creditsDropdown });
          this.chosenCreditSettings.push(newCreditSetting);
        });
      }),
    );

    this.variableCreditCoursesSubscription =
      this.variableCreditCourses$.subscribe();
  }

  public ngOnDestroy() {
    this.variableCreditCoursesSubscription.unsubscribe();
  }

  private courseIsFromAnActiveOrFutureTerm(
    course: CourseBase,
    active: TermCode[],
  ): boolean {
    if (course.termCode === null) {
      return false;
    }

    const termCode = TermCode.decodeOrThrow(course.termCode);
    return (
      TermCode.isActive(active, termCode) || TermCode.isFuture(active, termCode)
    );
  }

  public comparePlans(a: RawPlan | null, b: RawPlan | null): boolean {
    if (!a || !b) {
      return false;
    } else {
      return a.roadmapId === b.roadmapId;
    }
  }

  public darsInstitutionCode<T>(fallback: T): string | T {
    const control = this.chosenProgram.get('institution');
    if (control !== null) {
      return control.value.toString();
    } else {
      return fallback;
    }
  }

  public darsDegreeProgramCode<T>(fallback: T): string | T {
    const control = this.chosenProgram.get('planOrProgram');
    if (control !== null) {
      return control.value.toString();
    } else {
      return fallback;
    }
  }

  public degreePlanId(): number | null {
    return (
      (this.chosenAuditSettings.controls.degreePlan.value as RawPlan)
        ?.roadmapId ?? null
    );
  }

  public degreePlannerPlanName<T>(fallback: T): string | T {
    const control = this.chosenAuditSettings.get('degreePlan');
    const coursesControl = this.chosenAuditSettings.get('includeCoursesFrom');

    if (coursesControl && coursesControl.value === 'planned') {
      if (control && control.value !== null) {
        return control.value.name;
      } else {
        return fallback;
      }
    } else {
      return fallback;
    }
  }

  public darsHonorsOptionCode<T>(fallback: T): string | T {
    const control = this.chosenAuditSettings.get('honorsOptions');
    if (control !== null) {
      return control.value.toString();
    } else {
      return fallback;
    }
  }

  // If a degree plan name is included in the request we do not need this property
  public includeCoursesFrom<T>(fallback: T): string | T {
    const control = this.chosenAuditSettings.get('includeCoursesFrom');
    if (control && control.value !== 'planned') {
      return control.value.toString();
    } else {
      return fallback;
    }
  }

  public fixedCredits(): NewWhatIfAuditFields['fixedCredits'] {
    if (
      this.chosenAuditSettings.controls.includeCoursesFrom.value !== 'planned'
    ) {
      return [];
    }

    return (this.chosenCreditSettings.value as any[]).map(
      (group: { course: CourseBase; creditsDropdown: number }) => {
        return {
          termCode: group.course.termCode || '',
          subjectCode: group.course.subjectCode || '',
          recordId: group.course.id as number,
          credits: group.creditsDropdown,
        };
      },
    );
  }

  public submitAudit() {
    this.dialogRef.close({
      roadmapId: this.degreePlanId(),
      darsInstitutionCode: this.darsInstitutionCode(''),
      darsDegreeProgramCode: this.darsDegreeProgramCode(''),
      degreePlannerPlanName: this.degreePlannerPlanName(''),
      darsHonorsOptionCode: this.darsHonorsOptionCode(''),
      whichEnrolledCoursesIncluded: this.includeCoursesFrom(''),
      fixedCredits: this.fixedCredits(),
    });
    this.snackBar.open(`New 'What-If' Audit has been requested`);
  }
}
