import { Component, Input, HostListener } from '@angular/core';
import { AuditMetadata } from '../models/audit-metadata';

@Component({
  selector: 'cse-metadata-mobile-view',
  templateUrl: './metadata-mobile-view.component.html',
  styleUrls: ['./metadata-mobile-view.component.scss'],
})
export class MetadataMobileViewComponent {
  @Input() public metadata!: AuditMetadata[];
  public scrolling = false;

  // Checking for window scroll events
  @HostListener('window:scroll', ['$event'])
  onScroll() {
    window.pageYOffset > 400
      ? (this.scrolling = true)
      : (this.scrolling = false);
  }
  public scrollTop() {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }
}
