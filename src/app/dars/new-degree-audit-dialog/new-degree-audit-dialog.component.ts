import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '@app/services/api.service';
import { DegreePrograms } from '../models/degree-program';
import { StudentDegreeProgram } from '../models/student-degree-program';
import { Observable, combineLatest, Subscription } from 'rxjs';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
  UntypedFormControl,
  UntypedFormArray,
  AbstractControl,
} from '@angular/forms';
import { HonorsOption } from '../models/honors-option';
import {
  map,
  filter,
  mergeMap,
  shareReplay,
  distinctUntilChanged,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { CourseBase } from '@app/core/models/course';
import { Store } from '@ngrx/store';
import { State } from '@app/dars/store/state';
import { RawPlan } from '@app/types/plans';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { getActiveTermCodes } from '@app/dars/store/selectors';
import { TermCode } from '@app/types/terms';
import * as s from '@app/types/schema';

const inclusiveRange = (from: number, to: number) => {
  const range: number[] = [];
  for (let i = from; i <= to; i++) {
    range.push(i);
  }
  return range;
};

export interface NewDegreeAuditFields {
  roadmapId: number | null;
  darsInstitutionCode: string;
  darsDegreeProgramCode: string;
  degreePlannerPlanName: string;
  darsHonorsOptionCode: string;
  whichEnrolledCoursesIncluded: string;
  fixedCredits: {
    termCode: string;
    subjectCode: string;
    recordId: number;
    credits: number;
  }[];
}

type CourseWithCreditRange = CourseBase & { range: number[] };

@Component({
  selector: 'cse-new-degree-audit-dialog',
  templateUrl: './new-degree-audit-dialog.component.html',
  styleUrls: ['./new-degree-audit-dialog.component.scss'],
})
export class NewDegreeAuditDialogComponent implements OnInit, OnDestroy {
  // Form-builder objects
  public chosenProgram: UntypedFormControl;
  public chosenAuditSettings: UntypedFormGroup;
  public chosenCreditSettings: UntypedFormArray;

  // API observables
  public degreePrograms$!: Observable<readonly StudentDegreeProgram[]>;
  public degreePlans$!: Observable<readonly RawPlan[]>;
  public honorsOptions$!: Observable<readonly HonorsOption[]>;
  public variableCreditCourses$!: Observable<readonly CourseWithCreditRange[]>;
  public variableCreditCoursesSubscription!: Subscription;

  constructor(
    private fb: UntypedFormBuilder,
    private api: ApiService,
    private store: Store<State>,
    private dialogRef: MatDialogRef<NewDegreeAuditDialogComponent>,
    private snackBar: MatSnackBar,
  ) {
    this.chosenProgram = fb.control('', Validators.required);

    this.chosenAuditSettings = fb.group({
      honorsOptions: fb.control(' ', Validators.required),
      includeCoursesFrom: fb.control('future', Validators.required),
      degreePlan: fb.control('', Validators.required),
    });

    this.chosenCreditSettings = fb.array([]);
  }

  private initDegreeProgram(): Observable<StudentDegreeProgram[]> {
    /**
     * Get the list of degree programs the student is enrolled in. If there are
     * more than one, set the first degree program to be the initial value of
     * the `chosenProgram` form input.
     *
     * This observable will only fire when the dialog is instantiated.
     */
    return this.api.getStudentDegreePrograms().pipe(shareReplay());
  }

  private initHonorsOptions(): Observable<HonorsOption[]> {
    /**
     * Get the honors options based on the parent institution of the degree
     * program chosen in step 1 of the audit dialog. Since different
     * institutions can have different honors options, this subscription needs
     * to be re-run whenever the degree program is switched.
     */
    return combineLatest<[DegreePrograms, unknown]>([
      this.api.getAllDegreePrograms(),
      this.chosenProgram.valueChanges,
    ]).pipe(
      map(([allInstitutions, chosenProgram]) => {
        const hasInstitutionCode = s.object({ darsInstitutionCode: s.string });
        if (hasInstitutionCode.matches(chosenProgram)) {
          const code = chosenProgram.darsInstitutionCode;
          if (allInstitutions.hasOwnProperty(code)) {
            return allInstitutions[code].honorsOptions ?? [];
          }
        }

        return [];
      }),
    );
  }

  private initVariableCreditCourses(): Observable<CourseWithCreditRange[]> {
    /**
     * When the user chooses a degree plan:
     *
     * 1. Load all of the courses from that degree plan
     * 2. Find all the courses that support a range of credits and that occur
     *    in an active or future term
     * 3. Compute the range of credit values supported by those courses and add
     *    that range to the course objects
     * 4. Populate the `chosenCreditSettings` form input with these courses
     *    using the credit range to populate each course's dropdown.
     */

    // prettier-ignore
    const chosenDegreePlan = (this.chosenAuditSettings.get('degreePlan') as AbstractControl).valueChanges;
    return chosenDegreePlan.pipe(
      filter((maybeDegreePlan): maybeDegreePlan is { roadmapId: number } => {
        return (
          !!maybeDegreePlan &&
          maybeDegreePlan.hasOwnProperty('roadmapId') &&
          typeof maybeDegreePlan['roadmapId'] === 'number'
        );
      }),
      map(maybeDegreePlan => maybeDegreePlan.roadmapId),
      distinctUntilChanged(),
      mergeMap(roadmapId => this.api.getPlanCourses(roadmapId)),
      withLatestFrom(this.store.select(getActiveTermCodes)),
      map(([courses, activeTermCodes]) => {
        return courses.filter(course => {
          return (
            !!course.creditMin &&
            !!course.creditMax &&
            course.creditMax > course.creditMin &&
            this.courseIsFromAnActiveOrFutureTerm(course, activeTermCodes)
          );
        });
      }),
      map(courses => {
        return courses.map((course): CourseWithCreditRange => {
          return {
            ...course,
            range: inclusiveRange(course.creditMin || 0, course.creditMax || 0),
          };
        });
      }),
      shareReplay(),
    );
  }

  public ngOnInit() {
    this.degreePrograms$ = this.initDegreeProgram().pipe(
      tap(degreePlans => {
        const firstDarsablePlan = degreePlans.find(
          p => p.darsInstitutionCode !== null,
        );
        this.chosenProgram.setValue(firstDarsablePlan ?? null);
      }),
    );

    this.variableCreditCourses$ = this.initVariableCreditCourses().pipe(
      tap(courses => {
        /**
         * This feels hacky and hopefully there's a better way to remove the old
         * course credit inputs and replace them with new course credit inputs.
         */

        // Remove the old credit settings form inputs
        while (this.chosenCreditSettings.length > 0) {
          this.chosenCreditSettings.removeAt(0);
        }

        // Add the new credit settings form inputs
        courses.forEach(course => {
          const initialCreditValue = course.credits || '';
          const creditsDropdown = this.fb.control(
            initialCreditValue,
            Validators.required,
          );
          const newCreditSetting = this.fb.group({ course, creditsDropdown });
          this.chosenCreditSettings.push(newCreditSetting);
        });
      }),
    );

    this.variableCreditCoursesSubscription =
      this.variableCreditCourses$.subscribe();

    /**
     * Get the list of user degree plans so that if they choose to include
     * planned courses in the audit, they can be presented with a choice of
     * which degree plan to load those planned courses from.
     *
     * Also set the primary degree plan to be the initial value of the
     * `degreePlan` form input.
     *
     * This observable will only fire when the dialog is instantiated.
     */
    this.degreePlans$ = this.api.getDegreePlans().pipe(
      tap(plans => {
        const primaryPlan = plans.find(plan => plan.primary);
        this.chosenAuditSettings.controls.degreePlan.setValue(primaryPlan);
      }),
    );

    this.honorsOptions$ = this.initHonorsOptions();
  }

  public ngOnDestroy() {
    this.variableCreditCoursesSubscription.unsubscribe();
  }

  private courseIsFromAnActiveOrFutureTerm(
    course: CourseBase,
    active: TermCode[],
  ): boolean {
    if (course.termCode === null) {
      return false;
    }

    const termCode = TermCode.decodeOrThrow(course.termCode);
    return (
      TermCode.isActive(active, termCode) || TermCode.isFuture(active, termCode)
    );
  }

  public darsInstitutionCode<T>(fallback: T): string | T {
    if (typeof this.chosenProgram.value === 'object') {
      return this.chosenProgram.value.darsInstitutionCode;
    } else {
      return fallback;
    }
  }

  public darsDegreeProgramCode<T>(fallback: T): string | T {
    if (typeof this.chosenProgram.value === 'object') {
      return this.chosenProgram.value.darsDegreeProgramCode;
    } else {
      return fallback;
    }
  }

  public degreePlanId(): number | null {
    return (
      (this.chosenAuditSettings.controls.degreePlan.value as RawPlan)
        ?.roadmapId ?? null
    );
  }

  public degreePlannerPlanName<T>(fallback: T): string | T {
    const control = this.chosenAuditSettings.get('degreePlan');
    const coursesControl = this.chosenAuditSettings.get('includeCoursesFrom');

    if (coursesControl && coursesControl.value === 'planned') {
      if (control && control.value !== null) {
        return control.value.name;
      } else {
        return fallback;
      }
    } else {
      return fallback;
    }
  }

  public darsHonorsOptionCode<T>(fallback: T): string | T {
    const control = this.chosenAuditSettings.get('honorsOptions');
    if (control !== null) {
      return control.value.toString();
    } else {
      return fallback;
    }
  }

  // If a degree plan name is included in the request we do not need this property
  public includeCoursesFrom<T>(fallback: T): string | T {
    const control = this.chosenAuditSettings.get('includeCoursesFrom');
    if (control && control.value !== 'planned') {
      return control.value.toString();
    } else {
      return fallback;
    }
  }

  public fixedCredits(): NewDegreeAuditFields['fixedCredits'] {
    if (
      this.chosenAuditSettings.controls.includeCoursesFrom.value !== 'planned'
    ) {
      return [];
    }

    return (this.chosenCreditSettings.value as any[]).map(
      (group: { course: CourseBase; creditsDropdown: number }) => {
        return {
          termCode: group.course.termCode || '',
          subjectCode: group.course.subjectCode || '',
          recordId: group.course.id as number,
          credits: group.creditsDropdown,
        };
      },
    );
  }

  public submitAudit() {
    this.dialogRef.close({
      roadmapId: this.degreePlanId(),
      darsInstitutionCode: this.darsInstitutionCode(''),
      darsDegreeProgramCode: this.darsDegreeProgramCode(''),
      degreePlannerPlanName: this.degreePlannerPlanName(''),
      darsHonorsOptionCode: this.darsHonorsOptionCode(''),
      whichEnrolledCoursesIncluded: this.includeCoursesFrom(''),
      fixedCredits: this.fixedCredits(),
    });
    this.snackBar.open('New Degree Audit has been requested');
  }
}
