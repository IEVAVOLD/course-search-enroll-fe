import { DARSState, INITIAL_DARS_STATE } from '@app/dars/store/state';
import { DarsActionTypes } from './actions';
// eslint-disable-next-line no-duplicate-imports
import * as darsActions from './actions';
import { groupAuditMetadata } from './utils';

type SupportedActions =
  | darsActions.DoneLoadingDARSView
  | darsActions.ErrorLoadingDARSView
  | darsActions.DoneRefreshingMetadata
  | darsActions.StartSendingAudit
  | darsActions.DoneSendingAudit
  | darsActions.ErrorSendingAudit
  | darsActions.StartLoadingAudit
  | darsActions.DoneLoadingAudit;

export function darsReducer(
  state = INITIAL_DARS_STATE,
  action: SupportedActions,
): DARSState {
  switch (action.type) {
    case DarsActionTypes.DoneLoadingDARSView: {
      return {
        ...state,
        ...action.payload,
      };
    }
    case DarsActionTypes.DoneRefreshingMetadata: {
      const outstanding =
        state.metadata.status === 'Loaded'
          ? { ...state.metadata.outstanding }
          : { program: 0, whatIf: 0 };

      return {
        ...state,
        metadata: {
          status: 'Loaded',
          outstanding,
          ...groupAuditMetadata(action.payload, state.degreePrograms),
        },
      };
    }
    case DarsActionTypes.StartSendingAudit: {
      if (state.metadata.status === 'Loaded') {
        const outstanding = { ...state.metadata.outstanding };
        outstanding[action.payload.auditType]++;

        return {
          ...state,
          metadata: {
            ...state.metadata,
            outstanding,
          },
        };
      } else {
        return state;
      }
    }
    case DarsActionTypes.DoneSendingAudit: {
      if (state.metadata.status === 'Loaded') {
        const outstanding = { ...state.metadata.outstanding };
        outstanding[action.payload.auditType]--;

        const pending = { ...state.metadata.pending };
        pending[action.payload.auditType]++;

        return {
          ...state,
          metadata: {
            ...state.metadata,
            outstanding,
            pending,
          },
        };
      } else {
        return state;
      }
    }
    case DarsActionTypes.ErrorSendingAudit: {
      if (state.metadata.status === 'Loaded') {
        const outstanding = { ...state.metadata.outstanding };
        outstanding[action.payload.auditType]--;

        return {
          ...state,
          metadata: {
            ...state.metadata,
            outstanding,
          },
        };
      } else {
        return state;
      }
    }
    case DarsActionTypes.ErrorLoadingDARSView: {
      return {
        ...state,
        metadata: {
          status: 'Error',
          message: action.payload.message,
        },
      };
    }
    case DarsActionTypes.StartLoadingAudit: {
      return {
        ...state,
        audits: {
          ...state.audits,
          [action.payload.darsDegreeAuditReportId]: {
            status: 'Loading',
            metadata: action.payload,
          },
        },
      };
    }
    case DarsActionTypes.DoneLoadingAudit: {
      return {
        ...state,
        audits: {
          ...state.audits,
          [action.payload.metadata.darsDegreeAuditReportId]: {
            status: 'Loaded',
            metadata: action.payload.metadata,
            audit: action.payload.audit,
          },
        },
      };
    }
    default:
      return state;
  }
}
