import { Observable, of, ReplaySubject, throwError } from 'rxjs';
import { DARSEffects } from '@app/dars/store/effects';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { ApiService } from '@app/services/api.service';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import {
  ActivatedRoute,
  convertToParamMap,
  ParamMap,
  Params,
} from '@angular/router';
import { Injectable } from '@angular/core';
import { DARSState, State } from '@dars/store/state';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TermCode } from '@app/types/terms';
import * as selectors from '@app/dars/store/selectors';
import * as appSelectors from '@app/selectors';
import studentDegreePlansJson from '../../../../testdata/studentDegreePlans.json';
import auditMetaDataJson from '../../../../testdata/auditMetaData.json';
import * as darsActions from '@dars/store/actions';
import { DarsActionTypes } from '@dars/store/actions';
import { success } from 'loadable.ts';

const INITIAL_DARS_STATE: DARSState = {
  activeTermCodes: [],
  hasLoaded: false,
  degreePrograms: [],
  metadata: { status: 'NotLoaded' },
  audits: {},
};

/**
 * An ActivateRoute test double with a `paramMap` observable.
 * Use the `setParamMap()` method to add the next `paramMap` value.
 * This is needed by the termCode tests, since the effect checks it to
 * see which term to return.
 */
@Injectable()
class ActivatedRouteStub {
  // Use a ReplaySubject to share previous values with subscribers
  // and pump new values into the `paramMap` observable
  private subject = new ReplaySubject<ParamMap>();
  private subject2 = new ReplaySubject<ParamMap>();

  /** The mock paramMap observable */
  readonly paramMap = this.subject.asObservable();
  /** The mock paramMap observable */
  readonly queryParams = this.subject2.asObservable();

  /** Set the paramMap observables's next value */
  setParamMap(params?: Params) {
    if (params) {
      this.subject.next(convertToParamMap(params));
    } else {
      this.subject.next();
    }
  }
  setQueryMap(params?: Params) {
    if (params) {
      let paramMapValue = convertToParamMap(params);
      this.subject2.next((paramMapValue as any)['params']);
    } else {
      this.subject2.next();
    }
  }
}

class MatSnackBarMock {
  open(_message: string, _action?: string, _config?: any) {
    return this;
  }
  afterDismissed() {
    return of();
  }
  onAction() {
    return of();
  }
}

describe('Effects', () => {
  let actions$: Observable<any>;
  let effects: DARSEffects;
  let store: MockStore<State>;
  let apiService: ApiService;
  let activatedRoute: any = new ActivatedRouteStub();
  let snackBar: MatSnackBar;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [
        DARSEffects,
        provideMockActions(() => actions$),
        provideMockStore({ initialState: { dars: INITIAL_DARS_STATE } }),
        { provide: ApiService, useClass: ApiService },
        { provide: ActivatedRoute, useValue: activatedRoute },
        { provide: MatSnackBar, useClass: MatSnackBarMock },
      ],
    }).compileComponents();
    apiService = TestBed.inject(ApiService);
    effects = TestBed.inject(DARSEffects);
    store = TestBed.inject(MockStore);
    activatedRoute = TestBed.inject(ActivatedRoute);

    activatedRoute.setQueryMap({ term: '1222' });
    snackBar = TestBed.inject(MatSnackBar);
  });
  describe('Effects.load$', () => {
    it('should fire if audit store is not loaded', done => {
      // set up the mock endpoints
      const getAuditsSpy = spyOn(apiService, 'getAuditMetadata').and.callFake(
        () => {
          return of(auditMetaDataJson as any);
        },
      );
      store.overrideSelector(
        appSelectors.terms.getActiveTermCodes,
        success([TermCode.decodeOrThrow('1222')]),
      );
      const getStudentDegreeProgramsSpy = spyOn(
        apiService,
        'getStudentDegreePrograms',
      ).and.callFake(() => {
        return of(studentDegreePlansJson as any);
      });
      store.overrideSelector(selectors.getDARSState, {
        ...INITIAL_DARS_STATE,
        hasLoaded: false,
      });

      // trigger the action
      actions$ = of(new darsActions.StartLoadingDARSView());
      let wasLoadCalled = false;
      // wait for the effect to fire
      effects.load$.subscribe(
        res => {
          // was the studentInfo endpoint called?
          expect(getAuditsSpy).toHaveBeenCalledTimes(1);
          expect(getStudentDegreeProgramsSpy).toHaveBeenCalledTimes(1);
          expect(store).toBeTruthy();
          expect(snackBar).toBeTruthy();
          expect(res.type).toEqual(DarsActionTypes.DoneLoadingDARSView);
          wasLoadCalled = true;
          done();
        },
        () => {
          done();
        },
        () => {
          expect(getAuditsSpy).toHaveBeenCalledTimes(1);
          expect(getStudentDegreeProgramsSpy).toHaveBeenCalledTimes(1);
          expect(wasLoadCalled).toBeTruthy();
          done();
        },
      );
    });
    it('should handle empty dars data', done => {
      // set up the mock endpoints
      const getAuditsSpy = spyOn(apiService, 'getAuditMetadata').and.callFake(
        () => {
          return of([] as any);
        },
      );
      store.overrideSelector(
        appSelectors.terms.getActiveTermCodes,
        success([TermCode.decodeOrThrow('1222')]),
      );
      const getStudentDegreeProgramsSpy = spyOn(
        apiService,
        'getStudentDegreePrograms',
      ).and.callFake(() => {
        return of([] as any);
      });
      //return 'not loaded' version of the store data
      store.overrideSelector(selectors.getDARSState, {
        ...INITIAL_DARS_STATE,
        hasLoaded: false,
      });

      // trigger the action
      actions$ = of(new darsActions.StartLoadingDARSView());
      let wasLoadCalled = false;
      // wait for the effect to fire
      effects.load$.subscribe(
        res => {
          // was the studentInfo endpoint called?
          expect(getAuditsSpy).toHaveBeenCalledTimes(1);
          expect(getStudentDegreeProgramsSpy).toHaveBeenCalledTimes(1);
          expect(store).toBeTruthy();
          expect(snackBar).toBeTruthy();
          expect(res.type).toEqual(DarsActionTypes.DoneLoadingDARSView);
          wasLoadCalled = true;
          done();
        },
        err => {
          fail(err);
        },
        () => {
          expect(getAuditsSpy).toHaveBeenCalledTimes(1);
          expect(getStudentDegreeProgramsSpy).toHaveBeenCalledTimes(1);
          expect(wasLoadCalled).toBeTruthy();
          done();
        },
      );
    });
    it('should not fire if audit store is loaded', done => {
      // set up the mock endpoints
      const getAuditsSpy = spyOn(apiService, 'getAuditMetadata').and.callFake(
        () => {
          return of(auditMetaDataJson as any);
        },
      );
      store.overrideSelector(
        appSelectors.terms.getActiveTermCodes,
        success([TermCode.decodeOrThrow('1222')]),
      );
      const getStudentDegreeProgramsSpy = spyOn(
        apiService,
        'getStudentDegreePrograms',
      ).and.callFake(() => {
        return of(studentDegreePlansJson as any);
      });
      //return 'loaded' version of the store data
      store.overrideSelector(selectors.getDARSState, {
        ...INITIAL_DARS_STATE,
        hasLoaded: true,
      });

      // trigger the action
      actions$ = of(new darsActions.StartLoadingDARSView());
      let wasLoadCalled = false;
      // wait for the effect to fire
      effects.load$.subscribe(
        res => {
          // was the studentInfo endpoint called?
          expect(getAuditsSpy).toHaveBeenCalledTimes(0);
          expect(getStudentDegreeProgramsSpy).toHaveBeenCalledTimes(0);
          expect(store).toBeTruthy();
          expect(snackBar).toBeTruthy();
          expect(res).toBeTruthy();
          expect(res.type).toEqual(DarsActionTypes.DoneLoadingDARSView);

          wasLoadCalled = true;
          done();
        },
        err => {
          fail(err);
        },
        () => {
          expect(wasLoadCalled).toBeTruthy();

          done();
        },
      );
    });
    it('should handle errors fetching data for the audit store', done => {
      // set up the mock endpoints
      const getAuditsSpy = spyOn(apiService, 'getAuditMetadata').and.callFake(
        () => {
          return of(auditMetaDataJson as any);
        },
      );
      store.overrideSelector(
        appSelectors.terms.getActiveTermCodes,
        success([TermCode.decodeOrThrow('1222')]),
      );
      const getStudentDegreeProgramsSpy = spyOn(
        apiService,
        'getStudentDegreePrograms',
      ).and.callFake(() => {
        return throwError({
          errors: [
            {
              status: '400',
              title: 'Bad Request',
              detail:
                'A student record was not found for any career, degree or certificate program supported by DARS. An audit cannot be requested or processed. If you believe this is an error, contact the Office of the Registrar DARS team at dars@em.wisc.edu.',
            },
          ],
        });
      });
      //return 'not loaded' version of the store data
      store.overrideSelector(selectors.getDARSState, {
        ...INITIAL_DARS_STATE,
        hasLoaded: false,
      });

      // trigger the action
      actions$ = of(new darsActions.StartLoadingDARSView());
      let wasLoadCalled = false;
      // wait for the effect to fire
      effects.load$.subscribe(
        res => {
          // was the studentInfo endpoint called?
          expect(getAuditsSpy).toHaveBeenCalledTimes(1);
          expect(getStudentDegreeProgramsSpy).toHaveBeenCalledTimes(1);
          expect(store).toBeTruthy();
          expect(snackBar).toBeTruthy();
          wasLoadCalled = true;
          expect(res.type).toEqual(DarsActionTypes.ErrorLoadingDARSView);
          // did the subscription deliver the data returned by the mock endpoint?
          done();
        },
        () => {
          done();
        },
        () => {
          expect(getAuditsSpy).toHaveBeenCalledTimes(1);
          expect(getStudentDegreeProgramsSpy).toHaveBeenCalledTimes(1);
          expect(wasLoadCalled).toBeTruthy();
          done();
        },
      );
    });
  });
  describe('Effects.refreshMetadata$', () => {
    it('should refresh metadata from API endpoint', done => {
      // set up the mock endpoints
      const getAuditsSpy = spyOn(apiService, 'getAuditMetadata').and.callFake(
        () => {
          return of(auditMetaDataJson as any);
        },
      );

      // trigger the action
      actions$ = of(new darsActions.RefreshMetadata());
      let wasLoadCalled = false;
      // wait for the effect to fire
      effects.refreshMetadata$.subscribe(
        res => {
          // was the studentInfo endpoint called?
          expect(getAuditsSpy).toHaveBeenCalledTimes(1);
          expect(res.type).toEqual(DarsActionTypes.DoneRefreshingMetadata);
          wasLoadCalled = true;
          done();
        },
        () => {
          done();
        },
        () => {
          expect(getAuditsSpy).toHaveBeenCalledTimes(1);
          expect(wasLoadCalled).toBeTruthy();
          done();
        },
      );
    });
    it('should refresh metadata from API endpoint and handle empty results', done => {
      // set up the mock endpoints
      const getAuditsSpy = spyOn(apiService, 'getAuditMetadata').and.callFake(
        () => {
          return of([] as any);
        },
      );

      // trigger the action
      actions$ = of(new darsActions.RefreshMetadata());
      let wasLoadCalled = false;
      // wait for the effect to fire
      effects.refreshMetadata$.subscribe(
        res => {
          // was the studentInfo endpoint called?
          expect(getAuditsSpy).toHaveBeenCalledTimes(1);
          expect(res.type).toEqual(DarsActionTypes.DoneRefreshingMetadata);
          wasLoadCalled = true;
          done();
        },
        () => {
          done();
        },
        () => {
          expect(getAuditsSpy).toHaveBeenCalledTimes(1);
          expect(wasLoadCalled).toBeTruthy();
          done();
        },
      );
    });
    it('should refresh metadata from API endpoint and handle errors', done => {
      // set up the mock endpoints
      const getAuditsSpy = spyOn(apiService, 'getAuditMetadata').and.callFake(
        () => {
          return throwError('ERROR received');
        },
      );

      // trigger the action
      actions$ = of(new darsActions.RefreshMetadata());
      let wasLoadCalled = false;
      // wait for the effect to fire
      effects.refreshMetadata$.subscribe(
        res => {
          // was the studentInfo endpoint called?
          expect(getAuditsSpy).toHaveBeenCalledTimes(1);
          expect(res.type).toEqual(DarsActionTypes.ErrorRefreshingMetadata);
          wasLoadCalled = true;
          done();
        },
        err => {
          fail(err);
        },
        () => {
          expect(wasLoadCalled).toBeTruthy();
          done();
        },
      );
    });
  });
  describe('Effects.getAudit$', () => {
    it('should fetch audit data from API endpoint', done => {
      // set up the mock endpoints
      const getAuditSpy = spyOn(apiService, 'getAudit').and.callFake(() => {
        return of({} as any);
      });

      store.overrideSelector(selectors.getDARSState, {
        ...INITIAL_DARS_STATE,
        hasLoaded: false,
        audits: [{ status: 'NotLoaded' }],
      });

      // trigger the action
      actions$ = of(
        new darsActions.StartLoadingAudit({
          darsDegreeAuditReportId: 12345,
        } as any),
      );
      let wasLoadCalled = false;
      // wait for the effect to fire
      effects.getAudit$.subscribe(
        res => {
          expect(getAuditSpy).toHaveBeenCalledTimes(1);
          expect(res.type).toEqual(DarsActionTypes.DoneLoadingAudit);
          wasLoadCalled = true;
          done();
        },
        err => {
          fail(err);
        },
        () => {
          expect(wasLoadCalled).toBeTruthy();
          done();
        },
      );
    });
    it('should handle error from API endpoint', done => {
      // set up the mock endpoints
      const getAuditSpy = spyOn(apiService, 'getAudit').and.callFake(() => {
        return throwError('ERROR');
      });

      store.overrideSelector(selectors.getDARSState, {
        ...INITIAL_DARS_STATE,
        hasLoaded: false,
        audits: [{ status: 'NotLoaded' }],
      });

      // trigger the action
      actions$ = of(
        new darsActions.StartLoadingAudit({
          darsDegreeAuditReportId: 12345,
        } as any),
      );
      let wasLoadCalled = false;
      // wait for the effect to fire
      effects.getAudit$.subscribe(
        res => {
          expect(getAuditSpy).toHaveBeenCalledTimes(1);
          expect(res.type).toEqual(DarsActionTypes.ErrorLoadingAudit);
          wasLoadCalled = true;
          done();
        },
        () => {
          done();
        },
        () => {
          expect(wasLoadCalled).toBeTruthy();
          done();
        },
      );
    });
  });
  describe('Effects.newAudit$', () => {
    it('should post audit request to API endpoint', done => {
      // set up the mock endpoints
      const newAuditSpy = spyOn(apiService, 'postAudit').and.callFake(() => {
        return of({} as any);
      });

      // trigger the action
      actions$ = of(
        new darsActions.StartSendingAudit({
          auditType: 'program',
          roadmapId: 1,
          darsInstitutionCode: '011',
          darsDegreeProgramCode: '123',
          degreePlannerPlanName: 'Default',
          whichEnrolledCoursesIncluded: 'none',
          darsHonorsOptionCode: '',
          fixedCredits: [],
        }),
      );
      let wasLoadCalled = false;
      // wait for the effect to fire
      effects.newAudit$.subscribe(
        res => {
          expect(newAuditSpy).toHaveBeenCalledTimes(1);
          expect(res.type).toEqual(DarsActionTypes.DoneSendingAudit);
          wasLoadCalled = true;
          done();
        },
        err => {
          fail(err);
        },
        () => {
          expect(wasLoadCalled).toBeTruthy();
          done();
        },
      );
    });
    it('should handle error posting audit request to API endpoint', done => {
      // set up the mock endpoints
      const newAuditSpy = spyOn(apiService, 'postAudit').and.callFake(() => {
        return throwError('ERROR');
      });
      const openSnackBarSpy = spyOn(snackBar, 'open').and.callThrough();

      // trigger the action
      actions$ = of(
        new darsActions.StartSendingAudit({
          auditType: 'program',
          roadmapId: 1,
          darsInstitutionCode: '011',
          darsDegreeProgramCode: '123',
          degreePlannerPlanName: 'Default',
          whichEnrolledCoursesIncluded: 'none',
          darsHonorsOptionCode: '',
          fixedCredits: [],
        }),
      );
      let wasLoadCalled = false;
      // wait for the effect to fire
      effects.newAudit$.subscribe(
        res => {
          expect(newAuditSpy).toHaveBeenCalledTimes(1);
          expect(openSnackBarSpy).toHaveBeenCalledTimes(1);
          expect(res.type).toEqual(DarsActionTypes.ErrorSendingAudit);
          wasLoadCalled = true;
          done();
        },
        err => {
          fail(err);
        },
        () => {
          expect(wasLoadCalled).toBeTruthy();
          done();
        },
      );
    });
  });
  describe('Effects.updateAudits$', () => {
    it('should propagate refresh metadata action upon completion of audit data request', done => {
      // trigger the action
      actions$ = of(
        new darsActions.DoneSendingAudit({
          auditType: 'program',
          darsJobId: '00012345',
        }),
      );
      let wasLoadCalled = false;
      // wait for the effect to fire
      effects.updateAudits$.subscribe(
        res => {
          expect(res.type).toEqual(DarsActionTypes.RefreshMetadata);
          wasLoadCalled = true;
          done();
        },
        err => {
          fail(err);
        },
        () => {
          expect(wasLoadCalled).toBeTruthy();
          done();
        },
      );
    });
  });
});
