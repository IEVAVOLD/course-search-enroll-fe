import { createSelector, createFeatureSelector } from '@ngrx/store';
import { DARSState } from './state';

export const getDARSState = createFeatureSelector<DARSState>('dars');

export const getActiveTermCodes = createSelector(
  getDARSState,
  (state: DARSState) => state.activeTermCodes,
);

export const metadataStatus = createSelector(
  getDARSState,
  (state: DARSState) => state.metadata.status,
);

export const metadataErrorMessage = createSelector(
  getDARSState,
  (state: DARSState) => {
    if (state.metadata.status === 'Error') {
      return state.metadata.message;
    } else {
      return '';
    }
  },
);

export const programMetadata = createSelector(
  getDARSState,
  (state: DARSState) => {
    if (state.metadata.status === 'Loaded') {
      return state.metadata.programMetadata;
    } else {
      return {};
    }
  },
);

export const outstandingAndPendingPrograms = createSelector(
  getDARSState,
  (state: DARSState) => {
    if (state.metadata.status === 'Loaded') {
      return {
        outstanding: state.metadata.outstanding.program,
        pending: state.metadata.pending.program,
      };
    } else {
      return {
        outstanding: 0,
        pending: 0,
      };
    }
  },
);

export const whatIfMetadata = createSelector(
  getDARSState,
  (state: DARSState) => {
    if (state.metadata.status === 'Loaded') {
      return state.metadata.whatIfMetadata;
    } else {
      return {};
    }
  },
);

export const outstandingAndPendingWhatIf = createSelector(
  getDARSState,
  (state: DARSState) => {
    if (state.metadata.status === 'Loaded') {
      return {
        outstanding: state.metadata.outstanding.whatIf,
        pending: state.metadata.pending.whatIf,
      };
    } else {
      return {
        outstanding: 0,
        pending: 0,
      };
    }
  },
);

export const getAudits = createSelector(
  getDARSState,
  (state: DARSState) => state.audits,
);

export const getAudit = createSelector(
  getDARSState,
  (state: DARSState, darsDegreeAuditReportId: number) =>
    state.audits[darsDegreeAuditReportId] || { status: 'NotLoaded' },
);

export const hasNoDegreePrograms = createSelector(
  getDARSState,
  (state: DARSState) => state.degreePrograms.length === 0,
);
