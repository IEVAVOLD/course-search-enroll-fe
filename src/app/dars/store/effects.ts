import { State } from '@app/dars/store/state';
import { Store } from '@ngrx/store';
import { forkJoinWithKeys, ifSuccessThenUnwrap } from '@app/core/utils';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { DarsActionTypes } from '@app/dars/store/actions';
// eslint-disable-next-line no-duplicate-imports
import * as darsActions from '@app/dars/store/actions';
import {
  mergeMap,
  map,
  catchError,
  withLatestFrom,
  tap,
  first,
} from 'rxjs/operators';
import { ApiService } from '@app/services/api.service';
import { of, forkJoin } from 'rxjs';
import * as selectors from '@app/dars/store/selectors';
import { groupAuditMetadata } from './utils';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';
import { AnalyticsService } from '@app/services/analytics.service';
import * as appSelectors from '@app/selectors';
import { TermCode } from '@app/types/terms';

const DEFAULT_METADATA_ERROR_MSG = 'Unknown error loading DARS audits';

function errorMessageFromUnknown(err: unknown): string {
  if (err instanceof HttpErrorResponse) {
    if (typeof err.error.message === 'string') {
      return err.error.message;
    } else {
      return `${err.error.errors[0].title}: ${err.error.errors[0].detail}`;
    }
  }

  switch (typeof err) {
    case 'object': {
      if (err === null) {
        return DEFAULT_METADATA_ERROR_MSG;
      } else {
        if (err.hasOwnProperty('message')) {
          return (err as any).message.toString();
        } else {
          return err.toString();
        }
      }
    }
    case 'string':
      return err;
    default:
      return DEFAULT_METADATA_ERROR_MSG;
  }
}

@Injectable()
export class DARSEffects {
  constructor(
    private actions$: Actions,
    private api: ApiService,
    private store$: Store<State>,
    private snackBar: MatSnackBar,
    private analytics: AnalyticsService,
  ) {}

  @Effect()
  load$ = this.actions$.pipe(
    ofType(DarsActionTypes.StartLoadingDARSView),
    withLatestFrom(this.store$.select(selectors.getDARSState)),
    mergeMap(([_, state]) => {
      if (state.hasLoaded) {
        return of(new darsActions.DoneLoadingDARSView(state));
      }

      return forkJoinWithKeys({
        degreePrograms: this.api.getStudentDegreePrograms(),
        metadata: this.api.getAuditMetadata(),
        activeTermCodes: this.store$
          .select(appSelectors.terms.getActiveTermCodes)
          .pipe(ifSuccessThenUnwrap(), first()),
      }).pipe(
        map(tuple => {
          return new darsActions.DoneLoadingDARSView({
            activeTermCodes: tuple.activeTermCodes,
            hasLoaded: true,
            degreePrograms: tuple.degreePrograms,
            metadata: {
              ...groupAuditMetadata(tuple.metadata, tuple.degreePrograms),
              status: 'Loaded',
              outstanding: { program: 0, whatIf: 0 },
              pending: { program: 0, whatIf: 0 },
            },
            audits: {},
          });
        }),
      );
    }),

    catchError((auditMetadataError: unknown) => {
      return of(
        new darsActions.ErrorLoadingDARSView({
          message: errorMessageFromUnknown(auditMetadataError),
        }),
      );
    }),
  );

  @Effect()
  refreshMetadata$ = this.actions$.pipe(
    ofType<darsActions.RefreshMetadata>(DarsActionTypes.RefreshMetadata),
    mergeMap(action =>
      this.api.getAuditMetadata().pipe(
        map(metadata => new darsActions.DoneRefreshingMetadata(metadata)),
        tap(() => action.payload && action.payload.callback()),
        catchError(() => {
          return of(
            new darsActions.ErrorRefreshingMetadata({
              message: 'Error loading metadata. Please try again',
            }),
          );
        }),
      ),
    ),
  );

  @Effect()
  getAudit$ = this.actions$.pipe(
    ofType<darsActions.StartLoadingAudit>(DarsActionTypes.StartLoadingAudit),
    withLatestFrom(this.store$.select(selectors.getAudits)),
    mergeMap(([action, audits]) => {
      const metadata = action.payload;
      const darsDegreeAuditReportId = metadata.darsDegreeAuditReportId;
      const info = audits[darsDegreeAuditReportId];

      if (info !== undefined && info.status === 'Loaded') {
        return of(
          new darsActions.DoneLoadingAudit({ metadata, audit: info.audit }),
        );
      } else {
        return this.api.getAudit(darsDegreeAuditReportId).pipe(
          map(audit => {
            return new darsActions.DoneLoadingAudit({ metadata, audit });
          }),
        );
      }
    }),
  );

  @Effect()
  newAudit$ = this.actions$.pipe(
    ofType(DarsActionTypes.StartSendingAudit),
    mergeMap((action: darsActions.StartSendingAudit) => {
      const submitAudit = () => {
        return this.api.postAudit({ ...action.payload }).pipe(
          tap(() =>
            this.analytics.event(
              'Audit',
              'generate-audit',
              action.payload.auditType,
            ),
          ),
          map(({ darsJobId }) => {
            return new darsActions.DoneSendingAudit({
              auditType: action.payload.auditType,
              darsJobId,
            });
          }),
        );
      };

      const handleError = () => {
        this.snackBar.open('Unable to generate audit');
        return of(
          new darsActions.ErrorSendingAudit({
            auditType: action.payload.auditType,
            message: 'Unable to generate audit',
          }),
        );
      };

      const planId = action.payload.roadmapId;
      if (planId !== null && action.payload.fixedCredits.length > 0) {
        return forkJoin(
          action.payload.fixedCredits.map(fixed => {
            const plannedCourseId = fixed.recordId;
            const termCode = TermCode.decodeOrNull(fixed.termCode)!;
            const creditRange = { min: fixed.credits, max: fixed.credits };
            return this.api.putPlanCourse(
              planId,
              { plannedCourseId, creditRange },
              termCode,
            );
          }),
        ).pipe(mergeMap(submitAudit), catchError(handleError));
      } else {
        return submitAudit().pipe(catchError(handleError));
      }
    }),
  );

  @Effect()
  updateAudits$ = this.actions$.pipe(
    ofType(DarsActionTypes.DoneSendingAudit),
    map(() => new darsActions.RefreshMetadata()),
  );
}
