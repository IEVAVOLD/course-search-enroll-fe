import { DARSState } from '@app/dars/store/state';
import { Action } from '@ngrx/store';
import {
  AuditMetadata,
  AuditMetadataWithReportId,
} from '../models/audit-metadata';
import { Audit } from '../models/audit/audit';

export enum DarsActionTypes {
  ErrorLoadingDARSView = '[DARS] Error Loading DARS View',
  StartLoadingDARSView = '[DARS] Start Loading DARS View',
  DoneLoadingDARSView = '[DARS] Done Loading DARS View',

  RefreshMetadata = '[DARS] Refresh Metadata',
  DoneRefreshingMetadata = '[DARS] Done Refreshing Metadata',
  ErrorRefreshingMetadata = '[DARS] Error Refreshing Metadata',

  ErrorLoadingAudit = '[DARS] Error Loading Audit',
  StartLoadingAudit = '[DARS] Start Loading Audit',
  DoneLoadingAudit = '[DARS] Done Loading Audit',

  ErrorSendingAudit = '[DARS] Error Sending Audit',
  StartSendingAudit = '[DARS] Start Sending Audit',
  DoneSendingAudit = '[DARS] Done Sending Audit',
}

export class ErrorLoadingDARSView implements Action {
  public readonly type = DarsActionTypes.ErrorLoadingDARSView;
  constructor(public payload: { message: string }) {}
}

export class StartLoadingDARSView implements Action {
  public readonly type = DarsActionTypes.StartLoadingDARSView;
}

export class DoneLoadingDARSView implements Action {
  public readonly type = DarsActionTypes.DoneLoadingDARSView;
  constructor(public payload: DARSState) {}
}

export class RefreshMetadata implements Action {
  public readonly type = DarsActionTypes.RefreshMetadata;
  constructor(public payload?: { callback: () => void }) {}
}

export class DoneRefreshingMetadata implements Action {
  public readonly type = DarsActionTypes.DoneRefreshingMetadata;
  constructor(public payload: AuditMetadata[]) {}
}

export class ErrorRefreshingMetadata implements Action {
  public readonly type = DarsActionTypes.ErrorRefreshingMetadata;
  constructor(public payload: { message: string }) {}
}

export class ErrorLoadingAudit implements Action {
  public readonly type = DarsActionTypes.ErrorLoadingAudit;
  constructor(public payload: { message: string }) {}
}

export class StartLoadingAudit implements Action {
  public readonly type = DarsActionTypes.StartLoadingAudit;
  constructor(public payload: AuditMetadataWithReportId) {}
}

export class DoneLoadingAudit implements Action {
  public readonly type = DarsActionTypes.DoneLoadingAudit;
  constructor(
    public payload: { metadata: AuditMetadataWithReportId; audit: Audit },
  ) {}
}

export class ErrorSendingAudit implements Action {
  public readonly type = DarsActionTypes.ErrorSendingAudit;
  constructor(
    public payload: { auditType: 'program' | 'whatIf'; message: string },
  ) {}
}

export class StartSendingAudit implements Action {
  public readonly type = DarsActionTypes.StartSendingAudit;
  constructor(
    public payload: {
      auditType: 'program' | 'whatIf';
      roadmapId: number | null;
      darsInstitutionCode: string;
      darsDegreeProgramCode: string;
      darsHonorsOptionCode: string;
      degreePlannerPlanName?: string;
      whichEnrolledCoursesIncluded?: string;
      fixedCredits: {
        termCode: string;
        subjectCode: string;
        recordId: number;
        credits: number;
      }[];
    },
  ) {}
}

export class DoneSendingAudit implements Action {
  public readonly type = DarsActionTypes.DoneSendingAudit;
  constructor(
    public payload: { auditType: 'program' | 'whatIf'; darsJobId: string },
  ) {}
}
