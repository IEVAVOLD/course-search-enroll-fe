import { GlobalState } from '@app/state';
import { AuditMetadata, AuditMetadataMap } from '../models/audit-metadata';
import { Audit } from '../models/audit/audit';
import { StudentDegreeProgram } from '../models/student-degree-program';
import { TermCode } from '@app/types/terms';

export type MetadataStatus =
  | { status: 'Error'; message: string }
  | { status: 'NotLoaded' }
  | { status: 'Loading' }
  | {
      status: 'Loaded';
      outstanding: { program: number; whatIf: number };
      pending: { program: number; whatIf: number };
      programMetadata: AuditMetadataMap;
      whatIfMetadata: AuditMetadataMap;
    };

export type AuditStatus =
  | { status: 'Error'; message: string }
  | { status: 'NotLoaded' }
  | { status: 'Loading'; metadata: AuditMetadata }
  | { status: 'Loaded'; metadata: AuditMetadata; audit: Audit };

export interface DARSState {
  activeTermCodes: TermCode[];
  hasLoaded: boolean;
  degreePrograms: StudentDegreeProgram[];
  metadata: MetadataStatus;
  audits: { [darsDegreeAuditReportId: number]: AuditStatus };
}

export interface State extends GlobalState {
  dars: DARSState;
}

export const INITIAL_DARS_STATE: DARSState = {
  activeTermCodes: [],
  hasLoaded: false,
  degreePrograms: [],
  metadata: { status: 'NotLoaded' },
  audits: {},
};
