import { INITIAL_DARS_STATE } from '@dars/store/state';
import * as darsActions from '@dars/store/actions';
import { darsReducer } from '@dars/store/reducer';

describe('DarsState', () => {
  describe('DoneLoadingDARSView', () => {
    it('should return the new state', () => {
      const action = new darsActions.DoneLoadingDARSView(INITIAL_DARS_STATE);
      const state = darsReducer(INITIAL_DARS_STATE, action);

      expect(state).toStrictEqual(INITIAL_DARS_STATE);
    });
  });
  describe('ErrorLoadingDARSView', () => {
    it('should return the failed state', () => {
      const action = new darsActions.ErrorLoadingDARSView({ message: 'ERROR' });
      const state = darsReducer(INITIAL_DARS_STATE, action);
      let errorState = { metadata: { status: 'Error', message: 'ERROR' } };
      let expected = {
        ...INITIAL_DARS_STATE,
        ...errorState,
      };
      expect(state).toStrictEqual(expected);
    });
  });
});
