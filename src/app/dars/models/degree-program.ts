import { HonorsOption } from './honors-option';

export interface DegreeProgram {
  darsDegreeProgramCode: string;
  darsInstitutionCode: string;
  darsInstitutionCodeDescription: string;
  darsDegreeProgramDescription: string;
}

export interface DegreePrograms {
  [darsInstitutionCode: string]: {
    darsInstitutionCode: string;
    darsInstitutionCodeDescription: string;
    programs: DegreeProgram[];
    honorsOptions: HonorsOption[];
  };
}
