import { AuditMetadata } from './audit-metadata';

export interface StudentDegreeProgram {
  darsDegreeProgramCode: string;
  darsInstitutionCode: string | null;
  darsCatalogYearTerm: string;
  darsAlternateCatalogYearTerm1: string;
  sisAcademicPlanCode: string;
  sisAcademicPlanDescription: string;
  sisAcademicPlanDeclareDate: string;
  sisAcademicSubPlanCode: string;
  sisAcademicSubPlanDescription: string;
  auditRequestBody: AuditMetadata;
  darsable: unknown;
  darsableMessage: unknown;
}
