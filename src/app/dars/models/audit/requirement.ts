export interface Requirement {
  requirementName: string;
  status: { status: string; description: string };
  requirementContents: RequirementContents[];
}

export type RequirementContents = ContentType | SubRequirementCourses;

export interface ContentType {
  contentType:
    | 'blankLine'
    | 'hText'
    | 'noRequirementNeedsLine'
    | 'noRequirementTitle'
    | 'noSubrequirementAcceptCourses'
    | 'noSubrequirementNeedsSummaryLine'
    | 'noSubrequirementRejectCourses'
    | 'noSubrequirementTLine'
    | 'okRequirementTitle'
    | 'okSubrequirementAcceptCourses'
    | 'okSubrequirementEarnedLine'
    | 'okSubrequirementNeedsSummaryLine'
    | 'okSubrequirementTLine';
  lines: string[];
}

export interface SubRequirementCourse {
  term: string;
  course: string;
  credits: string;
  grade: string;
  courseNote: null | string;
  courseTitle: string;
}

export interface SubRequirementCourses {
  contentType: 'okSubrequirementCourses' | 'noSubrequirementCourses';
  subRequirementCourses: SubRequirementCourse[];
}
