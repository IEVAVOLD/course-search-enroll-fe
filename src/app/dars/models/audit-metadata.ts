export interface AuditMetadata {
  darsJobId: string;
  darsAuditRunDate: string;
  sisEmplId: string;
  darsInstitutionCode: string;
  darsInstitutionCodeDescription: string;
  darsDegreeProgramCode: string;
  darsDegreeProgramDescription?: string;
  darsStatusOfDegreeAuditRequest: string;
  darsHonorsOptionCode: string;
  darsHonorsOptionDescription: string;
  darsDegreeAuditReportId: number | null;
  darsCatalogYearTerm: string;
  whichEnrolledCoursesIncluded: string;
  degreePlannerPlanName: string;
}

export type AuditMetadataWithReportId = AuditMetadata & {
  darsDegreeAuditReportId: number;
};

export interface AuditMetadataMap {
  [darsDegreeAuditReportId: number]: AuditMetadataWithReportId;
}
