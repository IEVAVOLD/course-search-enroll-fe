import { Pipe, PipeTransform } from '@angular/core';
import { AuditSymbol } from '../models/audit-symbols';
import { AuditSymbolsService } from '../services/audit-symbols.service';

@Pipe({ name: 'courseNote' })
export class CourseNotePipe implements PipeTransform {
  constructor(public symbols: AuditSymbolsService) {}

  public transform(notes: string | null): AuditSymbol[] {
    if (notes === null) {
      return [];
    }

    const symbols: AuditSymbol[] = this.symbols.getByTaxonomy('course');

    return symbols.reduce((acc, symbol) => {
      return notes.includes(symbol.text) ? [...acc, symbol] : acc;
    }, [] as AuditSymbol[]);
  }
}

@Pipe({ name: 'courseNoteUnknown' })
export class CourseNoteUnkownPipe implements PipeTransform {
  constructor(public symbols: AuditSymbolsService) {}

  public transform(notes: string | null): string {
    if (notes === null) {
      return '';
    }

    const symbols = this.symbols.getByTaxonomy('course');

    symbols.forEach(symbol => {
      if (notes === null) {
        return;
      }

      const { text } = symbol;
      if (notes.includes(text)) {
        const length = text.length;
        const index = notes.indexOf(text);
        notes = notes.substr(0, index) + notes.substr(index + length);
      }
    });

    return notes.trim();
  }
}
