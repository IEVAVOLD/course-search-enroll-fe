import { Pipe, PipeTransform } from '@angular/core';
import { AuditSymbol } from '../models/audit-symbols';
import { AuditSymbolsService } from '../services/audit-symbols.service';

@Pipe({ name: 'requirementSymbols' })
export class RequirementSymbolsPipe implements PipeTransform {
  constructor(public symbols: AuditSymbolsService) {}

  public transform(lines: string[]): AuditSymbol[] {
    const singleLine = lines.join(' ').trim();
    const matches = singleLine.match(
      /^((IP)|(IN-P)|(PL)|(R)|(<>)|\+|\-|\*)+(?![a-zA-Z])/g,
    );
    const symbols: AuditSymbol[] = this.symbols.getByTaxonomy('subrequirement');

    if (matches && matches.length > 0) {
      const symbolString = singleLine.substr(0, matches[0].length);
      return symbols.reduce((acc, symbol) => {
        return symbolString.includes(symbol.text) ? [...acc, symbol] : acc;
      }, [] as AuditSymbol[]);
    }

    return [];
  }
}
