import { StartSendingAudit } from './../store/actions';
import { MediaMatcher } from '@angular/cdk/layout';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuditMetadata, AuditMetadataMap } from '../models/audit-metadata';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DARSState } from '../store/state';
import { Store } from '@ngrx/store';
import { State } from '@app/dars/store/state';
import * as selectors from '../store/selectors';
import { Observable, Subscription } from 'rxjs';
import * as darsActions from '../store/actions';
import {
  NewDegreeAuditDialogComponent,
  NewDegreeAuditFields,
} from '../new-degree-audit-dialog/new-degree-audit-dialog.component';
import {
  NewWhatIfAuditDialogComponent,
  NewWhatIfAuditFields,
} from '../new-what-if-audit-dialog/new-what-if-audit-dialog.component';
import { shareReplay, distinctUntilChanged, map } from 'rxjs/operators';
import { WebsocketService } from '@app/services/websocket.service';
import { DialogService } from '@app/shared/services/dialog.service';
import { negate } from '@app/core/utils';

const flattenAndSortMetadataMap = (metadataMap: AuditMetadataMap) => {
  return (Object.values(metadataMap) as AuditMetadata[]).sort((a, b) => {
    if (a.darsAuditRunDate > b.darsAuditRunDate) {
      return 1;
    } else if (a.darsAuditRunDate < b.darsAuditRunDate) {
      return -1;
    } else {
      return 0;
    }
  });
};

@Component({
  selector: 'cse-dars-view',
  templateUrl: './dars-view.component.html',
  styleUrls: ['./dars-view.component.scss'],
})
export class DARSViewComponent implements OnInit, OnDestroy {
  public metadataStatus$!: Observable<DARSState['metadata']['status']>;
  public displayMetadataLoader$!: Observable<boolean>;
  public displayMetadataError$!: Observable<boolean>;
  public auditMetadataErrorMessage$!: Observable<string>;
  public programWaiting$!: Observable<{ outstanding: number; pending: number }>;
  public programMetadata$!: Observable<AuditMetadata[]>;
  public whatIfWaiting$!: Observable<{ outstanding: number; pending: number }>;
  public whatIfMetadata$!: Observable<AuditMetadata[]>;
  public mobileBreakpoint: MediaQueryList;
  public messageSub: Subscription;

  public hasSomeDegreePrograms$ = this.store
    .select(selectors.hasNoDegreePrograms)
    .pipe(negate);

  constructor(
    private store: Store<State>,
    private dialog2: DialogService,
    public mediaMatcher: MediaMatcher,
    websocketService: WebsocketService,
    private snackBar: MatSnackBar,
  ) {
    this.mobileBreakpoint = mediaMatcher.matchMedia('(max-width: 959px)');
    this.messageSub = websocketService
      .listenByType('auditStatus')
      .subscribe(() => {
        this.store.dispatch(
          new darsActions.RefreshMetadata({
            callback: () => this.snackBar.open('Audit Complete'),
          }),
        );
      });
  }

  public ngOnInit() {
    this.store.dispatch(new darsActions.StartLoadingDARSView());
    this.metadataStatus$ = this.store.select(selectors.metadataStatus);
    this.displayMetadataLoader$ = this.metadataStatus$.pipe(
      map(status => status === 'Loading' || status === 'NotLoaded'),
    );
    this.displayMetadataError$ = this.metadataStatus$.pipe(
      map(status => status === 'Error'),
    );
    this.auditMetadataErrorMessage$ = this.store.select(
      selectors.metadataErrorMessage,
    );
    this.programWaiting$ = this.store.select(
      selectors.outstandingAndPendingPrograms,
    );
    this.programMetadata$ = this.store
      .select(selectors.programMetadata)
      .pipe(
        distinctUntilChanged(),
        map(flattenAndSortMetadataMap),
        shareReplay(),
      );
    this.whatIfWaiting$ = this.store.select(
      selectors.outstandingAndPendingWhatIf,
    );
    this.whatIfMetadata$ = this.store
      .select(selectors.whatIfMetadata)
      .pipe(
        distinctUntilChanged(),
        map(flattenAndSortMetadataMap),
        shareReplay(),
      );
  }

  public ngOnDestroy() {
    this.messageSub.unsubscribe();
  }

  public async openDegreeAuditDialog() {
    const event = await this.dialog2.large<NewDegreeAuditFields>(
      NewDegreeAuditDialogComponent,
    );

    if (event) {
      return this.store.dispatch(
        new StartSendingAudit({
          auditType: 'program',
          roadmapId: event.roadmapId,
          darsInstitutionCode: event.darsInstitutionCode,
          darsDegreeProgramCode: event.darsDegreeProgramCode,
          degreePlannerPlanName: event.degreePlannerPlanName,
          whichEnrolledCoursesIncluded: event.whichEnrolledCoursesIncluded,
          darsHonorsOptionCode: event.darsHonorsOptionCode,
          fixedCredits: event.fixedCredits,
        }),
      );
    }
  }

  public async openWhatIfAuditDialog() {
    const event = await this.dialog2.large<NewWhatIfAuditFields>(
      NewWhatIfAuditDialogComponent,
    );

    if (event) {
      return this.store.dispatch(
        new StartSendingAudit({
          auditType: 'whatIf',
          roadmapId: event.roadmapId,
          darsInstitutionCode: event.darsInstitutionCode,
          darsDegreeProgramCode: event.darsDegreeProgramCode,
          degreePlannerPlanName: event.degreePlannerPlanName,
          whichEnrolledCoursesIncluded: event.whichEnrolledCoursesIncluded,
          darsHonorsOptionCode: event.darsHonorsOptionCode,
          fixedCredits: event.fixedCredits,
        }),
      );
    }
  }
}
