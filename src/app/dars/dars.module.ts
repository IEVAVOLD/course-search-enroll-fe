import { NgModule } from '@angular/core';
import { DARSViewComponent } from './dars-view/dars-view.component';
import { EffectsModule } from '@ngrx/effects';
import { SharedModule } from '@app/shared/shared.module';
import { DARSEffects } from './store/effects';
import { DarsAuditComponent } from './audit/audit.component';
import { AuditLegendComponent } from './audit-legend/audit-legend.component';
import { DarsMetadataTableComponent } from './metadata-table/metadata-table.component';
import { MetadataMobileViewComponent } from './metadata-mobile-view/metadata-mobile-view.component';
import { MatStepperModule } from '@angular/material/stepper';
import { StoreModule } from '@ngrx/store';
import { darsReducer } from './store/reducer';
import { RequirementTitlePipe } from './pipes/requirement-title.pipe';
import { RequirementBodyPipe } from './pipes/requirement-body.pipe';
import { RequirementLinePipe } from './pipes/requirement-line.pipe';
import { RequirementSymbolsPipe } from './pipes/requirement-symbols.pipe';
import { SchoolOrCollegePipe } from './pipes/school-college.pipe';
import { AuditNamePipe } from './pipes/name-format.pipe';
import { CourseNotePipe, CourseNoteUnkownPipe } from './pipes/course-note.pipe';
import { CourseGradePipe } from './pipes/course-grade.pipe';
import { NewDegreeAuditDialogComponent } from './new-degree-audit-dialog/new-degree-audit-dialog.component';
import { NewWhatIfAuditDialogComponent } from './new-what-if-audit-dialog/new-what-if-audit-dialog.component';
import { AuditViewComponent } from './dars-audit-view/dars-audit-view.component';
import { AuditSymbolComponent } from './audit-symbol/audit-symbol.component';
import { SortMetadataPipe } from './pipes/sort-metadata.pipe';
import { DarsRoutingModule } from './dars-routing.module';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';

@NgModule({
  imports: [
    StoreModule.forFeature('dars', darsReducer),
    EffectsModule.forFeature([DARSEffects]),
    SharedModule,
    MatStepperModule,
    DarsRoutingModule,
    MatPaginatorModule,
    MatTableModule,
    FlexLayoutModule,
    MatCardModule,
    MatTabsModule,
  ],
  exports: [MatStepperModule],
  declarations: [
    SortMetadataPipe,
    RequirementTitlePipe,
    RequirementBodyPipe,
    RequirementLinePipe,
    RequirementSymbolsPipe,
    SchoolOrCollegePipe,
    AuditNamePipe,
    CourseNotePipe,
    CourseNoteUnkownPipe,
    CourseGradePipe,
    NewDegreeAuditDialogComponent,
    NewWhatIfAuditDialogComponent,
    DARSViewComponent,
    AuditViewComponent,
    DarsAuditComponent,
    AuditLegendComponent,
    DarsMetadataTableComponent,
    MetadataMobileViewComponent,
    AuditSymbolComponent,
  ],
})
export class DARSModule {}
