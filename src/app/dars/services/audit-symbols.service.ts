import { Injectable } from '@angular/core';
import { AuditSymbol, AuditTextSymbol } from '../models/audit-symbols';

@Injectable({
  providedIn: 'root',
})
export class AuditSymbolsService {
  // prettier-ignore
  private symbols: AuditSymbol[] = [
    { type: 'text', text: '>D', tooltip: 'Duplicate course - retains GPA effect', taxonomy: 'course' },
    { type: 'text', text: '>R', tooltip: 'Repeatable course', taxonomy: 'course' },
    { type: 'text', text: '>S', tooltip: 'Credit split between requirements', taxonomy: 'course' },
    { type: 'text', text: '(R)', tooltip: 'Required course', taxonomy: 'course' },
    { type: 'text', text: '(X)', tooltip: 'Original course value', taxonomy: 'course' },
    { type: 'text', text: 'EI', tooltip: 'Extended incomplete', taxonomy: 'grade' },
    { type: 'text', text: 'CR', tooltip: 'Credit (credit/no credit courses)', taxonomy: 'grade' },
    { type: 'text', text: 'HS', tooltip: 'High school unit', taxonomy: 'grade' },
    { type: 'text', text: 'IN', tooltip: 'Incomplete (credit/no credit courses)', taxonomy: 'grade' },
    { type: 'text', text: 'INP', tooltip: 'In-progress course (current term)', taxonomy: 'grade' },
    { type: 'text', text: 'I', tooltip: 'Incomplete', taxonomy: 'grade' },
    { type: 'text', text: 'N', tooltip: 'No credit (credit/no credit courses)', taxonomy: 'grade' },
    { type: 'text', text: 'NR', tooltip: 'Not reported', taxonomy: 'grade' },
    { type: 'text', text: 'NW', tooltip: 'No work', taxonomy: 'grade' },
    { type: 'text', text: 'PL', tooltip: 'Planned course', taxonomy: 'grade' },
    { type: 'text', text: 'P', tooltip: 'Progress', taxonomy: 'grade' },
    { type: 'text', text: 'TI', tooltip: 'Transfer (in progress)', taxonomy: 'grade' },
    { type: 'text', text: 'PS', tooltip: 'Mock/pseudo course', taxonomy: 'grade' },
    { type: 'text', text: 'Q', tooltip: 'Question on credits or honors', taxonomy: 'grade' },
    { type: 'text', text: 'S', tooltip: 'Satisfactory (pass/fail and audit courses)', taxonomy: 'grade' },
    { type: 'text', text: 'SD', tooltip: 'Satisfactory-Disruption', taxonomy: 'grade' },
    { type: 'text', text: 'T', tooltip: 'Transfer/test/advanced standing course', taxonomy: 'grade' },
    { type: 'text', text: 'U', tooltip: 'Unsatisfactory (pass/fail courses)', taxonomy: 'grade' },
    { type: 'text', text: 'UD', tooltip: 'Unsatisfactory-Disruption', taxonomy: 'grade' },
    { type: 'icon', text: 'OK', icon: 'check_circle', tooltip: 'Requirement complete', color: 'green', taxonomy: 'requirement' },
    { type: 'icon', text: 'NO', icon: 'cancel', tooltip: 'Requirement not complete', color: 'red', taxonomy: 'requirement' },
    { type: 'icon', text: 'PL', icon: 'planned', tooltip: 'Requirement uses planned course', color: 'blue', taxonomy: 'requirement' },
    { type: 'text', text: 'IP', tooltip: 'Requirement uses in-progress credit/courses', taxonomy: 'requirement' },
    { type: 'text', text: 'IN-P', tooltip: 'Sub-requirement uses in progress credit/courses', taxonomy: 'subrequirement' },
    { type: 'text', text: 'R', tooltip: 'Required sub-requirement (mandatory)', taxonomy: 'subrequirement' },
    { type: 'text', text: '<>', tooltip: 'Optional/other requirement in OR\'d set complete', taxonomy: 'subrequirement' },
    { type: 'icon', text: '+', icon: 'check', tooltip: 'Sub-requirement complete', color: 'green', taxonomy: 'subrequirement' },
    { type: 'icon', text: '-', icon: 'close', tooltip: 'Sub-requirement not complete', color: 'red', taxonomy: 'subrequirement' },
    { type: 'text', text: 'PL', tooltip: 'Sub-requirement uses planned course', taxonomy: 'subrequirement' },
    { type: 'text', text: '*', tooltip: 'Optional sub-requirement, courses assigned', taxonomy: 'subrequirement' },
    { type: 'text', text: ' ', tooltip: 'Optional sub-requirement, no courses assigned', taxonomy: 'subrequirement' },
    { type: 'text', text: 'AC', tooltip: 'Course approved for requirement/sub-requirement', taxonomy: 'exception' },
    { type: 'text', text: 'IC', tooltip: 'Course inserted into requirement/sub-requirement', taxonomy: 'exception' },
    { type: 'text', text: 'EC', tooltip: 'Course exchanged for specified course', taxonomy: 'exception' },
    { type: 'text', text: 'FC', tooltip: 'Course forced into requirement/sub-requirement', taxonomy: 'exception' },
    { type: 'text', text: 'CM', tooltip: 'Course modified', taxonomy: 'exception' },
    { type: 'text', text: 'CY', tooltip: 'Catalog year modified', taxonomy: 'exception' },
    { type: 'text', text: 'DC', tooltip: 'Course deleted from requirement/sub-requirement', taxonomy: 'exception' },
    { type: 'text', text: 'RM', tooltip: 'Requirement modified', taxonomy: 'exception' },
    { type: 'text', text: 'WC', tooltip: 'Waive course', taxonomy: 'exception' },
    { type: 'text', text: 'WP', tooltip: 'Waive mock/pseudo course', taxonomy: 'exception' },
  ];

  public getAll(): AuditSymbol[] {
    return this.symbols;
  }

  public getByTaxonomy(tax: AuditTextSymbol['taxonomy']): AuditSymbol[] {
    return this.symbols.filter(s => s.taxonomy && s.taxonomy === tax);
  }

  public findByText(text: string): AuditSymbol | undefined {
    return this.symbols.find(s => s.text === text);
  }

  public findByTextAndTax(
    text: string,
    tax: AuditTextSymbol['taxonomy'],
  ): AuditSymbol | undefined {
    return this.symbols.find(s => s.text === text && s.taxonomy === tax);
  }
}
