import { ActionReducerMap, MetaReducer, createReducer, on } from '@ngrx/store';
import * as actions from './actions';
import { GlobalState, Prefs } from './state';
import {
  Loadable,
  monad,
  success,
  failed as didFail,
  LOADING,
  isSuccess,
  isLoading,
} from 'loadable.ts';
import { TermCode, Terms } from './types/terms';
import produce from 'immer';
import {
  CourseRef,
  HasCurrentId,
  HasTermCode,
  RoadmapCourseRef,
  SavedCourseRef,
} from './types/courses';
import { HasCourseRef } from './types/courses/attributes';

const EMPTY_TERM_COURSES: GlobalState['courses'][string] = {
  isValidating: false,
  hintToRevalidate: false,
  roadmap: null,
  current: null,
};

namespace student {
  export const found = on<
    GlobalState['student'],
    [typeof actions.student.found]
  >(actions.student.found, (_state, { info }) => {
    return success(info);
  });

  export const failed = on<
    GlobalState['student'],
    [typeof actions.student.failed]
  >(actions.student.failed, _state => {
    return didFail(null);
  });
}

namespace courses {
  export const load = on<GlobalState['courses'], [typeof actions.courses.load]>(
    actions.courses.load,
    (state, { termCode }) => {
      return produce(state, draft => {
        const key = TermCode.encode(termCode);

        if (!draft[key]) {
          draft[key] = { ...EMPTY_TERM_COURSES };
        }

        draft[key].isValidating = false;
        draft[key].roadmap = LOADING;
        draft[key].current = LOADING;
      });
    },
  );

  export const done = on<GlobalState['courses'], [typeof actions.courses.done]>(
    actions.courses.done,
    (state, { termCode, roadmap, current }) => {
      return produce(state, draft => {
        const key = TermCode.encode(termCode);

        if (!draft[key]) {
          draft[key] = { ...EMPTY_TERM_COURSES };
        }

        draft[key].isValidating = false;
        draft[key].roadmap = roadmap;
        draft[key].current = current;
      });
    },
  );

  export namespace roadmap {
    const findRoadmapCourse = (
      { termCode, ref }: HasTermCode & HasCourseRef<RoadmapCourseRef>,
      courses: GlobalState['courses'],
    ) => {
      const encoded = TermCode.encode(termCode);
      const roadmap = courses[encoded]?.roadmap;
      if (!roadmap || !isSuccess(roadmap)) {
        return null;
      }

      return roadmap.value.find(c => c.ref.roadmapId === ref.roadmapId) ?? null;
    };

    export const load = on<
      GlobalState['courses'],
      [typeof actions.courses.roadmap.load]
    >(actions.courses.roadmap.load, (state, { termCode }) => {
      return produce(state, draft => {
        const key = TermCode.encode(termCode);

        if (!draft[key]) {
          draft[key] = { ...EMPTY_TERM_COURSES };
        }

        draft[key].isValidating = false;
        draft[key].roadmap = LOADING;
      });
    });

    export const done = on<
      GlobalState['courses'],
      [typeof actions.courses.roadmap.done]
    >(actions.courses.roadmap.done, (state, { termCode, roadmap }) => {
      return produce(state, draft => {
        const key = TermCode.encode(termCode);

        if (!draft[key]) {
          draft[key] = { ...EMPTY_TERM_COURSES };
        }

        draft[key].isValidating = false;
        draft[key].roadmap = roadmap;
      });
    });

    export const modifyWithValidation = on<
      GlobalState['courses'],
      [
        typeof actions.courses.roadmap.addCourseWithoutPack,
        typeof actions.courses.roadmap.addCourseWithPack,
      ]
    >(
      actions.courses.roadmap.addCourseWithoutPack,
      actions.courses.roadmap.addCourseWithPack,
      (state, { course }) => {
        return produce(state, draft => {
          const key = TermCode.encode(course.termCode);

          if (!draft[key]) {
            draft[key] = { ...EMPTY_TERM_COURSES };
          }

          draft[key].isValidating = true;
          draft[key].hintToRevalidate = false;
          draft[key].roadmap = LOADING;
        });
      },
    );

    export const modify = on<
      GlobalState['courses'],
      [typeof actions.courses.roadmap.removePack]
    >(actions.courses.roadmap.removePack, (state, { course }) => {
      return produce(state, draft => {
        const key = TermCode.encode(course.termCode);

        if (!draft[key]) {
          draft[key] = { ...EMPTY_TERM_COURSES };
        }

        draft[key].roadmap = LOADING;
      });
    });

    export const removeCoursesAndPacks = on<
      GlobalState['courses'],
      [typeof actions.courses.roadmap.removeCoursesAndPacks]
    >(actions.courses.roadmap.removeCoursesAndPacks, (state, { courses }) => {
      if (courses.length < 1) {
        return state;
      }

      return produce(state, draft => {
        const key = TermCode.encode(courses[0].termCode);

        if (!draft[key]) {
          draft[key] = { ...EMPTY_TERM_COURSES };
        }

        draft[key].isValidating = true;
        draft[key].roadmap = LOADING;
      });
    });

    export const modifyOnBulk = on<
      GlobalState['courses'],
      [typeof actions.courses.roadmap.addCourseWithPackBulk]
    >(actions.courses.roadmap.addCourseWithPackBulk, (state, { courses }) => {
      if (courses.length < 1) {
        return state;
      }

      return produce(state, draft => {
        const key = TermCode.encode(courses[0].course.termCode);

        if (!draft[key]) {
          draft[key] = { ...EMPTY_TERM_COURSES };
        }

        draft[key].isValidating = true;
        draft[key].roadmap = LOADING;
      });
    });

    export const validate = on<
      GlobalState['courses'],
      [typeof actions.courses.roadmap.validate]
    >(actions.courses.roadmap.validate, (state, { termCode }) => {
      return produce(state, draft => {
        const key = TermCode.encode(termCode);

        if (!draft[key]) {
          draft[key] = { ...EMPTY_TERM_COURSES };
        }

        draft[key].hintToRevalidate = false;
        draft[key].isValidating = true;
        draft[key].roadmap = LOADING;
      });
    });

    export const validationFailed = on<
      GlobalState['courses'],
      [typeof actions.courses.roadmap.validationFailed]
    >(actions.courses.roadmap.validationFailed, (state, { termCode }) => {
      return produce(state, draft => {
        const key = TermCode.encode(termCode);
        if (draft[key]) {
          draft[key].isValidating = false;
        }
      });
    });

    export const hintToRevalidate = on<
      GlobalState['courses'],
      [typeof actions.courses.roadmap.hintToRevalidate]
    >(actions.courses.roadmap.hintToRevalidate, (state, { termCodes }) => {
      return produce(state, draft => {
        for (const termCode of termCodes) {
          const key = TermCode.encode(termCode);
          if (!draft[key]) {
            draft[key] = { ...EMPTY_TERM_COURSES };
          }

          draft[key].hintToRevalidate = true;
        }
      });
    });

    export namespace packs {
      export const load = on<
        GlobalState['courses'],
        [typeof actions.courses.roadmap.packs.load]
      >(actions.courses.roadmap.packs.load, (state, { course }) => {
        return produce(state, draft => {
          const found = findRoadmapCourse(course, draft);

          if (found) {
            if (found.package) {
              found.package.details = LOADING;
            }

            found.packages = LOADING;
          }
        });
      });

      export const done = on<
        GlobalState['courses'],
        [typeof actions.courses.roadmap.packs.done]
      >(
        actions.courses.roadmap.packs.done,
        (state, { course, pack, packs }) => {
          return produce(state, draft => {
            const found = findRoadmapCourse(course, draft);

            if (found) {
              if (found.package) {
                found.package.details = pack;
              }

              found.packages = packs;
            }
          });
        },
      );
    }
  }

  export namespace current {
    const findCurrentCOurse = (
      { termCode, currentId }: HasTermCode & HasCurrentId,
      courses: GlobalState['courses'],
    ) => {
      const encoded = TermCode.encode(termCode);
      const current = courses[encoded]?.current;
      if (!current || !isSuccess(current)) {
        return null;
      }

      return current.value.find(c => c.currentId === currentId) ?? null;
    };

    export const load = on<
      GlobalState['courses'],
      [typeof actions.courses.current.load, typeof actions.courses.current.drop]
    >(
      actions.courses.current.load,
      actions.courses.current.drop,
      (state, { termCode }) => {
        return produce(state, draft => {
          const key = TermCode.encode(termCode);

          if (!draft[key]) {
            draft[key] = { ...EMPTY_TERM_COURSES };
          }

          draft[key].current = LOADING;
        });
      },
    );

    export const done = on<
      GlobalState['courses'],
      [typeof actions.courses.current.done]
    >(actions.courses.current.done, (state, { termCode, current }) => {
      return produce(state, draft => {
        const key = TermCode.encode(termCode);

        if (!draft[key]) {
          draft[key] = { ...EMPTY_TERM_COURSES };
        }

        draft[key].current = current;
      });
    });

    export const enroll = on<
      GlobalState['courses'],
      [typeof actions.courses.current.enroll]
    >(actions.courses.current.enroll, (state, { termCode }) => {
      return produce(state, draft => {
        const key = TermCode.encode(termCode);

        draft[key] = { ...EMPTY_TERM_COURSES };
        draft[key].roadmap = LOADING;
        draft[key].current = LOADING;
      });
    });

    export const edit = on<
      GlobalState['courses'],
      [typeof actions.courses.current.edit]
    >(actions.courses.current.edit, (state, { course: { termCode } }) => {
      return produce(state, draft => {
        const key = TermCode.encode(termCode);

        if (draft[key]) {
          draft[key].current = LOADING;
        }
      });
    });

    export namespace pack {
      export const load = on<
        GlobalState['courses'],
        [typeof actions.courses.current.pack.load]
      >(actions.courses.current.pack.load, (state, { course }) => {
        return produce(state, draft => {
          const found = findCurrentCOurse(course, draft);

          if (found) {
            found.package.details = LOADING;
          }
        });
      });

      export const done = on<
        GlobalState['courses'],
        [typeof actions.courses.current.pack.done]
      >(actions.courses.current.pack.done, (state, { course, pack }) => {
        return produce(state, draft => {
          const found = findCurrentCOurse(course, draft);

          if (found) {
            found.package.details = pack;
          }
        });
      });
    }

    export namespace details {
      export const load = on<
        GlobalState['courses'],
        [typeof actions.details.load]
      >(actions.details.load, (state, { course }) => {
        return produce(state, draft => {
          if (course.ref.kind === 'current') {
            const found = findCurrentCOurse(
              {
                termCode: course.ref.termCode,
                currentId: course.ref.currentId,
              },
              draft,
            );

            if (found) {
              found.details = { mode: 'loading' };
            }
          }
        });
      });

      export const done = on<
        GlobalState['courses'],
        [typeof actions.details.done]
      >(actions.details.done, (state, { course, details }) => {
        return produce(state, draft => {
          if (course.ref.kind === 'current') {
            const found = findCurrentCOurse(
              {
                termCode: course.ref.termCode,
                currentId: course.ref.currentId,
              },
              draft,
            );

            if (found) {
              if (isSuccess(details)) {
                found.details = { mode: 'loaded', ...details.value };
              } else if (isLoading(details)) {
                found.details = { mode: 'loading' };
              } else {
                found.details = { mode: 'error' };
              }
            }
          }
        });
      });
    }
  }
}

namespace saved {
  export const load = on<
    GlobalState['saved'],
    [
      typeof actions.saved.load,
      typeof actions.saved.add,
      typeof actions.saved.remove,
    ]
  >(actions.saved.load, actions.saved.add, actions.saved.remove, () => {
    return LOADING;
  });

  export const done = on<GlobalState['saved'], [typeof actions.saved.done]>(
    actions.saved.done,
    (_, { saved }) => {
      return saved;
    },
  );

  export namespace details {
    const findSavedCourse = (
      ref: SavedCourseRef,
      saved: GlobalState['saved'],
    ) => {
      if (saved === null || !isSuccess(saved)) {
        return null;
      }

      return saved.value.find(s => CourseRef.equals(s.ref, ref)) ?? null;
    };

    export const load = on<GlobalState['saved'], [typeof actions.details.load]>(
      actions.details.load,
      (state, { course }) => {
        if (course.ref.kind === 'saved') {
          return produce(state, draft => {
            const found = findSavedCourse(course.ref as SavedCourseRef, draft);
            if (found) {
              found.details = { mode: 'loading' };
            }
          });
        }

        return state;
      },
    );

    export const done = on<GlobalState['saved'], [typeof actions.details.done]>(
      actions.details.done,
      (state, { course, details }) => {
        if (course.ref.kind === 'saved') {
          return produce(state, draft => {
            const found = findSavedCourse(course.ref as SavedCourseRef, draft);
            if (found) {
              if (isSuccess(details)) {
                found.details = { mode: 'loaded', ...details.value };
              } else if (isLoading(details)) {
                found.details = { mode: 'loading' };
              } else {
                found.details = { mode: 'error' };
              }
            }
          });
        }

        return state;
      },
    );
  }
}

export const reducers: ActionReducerMap<GlobalState> = {
  prefs: createReducer<Loadable<Prefs>>(
    LOADING,
    on(actions.prefs.found, (_state, { prefs }) => {
      return success(prefs);
    }),
    on(actions.prefs.failed, _state => {
      return didFail(null);
    }),
    on(actions.prefs.setKey, (state, { key, value }) => {
      return monad(state).map(prefs => {
        return { ...prefs, [key]: value };
      });
    }),
    on(actions.prefs.setNestedKey, (state, { pref, key, value }) => {
      return monad(state).map(prefs => {
        const old: unknown = prefs[pref];
        if (typeof old === 'object' && old !== null) {
          return { ...prefs, [pref]: { ...old, [key]: value } };
        } else {
          return { ...prefs, [pref]: { [key]: value } };
        }
      });
    }),
  ),
  currentTermCode: createReducer<Loadable<TermCode>>(
    LOADING,
    on(
      actions.currentTermCode.set,
      actions.terms.found,
      (_state, { currentTermCode }) => success(currentTermCode),
    ),
  ),
  terms: createReducer<Loadable<Terms>>(
    LOADING,
    on(actions.terms.found, (_state, { terms }) => {
      return success(terms);
    }),
    on(actions.terms.failed, _state => {
      return didFail(null);
    }),
  ),
  student: createReducer(LOADING, student.found, student.failed),
  courses: createReducer<GlobalState['courses']>(
    {},
    courses.load,
    courses.done,
    courses.roadmap.load,
    courses.roadmap.done,
    courses.roadmap.modifyWithValidation,
    courses.roadmap.modify,
    courses.roadmap.removeCoursesAndPacks,
    courses.roadmap.modifyOnBulk,
    courses.roadmap.validate,
    courses.roadmap.validationFailed,
    courses.roadmap.hintToRevalidate,
    courses.roadmap.packs.load,
    courses.roadmap.packs.done,
    courses.current.load,
    courses.current.done,
    courses.current.enroll,
    courses.current.edit,
    courses.current.pack.load,
    courses.current.pack.done,
    courses.current.details.load,
    courses.current.details.done,
  ),
  saved: createReducer(
    null,
    saved.load,
    saved.done,
    saved.details.load,
    saved.details.done,
  ),
};

export const metaReducers: MetaReducer<GlobalState>[] = [];

export const runtimeChecks = {
  strictStateImmutability: true,
  strictActionImmutability: true,
  strictStateSerializability: false,
  strictActionSerializability: false,
};
