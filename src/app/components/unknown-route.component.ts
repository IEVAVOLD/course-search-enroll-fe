import { Component } from '@angular/core';

@Component({
  selector: 'cse-unknown-route',
  template: `
    <h1>404 Error</h1>
    <cse-bucky-emote emote="shrug"></cse-bucky-emote>
    <p>The page you&rsquo;re looking for doesn&rsquo;t exist.</p>
  `,
  styles: [
    `
      :host {
        display: flex;
        flex-direction: column;
        align-items: center;
        text-align: center;
      }

      cse-bucky-emote {
        margin: 1rem auto;
      }
    `,
  ],
})
export class UnknownRouteComponent {}
