import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, Inject, OnInit } from '@angular/core';
import { ActivationStart, Router } from '@angular/router';
import { negate } from '@app/core/utils';
import { Breakpoints } from '@app/shared/breakpoints';
import { GlobalState } from '@app/state';
import { Store } from '@ngrx/store';
import { filter, map } from 'rxjs/operators';
import * as globalSelectors from '@app/selectors';
import { DialogService } from '@app/shared/services/dialog.service';
import { FeedbackDialogComponent } from '../feedback-dialog.component';
import { HelpDialogComponent } from '../help-dialog.component';
import { IS_PRIVATE } from '@app/services/is-private.token';
import { isSuccess } from 'loadable.ts';
import { DebugService } from '@app/shared/debug/debug.service';

const isActivationStart = filter((thing): thing is ActivationStart => {
  return thing instanceof ActivationStart;
});

@Component({
  selector: 'cse-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public isMobile$ = this.breakpoints
    .observe(Breakpoints.Mobile)
    .pipe(map(({ matches }) => matches));

  public isDesktop$ = this.isMobile$.pipe(negate);

  public name!: string;

  public letter$ = this.store.select(globalSelectors.student.getLetter).pipe(
    map(loadable => {
      if (isSuccess(loadable)) {
        return loadable.value;
      } else {
        return null;
      }
    }),
  );

  constructor(
    public debug: DebugService,
    private breakpoints: BreakpointObserver,
    private router: Router,
    private store: Store<GlobalState>,
    private dialog: DialogService,
    @Inject(IS_PRIVATE) public isPrivate: boolean,
  ) {}

  ngOnInit() {
    this.router.events
      .pipe(
        isActivationStart,
        map(event => event.snapshot.data?.name as unknown),
        map(name => {
          if (typeof name === 'string') {
            return name;
          } else {
            return '';
          }
        }),
      )
      .subscribe(name => (this.name = name));
  }

  onHelp() {
    this.dialog.small(HelpDialogComponent);
  }

  onFeedback() {
    this.dialog.small(FeedbackDialogComponent);
  }
}
