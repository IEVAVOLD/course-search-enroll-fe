import { Component } from '@angular/core';

@Component({
  selector: 'cse-feedback-dialog',
  template: `
    <cse-dialog headline="Feedback">
      <cse-dialog-body class="cse-typeset">
        <p>
          Please tell us what you think about the new all-in-one, integrated
          home for course search, scheduling, and enrollment.
        </p>
      </cse-dialog-body>
      <cse-dialog-actions>
        <button mat-button mat-dialog-close>Close</button>
        <div class="spacer"></div>
        <a
          href="https://uwmadison.co1.qualtrics.com/jfe/form/SV_cMEHiSdY5OeNFDT"
          target="_blank"
          mat-dialog-close
          mat-raised-button
          color="primary"
        >
          Share feedback
        </a>
      </cse-dialog-actions>
    </cse-dialog>
  `,
})
export class FeedbackDialogComponent {}
