import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {
  mergeMap,
  map,
  filter,
  withLatestFrom,
  switchMap,
  catchError,
  first,
  tap,
} from 'rxjs/operators';
import { pollWhileStatusIsOneOf, takeFirstSuccess } from '@app/core/operators';
import * as s from '@app/types/schema';
import { TermCode } from '@app/types/terms';
import { Store } from '@ngrx/store';
import * as globalSelectors from '@app/selectors';
import * as viewSelectors from '@scheduler/store/selectors';
import { notNull, ifSuccessThenUnwrap } from '@app/core/utils';
import {
  Course,
  CourseRef,
  CurrentCourse,
  RoadmapCourse,
} from '@app/types/courses';
import { of, Observable, forkJoin } from 'rxjs';
import * as actions from './actions';
import * as globalActions from '@app/actions';
import { GlobalState } from '@app/state';
import { ApiService } from '@app/services/api.service';
import { isSuccess } from 'loadable.ts';
import { RawCourseEntity, Schedule } from '@app/types/schedules';
import { Json } from '@app/types/json';
import {
  CachePayload,
  CachePayloadTable,
  SchedulerState,
  SchedulesError,
} from './state';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { MatSnackBar } from '@angular/material/snack-bar';

const STATUSES = ['New', 'InProgress'];

const CACHED_REQUEST_KEY = 'schedules';

@Injectable()
export class Effects {
  constructor(
    private actions$: Actions,
    private store: Store<GlobalState>,
    private api: ApiService,
    private announcer: LiveAnnouncer,
    private snackbar: MatSnackBar,
  ) {}

  private chosenTermCode: Observable<TermCode> = this.store
    .select(globalSelectors.currentTermCode.get)
    .pipe(ifSuccessThenUnwrap());

  @Effect()
  LifecycleInit$ = this.actions$.pipe(
    ofType(actions.lifecycle.init),
    mergeMap(() =>
      this.store.select(globalSelectors.currentTermCode.get).pipe(
        filter(isSuccess),
        first(),
        map(loadable => loadable.value),
      ),
    ),
    map(termCode => globalActions.courses.loadIfNotAlready({ termCode })),
  );

  @Effect()
  setTerm$ = this.actions$.pipe(
    ofType(globalActions.currentTermCode.set),
    map(({ currentTermCode }) =>
      globalActions.courses.loadIfNotAlready({ termCode: currentTermCode }),
    ),
  );

  @Effect()
  LoadFromCache$ = this.actions$.pipe(
    ofType(globalActions.currentTermCode.set, actions.lifecycle.init),
    pollWhileStatusIsOneOf(STATUSES, () => this.api.getSchedulerStatus()),
    withLatestFrom(
      this.chosenTermCode,
      this.store.select(globalSelectors.prefs.getKey, CACHED_REQUEST_KEY).pipe(
        map(value => {
          if (CachePayloadTable.schema.matches(value)) {
            return value;
          } else {
            return null;
          }
        }),
      ),
    ),
    mergeMap(([status, termCode, cache]) => {
      const cached = cache?.[TermCode.encode(termCode)] ?? null;

      if (s.object({ status: s.constant('Error') }).matches(status)) {
        const message = s.object({ reason: s.string }).matches(status)
          ? status.reason
          : null;
        const kind = determineErrorKindFromStatusReason(message);

        return of(actions.scheduler.error({ kind, message }));
      }

      return forkJoin([
        this.api.getScheduleResults(termCode),
        this.store
          .select(globalSelectors.courses.getRoadmapForTerm, termCode)
          .pipe(
            takeFirstSuccess(),
            catchError(_err => of(null)),
          ),
        this.store
          .select(globalSelectors.courses.getCurrentForTerm, termCode)
          .pipe(
            takeFirstSuccess(),
            catchError(_err => of(null)),
          ),
      ]).pipe(
        withLatestFrom(
          this.store
            .select(globalSelectors.terms.getNonZeroTerm, termCode)
            .pipe(ifSuccessThenUnwrap()),
        ),
        map(([[rawOrNull, roadmap, current], term]) => {
          if (roadmap === null || current === null) {
            return actions.scheduler.error({
              kind: 'network',
              payload: cached,
            });
          }

          const checkedCourses = (cached?.checkedCourses ?? []).filter(ref => {
            return (
              roadmap.some(c => CourseRef.equals(c.ref, ref)) ||
              current.some(c => {
                /**
                 * ROENROLL-2602
                 *
                 * When using the cached schedule data to populate the data
                 * structure that tracks which courses are checked, make sure
                 * to EXCLUDE dropped courses since isn't a way in the UI to
                 * uncheck those courses otherwise they'll remain in the
                 * schedule generation payload for the rest of the term.
                 */
                const hasMatchingRef = CourseRef.equals(c.ref, ref);
                const isNotDropped = c.enrolledStatus !== 'dropped';
                return hasMatchingRef && isNotDropped;
              })
            );
          });

          if (rawOrNull && cached !== null) {
            const raw = rawOrNull.feasibleSolutions.length
              ? rawOrNull.feasibleSolutions
              : [rawOrNull.bestSolution];

            const lockedClassNumbers = roadmap
              .map(course => course.package?.classNumber ?? null)
              .filter(notNull);

            const enrolledClassNumbers = current.flatMap(course => {
              if (course.enrolledStatus === 'dropped') {
                // If the course is dropped exclude its class numbers from the
                // set of classes used to generate schedules.
                return [];
              }

              const classNums = [course.package.classNumber];

              if (course.package.relatedClassNumber1 !== null) {
                classNums.push(course.package.relatedClassNumber1);
              }

              if (course.package.relatedClassNumber2 !== null) {
                classNums.push(course.package.relatedClassNumber2);
              }

              classNums.push(...course.package.autoEnrollClasses);

              return classNums;
            });

            const schedules = raw.map(raw =>
              Schedule.from(
                term,
                {
                  locked: lockedClassNumbers,
                  enrolled: enrolledClassNumbers,
                  toCourseRef: entity => {
                    const isSimilar = similarToEntity(entity);

                    if (entity.courseOrigin === 'ROADMAP') {
                      return roadmap.find(isSimilar)?.ref ?? null;
                    } else if (entity.courseOrigin === 'ENROLLED') {
                      return current.find(isSimilar)?.ref ?? null;
                    } else {
                      return null;
                    }
                  },
                },
                raw,
                !rawOrNull.feasibleSolutions.length,
              ),
            );

            const checkedPacks: SchedulerState['checkedPacks'] = {};
            Object.entries(cached.checkedPacks).forEach(
              ([roadmapId, checkedState]) => {
                const course = roadmap.find(c => c.ref.roadmapId === roadmapId);
                if (course && course.package === null) {
                  checkedPacks[roadmapId] = checkedState;
                }
              },
            );

            return actions.scheduler.done({
              request: cached.request,
              schedules,
              checkedCourses,
              checkedPacks,
            });
          }

          // There are no schedules. This could be because:
          // - The user has never generated schedules for this term
          // - The user DID generate schedules, but they have expired

          // The user's preferences can contain a map of term-codes to schedule
          // generation requests. Each term-code key maps to the most recent
          // schedule generation payload used in that term. If such a request
          // payload exists for the current term, it can be used to try and
          // regenerate the expired schedules.
          const error: SchedulesError =
            cached !== null
              ? { kind: 'expired', payload: { ...cached, checkedCourses } }
              : { kind: 'novel-term' };
          return actions.scheduler.error(error);
        }),
      );
    }),
  );

  @Effect()
  regenerate$ = this.actions$.pipe(
    ofType(actions.scheduler.regenerate),
    withLatestFrom(this.store.select(viewSelectors.getCurrentRequestOrNull)),
    map(([, requestOrNull]) => requestOrNull),
    filter(notNull),
    map(request => actions.scheduler.start({ request })),
  );

  @Effect()
  Generate$ = this.actions$.pipe(
    ofType(actions.scheduler.start),
    withLatestFrom(
      this.chosenTermCode,
      this.store.select(viewSelectors.getCheckedCourses),
      this.store.select(viewSelectors.getCheckedPacks),
    ),
    switchMap(([{ request }, termCode, checkedCourses, checkedPacks]) => {
      // The function to call for each poll cycle
      const pollFn = () => this.api.getSchedulerStatus();

      const errorStatus = s.object({
        status: s.constant('Error'),
        reason: s.nullable(s.string),
      });

      this.announcer.announce('Started generating schedules', 'assertive');

      return this.api.postSchedules(request).pipe(
        pollWhileStatusIsOneOf(STATUSES, pollFn),
        mergeMap(finalStatus => {
          if (errorStatus.matches(finalStatus)) {
            const message = finalStatus.reason;
            const kind = determineErrorKindFromStatusReason(message);

            this.announcer.announce(
              'Unable to generate schedules',
              'assertive',
            );

            return of(actions.scheduler.error({ kind, message }));
          }

          return forkJoin([
            this.api.getScheduleResults(termCode),
            this.store
              .select(globalSelectors.courses.getRoadmapForTerm, termCode)
              .pipe(
                takeFirstSuccess(),
                catchError(_err => of(null)),
              ),
            this.store
              .select(globalSelectors.courses.getCurrentForTerm, termCode)
              .pipe(
                takeFirstSuccess(),
                catchError(_err => of(null)),
              ),
          ]).pipe(
            withLatestFrom(
              this.store
                .select(globalSelectors.terms.getNonZeroTerm, termCode)
                .pipe(ifSuccessThenUnwrap()),
            ),
            map(([[rawOrNull, roadmap, current], term]) => {
              if (roadmap === null || current === null) {
                return actions.scheduler.error({
                  kind: 'network',
                  payload: null,
                });
              }

              if (rawOrNull) {
                const raw = rawOrNull.feasibleSolutions.length
                  ? rawOrNull.feasibleSolutions
                  : [rawOrNull.bestSolution];

                const roadmapClassNums = roadmap
                  .map(course => course.package?.classNumber ?? null)
                  .filter(notNull);

                const currentClassNums = current.flatMap(course => {
                  const classNums = [course.package.classNumber];

                  if (course.package.relatedClassNumber1 !== null) {
                    classNums.push(course.package.relatedClassNumber1);
                  }

                  if (course.package.relatedClassNumber2 !== null) {
                    classNums.push(course.package.relatedClassNumber2);
                  }

                  classNums.push(...course.package.autoEnrollClasses);

                  return classNums;
                });

                const schedules = raw.map(raw => {
                  return Schedule.from(
                    term,
                    {
                      locked: roadmapClassNums,
                      enrolled: currentClassNums,
                      toCourseRef: entity => {
                        const isSimilar = similarToEntity(entity);

                        if (entity.courseOrigin === 'ROADMAP') {
                          return roadmap.find(isSimilar)?.ref ?? null;
                        } else if (entity.courseOrigin === 'ENROLLED') {
                          return current.find(isSimilar)!.ref;
                        } else {
                          console.assert('NO COURSES MATCHED ENTITY');
                          return null;
                        }
                      },
                    },
                    raw,
                    !rawOrNull.feasibleSolutions.length,
                  );
                });

                this.announcer.announce(
                  'Successfully generated schedules',
                  'assertive',
                );

                return actions.scheduler.done({
                  request,
                  schedules,
                  checkedCourses,
                  checkedPacks,
                });
              }

              this.announcer.announce(
                'No schedules could be generated',
                'assertive',
              );

              return actions.scheduler.error({
                kind: 'other',
                message: 'No schedules could be generated',
              });
            }),
          );
        }),
      );
    }),
  );

  @Effect()
  UpdatePrefsAfterScheduleGeneration$ = this.actions$.pipe(
    ofType(actions.scheduler.start),
    withLatestFrom(
      this.store.select(viewSelectors.getCheckedCourses),
      this.store.select(viewSelectors.getCheckedPacks),
    ),
    map(([{ request }, checkedCourses, checkedPacks]) => {
      const value: CachePayload = { request, checkedCourses, checkedPacks };
      return globalActions.prefs.setNestedKey({
        pref: CACHED_REQUEST_KEY,
        key: request.termCode,
        value: value as any as Json,
      });
    }),
  );

  @Effect()
  ShowPackages$ = this.actions$.pipe(
    ofType(actions.packages.show),
    mergeMap(({ ref }) => {
      return this.store
        .select(globalSelectors.getCourseByRef, ref)
        .pipe(takeFirstSuccess());
    }),
    filter((course): course is RoadmapCourse | CurrentCourse => {
      return RoadmapCourse.is(course) || CurrentCourse.is(course);
    }),
    map(course => {
      if (RoadmapCourse.is(course)) {
        const hasLoadedPack =
          course.package === null
            ? true
            : course.package.details && isSuccess(course.package.details);

        const hasLoadedPacks = course.packages && isSuccess(course.packages);

        if (!hasLoadedPack || !hasLoadedPacks) {
          return globalActions.courses.roadmap.packs.load({
            course,
          });
        }
      } else if (CurrentCourse.is(course)) {
        if (!course.package.details || !isSuccess(course.package.details)) {
          return globalActions.courses.current.pack.load({ course });
        }
      }

      return null;
    }),
    filter(notNull),
  );

  @Effect()
  addBreak$ = this.actions$.pipe(
    ofType(actions.breaks.add),
    mergeMap(({ termCode, brk }) => this.api.postScheduleBreak(termCode, brk)),
    map(() => actions.scheduler.regenerate()),
    catchError(() => of(actions.scheduler.regenerate())),
  );

  @Effect()
  updateBreak$ = this.actions$.pipe(
    ofType(actions.breaks.update),
    mergeMap(({ termCode, brk }) => this.api.putScheduleBreak(termCode, brk)),
    map(() => actions.scheduler.regenerate()),
    catchError(() => of(actions.scheduler.regenerate())),
  );

  @Effect()
  removeBreak$ = this.actions$.pipe(
    ofType(actions.breaks.remove),
    mergeMap(({ id }) => this.api.deleteScheduleBreak(id)),
    map(() => actions.scheduler.regenerate()),
    catchError(() => of(actions.scheduler.regenerate())),
  );

  @Effect({ dispatch: false })
  markToast$ = this.actions$.pipe(
    ofType(actions.compare.mark),
    tap(({ index }) => {
      this.snackbar.open(`Schedule #${index + 1} was marked for comparison`);
    }),
  );

  @Effect({ dispatch: false })
  unmarkToast$ = this.actions$.pipe(
    ofType(actions.compare.unmark),
    tap(({ index }) => {
      this.snackbar.open(`Schedule #${index + 1} was unmarked for comparison`);
    }),
  );
}

const similarToEntity = (entity: RawCourseEntity) => {
  return (course: Course): boolean => {
    return (
      course.courseId === entity.courseId &&
      course.subject.code === entity.subjectCode
    );
  };
};

const determineErrorKindFromStatusReason = (reason: string | null) => {
  return reason === 'No Eligible Packages' ? 'eligibility' : 'other';
};
