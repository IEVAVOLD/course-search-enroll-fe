import { createReducer, on } from '@ngrx/store';
import { CourseRef } from '@app/types/courses';
import { produce } from 'immer';
import * as actions from './actions';
import * as globalActions from '@app/actions';
import { SchedulerState, EMPTY_STATE, SchedulesError } from './state';
import { failed, isSuccess, LOADING, success } from 'loadable.ts';
import { unique } from '@app/core/utils';

namespace lifecycle {
  export const init = on<SchedulerState, [typeof actions.lifecycle.init]>(
    actions.lifecycle.init,
    state => {
      return produce(state, _draft => {
        // empty: maybe remove?
      });
    },
  );

  export const setTerm = on<
    SchedulerState,
    [typeof globalActions.currentTermCode.set]
  >(globalActions.currentTermCode.set, state => {
    return produce(state, draft => {
      draft.schedules = LOADING;
      draft.courseInteraction = 'spinning';
      draft.showScheduleList = false;
    });
  });
}

const onSelectCourse = on<SchedulerState, [typeof actions.selectCourse]>(
  actions.selectCourse,
  (state, { ref, isChecked }): SchedulerState => {
    return produce(state, draft => {
      if (isChecked) {
        const isAlreadyChecked = state.checkedCourses.some(
          CourseRef.equalsCurry(ref),
        );
        if (!isAlreadyChecked) {
          draft.checkedCourses.push(ref);
        }
      } else {
        draft.checkedCourses = draft.checkedCourses.filter(
          r => !CourseRef.equals(ref, r),
        );
      }
    });
  },
);

namespace packages {
  export const show = on<SchedulerState, [typeof actions.packages.show]>(
    actions.packages.show,
    (state, { ref }) => {
      return produce(state, draft => {
        draft.viewing = ref;
      });
    },
  );

  export const hide = on<SchedulerState, [typeof actions.packages.hide]>(
    actions.packages.hide,
    state => {
      return produce(state, draft => {
        draft.viewing = null;
      });
    },
  );

  export const setCheckedState = on<
    SchedulerState,
    [typeof actions.packages.setCheckedState]
  >(
    actions.packages.setCheckedState,
    (state, { ref: { roadmapId }, checked }) => {
      return produce(state, draft => {
        draft.checkedPacks[roadmapId] = checked;
      });
    },
  );
}

namespace scheduler {
  export const start = on<SchedulerState, [typeof actions.scheduler.start]>(
    actions.scheduler.start,
    state => {
      return produce(state, draft => {
        draft.schedules = LOADING;
        draft.courseInteraction = 'disabled';
        draft.showScheduleList = false;
      });
    },
  );

  export const done = on<SchedulerState, [typeof actions.scheduler.done]>(
    actions.scheduler.done,
    (state, { request, schedules, checkedCourses, checkedPacks }) => {
      return produce(state, draft => {
        draft.schedules = success({
          request,
          index: 0,
          all: schedules,
          compare: [],
        });

        draft.checkedCourses = checkedCourses;
        draft.courseInteraction = 'enabled';
        draft.checkedPacks = checkedPacks; // TODO
      });
    },
  );

  export const error = on<SchedulerState, [typeof actions.scheduler.error]>(
    actions.scheduler.error,
    (state, err) => {
      return produce(state, draft => {
        draft.schedules = failed(err as SchedulesError);
        draft.courseInteraction = 'enabled';

        if (err.kind === 'expired') {
          draft.checkedCourses = err.payload.checkedCourses;
          draft.checkedPacks = err.payload.checkedPacks; // TODO
        }

        if (err.kind === 'network' && err.payload !== null) {
          draft.checkedCourses = err.payload.checkedCourses;
          draft.checkedPacks = err.payload.checkedPacks; // TODO
        }
      });
    },
  );

  export const next = on<SchedulerState, [typeof actions.scheduler.next]>(
    actions.scheduler.next,
    state => {
      return produce(state, draft => {
        if (isSuccess(draft.schedules)) {
          const total = draft.schedules.value.all.length;
          const oldIndex = draft.schedules.value.index;
          const newIndex = total > 0 ? (oldIndex + 1) % total : 0;
          draft.schedules.value.index = newIndex;
        }
      });
    },
  );

  export const prev = on<SchedulerState, [typeof actions.scheduler.prev]>(
    actions.scheduler.prev,
    state => {
      return produce(state, draft => {
        if (isSuccess(draft.schedules)) {
          const total = draft.schedules.value.all.length;
          const oldIndex = draft.schedules.value.index;
          // modulo is weird regarding negative numbers in JS
          const newIndex = total > 0 ? (oldIndex + total - 1) % total : 0;
          draft.schedules.value.index = newIndex;
        }
      });
    },
  );

  export const jump = on<SchedulerState, [typeof actions.scheduler.jump]>(
    actions.scheduler.jump,
    (state, { index }) => {
      return produce(state, draft => {
        if (isSuccess(draft.schedules)) {
          const total = draft.schedules.value.all.length;
          const newIndex = total > 0 ? index % total : 0;
          draft.schedules.value.index = newIndex;
        }
      });
    },
  );

  export const retry = on<SchedulerState, [typeof actions.scheduler.retry]>(
    actions.scheduler.retry,
    (state): SchedulerState => {
      const newState = { ...state };
      newState.schedules = LOADING;
      newState.showScheduleList = false;
      return newState;
    },
  );
}

namespace compare {
  export const mark = on<SchedulerState, [typeof actions.compare.mark]>(
    actions.compare.mark,
    (state, { index }): SchedulerState => {
      return produce(state, draft => {
        if (!isSuccess(draft.schedules)) {
          return;
        }

        const addIndex = [...draft.schedules.value.compare, index];
        const makeUnique = unique(addIndex);
        draft.schedules.value.compare = makeUnique;
      });
    },
  );

  export const unmark = on<SchedulerState, [typeof actions.compare.unmark]>(
    actions.compare.unmark,
    (state, { index }): SchedulerState => {
      return produce(state, draft => {
        if (!isSuccess(draft.schedules)) {
          return;
        }

        draft.schedules.value.compare = draft.schedules.value.compare.filter(
          marked => marked !== index,
        );
      });
    },
  );
}

namespace list {
  export const show = on<SchedulerState, [typeof actions.list.show]>(
    actions.list.show,
    (state): SchedulerState => {
      return produce(state, draft => {
        draft.showScheduleList = true;
      });
    },
  );

  export const hide = on<SchedulerState, [typeof actions.list.hide]>(
    actions.list.hide,
    (state): SchedulerState => {
      return produce(state, draft => {
        draft.showScheduleList = false;
      });
    },
  );
}

export const reducer = createReducer(
  EMPTY_STATE,

  lifecycle.init,
  lifecycle.setTerm,

  onSelectCourse,

  packages.show,
  packages.hide,
  packages.setCheckedState,

  scheduler.start,
  scheduler.done,
  scheduler.error,
  scheduler.next,
  scheduler.prev,
  scheduler.jump,
  scheduler.retry,

  compare.mark,
  compare.unmark,

  list.show,
  list.hide,
);
