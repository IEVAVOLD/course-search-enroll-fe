import {
  CourseRef,
  CurrentCourseRef,
  EnrollmentPackage,
  RoadmapCourseRef,
  RoadmapId,
} from '@app/types/courses';
import { Schedule, GenerateRequest } from '@app/types/schedules';
import { Loadable, LOADING } from 'loadable.ts';
import Schema, * as s from '@app/types/schema';

export type CourseInteraction =
  // Set when schedules are loading from the cache
  | 'spinning'

  // Set when schedule generation is in-progress
  | 'disabled'

  // Set when the schedules are done loading (either success or failure)
  | 'enabled';

interface SchedulesSuccess {
  request: GenerateRequest;
  index: number;
  all: Schedule[];
  compare: number[];
}

export type SchedulesError =
  | { kind: 'eligibility'; message: string | null }
  | { kind: 'novel-term' }
  | { kind: 'expired'; payload: CachePayload }
  | { kind: 'network'; payload: CachePayload | null }
  | { kind: 'other'; message: string | null };

export type CheckedPackages =
  | { kind: 'unchanged' }
  | { kind: 'changed'; docIds: Array<EnrollmentPackage['docId']> };

namespace CheckedPackages {
  export const schema: Schema<CheckedPackages> = s
    .or(s.object({ kind: s.constant('unchanged') }))
    .or(s.object({ kind: s.constant('changed'), docIds: s.array(s.string) }));
}

export interface SchedulerState {
  schedules: Loadable<SchedulesSuccess, SchedulesError>;
  viewing: RoadmapCourseRef | CurrentCourseRef | null;
  checkedCourses: CourseRef[];
  courseInteraction: CourseInteraction;
  checkedPacks: Record<RoadmapId, CheckedPackages>;
  showScheduleList: boolean;
}

export const EMPTY_STATE: SchedulerState = {
  schedules: LOADING,
  viewing: null,
  checkedCourses: [],
  courseInteraction: 'spinning',
  checkedPacks: {},
  showScheduleList: false,
};

export interface CachePayload {
  request: GenerateRequest;
  checkedCourses: SchedulerState['checkedCourses'];
  checkedPacks: SchedulerState['checkedPacks'];
}

export namespace CachePayload {
  export const schema = s.object<CachePayload>({
    request: GenerateRequest.schema,
    checkedCourses: s.array(CourseRef.schema),
    checkedPacks: s.record(CheckedPackages.schema),
  });
}

export interface CachePayloadTable {
  [termCode: string]: CachePayload;
}

export namespace CachePayloadTable {
  export const schema: Schema<CachePayloadTable> = s.record(
    CachePayload.schema,
  );
}
