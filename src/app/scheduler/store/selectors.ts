import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CourseInteraction, SchedulerState } from '@scheduler/store/state';
import { isLoading, isSuccess, Loadable, monad } from 'loadable.ts';
import { GenerateRequest, Schedule } from '@app/types/schedules';
import * as globalSelectors from '@app/selectors';
import { TermCode } from '@app/types/terms';

const getSchedulerState = createFeatureSelector<SchedulerState>('scheduler');

export const getSchedulesState = createSelector(
  getSchedulerState,
  (state: SchedulerState) => state.schedules,
);

export const getViewingRef = createSelector(
  getSchedulerState,
  (state: SchedulerState) => state.viewing,
);

export const getCurrentRequestOrNull = createSelector(
  getSchedulerState,
  (state: SchedulerState): GenerateRequest | null => {
    if (isSuccess(state.schedules)) {
      return state.schedules.value.request;
    } else {
      return null;
    }
  },
);

export const getSchedules = createSelector(
  getSchedulerState,
  (state: SchedulerState): Loadable<Schedule[]> => {
    return monad(state.schedules).map(tuple => tuple.all);
  },
);

export const getIsValidating = createSelector(
  globalSelectors.courses.getAll,
  globalSelectors.currentTermCode.get,
  (state, currentTermCode): boolean => {
    if (!isSuccess(currentTermCode)) {
      return false;
    }

    const encoded = TermCode.encode(currentTermCode.value);
    const courses = state[encoded];

    if (!courses) {
      return false;
    }

    return courses.isValidating;
  },
);

export const getCourseInteraction = createSelector(
  getIsValidating,
  (isValidating: boolean): CourseInteraction => {
    return isValidating ? 'disabled' : 'enabled';
  },
);

export const getSchedule = createSelector(
  getSchedulerState,
  (state: SchedulerState): Loadable<Schedule> => {
    return monad(state.schedules).map(tuple => tuple.all[tuple.index]);
  },
);

export const getCurrentScheduleIndex = createSelector(
  getSchedulerState,
  (state: SchedulerState): number => {
    if (isSuccess(state.schedules)) {
      return state.schedules.value.index;
    } else {
      return 0;
    }
  },
);

export const getCheckedCourses = createSelector(
  getSchedulerState,
  (state: SchedulerState) => state.checkedCourses,
);

export const areSchedulesLoading = createSelector(
  getSchedulerState,
  (state: SchedulerState): boolean => {
    return state.schedules !== null && isLoading(state.schedules);
  },
);

export const getCompareIndices = createSelector(
  getSchedulerState,
  (state: SchedulerState): Loadable<number[]> => {
    return monad(state.schedules).map(({ compare }) => compare);
  },
);

export const getCheckedPacks = createSelector(
  getSchedulerState,
  (state: SchedulerState) => {
    return state.checkedPacks;
  },
);

export const getShowScheduleList = createSelector(
  getSchedulerState,
  (state: SchedulerState) => state.showScheduleList,
);
