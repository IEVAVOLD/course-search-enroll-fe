import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CourseEntityBlock } from '@app/types/schedules';
import { GlobalState } from '@app/state';
import { Store } from '@ngrx/store';
import * as viewActions from '@scheduler/store/actions';
import * as globalActions from '@app/actions';

@Component({
  selector: 'cse-save-dialog',
  templateUrl: './save-dialog.component.html',
  styleUrls: ['./save-dialog.component.scss'],
})
export class SaveDialogComponent {
  public step: 'start' | 'confirmSave' | 'confirmRemove';

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: CourseEntityBlock['data'],
    private store: Store<GlobalState>,
  ) {
    this.step = 'start';
  }

  viewPackages() {
    const ref = this.data.course.ref;
    if (ref !== null) {
      this.store.dispatch(viewActions.packages.show({ ref }));
    }
  }

  save() {
    if (this.data.isEnrolled) {
      return;
    }

    this.store.dispatch(
      globalActions.courses.roadmap.addCourseWithPack({
        course: {
          termCode: this.data.course.termCode,
          subject: { code: this.data.course.subjectCode },
          courseId: this.data.course.courseId,
          shortCatalog: this.data.course.shortCatalog,
        },
        pack: {
          classNumber: this.data.package.classNumber,
          relatedClassNumbers: this.data.package.relatedClassNumbers,
        },
        credits: this.data.course.credits.min,
        joinWaitlist: false,
        withHonors: false,
        classPermissionNumber: null,
      }),
    );
  }

  remove() {
    if (this.data.isEnrolled) {
      return;
    }

    this.store.dispatch(
      globalActions.courses.roadmap.removePack({
        course: {
          termCode: this.data.course.termCode,
          subject: { code: this.data.course.subjectCode },
          courseId: this.data.course.courseId,
          shortCatalog: this.data.course.shortCatalog,
        },
      }),
    );
  }

  confirmSave() {
    this.step = 'confirmSave';
  }

  confirmRemove() {
    this.step = 'confirmRemove';
  }

  cancelConfirm() {
    this.step = 'start';
  }
}
