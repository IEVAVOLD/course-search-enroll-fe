import { Component } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import {
  startComesBeforeEnd,
  someAreTrue,
  generateTimeIncrements,
} from '../edit-break-dialog/edit-break-dialog.component';

@Component({
  selector: 'cse-add-break-dialog',
  templateUrl: './add-break-dialog.component.html',
  styleUrls: ['./add-break-dialog.component.scss'],
})
export class AddBreakDialogComponent {
  public increments = generateTimeIncrements();

  public form = new UntypedFormGroup({
    days: new UntypedFormGroup(
      {
        sun: new UntypedFormControl(false),
        mon: new UntypedFormControl(false),
        tue: new UntypedFormControl(false),
        wed: new UntypedFormControl(false),
        thu: new UntypedFormControl(false),
        fri: new UntypedFormControl(false),
        sat: new UntypedFormControl(false),
      },
      [someAreTrue],
    ),
    times: new UntypedFormGroup(
      {
        startSeconds: new UntypedFormControl(null),
        endSeconds: new UntypedFormControl(null),
      },
      [startComesBeforeEnd],
    ),
    label: new UntypedFormControl(null),
  });
}
