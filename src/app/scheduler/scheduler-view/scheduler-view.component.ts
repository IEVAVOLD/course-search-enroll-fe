import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { MatSidenav } from '@angular/material/sidenav';
import { Store } from '@ngrx/store';
import { BreakpointObserver } from '@angular/cdk/layout';
import {
  ifSuccessThenUnwrap,
  notNull,
  unique,
  unsubscribeAll,
} from '@app/core/utils';
import { GlobalState, Prefs } from '@app/state';
import { TermCode } from '@app/types/terms';
import { Breakpoints } from '@app/shared/breakpoints';
import * as actions from '@scheduler/store/actions';
import * as globalActions from '@app/actions';
import * as globalSelectors from '@app/selectors';
import * as viewSelectors from '@scheduler/store/selectors';
import {
  combineLatest,
  Observable,
  of,
  ReplaySubject,
  Subscription,
} from 'rxjs';
import {
  catchError,
  filter,
  first,
  map,
  startWith,
  withLatestFrom,
} from 'rxjs/operators';
import { isSuccess, Loadable, LOADING, monad } from 'loadable.ts';
import { MatSelectChange } from '@angular/material/select';
import {
  CourseRef,
  CurrentCourse,
  DocId,
  EnrollmentPackage,
  HasCourseRef,
  RoadmapCourse,
  RoadmapCourseRef,
  RoadmapId,
} from '@app/types/courses';
import { SaveDialogComponent } from '../save-dialog/save-dialog.component';
import { DialogService } from '@app/shared/services/dialog.service';
import { Block } from '@app/shared/pipes/layout-blocks.pipe';
import { Actions, ofType } from '@ngrx/effects';
import {
  BreakEntityBlock,
  CourseEntityBlock,
  ExistingScheduleBreak,
  GenerateRequest,
  NewScheduleBreak,
  Schedule,
} from '@app/types/schedules';
import { PreferencesDialogComponent } from '../preferences-dialog/preferences-dialog.component';
import * as s from '@app/types/schema';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { CheckedPackages, SchedulerState } from '@app/scheduler/store/state';
import { takeFirstSuccess } from '@app/core/operators';
import { AddBreakDialogComponent } from '../add-break-dialog/add-break-dialog.component';
import { EditBreakDialogComponent } from '../edit-break-dialog/edit-break-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SaveAllDialogComponent } from '../save-all-dialog/save-all-dialog.component';
import { LiveAnnouncer } from '@angular/cdk/a11y';

@Component({
  selector: 'cse-scheduler-view',
  templateUrl: './scheduler-view.component.html',
  styleUrls: ['./scheduler-view.component.scss'],
  animations: [
    trigger('pane', [
      transition('* <=> onscreen', animate('.3s ease-in-out')),
      state('offscreenLeft', style({ transform: 'translateX(-100%)' })),
      state('onscreen', style({ transform: 'translateX(0)' })),
      state('offscreenRight', style({ transform: 'translateX(100%)' })),
    ]),
  ],
})
export class SchedulerViewComponent implements OnInit, OnDestroy {
  @ViewChild('sidenav') public sidenavRef!: MatSidenav;
  public sidenavMode: 'packages' | 'compare' | 'exam' = 'packages';

  public isMobile = new ReplaySubject<boolean>();
  public paneState: PaneState;
  private subscriptions: Subscription[];

  public isValidating$ = this.store.select(viewSelectors.getIsValidating);

  public schedulesState$ = this.store.select(viewSelectors.getSchedulesState);

  public schedules$ = this.store.select(viewSelectors.getSchedules);

  public courseInteraction$ = this.store.select(
    viewSelectors.getCourseInteraction,
  );

  public schedule$ = this.store.select(viewSelectors.getSchedule);

  public hasConflicts$ = this.schedule$.pipe(
    map(loadable => isSuccess(loadable) && loadable.value.hasConflicts),
  );

  public blocks$ = this.schedule$.pipe(
    map(loadable => {
      return monad(loadable).map(schedule => {
        return schedule.entities.flatMap<Block>(entity => entity.blocks);
      });
    }),
  );

  public currentScheduleIndex$ = this.store.select(
    viewSelectors.getCurrentScheduleIndex,
  );

  public activeTermCodes$ = this.store.select(
    globalSelectors.terms.getActiveTermCodes,
  );
  public termCode$: Observable<TermCode> = this.store
    .select(globalSelectors.currentTermCode.get)
    .pipe(ifSuccessThenUnwrap());

  public cartCourses$ = combineLatest([
    this.termCode$.pipe(map(TermCode.encode)),
    this.store.select(globalSelectors.courses.getAll),
  ]).pipe(map(([termCode, courses]) => courses[termCode]?.roadmap ?? LOADING));

  public enrolledCourses$ = combineLatest([
    this.termCode$.pipe(map(TermCode.encode)),
    this.store.select(globalSelectors.courses.getAll),
  ]).pipe(
    map(([termCode, courses]) => courses[termCode]?.current ?? LOADING),
    map(loadable =>
      monad(loadable).map(courses =>
        courses.filter(course => course.enrolledStatus === 'enrolled'),
      ),
    ),
  );

  public waitlistedCourses$ = combineLatest([
    this.termCode$.pipe(map(TermCode.encode)),
    this.store.select(globalSelectors.courses.getAll),
  ]).pipe(
    map(([termCode, courses]) => courses[termCode]?.current ?? LOADING),
    map(loadable =>
      monad(loadable).map(courses =>
        courses.filter(course => course.enrolledStatus === 'waitlisted'),
      ),
    ),
  );

  public checkedCourseRefs$ = this.store.select(
    viewSelectors.getCheckedCourses,
  );

  public totalCredits$ = combineLatest([
    this.checkedCourseRefs$,
    this.cartCourses$.pipe(filter(notNull), ifSuccessThenUnwrap()),
    this.enrolledCourses$.pipe(filter(notNull), ifSuccessThenUnwrap()),
    this.waitlistedCourses$.pipe(filter(notNull), ifSuccessThenUnwrap()),
  ]).pipe(
    map(
      ([checked, cart, enrolled, waitlisted]): Array<
        RoadmapCourse | CurrentCourse
      > => {
        return [...cart, ...enrolled, ...waitlisted].filter(course =>
          checked.some(CourseRef.equalsCurry(course.ref)),
        );
      },
    ),
    startWith([]),
  );

  public compareIndices$ = this.store.select(viewSelectors.getCompareIndices);

  public isMarkedForComparison$ = this.schedulesState$.pipe(
    map(tuple => {
      if (!isSuccess(tuple)) {
        return false;
      } else {
        return tuple.value.compare.includes(tuple.value.index);
      }
    }),
  );

  public comparisonToolbar$ = this.schedulesState$.pipe(
    map(loadable => {
      if (!isSuccess(loadable)) {
        return [];
      }

      const { all, compare } = loadable.value;
      return compare.map(index => {
        return { index, schedule: all[index] };
      });
    }),
  );

  public comparisonSchedules$ = this.schedulesState$.pipe(
    map(loadable => {
      return monad(loadable).map(({ all, compare }) => {
        return compare.map(index => {
          const schedule = all[index];
          const blocks = schedule.entities.flatMap<Block>(
            entity => entity.blocks,
          );
          return { index, schedule, blocks };
        });
      });
    }),
  );

  public totalMarkedForComparison$ = this.comparisonSchedules$.pipe(
    map(loadable => (isSuccess(loadable) ? loadable.value.length : 0)),
    startWith(0),
  );

  public viewingCourse$ = combineLatest([
    this.store.select(viewSelectors.getViewingRef),
    this.store.select(globalSelectors.courses.getAll),
  ]).pipe(
    map(([ref, courses]) => {
      if (ref === null) {
        return null;
      }

      type Category = Loadable<Array<RoadmapCourse | CurrentCourse>> | null;

      const termCode = TermCode.encode(ref.termCode);
      const category: Category = RoadmapCourseRef.is(ref)
        ? courses[termCode]?.roadmap ?? null
        : courses[termCode]?.current ?? null;

      if (category === null || !isSuccess(category)) {
        return null;
      }

      return category.value.find(CourseRef.matches(ref)) ?? null;
    }),
  );

  public viewingChosenPackage$ = this.viewingCourse$.pipe(
    map(course => {
      if (RoadmapCourse.is(course)) {
        return course.package?.details;
      } else if (CurrentCourse.is(course)) {
        return course.package.details;
      } else {
        return null;
      }
    }),
  );

  public viewingAllPackages$ = this.viewingCourse$.pipe(
    map(course => {
      if (!course || !RoadmapCourse.is(course)) {
        return null;
      } else {
        return course.packages;
      }
    }),
  );

  public viewingCourseWithoutChosenPackage$ = this.viewingCourse$.pipe(
    map(course => {
      if (RoadmapCourse.is(course)) {
        return course.package === null;
      } else {
        return false;
      }
    }),
  );

  public checkedPacks$ = this.store.select(viewSelectors.getCheckedPacks);

  public listOfVisibleCheckedPackages$ = combineLatest([
    this.viewingCourse$,
    this.checkedPacks$,
  ]).pipe(
    map(([course, checkedDocIds]): DocId[] => {
      if (!RoadmapCourse.is(course) || course.package) {
        // If the course:
        //
        // - Is NOT a roadmap course
        // - Or course is a roadmap course but the user has already chosen
        //   an enrollment package for the roadmap course.
        //
        // then don't let the user filter the packages used by schedule
        // generation any further.
        return [];
      }

      const roadmapId = course.ref.roadmapId;
      const checkedState = checkedDocIds[roadmapId] ?? { kind: 'unchanged' };

      if (!course.packages || !isSuccess(course.packages)) {
        return [];
      }

      if (checkedState.kind === 'unchanged') {
        return course.packages.value.map(pack => pack.docId);
      } else {
        return checkedState.docIds;
      }

      return [];
    }),
  );

  public getShowScheduleList$ = this.store.select(
    viewSelectors.getShowScheduleList,
  );

  public leftLabel$ = this.getShowScheduleList$.pipe(
    withLatestFrom(
      this.store.select(viewSelectors.getSchedules).pipe(
        map(loadable => {
          if (loadable && isSuccess(loadable)) {
            return loadable.value.length;
          } else {
            return 0;
          }
        }),
      ),
    ),
    map(([isListShown, totalSchedules]) => {
      if (isListShown) {
        return `${totalSchedules} schedules`;
      } else {
        return 'Scheduler';
      }
    }),
  );

  public scheduleList$ = this.store
    .select(viewSelectors.getSchedulesState)
    .pipe(
      map(state => {
        if (!isSuccess(state)) {
          return [];
        }

        const { compare, all, index: activeIndex } = state.value;

        return all.map((schedule, index) => {
          return {
            index,
            isMarked: compare.includes(index),
            isActive: index === activeIndex,
            label: `Schedule ${index + 1}`,
            blocks: schedule.entities.flatMap<Block>(entity => entity.blocks),
          };
        });
      }),
    );

  public sectionsLabel$ = this.viewingCourse$.pipe(
    map(course => {
      if (!course) {
        return 'Course sections';
      } else {
        return `${course.shortCatalog} sections`;
      }
    }),
  );

  constructor(
    private store: Store<GlobalState>,
    private dialog: DialogService,
    private snackbar: MatSnackBar,
    private announcer: LiveAnnouncer,
    breakpoints: BreakpointObserver,
    actions$: Actions,
  ) {
    this.paneState = PaneState.init(breakpoints.isMatched(Breakpoints.Mobile));

    this.subscriptions = [
      breakpoints
        .observe(Breakpoints.Mobile)
        .subscribe(({ matches: isMobile }) => {
          this.isMobile.next(isMobile);

          if (isMobile) {
            this.paneState = this.paneState.switchToMobile();
          } else {
            this.paneState = this.paneState.switchToDesktop();
          }
        }),

      actions$
        .pipe(ofType(actions.scheduler.start, actions.scheduler.done))
        .subscribe(() => {
          this.paneState = this.paneState.focusOnSchedules();
        }),

      actions$.pipe(ofType(actions.packages.show)).subscribe(() => {
        this.sidenavMode = 'packages';
        this.sidenavRef.open();
      }),
    ];
  }

  ngOnInit() {
    this.store.dispatch(actions.lifecycle.init());
  }

  ngOnDestroy() {
    unsubscribeAll(this.subscriptions);
  }

  /**
   * Prereq: Init action has finished (cart and enrolled courses are loaded)
   */
  async load() {
    const hasMarkedSchedules = await this.store
      .select(viewSelectors.getSchedulesState)
      .pipe(
        map(s => (isSuccess(s) ? s.value.compare.length > 0 : false)),
        first(),
      )
      .toPromise();

    if (hasMarkedSchedules) {
      const shouldRefresh = await this.dialog.confirm({
        headline: 'Refresh schedules?',
        reasoning: [
          `Generating new schedules will refresh your schedules and the
          schedules you currently have marked for comparison will be removed.`,
        ],
        affirmative: 'Refresh',
      });

      if (shouldRefresh === false) {
        return;
      }
    }

    this.paneState = this.paneState.focusOnSchedules();

    const termCode = await this.termCode$.pipe(first()).toPromise();

    const checkedRefs = await this.checkedCourseRefs$.pipe(first()).toPromise();

    const roadmap = await this.store
      .select(globalSelectors.courses.getRoadmapForTerm, termCode)
      .pipe(
        takeFirstSuccess(),
        catchError(_err => of(<RoadmapCourse[]>[])),
      )
      .toPromise();

    const current = await this.store
      .select(globalSelectors.courses.getCurrentForTerm, termCode)
      .pipe(
        takeFirstSuccess(),
        catchError(_err => of(<CurrentCourse[]>[])),
      )
      .toPromise();

    const prefs = await this.store
      .select(globalSelectors.prefs.getAll)
      .pipe(first())
      .toPromise();

    const checkedPackages = await this.store
      .select(viewSelectors.getCheckedPacks)
      .pipe(first())
      .toPromise();

    const request = buildRequest(
      termCode,
      checkedRefs,
      roadmap,
      current,
      checkedPackages,
      prefs,
    );

    this.store.dispatch(actions.scheduler.start({ request }));
  }

  onPrevSchedule() {
    this.store.dispatch(actions.scheduler.prev());
  }

  onNextSchedule() {
    this.store.dispatch(actions.scheduler.next());
  }

  compareTermCodes(t1: unknown, t2: unknown): boolean {
    return TermCode.is(t1) && TermCode.is(t2) && TermCode.equals(t1, t2);
  }

  userChangedTerm({ value }: MatSelectChange) {
    if (TermCode.is(value)) {
      this.store.dispatch(
        globalActions.currentTermCode.set({ currentTermCode: value }),
      );
    } else {
      console.assert(TermCode.is(value), 'expected a term-code');
    }
  }

  onHideSchedules() {
    this.paneState = this.paneState.focusOnCourses();
  }

  onToggleCourse([course, event]: [HasCourseRef, MatCheckboxChange]) {
    this.store.dispatch(
      actions.selectCourse({ ref: course.ref, isChecked: event.checked }),
    );
  }

  onOpenPackages(course: RoadmapCourse | CurrentCourse) {
    this.store.dispatch(actions.packages.show({ ref: course.ref }));
  }

  onClosePackages() {
    this.sidenavRef.close();
    this.store.dispatch(actions.packages.hide());
  }

  async onRetryCart() {
    const termCode = await this.termCode$.pipe(first()).toPromise();
    this.store.dispatch(globalActions.courses.roadmap.load({ termCode }));
  }

  async onRetryCurrent() {
    const termCode = await this.termCode$.pipe(first()).toPromise();
    this.store.dispatch(globalActions.courses.current.load({ termCode }));
  }

  async onOpenAddBreakDialog() {
    const termCode = await this.termCode$.pipe(first()).toPromise();
    const response = await this.dialog.small(AddBreakDialogComponent);
    if (NewScheduleBreak.schema.matches(response)) {
      this.store.dispatch(actions.breaks.add({ termCode, brk: response }));
    }
  }

  async onBlockClick(
    data: CourseEntityBlock['data'] | BreakEntityBlock['data'],
  ) {
    if (data.kind === 'break') {
      const termCode = await this.termCode$.pipe(first()).toPromise();
      const response = await this.dialog.small(EditBreakDialogComponent, data);
      if (ExistingScheduleBreak.schema.matches(response)) {
        this.snackbar.open('You have updated a schedule break.');
        this.store.dispatch(actions.breaks.update({ termCode, brk: response }));
      } else if (response === 'remove') {
        this.snackbar.open('You have removed a schedule break.');
        this.store.dispatch(actions.breaks.remove({ id: data.id }));
      }
    } else {
      this.dialog.small(SaveDialogComponent, data);
    }
  }

  onOpenSaveAllDialog(data: Schedule) {
    this.dialog.large(SaveAllDialogComponent, data);
  }

  onOpenExamSchedule() {
    this.sidenavMode = 'exam';
    this.sidenavRef.open();
  }

  onCloseExamSchedule() {
    this.sidenavRef.close();
  }

  onToggleMarkedForComparison(index: number, isMarked: boolean) {
    if (isMarked) {
      this.store.dispatch(actions.compare.unmark({ index }));
    } else {
      this.store.dispatch(actions.compare.mark({ index }));
    }
  }

  onCloseCompare() {
    this.sidenavRef.close();
  }

  onOpenCompare() {
    this.sidenavMode = 'compare';
    this.sidenavRef.open();
  }

  onJumpTo(index: number) {
    this.paneState = this.paneState.focusOnSchedules();
    this.store.dispatch(actions.scheduler.jump({ index }));
  }

  onViewFromCompare(index: number) {
    this.onCloseCompare();
    this.store.dispatch(actions.scheduler.jump({ index }));
  }

  async onOpenPreferencesDialog() {
    const prefs = await this.store
      .select(globalSelectors.prefs.getAll)
      .pipe(first())
      .toPromise();

    const prefsSchema = s.object({
      scheduler: s.object({
        options: s.object({
          waitlistedPackages: s.unknown,
          specialGroupsPackages: s.unknown,
          allowConflicts: s.unknown,
        }),
      }),
    });

    const options: GenerateRequest['options'] =
      isSuccess(prefs) && prefsSchema.matches(prefs.value)
        ? {
            waitlistedPackages:
              prefs.value.scheduler.options.waitlistedPackages === true,
            specialGroupsPackages:
              prefs.value.scheduler.options.specialGroupsPackages === true,
            allowConflicts:
              prefs.value.scheduler.options.allowConflicts === true,
          }
        : {
            waitlistedPackages: false,
            specialGroupsPackages: false,
            allowConflicts: false,
          };

    const response = await this.dialog.small(
      PreferencesDialogComponent,
      options,
    );

    const responseSchema = s.object({
      showWaitlisted: s.boolean,
      showSpecialGroups: s.boolean,
      showConflicts: s.boolean,
    });

    if (responseSchema.matches(response)) {
      const changedWaitlisted =
        response.showWaitlisted !== options.waitlistedPackages;
      const changedSpecialGroups =
        response.showSpecialGroups !== options.specialGroupsPackages;
      const changedConflicts =
        response.showConflicts !== options.allowConflicts;

      if (!changedWaitlisted && !changedSpecialGroups && !changedConflicts) {
        console.log('no need to update scheduler prefs');
        return;
      }

      this.store.dispatch(
        globalActions.prefs.setKey({
          key: 'scheduler',
          value: {
            options: {
              waitlistedPackages: response.showWaitlisted,
              specialGroupsPackages: response.showSpecialGroups,
              allowConflicts: response.showConflicts,
            },
          },
        }),
      );

      const oldRequest = await this.store
        .select(viewSelectors.getCurrentRequestOrNull)
        .pipe(first())
        .toPromise();

      if (oldRequest) {
        const request = {
          ...oldRequest,
          options: {
            waitlistedPackages: response.showWaitlisted,
            specialGroupsPackages: response.showSpecialGroups,
            allowConflicts: response.showConflicts,
          },
        };

        this.store.dispatch(actions.scheduler.start({ request: request }));
      }
    }
  }

  async onChangePackageCheckedState(
    { ref, packages: packs }: RoadmapCourse,
    pack: EnrollmentPackage,
    event: MatCheckboxChange,
  ) {
    const checkedPacks = await this.store
      .select(viewSelectors.getCheckedPacks)
      .pipe(first())
      .toPromise();
    const packState = checkedPacks[ref.roadmapId] ?? { kind: 'unchanged' };
    const allDocIds =
      packs && isSuccess(packs) ? packs.value.map(p => p.docId) : [];

    let prevCheckedDocIds: DocId[] = [];
    if (packState.kind === 'unchanged') {
      prevCheckedDocIds = allDocIds.slice();
    } else {
      prevCheckedDocIds = packState.docIds.slice();
    }

    let nextCheckedDocIds = prevCheckedDocIds;
    if (event.checked) {
      nextCheckedDocIds = unique([...nextCheckedDocIds, pack.docId]);
    } else {
      nextCheckedDocIds = prevCheckedDocIds.filter(d => d !== pack.docId);
    }

    const includesAllDocIds = allDocIds.every(d =>
      nextCheckedDocIds.includes(d),
    );

    const newPackState: CheckedPackages = includesAllDocIds
      ? { kind: 'unchanged' }
      : { kind: 'changed', docIds: nextCheckedDocIds };

    this.store.dispatch(
      actions.packages.setCheckedState({
        ref,
        checked: newPackState,
      }),
    );
  }

  onRefreshSchedules(request: GenerateRequest) {
    this.store.dispatch(actions.scheduler.start({ request }));
  }

  onRemovePackage(course: RoadmapCourse) {
    this.onClosePackages();

    this.store.dispatch(
      globalActions.courses.roadmap.removePack({
        course,
      }),
    );
  }

  onChoosePackage(course: RoadmapCourse, pack: EnrollmentPackage) {
    this.onClosePackages();

    this.store.dispatch(
      globalActions.courses.roadmap.addCourseWithPack({
        course,
        pack: {
          classNumber: parseInt(pack.id, 10),
          relatedClassNumbers: pack.relatedClassNumbers,
        },
        credits: pack.credits.min,
        joinWaitlist: false,
        withHonors: false,
        classPermissionNumber: null,
      }),
    );
  }

  onHideScheduleList() {
    this.announcer.announce('Closed schedule list', 'assertive');
    this.paneState = this.paneState.focusOnSchedules();
    this.store.dispatch(actions.list.hide());
  }

  onShowScheduleList() {
    this.announcer.announce('Opened schedule list', 'assertive');
    this.paneState = this.paneState.focusOnCourses();
    this.store.dispatch(actions.list.show());

    // a hack so that the selected element is not null and
    // schedule-list does not need to be a component with ngOnInit()
    setTimeout(() => {
      const elm = document.getElementById('schedule-list-item-btn-0');
      if (elm !== null) {
        elm.focus();
      }
    }, 0);
  }

  async onToggleScheduleList() {
    const isShown = await this.getShowScheduleList$.pipe(first()).toPromise();

    if (isShown) {
      this.onHideScheduleList();
    } else {
      this.onShowScheduleList();
    }
  }
}

interface PaneStateInstance {
  courses: 'offscreenLeft' | 'onscreen';
  schedules: 'offscreenLeft' | 'onscreen' | 'offscreenRight';
}

interface PaneStateInstancePair {
  mobile: PaneStateInstance;
  desktop: PaneStateInstance;
}

class PaneState {
  /**
   * A series of constants capturing the legal combinations of pane animation
   * states. These constant objects will be passed to the `animState` observable
   * inside the MyCoursesViewComponent class.
   */
  public static FOCUS_ON_COURSES: PaneStateInstancePair = {
    mobile: {
      courses: 'onscreen',
      schedules: 'offscreenRight',
    },
    desktop: {
      courses: 'onscreen',
      schedules: 'onscreen',
    },
  };

  public static FOCUS_ON_SCHEDULES: PaneStateInstancePair = {
    mobile: {
      courses: 'offscreenLeft',
      schedules: 'onscreen',
    },
    desktop: {
      courses: 'onscreen',
      schedules: 'onscreen',
    },
  };

  private constructor(
    public readonly breakpoint: 'mobile' | 'desktop',
    public readonly pair: PaneStateInstancePair,
  ) {}

  get courses(): PaneStateInstance['courses'] {
    return this.pair[this.breakpoint].courses;
  }

  get schedules(): PaneStateInstance['schedules'] {
    return this.pair[this.breakpoint].schedules;
  }

  public static init(isMobile: boolean): PaneState {
    return new PaneState(
      isMobile ? 'mobile' : 'desktop',
      PaneState.FOCUS_ON_COURSES,
    );
  }

  public switchToMobile(): PaneState {
    return new PaneState('mobile', this.pair);
  }

  public switchToDesktop(): PaneState {
    return new PaneState('desktop', this.pair);
  }

  public focusOnCourses(): PaneState {
    return new PaneState(this.breakpoint, PaneState.FOCUS_ON_COURSES);
  }

  public focusOnSchedules(): PaneState {
    return new PaneState(this.breakpoint, PaneState.FOCUS_ON_SCHEDULES);
  }
}

const buildRequest = (
  termCode: TermCode,
  checkedRefs: CourseRef[],
  roadmap: RoadmapCourse[],
  current: CurrentCourse[],
  checkedPacks: SchedulerState['checkedPacks'],
  prefs: Loadable<Prefs>,
): GenerateRequest => {
  const isChecked = (course: HasCourseRef) =>
    checkedRefs.some(CourseRef.equalsCurry(course.ref));

  const options = buildOptions(prefs);

  const packSubset: Record<RoadmapId, DocId[]> = {};
  roadmap.filter(isChecked).forEach(course => {
    const roadmapId = course.ref.roadmapId;
    const packState = checkedPacks[roadmapId] ?? { kind: 'unchanged' };

    if (packState.kind === 'unchanged') {
      // The user hasn't chosen a subset of packages to use when generating
      // schedules so don't limit which packages the backend can use when
      // generating new schedules.
    } else {
      // Use the list of checked docIds.
      packSubset[roadmapId] = packState.docIds;
    }
  });

  return GenerateRequest.from(
    termCode,
    roadmap.filter(isChecked),
    packSubset,
    current.filter(isChecked),
    options,
  );
};

const buildOptions = (
  loadable: Loadable<Prefs>,
): GenerateRequest['options'] => {
  const optionsSchema = s.object({
    scheduler: s.object({
      options: s.object({
        waitlistedPackages: s.unknown,
        specialGroupsPackages: s.unknown,
        allowConflicts: s.unknown,
      }),
    }),
  });

  if (isSuccess(loadable) && optionsSchema.matches(loadable.value)) {
    const saved = loadable.value.scheduler.options;
    return {
      waitlistedPackages: saved.waitlistedPackages === true,
      specialGroupsPackages: saved.specialGroupsPackages === true,
      allowConflicts: saved.allowConflicts === true,
    };
  }

  return {
    waitlistedPackages: false,
    specialGroupsPackages: false,
    allowConflicts: false,
  };
};
