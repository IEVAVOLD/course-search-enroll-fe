# Scheduler View component

Right now the `scheduler-view` component contains a one-week calender layout `cse-schedule`, displaying `cse-meeting`s as blocks.

## Displaying conflcting meetings

In `src/app/shared/components/schedule/schedule.component.ts`, the `detectConflicts()` function is used to generate a list of conflict **groups**, where each group contains two or more **columns** of meeting **indexes**. Next, `conflictsLayout()` will assign horizontal layout parameters for each meeting index. Finally, in `ngAfterContentInit()`, layouts of all meetings will be calculated and applied.

The `detectConflicts()` works in the following ways:

1. Before it is run, meetings are already sorted by their `day`, `start`, and `duration`. Therefore, suppose we have two indexs _a_ < _b_, _b_ always starts (equal to or) later than _a_, **but might end earlier, equal to, or later than _a_**.
1. At each iteration, we first find a **group** `i` through `j-1`, where there is at least one conflict between any two indexes. We do this by calling `hasConflict()` for all pairs of indexes among `i` and `j-1`. Note that we cannot simply compare a meeting only with the one before it. Take this `[(][)]` as an example, the two brackets are not conflicting with each other, but the parenthesis conflicts with both of them, so these three meetings should be counted as one group, not two.
1. We now need to construct the **columns**. Looping from index `i` to `j-1`, we add indexes into the current column not conflicting with each other and mark them as visited. When we reach the end, we have constructed one column. We then repeat the process until all indexes are marked visited.
1. We repeat the above iteration until all **groups** are constructed.
