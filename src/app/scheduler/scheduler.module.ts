import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SchedulerViewComponent } from './scheduler-view/scheduler-view.component';
import { SaveDialogComponent } from './save-dialog/save-dialog.component';
import { SaveAllDialogComponent } from './save-all-dialog/save-all-dialog.component';
import { StoreModule } from '@ngrx/store';
import { reducer } from './store/reducer';
import { SharedModule } from '@app/shared/shared.module';
import { EffectsModule } from '@ngrx/effects';
import { Effects } from '@app/scheduler/store/effects';
import { MatBadgeModule } from '@angular/material/badge';
import { CourseCategoryComponent } from './course-category/course-category.component';
import { WelcomeMessageComponent } from './welcome-message/welcome-message.component';
import { ExpiredMessageComponent } from './expired-message/expired-message.component';
import { EmptyMessageComponent } from './empty-message/empty-message.component';
import { NetworkErrorMessageComponent } from './network-error-message/network-error-message.component';
import { PreferencesDialogComponent } from './preferences-dialog/preferences-dialog.component';
import { AddBreakDialogComponent } from './add-break-dialog/add-break-dialog.component';
import { EditBreakDialogComponent } from './edit-break-dialog/edit-break-dialog.component';

const routes: Routes = [
  {
    path: '',
    component: SchedulerViewComponent,
  },
];

@NgModule({
  declarations: [
    SchedulerViewComponent,
    SaveDialogComponent,
    SaveAllDialogComponent,
    CourseCategoryComponent,
    WelcomeMessageComponent,
    EmptyMessageComponent,
    NetworkErrorMessageComponent,
    ExpiredMessageComponent,
    PreferencesDialogComponent,
    AddBreakDialogComponent,
    EditBreakDialogComponent,
  ],
  imports: [
    SharedModule,
    MatBadgeModule,
    StoreModule.forFeature('scheduler', reducer),
    EffectsModule.forFeature([Effects]),
    RouterModule.forChild(routes),
  ],
})
export class SchedulerModule {}
