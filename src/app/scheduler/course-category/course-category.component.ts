import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { CourseRef, CurrentCourse, RoadmapCourse } from '@app/types/courses';
import { Loadable } from 'loadable.ts';
import { CourseInteraction, SchedulerState } from '../store/state';

@Component({
  selector: 'cse-course-category',
  templateUrl: './course-category.component.html',
  styleUrls: ['./course-category.component.scss'],
})
export class CourseCategoryComponent {
  @Input() public icon!: string;
  @Input() public name!: string;
  @Input() public checked!: CourseRef[];
  @Input() public courses!: Loadable<RoadmapCourse[] | CurrentCourse[]>;
  @Input() public interaction!: CourseInteraction;
  @Input() public checkedPacks!: SchedulerState['checkedPacks'];
  @Output() public toggle = new EventEmitter<
    [RoadmapCourse | CurrentCourse, MatCheckboxChange]
  >();
  @Output() public view = new EventEmitter<RoadmapCourse | CurrentCourse>();
  @Output() public retry = new EventEmitter<void>();

  isChecked(course: RoadmapCourse | CurrentCourse) {
    return this.checked.some(CourseRef.equalsCurry(course.ref));
  }

  isLocked(course: RoadmapCourse | CurrentCourse) {
    return RoadmapCourse.is(course) && course.package !== null;
  }

  hasModifiedPackList(course: RoadmapCourse | CurrentCourse): boolean {
    if (RoadmapCourse.is(course) && course.package === null) {
      const packState = this.checkedPacks[course.ref.roadmapId];
      return packState?.kind === 'changed';
    }

    return false;
  }

  totalChosenPacks(course: RoadmapCourse | CurrentCourse): string {
    if (RoadmapCourse.is(course) && course.package === null) {
      const packState = this.checkedPacks[course.ref.roadmapId];
      if (packState && packState.kind === 'changed') {
        const total = packState.docIds.length;
        const noun = total === 1 ? 'section' : 'sections';
        return `Using ${total} ${noun} to generate schedules`;
      }
    }

    return '';
  }
}
