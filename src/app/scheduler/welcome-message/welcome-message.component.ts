import { Component } from '@angular/core';

@Component({
  selector: 'cse-welcome-message',
  templateUrl: './welcome-message.component.html',
  styleUrls: ['./welcome-message.component.scss'],
})
export class WelcomeMessageComponent {}
