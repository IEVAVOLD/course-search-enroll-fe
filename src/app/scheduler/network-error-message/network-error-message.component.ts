import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'cse-network-error-message',
  templateUrl: './network-error-message.component.html',
  styleUrls: ['./network-error-message.component.scss'],
})
export class NetworkErrorMessageComponent {
  @Input() public disableRefresh!: boolean;
  @Output() public refresh = new EventEmitter<void>();
}
