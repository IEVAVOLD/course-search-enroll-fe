import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Course, MeetingLocation, RoadmapCourseRef } from '@app/types/courses';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GlobalState } from '@app/state';
import { Store } from '@ngrx/store';
import * as globalActions from '@app/actions';
import { CourseEntity, Schedule } from '@app/types/schedules';
import { Router } from '@angular/router';
import { UntypedFormControl } from '@angular/forms';
import { BehaviorSubject, combineLatest, Observable, Subscription } from 'rxjs';
import { isValidCreditValue } from '@app/my-courses/review-enroll-dialog/review-enroll-dialog.component';
import { notNull, unsubscribeAll } from '@app/core/utils';

export type Credits = Course['credits'];

// Please refer to src/app/my-courses/review-enroll-dialog/review-enroll-dialog.component.ts
// for a detailed explanation of the following fields.
export interface CourseEntityWithOptions extends CourseEntity {
  waitlistCtrl: UntypedFormControl | null;
  honorsCtrl: UntypedFormControl | null;

  creditValue: BehaviorSubject<Credits>;
  creditsCtrl: UntypedFormControl;
}

@Component({
  selector: 'cse-save-all-dialog',
  templateUrl: './save-all-dialog.component.html',
  styleUrls: ['./save-all-dialog.component.scss'],
})
export class SaveAllDialogComponent implements OnInit, OnDestroy {
  public step!: 'start' | 'finish';

  public instructors!: string[];
  public status!: string;
  public sectionsInfo!: {
    sectionTypeAndNumber: string;
    days: string;
    time: string;
    location: MeetingLocation;
  }[];

  public courseEntitiesWithOptions!: CourseEntityWithOptions[];
  public subscriptions: Subscription[] = [];

  public totalCreditValue!: Observable<Credits[]>;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: Schedule,
    private store: Store<GlobalState>,
    private router: Router,
  ) {}

  ngOnInit() {
    this.step = 'start';
    this.courseEntitiesWithOptions = this.data.entities
      .flatMap(e => (e.kind === 'course' ? e : []))
      .filter(e => e.ref?.kind === 'roadmap')
      .map(e => {
        const waitlistCtrl = e.package.supportsWaitlist
          ? new UntypedFormControl('notChosen')
          : null;

        const honorsCtrl = e.package.supportsHonors
          ? new UntypedFormControl('notChosen')
          : null;

        const creditValue = new BehaviorSubject(e.course.credits);
        const creditsCtrl = new UntypedFormControl(e.course.credits.min);

        this.subscriptions.push(
          creditsCtrl.valueChanges.subscribe(newVal => {
            if (isValidCreditValue(newVal, e.course.credits)) {
              creditValue.next({ min: newVal, max: newVal });
            }
          }),
        );

        return {
          ...e,
          waitlistCtrl,
          honorsCtrl,
          creditValue,
          creditsCtrl,
        };
      });

    this.totalCreditValue = combineLatest(
      this.courseEntitiesWithOptions.map(({ creditValue }) => creditValue),
    );
  }

  getSectionNames(sections: { name: string }[]) {
    return sections.map(s => s.name).join(', ');
  }

  save() {
    const courses = this.courseEntitiesWithOptions.map(entity => ({
      course: {
        termCode: entity.package.termCode,
        subject: { code: entity.course.subject.code },
        courseId: entity.course.courseId,
        shortCatalog: entity.course.shortCatalog,
      },
      pack: entity.package,
      credits: entity.creditValue.value.min,
      joinWaitlist: entity.waitlistCtrl?.value === 'chosen',
      withHonors: entity.honorsCtrl?.value === 'chosen',
      classPermissionNumber: null,
    }));

    if (courses.length > 0) {
      this.store.dispatch(
        globalActions.courses.roadmap.addCourseWithPackBulk({ courses }),
      );
      this.step = 'finish';
    }
  }

  cancel() {
    this.step = 'start';
  }

  goToCart() {
    const checked: RoadmapCourseRef[] = this.courseEntitiesWithOptions
      .map(entity => entity.ref)
      .filter(notNull)
      .filter(RoadmapCourseRef.is);

    this.router.navigate(['my-courses'], {
      state: {
        showCart: this.data.termCode,
        checked,
      },
    });
  }

  ngOnDestroy() {
    unsubscribeAll(this.subscriptions);
  }
}
