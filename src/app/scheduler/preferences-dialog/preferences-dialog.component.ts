import { Component, Inject } from '@angular/core';
import { GenerateRequest } from '@app/types/schedules';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';

@Component({
  selector: 'cse-preferences-dialog',
  templateUrl: './preferences-dialog.component.html',
  styleUrls: ['./preferences-dialog.component.scss'],
})
export class PreferencesDialogComponent {
  public form = new UntypedFormGroup({
    showWaitlisted: new UntypedFormControl(this.options.waitlistedPackages),
    showSpecialGroups: new UntypedFormControl(
      this.options.specialGroupsPackages,
    ),
    showConflicts: new UntypedFormControl(this.options.allowConflicts),
  });

  constructor(
    @Inject(MAT_DIALOG_DATA) public options: GenerateRequest['options'],
  ) {}

  toggle(name: string) {
    const ctrl = this.form.get(name)!;
    ctrl.setValue(!ctrl.value);
  }
}
