import { Component, Inject } from '@angular/core';
import {
  AbstractControl,
  UntypedFormControl,
  UntypedFormGroup,
  ValidatorFn,
} from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { toHourMinuteMeridianUtc } from '@app/shared/date-formats';
import { BreakEntityBlock } from '@app/types/schedules';

@Component({
  selector: 'cse-edit-break-dialog',
  templateUrl: './edit-break-dialog.component.html',
  styleUrls: ['./edit-break-dialog.component.scss'],
})
export class EditBreakDialogComponent {
  public increments = generateTimeIncrements();

  public form = new UntypedFormGroup({
    id: new UntypedFormControl(this.data.id),
    days: new UntypedFormGroup(
      {
        sun: new UntypedFormControl(
          this.data.schedule.daysList.includes('SUNDAY'),
        ),
        mon: new UntypedFormControl(
          this.data.schedule.daysList.includes('MONDAY'),
        ),
        tue: new UntypedFormControl(
          this.data.schedule.daysList.includes('TUESDAY'),
        ),
        wed: new UntypedFormControl(
          this.data.schedule.daysList.includes('WEDNESDAY'),
        ),
        thu: new UntypedFormControl(
          this.data.schedule.daysList.includes('THURSDAY'),
        ),
        fri: new UntypedFormControl(
          this.data.schedule.daysList.includes('FRIDAY'),
        ),
        sat: new UntypedFormControl(
          this.data.schedule.daysList.includes('SATURDAY'),
        ),
      },
      [someAreTrue],
    ),
    times: new UntypedFormGroup(
      {
        startSeconds: new UntypedFormControl(this.data.schedule.startSeconds),
        endSeconds: new UntypedFormControl(
          this.data.schedule.startSeconds + this.data.schedule.durationSeconds,
        ),
      },
      [startComesBeforeEnd],
    ),
    label: new UntypedFormControl(this.data.name),
  });

  constructor(@Inject(MAT_DIALOG_DATA) public data: BreakEntityBlock['data']) {}
}

interface Increment {
  value: number; // milliseconds after midnight (in UTC)
  label: string;
}

const SEC_PER_MINUTE = 60;
const SEC_PER_HOUR = SEC_PER_MINUTE * 60;

/**
 * Generate a list of time increments to populate the start and end time
 * dropdowns. By default, these time increments run from 9am to 10pm (inclusive)
 * and occur every 15 minutes.
 *
 * Internally the increments are represented as the number of milliseconds after
 * midnight in the UTC timezone since this is the time format expected by the
 * schedule breaks endpoint.
 */
export const generateTimeIncrements = (
  startHour = 7,
  endHour = 12 + 10,
  minutesBetween = 15,
): Increment[] => {
  const start = startHour * SEC_PER_HOUR;
  const end = endHour * SEC_PER_HOUR;
  const interval = minutesBetween * SEC_PER_MINUTE;

  const incs: Increment[] = [];

  for (let value = start; value <= end; value += interval) {
    incs.push({
      value,
      label: toHourMinuteMeridianUtc(value * 1000),
    });
  }

  return incs;
};

export const someAreTrue: ValidatorFn = (group: AbstractControl) => {
  if (group instanceof UntypedFormGroup) {
    if (Object.values(group.controls).some(ctrl => !!ctrl.value)) {
      return null;
    } else {
      return { noneAreTrue: true };
    }
  }

  return null;
};

export const startComesBeforeEnd: ValidatorFn = (group: AbstractControl) => {
  if (group instanceof UntypedFormGroup) {
    const start: AbstractControl = group.get('startSeconds')!;
    const end: AbstractControl = group.get('endSeconds')!;

    if (
      typeof end.value === 'number' &&
      typeof start.value === 'number' &&
      end.value <= start.value
    ) {
      return { times: true };
    }
  }

  return null;
};
