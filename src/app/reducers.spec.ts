import { GlobalState } from '@app/state';
import { reducers } from '@app/reducers';

import studentInfo from '../../testdata/studentInfo.json';
import enrollmentsJson from '../../testdata/current-enrollments.json';
import roadmapJson from '../../testdata/get-roadmap-courses.json';

import * as actions from '@app/actions';
import { failed, LOADING, success } from 'loadable.ts';
import { TermCode } from '@app/types/terms';

describe('student', () => {
  describe('found action', () => {
    it('should return the new state', () => {
      const { INIT } = GlobalState;
      const action = actions.student.found({ info: studentInfo as any });
      const state = reducers.student(INIT.student, action);

      expect(state).toStrictEqual(success(studentInfo));
    });
  });
  describe('failed action', () => {
    it('should return the failed state', () => {
      const { INIT } = GlobalState;
      const action = actions.student.failed();
      const state = reducers.student(INIT.student, action);

      expect(state).toStrictEqual(failed(null));
    });
  });
});

describe('courses', () => {
  describe('load action', () => {
    it('should return the new state', () => {
      const { INIT } = GlobalState;
      const action = actions.courses.load({
        termCode: TermCode.decodeOrThrow('1224'),
      });
      const state = reducers.courses(INIT.courses, action);
      expect(state).toStrictEqual({
        1224: {
          isValidating: false,
          hintToRevalidate: false,
          roadmap: LOADING,
          current: LOADING,
        },
      });
    });
  });
  describe('done action', () => {
    it('should return the done state', () => {
      const { INIT } = GlobalState;
      const action = actions.courses.done({
        termCode: TermCode.decodeOrThrow('1222'),
        roadmap: success(roadmapJson as any),
        current: success(enrollmentsJson as any),
      });
      const state = reducers.courses(INIT.courses, action);

      let match = {
        1222: {
          isValidating: false,
          hintToRevalidate: false,
          roadmap: success(roadmapJson),
          current: success(enrollmentsJson),
        },
      };
      expect(state).toStrictEqual(match);
    });
  });
});
describe('roadmap', () => {
  describe('load action', () => {
    it('should return the new state', () => {
      const { INIT } = GlobalState;
      const action = actions.courses.roadmap.load({
        termCode: TermCode.decodeOrThrow('1224'),
      });
      const state = reducers.courses(INIT.courses, action);
      expect(state).toStrictEqual({
        1224: {
          isValidating: false,
          hintToRevalidate: false,
          roadmap: LOADING,
          current: null,
        },
      });
    });
  });
  describe('done action', () => {
    it('should return the done state', () => {
      const { INIT } = GlobalState;
      const action = actions.courses.roadmap.done({
        termCode: TermCode.decodeOrThrow('1222'),
        roadmap: success(roadmapJson as any),
      });
      const state = reducers.courses(INIT.courses, action);

      let match = {
        1222: {
          isValidating: false,
          hintToRevalidate: false,
          roadmap: success(roadmapJson),
          current: null,
        },
      };
      expect(state).toStrictEqual(match);
    });
  });
  describe('modifyWithValidation', () => {
    it('should return the loading state for the roadmap', () => {
      const { INIT } = GlobalState;
      let course = roadmapJson[0] as any;
      course = { ...course, termCode: TermCode.decodeOrThrow('1222') };
      const action = actions.courses.roadmap.addCourseWithoutPack({
        course: course,
      });
      const state = reducers.courses(INIT.courses, action);

      let match = {
        1222: {
          isValidating: true,
          hintToRevalidate: false,
          roadmap: LOADING,
          current: null,
        },
      };
      expect(state).toStrictEqual(match);
    });
  });
});
