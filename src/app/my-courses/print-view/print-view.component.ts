import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GlobalState } from '@app/state';
import { TermCode } from '@app/types/terms';
import { Store } from '@ngrx/store';
import {
  failed,
  isFailed,
  isLoading,
  Loadable,
  LOADING,
  success,
} from 'loadable.ts';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import * as globalSelectors from '@app/selectors';
import { ifSuccessThenUnwrap, sortBy } from '@app/core/utils';
import { catchError, map, share, switchMap, startWith } from 'rxjs/operators';
import { CurrentCourse } from '@app/types/courses';
import { ApiService } from '@app/services/api.service';
import { Block } from '@app/shared/pipes/layout-blocks.pipe';
import { GrayColorQueue } from '@app/types/colors';

interface Data {
  termCode: TermCode;
  courses: CurrentCourse[];
  blocks: Block[];
}

@Component({
  selector: 'cse-print-view',
  templateUrl: './print-view.component.html',
  styleUrls: ['./print-view.component.scss'],
})
export class PrintViewComponent implements OnInit {
  private didTriggerPrint = false;

  public termCode$ = new BehaviorSubject<Loadable<TermCode, string>>(LOADING);

  public activeTermCodes$ = this.store
    .select(globalSelectors.terms.getActiveTermCodes)
    .pipe(ifSuccessThenUnwrap());

  public terms$ = this.store
    .select(globalSelectors.terms.getAll)
    .pipe(ifSuccessThenUnwrap());

  public data$: Observable<Loadable<Data, string>> = combineLatest([
    this.termCode$,
    this.terms$,
  ]).pipe(
    switchMap(([termCode, terms]) => {
      const colors = new GrayColorQueue();

      if (isLoading(termCode)) {
        return of(LOADING);
      } else if (isFailed(termCode)) {
        return of(termCode);
      } else if (!TermCode.isActive(terms.activeTermCodes, termCode.value)) {
        const description = TermCode.describe(termCode.value);
        return of(failed(`${description} is not an active term`));
      }

      const encoded = TermCode.encode(termCode.value);
      const term = terms.nonZero[encoded];
      return this.api.getCurrentCourses(termCode.value).pipe(
        map(raw => raw.map(CurrentCourse.fromCurry(term))),
        map(cs => cs.filter(c => c.enrolledStatus === 'enrolled')),
        map(courses =>
          success({
            termCode: termCode.value,
            courses: courses.sort(sortBy(course => course.shortCatalog)),
            blocks: courses.flatMap(Block.fromCurrent(colors)),
          }),
        ),
        catchError(() => of(failed(`Unable to load courses`))),
      );
    }),
    startWith(LOADING),
    share(),
  );

  constructor(
    private store: Store<GlobalState>,
    private route: ActivatedRoute,
    private api: ApiService,
  ) {}

  ngOnInit() {
    const rawTermCode = this.route.snapshot.paramMap.get('termCode');
    const parsedTermCode = TermCode.decodeOrNull(rawTermCode);

    if (parsedTermCode) {
      this.termCode$.next(success(parsedTermCode));
    } else if (rawTermCode === null || rawTermCode.trim() === '') {
      this.termCode$.next(failed('Missing term'));
    } else {
      this.termCode$.next(failed(`${rawTermCode} is not a valid term`));
    }
  }

  onReadyToPrint() {
    if (!this.didTriggerPrint) {
      // Since the window.print function freezes JS execution, the timeout
      // pushes the call to the back of the even loop and gives the DOM
      // some time to update with the schedule.
      this.didTriggerPrint = true;
      window.print();
    }
  }
}
