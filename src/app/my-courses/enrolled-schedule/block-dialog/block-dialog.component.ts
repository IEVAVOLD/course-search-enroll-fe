import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MeetingLocation, MeetingSchedule } from '@app/types/courses';

interface Data {
  catalogRef: string;
  title: string;
  schedules: Record<string, Array<MeetingSchedule | null>>;
  locations: Record<string, MeetingLocation[]>;
}

@Component({
  selector: 'cse-block-dialog',
  templateUrl: './block-dialog.component.html',
  styleUrls: ['./block-dialog.component.scss'],
  preserveWhitespaces: true,
})
export class BlockDialogComponent {
  public catalogRef: string;
  public title: string;
  public schedules: Array<[string, MeetingSchedule | null]>;
  public locations: Array<[string, MeetingLocation]>;

  constructor(@Inject(MAT_DIALOG_DATA) data: Data) {
    this.catalogRef = data.catalogRef;
    this.title = data.title;

    // Format each meeting schedule
    this.schedules = Object.entries(data.schedules).flatMap(tuple => {
      return tuple[1].map((s): [string, MeetingSchedule | null] => {
        return [tuple[0], s];
      });
    });

    // Format each meeting location
    this.locations = Object.entries(data.locations).flatMap(tuple => {
      return tuple[1].map((l): [string, MeetingLocation] => {
        return [tuple[0], l];
      });
    });
  }
}
