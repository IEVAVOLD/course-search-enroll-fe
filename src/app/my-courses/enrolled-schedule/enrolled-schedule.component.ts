import { Component, Input } from '@angular/core';
import { Block } from '@app/shared/pipes/layout-blocks.pipe';
import { DialogService } from '@app/shared/services/dialog.service';
import { CurrentCourse, CurrentMeeting } from '@app/types/courses';
import { BlockDialogComponent } from '@my-courses/enrolled-schedule/block-dialog/block-dialog.component';

@Component({
  selector: 'cse-enrolled-schedule',
  templateUrl: './enrolled-schedule.component.html',
  styleUrls: ['./enrolled-schedule.component.scss'],
})
export class EnrolledScheduleComponent {
  @Input() public courses!: CurrentCourse[];
  @Input() public blocks!: Block[];

  constructor(private dialog: DialogService) {}

  onBlockClick(info: { course: CurrentCourse; meeting: CurrentMeeting }) {
    this.dialog.small(BlockDialogComponent, {
      catalogRef: `${info.course.shortCatalog} (${info.meeting.sectionType})`,
      title: info.course.title,
      schedules: info.course.package.schedules,
      locations: info.course.package.locations,
    });
  }
}
