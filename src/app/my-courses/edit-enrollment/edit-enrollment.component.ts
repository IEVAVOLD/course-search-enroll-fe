import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GlobalState } from '@app/state';
import { CurrentCourse, EnrollmentPackage } from '@app/types/courses';
import { Store } from '@ngrx/store';
import * as actions from '@app/actions';

@Component({
  selector: 'cse-edit-enrollment',
  templateUrl: './edit-enrollment.component.html',
  styleUrls: ['./edit-enrollment.component.scss'],
})
export class EditEnrollmentComponent implements OnInit {
  public course: CurrentCourse;
  public pack: EnrollmentPackage;
  public creditChoice: null | {
    ctrl: UntypedFormControl;
    range: { min: number; max: number };
  } = null;

  public honorsChoice!: null | UntypedFormControl;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    data: { course: CurrentCourse; pack: EnrollmentPackage },
    private store: Store<GlobalState>,
    private dialogRef: MatDialogRef<EditEnrollmentComponent>,
  ) {
    this.course = data.course;
    this.pack = data.pack;
  }

  ngOnInit() {
    if (this.pack.credits.min !== this.pack.credits.max) {
      this.creditChoice = {
        ctrl: new UntypedFormControl(
          this.course.package.credits,
          Validators.required,
        ),
        range: this.pack.credits,
      };
    }

    if (this.course.package.honors !== 'notAvailable') {
      this.honorsChoice = new UntypedFormControl(this.course.package.honors);
    }
  }

  onSaveChanges() {
    const credits: number = this.creditChoice
      ? this.creditChoice.ctrl.value
      : this.course.package.credits;

    const honors = this.honorsChoice
      ? this.honorsChoice.value === 'chosen'
      : false;

    const edit = actions.courses.current.edit({
      course: this.course,
      credits,
      honors,
    });

    this.store.dispatch(edit);
    this.dialogRef.close('edited');
  }
}
