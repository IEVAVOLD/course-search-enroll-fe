import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { notNull, unsubscribeAll } from '@app/core/utils';
import {
  Course,
  CurrentCourse,
  CurrentMeeting,
  Meeting,
  PartialPackageChoices,
  RawInstructor,
  RoadmapCourse,
  RoadmapMeeting,
} from '@app/types/courses';
import {
  BehaviorSubject,
  combineLatest,
  merge,
  Observable,
  of,
  Subscription,
} from 'rxjs';
import * as s from '@app/types/schema';
import { map, startWith } from 'rxjs/operators';
import { EnrollRequest } from '@app/types/enroll';

export type Credits = Course['credits'];

export interface CourseWithEnrollOptions extends Course {
  package: null | {
    classNumber: number;
    relatedClassNumbers: number[];
  };
  remainingTasks: Observable<string[]>;
  meetings: Meeting[];
  instructors: RawInstructor[];
  hasPackage: boolean;
  sectionCatalogRefs: string;

  waitlistCtrl: UntypedFormControl | null;
  honorsCtrl: UntypedFormControl | null;
  permissionNumberCtrl: UntypedFormControl | null;

  /**
   * This field is the source of truth for a course's credit value. NOT the
   * `creditsCtrl` field.
   *
   * If a course only supports a single credit value OR the student has already
   * selected a credit value, `min` and `max` will be the same number. If the
   * student has NOT selected a credit value, `min` and `max` will contain the
   * course's full range of valid credit values.
   *
   * This observable is used for calculating the credit sum at the bottom of the
   * dialog AND for calculating the course sum at the top of each course review
   * panel.
   */
  creditValue: BehaviorSubject<Credits>;

  /**
   * A form control created for all courses, even ones that do not support a
   * credit choice. This field is NOT the source of truth when calculating
   * credit sums or submitting the enroll request.
   */
  creditsCtrl: UntypedFormControl;

  /**
   * An array of all the credit values supported by a course. The credit
   * dropdown UI is shown for all courses where this property is longer than 1.
   */
  possibleCreditValues: number[];
}

@Component({
  selector: 'cse-review-enroll-dialog',
  templateUrl: './review-enroll-dialog.component.html',
  styleUrls: ['./review-enroll-dialog.component.scss'],
})
export class ReviewEnrollDialogComponent implements OnInit, OnDestroy {
  public withOptions: CourseWithEnrollOptions[] = [];
  public subscriptions: Subscription[] = [];
  public totalCredits!: Observable<Credits>;
  public remainingTasks!: Observable<string[]>;
  public hasRemainingTasks!: Observable<boolean>;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public courses: Array<RoadmapCourse | CurrentCourse>,
    public dialog: MatDialogRef<ReviewEnrollDialogComponent>,
  ) {}

  ngOnInit() {
    /**
     * This dialog will be passed a list of roadmap courses that the user want
     * to enroll in. For some of these courses the user may have chosen an
     * enrollment package that supports some enrollment options. These might
     * include:
     *
     * - Taking the course with honors
     * - Joining a waitlist if the course is full
     * - Picking a credit value from a range of acceptable values
     *
     * The enrollment dialog provides the user a final chance to review and
     * update these choices for all courses. The choices are presented to the
     * user as a set of form controls. The following loop creates all of these
     * form controls, populates the controls with appropriate initial values,
     * and creates any validation logic.
     *
     * This ultimately leads to a `withOptions` array that contains a list of
     * objects where each object contains info about one of the roadmap courses
     * and any form controls associated with that course. The shapes of these
     * objects are defined in the `CourseWithEnrollOptions` type.
     */
    this.withOptions = this.courses.map(original => {
      const meetings: Array<RoadmapMeeting | CurrentMeeting> =
        original.package?.classMeetings ?? [];

      const instructors = CurrentCourse.is(original)
        ? // For waitlisted courses
          []
        : // For cart courses
          (meetings as RoadmapMeeting[])
            .map(meeting => meeting.instructor)
            .filter(notNull)
            .reduce(RawInstructor.dedupe, []);

      const sectionCatalogRefs = CurrentCourse.is(original)
        ? // For waitlisted courses
          original.sections
            .map(section => `${section.sectionType} ${section.sectionNumber}`)
            .filter(unique)
            .join(', ')
        : // For cart courses
          (meetings as RoadmapMeeting[])
            .map(meeting => `${meeting.sectionType} ${meeting.sectionNumber}`)
            .filter(unique)
            .join(', ');

      const waitlistVal = CurrentCourse.is(original)
        ? // For waitlisted courses
          original.package.waitlist
        : // For cart courses
          original.package?.choices?.waitlist ?? 'notAvailable';
      const waitlistCtrl =
        waitlistVal !== 'notAvailable'
          ? new UntypedFormControl(waitlistVal)
          : null;

      const honorsVal = CurrentCourse.is(original)
        ? // For waitlisted courses
          original.package.honors
        : // For cart courses
          original.package?.choices?.honors ?? 'notAvailable';
      const honorsCtrl =
        honorsVal !== 'notAvailable' ? new UntypedFormControl(honorsVal) : null;

      const needsClassPermissionNumber =
        original.package?.classPermissionNumberNeeded === true;
      const permissionNumberCtrl = needsClassPermissionNumber
        ? new UntypedFormControl(original.classPermissionNumber)
        : null;

      /**
       * The credit form controls and RxJS subscriptions are a bit more complex
       * than the waitlist and honors options since any updates to a credit
       * value need to propagate to the total credit sum at the bottom of the
       * dialog and the course credit value in the course summary element.
       */

      const courseSupportsRange = original.credits.min !== original.credits.max;
      const creditsVal: PartialPackageChoices['credits'] | null =
        CurrentCourse.is(original)
          ? // For waitlisted courses
            {
              hasChoice: original.credits.max !== original.credits.min,
              total: original.package.credits,
            }
          : // For cart courses
            original.package?.choices?.credits ?? null;
      const choiceWasMade =
        creditsVal?.hasChoice &&
        isValidCreditValue(creditsVal?.total, original.credits);
      const creditValue: BehaviorSubject<Credits> =
        courseSupportsRange && choiceWasMade
          ? new BehaviorSubject({
              min: creditsVal?.total as number,
              max: creditsVal?.total as number,
            })
          : new BehaviorSubject(original.credits);

      const creditsCtrl = new UntypedFormControl(
        creditsVal?.hasChoice ? creditsVal?.total : original.credits.min,
      );

      const { min, max } = RoadmapCourse.is(original)
        ? original.package?.credits ?? original.credits
        : original.credits;

      const possibleCreditValues = Array.from({ length: max - min + 1 }).map(
        (_, index) => index + original.credits.min,
      );

      /**
       * Whenever the credit form control changes make sure the new value falls
       * within the rance of acceptable credit values and if so, update the
       * `creditValue` Subject with the new value. Recall that the `creditValue`
       * is the source-of-truth for what credit value will be used when
       * enrolling, NOT the `creditsCtrl`.
       */
      this.subscriptions.push(
        creditsCtrl.valueChanges.subscribe(newVal => {
          if (isValidCreditValue(newVal, original.credits)) {
            creditValue.next({ min: newVal, max: newVal });
          }
        }),
      );

      /**
       * Whenever the course enrollment options change, check if the course has
       * all the info ready to start enrolling.
       */
      const remainingTasks = merge(
        waitlistCtrl?.valueChanges ?? of(),
        honorsCtrl?.valueChanges ?? of(),
        permissionNumberCtrl?.valueChanges ?? of(),
        creditsCtrl.valueChanges,
      ).pipe(
        startWith(null),
        map(() => courseToTasks(course)),
      );

      const pack: CourseWithEnrollOptions['package'] = CurrentCourse.is(
        original,
      )
        ? // For waitlisted courses
          {
            classNumber: original.package.classNumber,
            relatedClassNumbers: [
              original.package.relatedClassNumber1,
              original.package.relatedClassNumber2,
            ].filter(notNull),
          }
        : // For cart courses
          original.package;

      const course: CourseWithEnrollOptions = {
        ...original,
        package: pack,
        remainingTasks,
        hasPackage: !!original.package,
        meetings: meetings.reduce(Meeting.dedupe, []),
        instructors,
        sectionCatalogRefs,

        waitlistCtrl,
        honorsCtrl,

        creditValue,
        creditsCtrl,
        possibleCreditValues,
        permissionNumberCtrl,
      };

      return course;
    });

    /**
     * Create an observable that sums the latest credit values for each course.
     * If a course supports a range of values and a single value hasn't been
     * chosen yet, the sum will also be a range.
     */
    this.totalCredits = combineLatest(
      this.withOptions.map(({ creditValue }) => creditValue),
    ).pipe(
      map(values =>
        values.reduce(
          (acc, next) => ({ min: acc.min + next.min, max: acc.max + next.max }),
          { min: 0, max: 0 },
        ),
      ),
    );

    this.remainingTasks = combineLatest(
      this.withOptions.map(({ remainingTasks }) => remainingTasks),
    ).pipe(map(all => all.flat()));

    this.hasRemainingTasks = this.remainingTasks.pipe(
      map(tasks => tasks.length > 0),
    );
  }

  ngOnDestroy() {
    unsubscribeAll(this.subscriptions);
  }

  async onFinishedReview() {
    /**
     * For each course selected for enrollment, convert that course to the
     * payload expected by the enroll backend.
     */

    this.dialog.close(
      this.withOptions
        .map(course => [course, toEnrollRequestOrNull(course)] as const)
        .filter(notNull),
    );
  }
}

const toEnrollRequestOrNull = (
  options: CourseWithEnrollOptions,
): EnrollRequest | null => {
  const pack = options.package;

  if (pack === null) {
    return null;
  }

  const subjectCode = options.subject.code;
  const courseId = options.courseId;
  const classNumber = `${pack.classNumber}`;
  const credits = options.creditValue.value.min;
  const waitlist = options.waitlistCtrl?.value === 'chosen' ? true : null;
  const honors = options.honorsCtrl?.value === 'chosen' ? true : null;
  const relatedClassNumber1 = toStringIfNumber(
    options.package?.relatedClassNumbers[0],
  );
  const relatedClassNumber2 = toStringIfNumber(
    options.package?.relatedClassNumbers[1],
  );
  const classPermissionNumber = options.permissionNumberCtrl
    ? parseInt(options.permissionNumberCtrl.value, 10)
    : null;

  return {
    subjectCode,
    courseId,
    classNumber,
    options: {
      credits,
      waitlist,
      honors,
      relatedClassNumber1,
      relatedClassNumber2,
      classPermissionNumber,
    },
  };
};

const toStringIfNumber = (thing: unknown): string | null => {
  if (typeof thing === 'number') {
    return `${thing}`;
  }
  return null;
};

const courseToTasks = (course: CourseWithEnrollOptions): string[] => {
  const tasks: string[] = [];
  if (!course.hasPackage) {
    tasks.push(`${course.shortCatalog} is missing an enrollment package`);
  } else if (!isValidCreditValue(course.creditsCtrl.value, course.credits)) {
    tasks.push(`${course.shortCatalog} is missing a credit value`);
  }

  return tasks;
};

export const isValidCreditValue = (
  val: unknown,
  range: Course['credits'],
): val is number => {
  return s.number.matches(val) && val >= range.min && val <= range.max;
};

const unique = <T>(next: T, index: number, all: T[]) => {
  return index === all.indexOf(next);
};
