import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'cse-category-indicator',
  templateUrl: './category-indicator.component.html',
  styleUrls: ['./category-indicator.component.scss'],
})
export class CategoryIndicatorComponent {
  @Input() public icon!: string;
  @Input() public name!: string;
  @Input() public selected!: boolean;
  @Input() public state!: 'loading' | 'error' | number;
  @Output() public whenSelected = new EventEmitter<void>();
}
