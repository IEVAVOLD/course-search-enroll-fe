import { createReducer, createAction, on, props } from '@ngrx/store';
import {
  RoadmapCourse,
  CurrentCourse,
  CourseRef,
  SavedCourse,
} from '@app/types/courses';
import { produce } from 'immer';
import * as actions from './actions';
import * as globalActions from '@app/actions';
import { MyCourses, EMPTY_STATE } from './state';

export const noop = createAction('[MY COURSES] noop');

namespace lifecycle {
  export const init = on<MyCourses, [typeof actions.lifecycle.init]>(
    actions.lifecycle.init,
    state => {
      return produce(state, draft => {
        if (draft.wasInitialized === 'ready') {
          draft.wasInitialized = 'pending';
        }
      });
    },
  );

  export const setTerm = on<
    MyCourses,
    [typeof globalActions.currentTermCode.set]
  >(globalActions.currentTermCode.set, state => {
    return produce(state, draft => {
      draft.wasInitialized = 'done';
    });
  });
}

export const showCart = createAction('[MY COURSES] show cart');
const onShowCart = on<MyCourses, [typeof showCart]>(
  showCart,
  (state): MyCourses => {
    return produce(state, draft => {
      if (state.chosenCategory !== 'cart') {
        draft.chosenCategory = 'cart';
        draft.areDetailsShown = null;
        draft.checked = [];
      }
    });
  },
);

export const showEnrolled = createAction('[MY COURSES] show enrolled');
const onShowEnrolled = on<MyCourses, [typeof showEnrolled]>(
  showEnrolled,
  (state): MyCourses => {
    return produce(state, draft => {
      if (state.chosenCategory !== 'enrolled') {
        draft.chosenCategory = 'enrolled';
        draft.areDetailsShown = null;
        draft.checked = [];
      }
    });
  },
);

export const showWaitlisted = createAction('[MY COURSES] show waitlisted');
const onShowWaitlisted = on<MyCourses, [typeof showWaitlisted]>(
  showWaitlisted,
  (state): MyCourses => {
    return produce(state, draft => {
      if (state.chosenCategory !== 'waitlisted') {
        draft.chosenCategory = 'waitlisted';
        draft.areDetailsShown = null;
        draft.checked = [];
      }
    });
  },
);

export const showDropped = createAction('[MY COURSES] show dropped');
const onShowDropped = on<MyCourses, [typeof showDropped]>(
  showDropped,
  (state): MyCourses => {
    return produce(state, draft => {
      if (state.chosenCategory !== 'dropped') {
        draft.chosenCategory = 'dropped';
        draft.areDetailsShown = null;
        draft.checked = [];
      }
    });
  },
);

export const showSavedForLater = createAction(
  '[MY COURSES] show savedForLater',
);
const onShowSavedForLater = on<MyCourses, [typeof showSavedForLater]>(
  showSavedForLater,
  (state): MyCourses => {
    return produce(state, draft => {
      if (state.chosenCategory !== 'saved') {
        draft.chosenCategory = 'saved';
        draft.areDetailsShown = null;
        draft.checked = [];
      }
    });
  },
);

export const showDetails = createAction(
  '[MY COURSES] show details',
  props<{ course: RoadmapCourse | CurrentCourse | SavedCourse }>(),
);
const onShowDetails = on<MyCourses, [typeof showDetails]>(
  showDetails,
  (state, { course }) => {
    return produce(state, draft => {
      draft.areDetailsShown = course.ref;
      draft.arePackagesShown = false;
    });
  },
);

export const saveForLater = createAction(
  '[MY COURSES] save for later',
  props<{ course: any }>(),
);
const onSaveForLater = on<MyCourses, [typeof saveForLater]>(
  saveForLater,
  (state): MyCourses => {
    // FIXME
    return state;
  },
);

namespace packages {
  export const show = on<MyCourses, [typeof actions.packages.show]>(
    actions.packages.show,
    (state): MyCourses => {
      return produce(state, draft => {
        draft.arePackagesShown = true;
      });
    },
  );

  export const hide = on<MyCourses, [typeof actions.packages.hide]>(
    actions.packages.hide,
    (state): MyCourses => {
      return produce(state, draft => {
        draft.arePackagesShown = false;
      });
    },
  );
}

export const selectCourse = createAction(
  '[MY COURSES] select course',
  props<{ ref: CourseRef; isChecked: boolean }>(),
);
const onSelectCourse = on<MyCourses, [typeof selectCourse]>(
  selectCourse,
  (state, { ref, isChecked }): MyCourses => {
    return produce(state, draft => {
      if (isChecked) {
        const isAlreadyChecked = state.checked.some(CourseRef.equalsCurry(ref));
        if (!isAlreadyChecked) {
          draft.checked.push(ref);
        }
      } else {
        draft.checked = draft.checked.filter(r => !CourseRef.equals(ref, r));
      }
    });
  },
);

const onSelectCourses = on<MyCourses, [typeof actions.checked.set]>(
  actions.checked.set,
  (state, { refs }): MyCourses => {
    return produce(state, draft => {
      draft.checked = refs;
    });
  },
);

export const clearChecked = createAction('my-courses/clear-checked');

const onClearChecked = on<MyCourses, [typeof clearChecked]>(
  clearChecked,
  state => {
    return produce(state, draft => {
      draft.checked = [];
    });
  },
);

export const hideDetails = createAction('[MY COURSES] hide details');

namespace schedule {
  export const show = on<MyCourses, [typeof actions.schedule.show]>(
    actions.schedule.show,
    (state): MyCourses => {
      return produce(state, draft => {
        draft.isScheduleShown = true;
      });
    },
  );

  export const hide = on<MyCourses, [typeof actions.schedule.hide]>(
    actions.schedule.hide,
    (state): MyCourses => {
      return produce(state, draft => {
        draft.isScheduleShown = false;
      });
    },
  );
}

const onCloseSidebars = on<
  MyCourses,
  [typeof globalActions.courses.current.drop, typeof hideDetails]
>(globalActions.courses.current.drop, hideDetails, (state): MyCourses => {
  return produce(state, draft => {
    draft.areDetailsShown = null;
    draft.arePackagesShown = false;
    draft.isScheduleShown = false;
  });
});

export const reducer = createReducer(
  EMPTY_STATE,

  lifecycle.init,
  lifecycle.setTerm,

  onCloseSidebars,

  onShowCart,
  onShowEnrolled,
  onShowWaitlisted,
  onShowDropped,
  onShowSavedForLater,
  onSelectCourse,
  onSelectCourses,
  onClearChecked,
  onSaveForLater,

  onShowDetails,

  packages.show,
  packages.hide,

  schedule.show,
  schedule.hide,
);
