import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { showDetails } from '@my-courses/store/reducer';
import {
  mergeMap,
  map,
  filter,
  withLatestFrom,
  switchMap,
  catchError,
  tap,
  first,
} from 'rxjs/operators';
import { TermCode } from '@app/types/terms';
import { Store } from '@ngrx/store';
import * as globalSelectors from '@app/selectors';
import * as viewSelectors from '@my-courses/store/selectors';
import { ifSuccessThenUnwrap } from '@app/core/utils';
import { forkJoin, of, Observable, pipe } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as globalActions from '@app/actions';
import * as actions from './actions';
import { GlobalState } from '@app/state';
import { ApiService } from '@app/services/api.service';
import { isSuccess } from 'loadable.ts';
import { AnalyticsService } from '@app/services/analytics.service';

@Injectable()
export class Effects {
  constructor(
    private actions$: Actions,
    private store: Store<GlobalState>,
    private snackbar: MatSnackBar,
    private api: ApiService,
    private analytics: AnalyticsService,
  ) {}

  private chosenTermCode: Observable<TermCode> = this.store
    .select(globalSelectors.currentTermCode.get)
    .pipe(ifSuccessThenUnwrap());

  @Effect()
  LifecycleInit$ = this.actions$.pipe(
    ofType(actions.lifecycle.init),
    mergeMap(() => {
      return forkJoin([
        this.store.select(globalSelectors.currentTermCode.get).pipe(
          filter(isSuccess),
          first(),
          map(loadable => loadable.value),
        ),
      ]);
    }),
    withLatestFrom(this.store.select(viewSelectors.getWasInitialized)),
    filter(([, wasInitialized]) => wasInitialized !== 'done'),
    switchMap(([[termCode]]) => {
      return [
        globalActions.courses.loadIfNotAlready({ termCode }),
        globalActions.saved.load(),
      ];
    }),
  );

  @Effect()
  setTerm$ = this.actions$.pipe(
    ofType(globalActions.currentTermCode.set),
    map(({ currentTermCode }) =>
      globalActions.courses.loadIfNotAlready({ termCode: currentTermCode }),
    ),
  );

  @Effect()
  showDetails$ = this.actions$.pipe(
    ofType(showDetails),
    filter(({ course }) => course.details.mode !== 'loaded'),
    map(({ course }) => globalActions.details.load({ course })),
  );

  @Effect()
  RoadmapMoveToSaved$ = this.actions$.pipe(
    ofType(actions.roadmap.moveToSaved),
    mergeMap(({ course }) => {
      const trueIfSuccessful = pipe(
        map(() => true),
        catchError(() => of(false)),
      );

      return forkJoin([
        this.api.deleteRoadmapCourses(course).pipe(trueIfSuccessful),
        this.api
          .postFavorite(course.subject.code, course.courseId)
          .pipe(trueIfSuccessful),
      ]).pipe(
        tap(() =>
          this.analytics.event(
            'Cart',
            'move-to-favorites',
            course.shortCatalog,
          ),
        ),
        tap(([didRemove, didSave]) => {
          const msg =
            didRemove && didSave
              ? `Saved ${course.shortCatalog} for later`
              : `Unable to save ${course.shortCatalog} for later`;

          this.snackbar.open(msg);
        }),
        switchMap(() => {
          return [
            globalActions.courses.roadmap.load({ termCode: course.termCode }),
            globalActions.saved.load(),
          ];
        }),
      );
    }),
  );

  @Effect()
  SavedMoveToCart$ = this.actions$.pipe(
    ofType(actions.saved.moveToCart),
    withLatestFrom(this.chosenTermCode),
    mergeMap(([{ course }, termCode]) => {
      const trueIfSuccessful = pipe(
        map(() => true),
        catchError(() => of(false)),
      );

      return forkJoin([
        this.api
          .postRoadmapCourseWithoutPackage({ ...course, termCode })
          .pipe(trueIfSuccessful),
        this.api.deleteFavorite(course).pipe(trueIfSuccessful),
      ]).pipe(
        tap(() =>
          this.analytics.event(
            'Favorites',
            'move-to-cart',
            course.shortCatalog,
          ),
        ),
        tap(([didAdd, didRemove]) => {
          const tc = TermCode.encode(termCode);
          const msg =
            didAdd && didRemove
              ? `Added ${course.shortCatalog} to your ${tc} cart`
              : `Unable to add ${course.shortCatalog} to your ${tc} cart`;
          this.snackbar.open(msg);
        }),
        switchMap(() => {
          return [
            globalActions.courses.roadmap.load({ termCode }),
            globalActions.saved.load(),
          ];
        }),
      );
    }),
  );

  @Effect()
  Swap$ = this.actions$.pipe(
    ofType(actions.swap.start),
    mergeMap(({ enroll, drop }) => {
      return this.api.postSwap(enroll, drop).pipe(
        switchMap(messages => {
          const firstMessage = messages[0];

          if (!firstMessage) {
            // Don't think this case should happen but we're going to handle it.
            return [
              actions.swap.error({
                description: 'No enrollment messages returned after the swap',
              }),
            ];
          }

          if (firstMessage.messageSeverity !== 'E') {
            this.analytics.event(
              'Enrolled',
              'swap-courses',
              `enroll "${enroll.shortCatalog}", drop "${drop.shortCatalog}"`,
            );

            const termCode = enroll.termCode;
            return [
              actions.swap.done({ didEnroll: enroll, didDrop: drop }),
              globalActions.courses.load({ termCode }),
            ];
          }

          return [
            actions.swap.error({ description: firstMessage.description }),
          ];
        }),
      );
    }),
  );
}
