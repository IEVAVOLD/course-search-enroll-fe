import { createFeatureSelector, createSelector } from '@ngrx/store';
import { MyCourses, CategoryName } from '@my-courses/store/state';
import { UNLOADED } from '@app/types/loadable';
import { TermCode } from '@app/types/terms';
import {
  Course,
  RoadmapCourse,
  CurrentCourse,
  HasLazyPackages,
  SavedCourse,
  CourseRef,
} from '@app/types/courses';
import { isSuccess, Loadable } from 'loadable.ts';
import * as globalSelectors from '@app/selectors';

const getMyCoursesState = createFeatureSelector<MyCourses>('myCourses');

export const getWasInitialized = createSelector(
  getMyCoursesState,
  state => state.wasInitialized,
);

export const getChosenCategoryName = createSelector(
  getMyCoursesState,
  (state: MyCourses) => state.chosenCategory,
);

export const getIsValidating = createSelector(
  globalSelectors.courses.getAll,
  globalSelectors.currentTermCode.get,
  (state, currentTermCode): boolean => {
    if (!isSuccess(currentTermCode)) {
      return false;
    }

    const encoded = TermCode.encode(currentTermCode.value);
    const courses = state[encoded];

    if (!courses) {
      return false;
    }

    return courses.isValidating;
  },
);

export const getHintToRevalidate = createSelector(
  globalSelectors.courses.getAll,
  globalSelectors.currentTermCode.get,
  (state, currentTermCode): boolean => {
    if (!isSuccess(currentTermCode)) {
      return false;
    }

    const encoded = TermCode.encode(currentTermCode.value);
    const courses = state[encoded];

    if (!courses) {
      return false;
    }

    return courses.hintToRevalidate;
  },
);

export const isChosenCategory = createSelector(
  getMyCoursesState,
  (state: MyCourses, props: CategoryName): boolean => {
    return state.chosenCategory === props;
  },
);

export const getRoadmapCourses = createSelector(
  globalSelectors.courses.getAll,
  globalSelectors.currentTermCode.get,
  (state, currentTermCode): Loadable<RoadmapCourse[]> | null => {
    if (!isSuccess(currentTermCode)) {
      return currentTermCode;
    }

    const encoded = TermCode.encode(currentTermCode.value);
    const courses = state[encoded];

    if (!courses) {
      return null;
    }

    return courses.roadmap;
  },
);

export const getCurrentCourses = createSelector(
  globalSelectors.courses.getAll,
  globalSelectors.currentTermCode.get,
  (allCourses, currentTermCode): Loadable<CurrentCourse[]> | null => {
    if (!isSuccess(currentTermCode)) {
      return currentTermCode;
    }

    const encoded = TermCode.encode(currentTermCode.value);
    const courses = allCourses[encoded];

    if (!courses) {
      return null;
    }

    return courses.current;
  },
);

export const getSavedCourses = createSelector(
  globalSelectors.saved.getAll,
  (state): Loadable<SavedCourse[]> | null => {
    return state;
  },
);

export const hasChecked = createSelector(
  getMyCoursesState,
  (state: MyCourses): boolean => state.checked.length > 0,
);

export const getChecked = createSelector(
  getMyCoursesState,
  (state: MyCourses): CourseRef[] => state.checked,
);

export const getDetailsCourseRef = createSelector(
  getMyCoursesState,
  (state: MyCourses): CourseRef | null => state.areDetailsShown,
);

export const getCourseShowingDetails = createSelector(
  getMyCoursesState,
  globalSelectors.courses.getAll,
  globalSelectors.saved.getAll,
  ({ areDetailsShown: ref }, courses, saved): Course | null => {
    if (ref === null) {
      return null;
    }

    if (ref.kind === 'saved') {
      if (saved && isSuccess(saved)) {
        return (
          saved.value.find(course => {
            return course.ref.savedId === ref.savedId;
          }) ?? null
        );
      }

      return null;
    }

    const encoded = TermCode.encode(ref.termCode);
    const term = courses[encoded];

    if (ref.kind === 'roadmap') {
      if (term?.roadmap && isSuccess(term.roadmap)) {
        return (
          term.roadmap.value.find(course => {
            return course.ref.roadmapId === ref.roadmapId;
          }) ?? null
        );
      }

      return null;
    }

    if (ref.kind === 'current') {
      if (term?.current && isSuccess(term.current)) {
        return (
          term.current.value.find(course => {
            return course.currentId === ref.currentId;
          }) ?? null
        );
      }

      return null;
    }

    return null;
  },
);

/**
 * DETAILS SELECTORS
 */

export const details = createSelector(
  getCourseShowingDetails,
  (course: Course | null) => {
    if (course === null) {
      return UNLOADED;
    } else {
      return course.details;
    }
  },
);

export const detailsMode = createSelector(details, ({ mode }) => mode);

/**
 * SECTIONS SELECTORS
 */

export const getPackages = createSelector(
  getCourseShowingDetails,
  (course): HasLazyPackages['packages'] => {
    if ((course as any)?.hasOwnProperty('packages')) {
      return (course as any).packages as HasLazyPackages['packages'];
    } else {
      return null;
    }
  },
);

export const isPackagesShown = createSelector(
  getMyCoursesState,
  (state): boolean => state.arePackagesShown,
);

/**
 * SCHEDULE SELECTORS
 */

export const isScheduleShown = createSelector(
  getMyCoursesState,
  (state): boolean => state.isScheduleShown,
);
