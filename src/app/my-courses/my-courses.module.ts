import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyCoursesViewComponent } from './my-courses-view/my-courses-view.component';
import { StoreModule } from '@ngrx/store';
import { reducer } from './store/reducer';
import { SharedModule } from '@app/shared/shared.module';
import { Effects } from './store/effects';
import { EffectsModule } from '@ngrx/effects';
import { CategoryActionsComponent } from './my-courses-view/category-actions/category-actions.component';
import { CategoryIndicatorComponent } from './category-indicator/category-indicator.component';
import { SwapDialogComponent } from './swap-dialog/swap-dialog.component';
import { EnrolledScheduleComponent } from './enrolled-schedule/enrolled-schedule.component';
import { BlockDialogComponent } from './enrolled-schedule/block-dialog/block-dialog.component';
import { PrintViewComponent } from './print-view/print-view.component';
import { ReviewEnrollDialogComponent } from './review-enroll-dialog/review-enroll-dialog.component';
import { EditEnrollmentComponent } from './edit-enrollment/edit-enrollment.component';

const routes: Routes = [
  {
    path: '',
    component: MyCoursesViewComponent,
  },
  {
    path: 'print/:termCode',
    data: {
      scrollStrategy: 'normal',
    },
    component: PrintViewComponent,
  },
];

@NgModule({
  declarations: [
    MyCoursesViewComponent,
    CategoryActionsComponent,
    CategoryIndicatorComponent,
    SwapDialogComponent,
    EnrolledScheduleComponent,
    BlockDialogComponent,
    PrintViewComponent,
    ReviewEnrollDialogComponent,
    EditEnrollmentComponent,
  ],
  imports: [
    SharedModule,
    StoreModule.forFeature('myCourses', reducer),
    EffectsModule.forFeature([Effects]),
    RouterModule.forChild(routes),
  ],
})
export class MyCoursesModule {}
