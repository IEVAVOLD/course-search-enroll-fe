import { Component, Inject, OnDestroy } from '@angular/core';
import { UntypedFormControl, ValidatorFn, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GlobalState } from '@app/state';
import {
  CourseRef,
  CurrentCourse,
  EnrolledStatus,
  RoadmapCourse,
  RoadmapCourseReady,
} from '@app/types/courses';
import { Store } from '@ngrx/store';
import * as viewSelectors from '@my-courses/store/selectors';
import * as viewActions from '@my-courses/store/actions';
import * as globalSelectors from '@app/selectors';
import * as globalActions from '@app/actions';
import { isSuccess, Loadable, LOADING, success } from 'loadable.ts';
import { Observable, Subscription } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { ifSuccessThenUnwrap, unsubscribeAll } from '@app/core/utils';
import { TermCode } from '@app/types/terms';
import * as s from '@app/types/schema';
import { Actions, ofType } from '@ngrx/effects';

export interface SwapDefaults {
  toEnrollIn?: CourseRef;
  toDrop?: CourseRef;
}

@Component({
  selector: 'cse-swap-dialog',
  templateUrl: './swap-dialog.component.html',
  styleUrls: ['./swap-dialog.component.scss'],
})
export class SwapDialogComponent implements OnDestroy {
  public step: 'pick' | 'waiting' | 'success' | 'error' = 'pick';
  public errorDescription: string | null = null;
  public termCode$: Observable<TermCode>;
  public enrollCandidates$: Observable<Loadable<RoadmapCourse[]>>;
  public dropCandidates$: Observable<Loadable<CurrentCourse[]>>;

  public enrollChoiceCtrl: UntypedFormControl;
  public dropChoiceCtrl: UntypedFormControl;

  private subscriptions: Subscription[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) defaults: SwapDefaults,
    private store: Store<GlobalState>,
    actions: Actions,
  ) {
    const hasCourseRef = (ref: CourseRef) => {
      return (candidate: RoadmapCourse | CurrentCourse) => {
        return CourseRef.matches(ref)(candidate);
      };
    };

    this.termCode$ = this.store
      .select(globalSelectors.currentTermCode.get)
      .pipe(ifSuccessThenUnwrap());

    this.enrollCandidates$ = this.store
      .select(viewSelectors.getRoadmapCourses)
      .pipe(map(unwrapCourses));

    this.dropCandidates$ = this.store
      .select(viewSelectors.getCurrentCourses, 'enrolled')
      .pipe(map(unwrapCourses), map(withCurrentStatus('enrolled')));

    this.enrollChoiceCtrl = new UntypedFormControl(null, [
      Validators.required,
      hasEnrollmentPackage,
    ]);
    this.subscriptions.push(
      this.enrollCandidates$
        .pipe(ifSuccessThenUnwrap())
        .subscribe(candidates => {
          if (defaults.toEnrollIn) {
            const candidate = candidates.find(
              hasCourseRef(defaults.toEnrollIn),
            );
            this.enrollChoiceCtrl.setValue(candidate ?? null);

            // If the control was populated with a course, Angular won't show
            // form validation errors unless the input is touched. If the control
            // has a course, we want validation errors shown to manually mark
            // the control as touched.
            if (candidate) {
              this.enrollChoiceCtrl.markAsTouched();
            }
          }
        }),
    );

    this.dropChoiceCtrl = new UntypedFormControl(null, Validators.required);
    this.subscriptions.push(
      this.dropCandidates$.pipe(ifSuccessThenUnwrap()).subscribe(candidates => {
        if (defaults.toDrop) {
          const candidate = candidates.find(hasCourseRef(defaults.toDrop));
          this.dropChoiceCtrl.setValue(candidate ?? null);

          // If the control was populated with a course, Angular won't show
          // form validation errors unless the input is touched. If the control
          // has a course, we want validation errors shown to manually mark
          // the control as touched.
          if (candidate) {
            this.dropChoiceCtrl.markAsTouched();
          }
        }
      }),
    );

    this.subscriptions.push(
      actions.pipe(ofType(viewActions.swap.done)).subscribe(() => {
        this.step = 'success';
      }),
    );

    this.subscriptions.push(
      actions
        .pipe(ofType(viewActions.swap.error))
        .subscribe(({ description }) => {
          this.step = 'error';
          this.errorDescription = description;
        }),
    );
  }

  ngOnDestroy() {
    unsubscribeAll(this.subscriptions);
  }

  onStartSwap() {
    const enroll: RoadmapCourseReady = this.enrollChoiceCtrl.value;
    const drop: CurrentCourse = this.dropChoiceCtrl.value;

    this.store.dispatch(
      viewActions.swap.start({
        enroll,
        drop,
      }),
    );

    this.step = 'waiting';
  }

  async onRetryRoadmapCourses() {
    const termCode = await this.termCode$.pipe(first()).toPromise();
    this.store.dispatch(globalActions.courses.roadmap.load({ termCode }));
  }

  async onRetryCurrentCourses() {
    const termCode = await this.termCode$.pipe(first()).toPromise();
    this.store.dispatch(globalActions.courses.current.load({ termCode }));
  }
}

const unwrapCourses = <T>(loadable: Loadable<T[]> | null) => {
  if (loadable === null) {
    return LOADING;
  } else if (isSuccess(loadable)) {
    return success(loadable.value);
  } else {
    return loadable;
  }
};

const hasEnrollmentPackage: ValidatorFn = ctrl => {
  const hasNonNullPackage = s.object({
    package: s.object({}),
  });

  const hasCreditValue = s.object({
    package: s.object({
      choices: s.object({
        credits: s.object({ total: s.number }),
      }),
    }),
  });

  if (hasCreditValue.matches(ctrl.value)) {
    return null;
  } else if (hasNonNullPackage.matches(ctrl.value)) {
    return { missingCredits: true };
  } else {
    return { missingPackage: true };
  }
};

const withCurrentStatus = (name: EnrolledStatus) => {
  return (
    category: Loadable<CurrentCourse[]> | null,
  ): Loadable<CurrentCourse[]> => {
    if (!category) {
      return LOADING;
    }

    if (!isSuccess(category)) {
      return category;
    } else {
      return success(
        category.value.filter(course => course.enrolledStatus === name),
      );
    }
  };
};
