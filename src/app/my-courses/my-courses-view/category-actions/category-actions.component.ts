import { Component } from '@angular/core';

@Component({
  selector: 'cse-category-actions-component',
  templateUrl: './category-actions.component.html',
  styleUrls: ['./category-actions.component.scss'],
})
export class CategoryActionsComponent {}
