import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import {
  trigger,
  transition,
  animate,
  state,
  style,
} from '@angular/animations';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Breakpoints } from '@app/shared/breakpoints';
import { Store } from '@ngrx/store';
import * as viewActions from '@my-courses/store/reducer';
import * as globalSelectors from '@app/selectors';
import * as viewSelectors from '@my-courses/store/selectors';
import { Subscription, ReplaySubject, combineLatest, Observable } from 'rxjs';
import {
  ifSuccessThenUnwrap,
  notNull,
  pluralize,
  unsubscribeAll,
} from '@app/core/utils';
import { TermCode } from '@app/types/terms';
import { MatSelectChange } from '@angular/material/select';
import {
  map,
  startWith,
  pairwise,
  take,
  first,
  withLatestFrom,
  shareReplay,
  filter,
} from 'rxjs/operators';
import { MatCheckboxChange } from '@angular/material/checkbox';
import {
  Course,
  CourseRef,
  CurrentCourse,
  EnrolledStatus,
  EnrollmentPackage,
  HasCourseRef,
  RoadmapCourse,
  SavedCourse,
  SavedCourseRef,
} from '@app/types/courses';
import * as actions from '@my-courses/store/actions';
import * as globalActions from '@app/actions';
import { DialogService } from '@app/shared/services/dialog.service';
import {
  failed,
  isFailed,
  isLoading,
  isSuccess,
  Loadable,
  LOADING,
  monad,
  success,
} from 'loadable.ts';
import { GlobalState } from '@app/state';
import * as s from '@app/types/schema';
import { toDateAndTimeWithDST } from '@app/shared/date-formats';
import { MatSidenav } from '@angular/material/sidenav';
import {
  SwapDefaults,
  SwapDialogComponent,
} from '../swap-dialog/swap-dialog.component';
import { Router } from '@angular/router';
import {
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { Block } from '@app/shared/pipes/layout-blocks.pipe';
import { FullColorQueue } from '@app/types/colors';
import { formatCredits } from '@app/shared/pipes/credits.pipe';
import { ReviewEnrollDialogComponent } from '../review-enroll-dialog/review-enroll-dialog.component';
import { CourseListItemComponent } from '@app/shared/components/course-list-item/course-list-item.component';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { EditEnrollmentComponent } from '../edit-enrollment/edit-enrollment.component';
import { Debug } from '@app/shared/debug/debug.decorator';
import { AnalyticsService } from '@app/services/analytics.service';

interface PackageWithEnrollOptions {
  supportsWaitlist: boolean;
  supportsHonors: boolean;
  supportsCredits: boolean;
  classPermissionNumberNeeded: boolean;
  credits: { min: number; max: number };
  group: UntypedFormGroup;
}

@Component({
  selector: 'cse-my-courses-view',
  templateUrl: './my-courses-view.component.html',
  styleUrls: ['./my-courses-view.component.scss'],
  animations: [
    trigger('pane', [
      transition('* <=> onscreen', animate('.3s ease-in-out')),
      state('offscreenLeft', style({ transform: 'translateX(-100%)' })),
      state('onscreen', style({ transform: 'translateX(0)' })),
      state('offscreenRight', style({ transform: 'translateX(100%)' })),
    ]),
  ],
})
export class MyCoursesViewComponent implements OnInit, OnDestroy {
  @ViewChild('sidenav') public sidenavRef!: MatSidenav;
  @ViewChild('detailCollapseButton')
  public detailsCollapseButtonRef!: ElementRef;
  @ViewChildren(CourseListItemComponent)
  public courseListItemRefs!: QueryList<CourseListItemComponent>;

  public renderPackages = false;
  public isMobile = new ReplaySubject<boolean>();
  public paneState: PaneState;
  private subscriptions: Subscription[];

  public activeTermCodes$ = this.store.select(
    globalSelectors.terms.getActiveTermCodes,
  );

  public termCode$: Observable<TermCode> = this.store
    .select(globalSelectors.currentTermCode.get)
    .pipe(ifSuccessThenUnwrap());

  /**
   * Observables for the visible category
   */

  public visibleCategoryName$ = this.store.select(
    viewSelectors.getChosenCategoryName,
  );

  public visibleCategoryHasCheckboxes$ = this.visibleCategoryName$.pipe(
    map(name => name !== 'dropped'),
  );

  public visibleCategory$: Observable<Loadable<Course[]>> = combineLatest([
    this.termCode$,
    this.visibleCategoryName$,
    this.store.select(globalSelectors.courses.getAll),
    this.store.select(globalSelectors.saved.getAll),
  ]).pipe(
    map(([termCode, categoryName, termCourses, saved]) => {
      if (categoryName === 'saved') {
        return saved ?? LOADING;
      }

      const encoded = TermCode.encode(termCode);
      const courses = termCourses[encoded];
      if (!courses) {
        return LOADING;
      }

      if (categoryName === 'cart') {
        return courses.roadmap ?? LOADING;
      }

      return monad(courses.current ?? LOADING).map(list => {
        return list.filter(course => course.enrolledStatus === categoryName);
      });
    }),
  );

  public visibleCategoryLabel$ = this.visibleCategoryName$.pipe(
    map((category): string => {
      switch (category) {
        case 'cart':
          return 'Cart courses';
        case 'enrolled':
          return 'Enrolled courses';
        case 'waitlisted':
          return 'Wait listed courses';
        case 'dropped':
          return 'Dropped courses';
        case 'saved':
          return 'Saved for later courses';
      }
    }),
  );

  // prettier-ignore
  public visibleCourses$: Observable<Loadable<Course[]>> = this.visibleCategory$;

  public isValidating$ = this.store.select(viewSelectors.getIsValidating);

  public hintToRevalidate$ = this.store.select(
    viewSelectors.getHintToRevalidate,
  );

  public checkedCourseRefs$ = this.store.select(viewSelectors.getChecked);

  public notExactlyOneCourseChecked$ = combineLatest([
    this.checkedCourseRefs$,
    this.visibleCourses$,
  ]).pipe(map(([ids, courses]) => !isSuccess(courses) || ids.length !== 1));

  /**
   * Observables for the status message next to each category on the left side
   * of the view. These indicate whether that category is loading, how many
   * courses it contains, or if there was an error
   */
  public categoryStatuses = {
    cart$: this.store
      .select(viewSelectors.getRoadmapCourses)
      .pipe(map(toCategoryState)),

    enrolled$: this.store
      .select(viewSelectors.getCurrentCourses)
      .pipe(map(withCurrentStatus('enrolled')), map(toCategoryState)),

    waitlisted$: this.store
      .select(viewSelectors.getCurrentCourses)
      .pipe(map(withCurrentStatus('waitlisted')), map(toCategoryState)),

    dropped$: this.store
      .select(viewSelectors.getCurrentCourses)
      .pipe(map(withCurrentStatus('dropped')), map(toCategoryState)),

    saved$: this.store
      .select(viewSelectors.getSavedCourses)
      .pipe(map(toCategoryState)),
  };

  public visibleCategory = {
    isCart$: this.visibleCategoryName$.pipe(map(n => n === 'cart')),
    isEnrolled$: this.visibleCategoryName$.pipe(map(n => n === 'enrolled')),
    isWaitlisted$: this.visibleCategoryName$.pipe(map(n => n === 'waitlisted')),
    isDropped$: this.visibleCategoryName$.pipe(map(n => n === 'dropped')),
    isSaved$: this.visibleCategoryName$.pipe(map(n => n === 'saved')),
  };

  public visibleCoursesCreditSum$ = combineLatest([
    this.visibleCourses$,
    this.visibleCategory.isSaved$,
  ]).pipe(
    map(([courses, isSaved]) =>
      monad(courses).map(isSaved ? () => '' : formatCredits),
    ),
  );

  public checkedCourses = {
    cart$: combineLatest([
      this.termCode$.pipe(map(TermCode.encode)),
      this.store.select(globalSelectors.courses.getAll),
      this.store.select(viewSelectors.getChecked),
    ]).pipe(
      map(([termCode, courses, refs]) => {
        const term = courses[termCode];
        if (term && term.roadmap && isSuccess(term.roadmap)) {
          return term.roadmap.value.filter(course =>
            refs.some(CourseRef.equalsCurry(course.ref)),
          );
        } else {
          return [];
        }
      }),
      shareReplay(1),
    ),
    enrolled$: combineLatest([
      this.termCode$.pipe(map(TermCode.encode)),
      this.store.select(globalSelectors.courses.getAll),
      this.store.select(viewSelectors.getChecked),
    ]).pipe(
      map(([termCode, courses, refs]) => {
        const term = courses[termCode];
        if (term && term.current && isSuccess(term.current)) {
          return term.current.value
            .filter(course => course.enrolledStatus === 'enrolled')
            .filter(course => refs.some(CourseRef.equalsCurry(course.ref)));
        } else {
          return [];
        }
      }),
      shareReplay(1),
    ),
    waitlisted$: combineLatest([
      this.termCode$.pipe(map(TermCode.encode)),
      this.store.select(globalSelectors.courses.getAll),
      this.store.select(viewSelectors.getChecked),
    ]).pipe(
      map(([termCode, courses, refs]) => {
        const term = courses[termCode];
        if (term && term.current && isSuccess(term.current)) {
          return term.current.value
            .filter(course => course.enrolledStatus === 'waitlisted')
            .filter(course => refs.some(CourseRef.equalsCurry(course.ref)));
        } else {
          return [];
        }
      }),
      shareReplay(1),
    ),
    dropped$: combineLatest([
      this.termCode$.pipe(map(TermCode.encode)),
      this.store.select(globalSelectors.courses.getAll),
      this.store.select(viewSelectors.getChecked),
    ]).pipe(
      map(([termCode, courses, refs]) => {
        const term = courses[termCode];
        if (term && term.current && isSuccess(term.current)) {
          return term.current.value
            .filter(course => course.enrolledStatus === 'dropped')
            .filter(course => refs.some(CourseRef.equalsCurry(course.ref)));
        } else {
          return [];
        }
      }),
      shareReplay(1),
    ),
    saved$: combineLatest([
      this.store.select(globalSelectors.saved.getAll),
      this.store.select(viewSelectors.getChecked),
    ]).pipe(
      map(([saved, refs]) => {
        if (saved && isSuccess(saved)) {
          return saved.value.filter(course =>
            refs.some(CourseRef.equalsCurry(course.ref)),
          );
        } else {
          return [];
        }
      }),
      shareReplay(1),
    ),
  };

  public emptyCategoryMessage$ = this.visibleCategoryName$.pipe(
    withLatestFrom(this.termCode$),
    map(([name, termCode]): string => {
      const termDesc = TermCode.describe(termCode);
      switch (name) {
        case 'cart':
          return `Your cart for ${termDesc} is empty.`;
        case 'enrolled':
          return `You have not enrolled in any courses for ${termDesc}.`;
        case 'waitlisted':
          return `You have not wait listed any courses for ${termDesc}.`;
        case 'dropped':
          return `You have not dropped any courses for ${termDesc}.`;
        case 'saved':
          return `You have not saved any courses for later.`;
      }
    }),
  );

  public enrollmentAppt$: Observable<Loadable<string | null>> = combineLatest([
    this.store.select(globalSelectors.student.getAll),
    this.store
      .select(globalSelectors.currentTermCode.get)
      .pipe(ifSuccessThenUnwrap(), map(TermCode.encode)),
  ]).pipe(
    map(([info, termCode]) => {
      return monad(info).flatMap(info => {
        if (apptSchema.matches(info)) {
          const appts = info.enrollmentImpacts?.registrationAppointments ?? [];
          const appt = appts.find(appt => appt.termCode === termCode);
          const date = appt
            ? toDateAndTimeWithDST(appt?.registrationDateTime)
            : null;
          return success(date);
        } else {
          return failed(null);
        }
      });
    }),
    startWith(LOADING),
  );

  public isPackagesShown$ = this.store.select(viewSelectors.isPackagesShown);

  public isScheduleShown$ = this.store.select(viewSelectors.isScheduleShown);

  public enrolledSchedule$ = this.store
    .select(viewSelectors.getCurrentCourses)
    .pipe(
      map(withCurrentStatus('enrolled')),
      map((loadable): Loadable<[CurrentCourse[], Block[]]> => {
        if (loadable && isSuccess(loadable)) {
          return success([
            loadable.value,
            loadable.value.flatMap(Block.fromCurrent(new FullColorQueue())),
          ]);
        } else if (loadable && isFailed(loadable)) {
          return loadable;
        } else {
          return LOADING;
        }
      }),
    );

  /**
   * Observables for the selected course and its details
   */

  public detailsCourseRef$ = this.store.select(
    viewSelectors.getDetailsCourseRef,
  );

  public selectedCourse = this.store.select(
    viewSelectors.getCourseShowingDetails,
  );

  public detailsLabel$ = this.store
    .select(viewSelectors.getCourseShowingDetails)
    .pipe(map(details => details?.shortCatalog ?? null));

  public selectedDetails = this.store.select(viewSelectors.details);

  public chosenPackageForSelectedCourse$ = this.store
    .select(viewSelectors.getCourseShowingDetails)
    .pipe(
      map(course => {
        if (RoadmapCourse.is(course)) {
          return course.package?.details;
        } else if (CurrentCourse.is(course)) {
          return course.package.details;
        } else {
          return null;
        }
      }),
    );

  public packagesForSelectedCourse$ = this.store.select(
    viewSelectors.getPackages,
  );

  public enrolledPackageCanBeEdited$ = combineLatest([
    this.store.select(viewSelectors.getCourseShowingDetails),
    this.chosenPackageForSelectedCourse$,
  ]).pipe(
    map(([course, chosenPack]): boolean => {
      if (
        !course ||
        !CurrentCourse.is(course) ||
        course.enrolledStatus === 'dropped'
      ) {
        return false;
      }

      const hasCreditRange =
        chosenPack && isSuccess(chosenPack)
          ? chosenPack.value.credits.min !== chosenPack.value.credits.max
          : false;
      const hasOptHonors = course.package.honors !== 'notAvailable';

      return hasCreditRange || hasOptHonors;
    }),
  );

  public packageChoices$: Observable<Record<string, PackageWithEnrollOptions>> =
    this.packagesForSelectedCourse$.pipe(
      map(packages => {
        if (!packages || !isSuccess(packages)) {
          return {};
        }

        const choices: Record<string, PackageWithEnrollOptions> = {};

        packages.value.forEach(
          ({
            credits,
            sections,
            supportsWaitlist,
            docId,
            classPermissionNumberNeeded,
          }) => {
            const supportsHonors = sections.some(
              sec => sec.honors === 'INSTRUCTOR_APPROVED',
            );

            const defaultCredits =
              credits.min === credits.max ? credits.min : null;

            const supportsCredits = credits.min !== credits.max;

            const group = new UntypedFormGroup({
              honors: new UntypedFormControl(supportsHonors ? 'without' : null),
              waitlist: new UntypedFormControl(
                supportsWaitlist ? 'without' : null,
              ),
              credits: new UntypedFormControl(defaultCredits, [
                Validators.required,
                Validators.min(credits.min),
                Validators.max(credits.max),
              ]),
              classPermissionNumber: new UntypedFormControl(null),
            });

            choices[docId] = {
              supportsHonors,
              supportsWaitlist,
              supportsCredits,
              classPermissionNumberNeeded,
              credits,
              group,
            };
          },
        );

        return choices;
      }),
    );

  public disableCourseActions$ = combineLatest([
    this.store.select(viewSelectors.hasChecked),
    this.visibleCourses$,
  ]).pipe(map(([hasChecked, courses]) => !hasChecked || !isSuccess(courses)));

  constructor(
    private store: Store<GlobalState>,
    breakpoints: BreakpointObserver,
    private dialog: DialogService,
    private announcer: LiveAnnouncer,
    private analytics: AnalyticsService,
    router: Router,
  ) {
    this.store.dispatch(actions.lifecycle.init());

    this.paneState = PaneState.init(breakpoints.isMatched(Breakpoints.Mobile));

    const navState = router.getCurrentNavigation()?.extras?.state;

    const showCart: unknown = navState?.showCart;
    const showSavedForLater = navState?.showSavedForLater === true;
    if (TermCode.is(showCart)) {
      this.store.dispatch(
        globalActions.currentTermCode.set({ currentTermCode: showCart }),
      );
      this.store.dispatch(viewActions.showCart());
    } else if (showSavedForLater) {
      this.store.dispatch(viewActions.showSavedForLater());
    }

    const checked: unknown = navState?.checked;
    if (s.array(CourseRef.schema).matches(checked)) {
      this.store.dispatch(actions.checked.set({ refs: checked }));
    }

    this.subscriptions = [
      breakpoints
        .observe(Breakpoints.Mobile)
        .subscribe(({ matches: isMobile }) => {
          this.isMobile.next(isMobile);

          if (isMobile) {
            this.paneState = this.paneState.switchToMobile();
          } else {
            this.paneState = this.paneState.switchToDesktop();
          }
        }),

      combineLatest([
        this.store
          .select(viewSelectors.detailsMode)
          .pipe(startWith(null), pairwise()),
      ]).subscribe(([[_fromDetails, toDetails]]) => {
        if (toDetails === 'loaded') {
          this.paneState = this.paneState.focusOnDetails();
          return;
        }

        if (toDetails === 'unloaded') {
          this.paneState = this.paneState.focusOnList();
          return;
        }

        // TODO: more work will have to be done here once the package view is built
      }),
    ];
  }

  @Debug('Hint to revalidate')
  async manualHintToRevalidate() {
    const termCode = await this.store
      .select(globalSelectors.currentTermCode.get)
      .pipe(ifSuccessThenUnwrap(), first())
      .toPromise();

    this.store.dispatch(
      globalActions.courses.roadmap.hintToRevalidate({
        eventId: 'XYZ',
        termCodes: [termCode],
      }),
    );
  }

  ngOnInit() {
    /**
     * When the view loads, check if the state wants the packages shown
     * immediately. If so, open the packages sidenav.
     */
    this.store
      .select(viewSelectors.isPackagesShown)
      .pipe(first())
      .toPromise()
      .then(isShown => {
        if (isShown) {
          this.onShowPackages();
        }
      });

    /**
     * When the view loads, check if the state wants the course schedule shown
     * immediately. If so, open the sidenav. I want to make this cleaner - Isaac
     */
    this.store
      .select(viewSelectors.isScheduleShown)
      .pipe(first())
      .toPromise()
      .then(isShown => {
        if (isShown) {
          this.onShowSchedule();
        }
      });
  }

  ngOnDestroy() {
    unsubscribeAll(this.subscriptions);

    this.store.dispatch(actions.schedule.hide());
  }

  async onSwapCheckedEnrolledCourse() {
    const toDrop = await this.checkedCourses.enrolled$
      .pipe(
        first(),
        map(courses => courses[0] as CurrentCourse | undefined),
        map(course => course?.ref),
      )
      .toPromise();

    this.store.dispatch(viewActions.clearChecked());
    await this.onSwap({ toDrop });
  }

  async onSwapVisibleEnrolledCourse() {
    const ref = await this.detailsCourseRef$.pipe(first()).toPromise();

    this.sidenavRef.close();

    if (ref && ref.kind === 'current') {
      await this.onSwap({ toDrop: ref });
    } else {
      await this.onSwap({});
    }
  }

  async onSwapCheckedCartCourse() {
    const toEnrollIn = await this.checkedCourses.cart$
      .pipe(
        first(),
        map(courses => courses[0] as RoadmapCourse | undefined),
        map(course => course?.ref),
      )
      .toPromise();

    this.sidenavRef.close();

    this.store.dispatch(viewActions.clearChecked());
    await this.onSwap({ toEnrollIn });
  }

  async onSwapVisibleCartCourse() {
    const ref = await this.detailsCourseRef$.pipe(first()).toPromise();

    if (ref && ref.kind === 'roadmap') {
      await this.onSwap({ toEnrollIn: ref });
    } else {
      await this.onSwap({});
    }
  }

  private async onSwap(defaults: SwapDefaults) {
    await this.dialog.large(SwapDialogComponent, defaults);
  }

  /**
   * Fired when the open animation STARTS. We want to start rendering the
   * package list so that it's rendering _while_ the sidenav is expanding
   * across the screen.
   */
  onSidenavOpenStart() {
    this.renderPackages = true;
  }

  /**
   * Fired when the open and close animations FINISHES. We don't want to
   * un-render the package list until it's fully off the screen.
   */
  async onSidenavOpenedChange(isOpen: boolean) {
    if (isOpen === false) {
      this.renderPackages = false;
      await this.onHidePackages();
    }
  }

  showCart() {
    this.store.dispatch(viewActions.showCart());
    this.paneState = this.paneState.focusOnList();
    this.announcer.announce('Showing cart courses', 'assertive');
  }

  showEnrolled() {
    this.store.dispatch(viewActions.showEnrolled());
    this.paneState = this.paneState.focusOnList();
    this.announcer.announce('Showing enrolled courses', 'assertive');
  }

  showWaitlisted() {
    this.store.dispatch(viewActions.showWaitlisted());
    this.paneState = this.paneState.focusOnList();
    this.announcer.announce('Showing wait listed courses', 'assertive');
  }

  showDropped() {
    this.store.dispatch(viewActions.showDropped());
    this.paneState = this.paneState.focusOnList();
    this.announcer.announce('Showing dropped courses', 'assertive');
  }

  showSavedForLater() {
    this.store.dispatch(viewActions.showSavedForLater());
    this.paneState = this.paneState.focusOnList();
    this.announcer.announce('Showing saved for later courses', 'assertive');
  }

  onHideList() {
    this.paneState = this.paneState.focusOnCategories();
  }

  async onDropEnrolledCourses() {
    const courses = await this.checkedCourses.enrolled$
      .pipe(first())
      .toPromise();

    this.store.dispatch(viewActions.clearChecked());
    this.onDrop(courses);
  }

  async onDropWaitlistedCourses() {
    const courses = await this.checkedCourses.waitlisted$
      .pipe(first())
      .toPromise();

    this.store.dispatch(viewActions.clearChecked());
    this.onDrop(courses);
  }

  async onDropVisibleCourse() {
    const course = await this.store
      .select(viewSelectors.getCourseShowingDetails)
      .pipe(first(), filter(notNull), filter(CurrentCourse.is))
      .toPromise();

    this.onDrop([course]);
  }

  private async onDrop(courses: CurrentCourse[]) {
    if (courses.length === 0) {
      return;
    }

    const shouldDropCourses = await this.dialog.confirm({
      headline: 'Are you sure?',
      reasoning: [`Do you want to drop the following ${pluralize(courses)}:`],
      courses,
      affirmative: `Drop ${pluralize(courses)}`,
    });

    if (shouldDropCourses) {
      await this.onHidePackages();
      this.onHideDetails();
      const termCode = await this.termCode$.pipe(first()).toPromise();
      this.store.dispatch(
        globalActions.courses.current.drop({ termCode, courses }),
      );
    }
  }

  compareTermCodes(t1: unknown, t2: unknown): boolean {
    return TermCode.is(t1) && TermCode.is(t2) && TermCode.equals(t1, t2);
  }

  userChangedTerm({ value }: MatSelectChange) {
    if (TermCode.is(value)) {
      this.store.dispatch(
        globalActions.currentTermCode.set({ currentTermCode: value }),
      );
      this.store.dispatch(viewActions.hideDetails());
    } else {
      console.assert(TermCode.is(value), 'expected a term-code');
    }
  }

  onToggleCourse(course: HasCourseRef, event: MatCheckboxChange) {
    this.store.dispatch(
      viewActions.selectCourse({ ref: course.ref, isChecked: event.checked }),
    );
  }

  onShowDetails(course: RoadmapCourse | CurrentCourse | SavedCourse) {
    if (this.detailsCollapseButtonRef) {
      (this.detailsCollapseButtonRef as any).focus();
    }

    this.store.dispatch(viewActions.showDetails({ course }));
    this.paneState = this.paneState.focusOnDetails();
    this.announcer.announce('Showing course details', 'assertive');
  }

  async onHideDetails() {
    const selectedCourseRef = await this.store
      .select(viewSelectors.getDetailsCourseRef)
      .pipe(first())
      .toPromise();

    if (selectedCourseRef !== null) {
      this.courseListItemRefs
        .toArray()
        .find(courseListItem => {
          return (
            courseListItem.ref &&
            CourseRef.equals(selectedCourseRef, courseListItem.ref)
          );
        })
        ?.focus();
    }

    this.store.dispatch(viewActions.hideDetails());

    this.announcer.announce('Hiding course details', 'assertive');
  }

  onShowSchedule() {
    this.store.dispatch(actions.schedule.show());
    this.sidenavRef.open();

    this.announcer.announce('Show enrolled schedule', 'assertive');
  }

  async onHideSchedule() {
    this.store.dispatch(actions.schedule.show());
    await this.sidenavRef.close();

    this.announcer.announce('Hiding enrolled schedule', 'assertive');
  }

  async onShowPackages() {
    const course = await this.store
      .select(viewSelectors.getCourseShowingDetails)
      .pipe(first())
      .toPromise();

    if (!RoadmapCourse.is(course) && !CurrentCourse.is(course)) {
      return;
    }

    if (RoadmapCourse.is(course)) {
      const hasLoadedPack =
        course.package === null
          ? true
          : course.package.details && isSuccess(course.package.details);

      const hasLoadedPacks = course.packages && isSuccess(course.packages);

      if (!hasLoadedPack || !hasLoadedPacks) {
        this.store.dispatch(
          globalActions.courses.roadmap.packs.load({
            course,
          }),
        );
      }
    } else if (CurrentCourse.is(course)) {
      if (!course.package.details || !isSuccess(course.package.details)) {
        this.store.dispatch(
          globalActions.courses.current.pack.load({ course }),
        );
      }
    }

    this.store.dispatch(actions.packages.show());
    this.sidenavRef.open();

    this.announcer.announce('Showing course sections', 'assertive');
  }

  async onRetryPacks() {
    const course = await this.store
      .select(viewSelectors.getCourseShowingDetails)
      .pipe(first())
      .toPromise();

    if (RoadmapCourse.is(course)) {
      this.store.dispatch(
        globalActions.courses.roadmap.packs.load({
          course,
        }),
      );
    } else if (CurrentCourse.is(course)) {
      this.store.dispatch(globalActions.courses.current.pack.load({ course }));
    }
  }

  async onRetryAllPackages() {
    return await this.onRetryPacks();
  }

  async onHidePackages() {
    this.store.dispatch(actions.packages.hide());
    if (this.sidenavRef.opened) {
      await this.sidenavRef.close();
    }

    this.announcer.announce('Hiding course sections', 'assertive');
  }

  async onRemoveVisibleFromCart() {
    const course = await this.store
      .select(viewSelectors.getCourseShowingDetails)
      .pipe(first())
      .toPromise();

    if (!RoadmapCourse.is(course)) {
      return;
    }

    if (await this.onRemove([course])) {
      await this.onHidePackages();
      this.onHideDetails();
      this.paneState = this.paneState.focusOnList();
    }
  }

  async onRemoveCheckedFromCart() {
    const courses = await this.checkedCourses.cart$.pipe(first()).toPromise();

    this.store.dispatch(viewActions.clearChecked());
    await this.onRemove(courses);
  }

  private async onRemove(courses: RoadmapCourse[]): Promise<boolean> {
    const shouldRemove = await this.dialog.confirm({
      headline: 'Are you sure?',
      reasoning: [
        courses.length === 1
          ? `This action will remove 1 course from your cart:`
          : `This action will remove ${courses.length} courses from your cart:`,
      ],
      courses,
      affirmative: 'Remove',
    });

    if (shouldRemove) {
      this.store.dispatch(
        globalActions.courses.roadmap.removeCoursesAndPacks({ courses }),
      );
    }

    return shouldRemove;
  }

  async onChosenRemovePackage() {
    const course = await this.store
      .select(viewSelectors.getCourseShowingDetails)
      .pipe(first())
      .toPromise();

    if (!course || !RoadmapCourse.is(course)) {
      return;
    }

    const shouldRemove = await this.dialog.confirm({
      headline: 'Are you sure?',
      reasoning: [`This action will remove the saved section from:`],
      courses: [course],
      affirmative: 'Remove section',
    });

    if (shouldRemove) {
      await this.onHidePackages();
      this.onHideDetails();
      this.store.dispatch(globalActions.courses.roadmap.removePack({ course }));
    }
  }

  async onRemoveViewedSaveForLaterCourse() {
    const currentCategory = await this.store
      .select(viewSelectors.getChosenCategoryName)
      .pipe(take(1))
      .toPromise();

    if (currentCategory !== 'saved') {
      return;
    }

    const course = await this.store
      .select(viewSelectors.getCourseShowingDetails)
      .pipe(take(1))
      .toPromise();

    if (!course) {
      return;
    }

    const shouldRemove = await this.dialog.confirm({
      headline: 'Are you sure?',
      reasoning: [`This action will remove 1 saved course:`],
      courses: [course],
      affirmative: 'Remove',
    });

    if (shouldRemove) {
      // This cast is okay because if the current category is 'saved', then the
      // course showing details should always be a SavedCourse.
      const courses = [course as SavedCourse];
      this.store.dispatch(viewActions.clearChecked());
      this.store.dispatch(globalActions.saved.remove({ courses }));
    }
  }

  async onRemoveCheckedSavedForLaterCourses() {
    const refs = await this.store
      .select(viewSelectors.getChecked)
      .pipe(
        first(),
        map(refs => refs.filter(SavedCourseRef.is)),
      )
      .toPromise();

    const saved = await this.store
      .select(globalSelectors.saved.getAll)
      .pipe(first(), ifSuccessThenUnwrap())
      .toPromise();

    const courses = saved.filter(course =>
      refs.some(CourseRef.equalsCurry(course.ref)),
    );

    const shouldRemove = await this.dialog.confirm({
      headline: 'Are you sure?',
      reasoning: [
        courses.length === 1
          ? `This action will remove 1 saved course:`
          : `This action will remove ${courses.length} saved courses:`,
      ],
      courses,
      affirmative: 'Remove',
    });

    if (shouldRemove) {
      this.store.dispatch(globalActions.saved.remove({ courses }));
    }
  }

  async onRevalidateCheckedCourses() {
    const termCode = await this.termCode$.pipe(first()).toPromise();

    const courses = await this.checkedCourses.cart$.pipe(first()).toPromise();

    this.store.dispatch(
      globalActions.courses.roadmap.validate({ termCode, courses }),
    );
  }

  async onRevalidateEntireCart() {
    const termCode = await this.termCode$.pipe(first()).toPromise();

    const visibleCourses = await this.visibleCourses$
      .pipe(first(), ifSuccessThenUnwrap())
      .toPromise();

    const courses = visibleCourses.filter(RoadmapCourse.is);

    this.analytics.event('Cart', 'take-revalidate-hint');
    this.store.dispatch(
      globalActions.courses.roadmap.validate({ termCode, courses }),
    );
  }

  async onEnrollFromChecked() {
    const courses = await this.checkedCourses.cart$.pipe(first()).toPromise();
    this.store.dispatch(viewActions.clearChecked());
    this.onEnroll(courses);
  }

  async onEnrollFromWaitlist() {
    const courses = await this.checkedCourses.waitlisted$
      .pipe(first())
      .toPromise();
    this.store.dispatch(viewActions.clearChecked());
    await this.onEnroll(courses);
  }

  async onEnrollFromVisible() {
    const course = await this.store
      .select(viewSelectors.getCourseShowingDetails)
      .pipe(first())
      .toPromise();

    this.sidenavRef.close();

    if (course && (RoadmapCourse.is(course) || CurrentCourse.is(course))) {
      await this.onEnroll([course]);
    }
  }

  private async onEnroll(courses: Array<RoadmapCourse | CurrentCourse>) {
    const requests = await this.dialog.large(
      ReviewEnrollDialogComponent,
      courses,
    );

    if (s.array(s.any).matches(requests)) {
      const termCode = await this.termCode$.pipe(first()).toPromise();

      this.store.dispatch(
        globalActions.courses.current.enroll({
          termCode,
          requests,
        }),
      );
    } else {
      console.log('closed enroll dialog without triggering enroll');
    }
  }

  async onAddPackage(pack: EnrollmentPackage, choices: UntypedFormGroup) {
    const honors = choices.get('honors')?.value === 'with' ?? null;
    const waitlist = choices.get('waitlist')?.value === 'with' ?? null;
    const credits = choices.get('credits')?.value!;
    const classPermissionNumber = toNumberOrNull(
      choices.get('classPermissionNumber')?.value!,
    );
    const course = await this.store
      .select(viewSelectors.getCourseShowingDetails)
      .pipe(first())
      .toPromise();

    if (!course || !RoadmapCourse.is(course)) {
      return;
    }

    await this.onHidePackages();
    this.onHideDetails();
    this.store.dispatch(
      globalActions.courses.roadmap.addCourseWithPack({
        course,
        pack,
        credits,
        joinWaitlist: waitlist,
        withHonors: honors,
        classPermissionNumber,
      }),
    );
  }

  async retryCategory() {
    const currentCategory = await this.store
      .select(viewSelectors.getChosenCategoryName)
      .pipe(take(1))
      .toPromise();

    const termCode = await this.termCode$.pipe(first()).toPromise();

    if (currentCategory === 'cart') {
      this.store.dispatch(globalActions.courses.roadmap.load({ termCode }));
    } else if (currentCategory === 'saved') {
      this.store.dispatch(globalActions.saved.load());
    } else {
      this.store.dispatch(globalActions.courses.current.load({ termCode }));
    }
  }

  async onMoveCheckedSavedToCart() {
    const courses = await this.checkedCourses.saved$.pipe(first()).toPromise();

    if (courses.length === 1) {
      this.store.dispatch(viewActions.clearChecked());
      this.onMoveSavedToCart(courses[0]);
    }
  }

  async onMoveVisibleSavedToCart() {
    const course = await this.store
      .select(viewSelectors.getCourseShowingDetails)
      .pipe(first())
      .toPromise();

    if (course && SavedCourse.is(course)) {
      this.onMoveSavedToCart(course);
    }
  }

  private onMoveSavedToCart(course: SavedCourse) {
    this.store.dispatch(actions.saved.moveToCart({ course }));
  }

  async onMoveVisibleCartToSaved() {
    const course = await this.store
      .select(viewSelectors.getCourseShowingDetails)
      .pipe(first())
      .toPromise();

    if (course && RoadmapCourse.is(course)) {
      this.store.dispatch(viewActions.clearChecked());
      this.onMoveCartToSaved(course);
    }
  }

  private async onMoveCartToSaved(course: RoadmapCourse) {
    const doMove = await this.dialog.confirm({
      headline: 'Are you sure?',
      reasoning: [
        `Saving ${course.shortCatalog} for later will remove it from your cart and remove any sections you may have selected.`,
        'You can always re-select those sections again in the future.',
      ],
      affirmative: 'Save for later',
    });

    if (doMove) {
      this.store.dispatch(viewActions.clearChecked());
      this.store.dispatch(actions.roadmap.moveToSaved({ course }));
    }
  }

  isSelected(course: HasCourseRef, selected: CourseRef | null): boolean {
    if (selected) {
      return CourseRef.equals(course.ref, selected);
    } else {
      return false;
    }
  }

  isChecked(course: HasCourseRef, checked: CourseRef[]): boolean {
    return checked.some(CourseRef.equalsCurry(course.ref));
  }

  async onEditEnrolledPackage(pack: EnrollmentPackage) {
    const course = await this.store
      .select(viewSelectors.getCourseShowingDetails)
      .pipe(first())
      .toPromise();

    if (!course && CurrentCourse.is(course)) {
      return;
    }

    const didEdit = await this.dialog.small(EditEnrollmentComponent, {
      pack,
      course,
    });
    if (didEdit === 'edited') {
      this.sidenavRef.close();
    }
  }
}

/**
 * An interface that encodes the animation states available to each pane of
 * the layout. Keeping all of the animation states in a single object reduces
 * the chances of a bug causing the states to become out-of-sync.
 */
interface PaneStateInstance {
  categories: 'offscreenLeft' | 'onscreen';
  list: 'offscreenLeft' | 'onscreen' | 'offscreenRight';
  details: 'offscreenLeft' | 'onscreen' | 'offscreenRight';
}

interface PaneStateInstancePair {
  mobile: PaneStateInstance;
  desktop: PaneStateInstance;
}

class PaneState {
  /**
   * A series of constants capturing the legal combinations of pane animation
   * states. These constant objects will be passed to the `animState` observable
   * inside the MyCoursesViewComponent class.
   */
  public static FOCUS_ON_CATEGORIES: PaneStateInstancePair = {
    mobile: {
      categories: 'onscreen',
      list: 'offscreenRight',
      details: 'offscreenRight',
    },
    desktop: {
      categories: 'onscreen',
      list: 'onscreen',
      details: 'offscreenLeft',
    },
  };

  public static FOCUS_ON_LIST: PaneStateInstancePair = {
    mobile: {
      categories: 'offscreenLeft',
      list: 'onscreen',
      details: 'offscreenRight',
    },
    desktop: {
      categories: 'onscreen',
      list: 'onscreen',
      details: 'offscreenLeft',
    },
  };

  public static FOCUS_ON_DETAILS: PaneStateInstancePair = {
    mobile: {
      categories: 'offscreenLeft',
      list: 'offscreenLeft',
      details: 'onscreen',
    },
    desktop: {
      categories: 'onscreen',
      list: 'onscreen',
      details: 'onscreen',
    },
  };

  private constructor(
    public readonly breakpoint: 'mobile' | 'desktop',
    public readonly pair: PaneStateInstancePair,
  ) {}

  get categories(): PaneStateInstance['categories'] {
    return this.pair[this.breakpoint].categories;
  }

  get list(): PaneStateInstance['list'] {
    return this.pair[this.breakpoint].list;
  }

  get details(): PaneStateInstance['details'] {
    return this.pair[this.breakpoint].details;
  }

  public static init(isMobile: boolean): PaneState {
    return new PaneState(
      isMobile ? 'mobile' : 'desktop',
      PaneState.FOCUS_ON_CATEGORIES,
    );
  }

  public switchToMobile(): PaneState {
    return new PaneState('mobile', this.pair);
  }

  public switchToDesktop(): PaneState {
    return new PaneState('desktop', this.pair);
  }

  public focusOnCategories(): PaneState {
    return new PaneState(this.breakpoint, PaneState.FOCUS_ON_CATEGORIES);
  }

  public focusOnList(): PaneState {
    return new PaneState(this.breakpoint, PaneState.FOCUS_ON_LIST);
  }

  public focusOnDetails(): PaneState {
    return new PaneState(this.breakpoint, PaneState.FOCUS_ON_DETAILS);
  }
}

const withCurrentStatus = (name: EnrolledStatus) => {
  return (category: Loadable<CurrentCourse[]> | null) => {
    if (!category) {
      return category;
    }

    return monad(category).map(courses => {
      return courses.filter(course => course.enrolledStatus === name);
    });
  };
};

const toCategoryState = (category: Loadable<unknown[]> | null) => {
  if (category === null || isLoading(category)) {
    return 'loading';
  } else if (isSuccess(category)) {
    return category.value.length;
  } else {
    return 'error';
  }
};

const apptSchema = s.object({
  enrollmentImpacts: s.nullable(
    s.object({
      registrationAppointments: s.array(
        s.object({
          termCode: s.string,
          registrationDateTime: s.number,
        }),
      ),
    }),
  ),
});

const toNumberOrNull = (thing: unknown): number | null => {
  if (typeof thing === 'string' && /^\d+$/.test(thing)) {
    return parseInt(thing, 10);
  } else if (typeof thing === 'number') {
    return thing;
  } else {
    return null;
  }
};
