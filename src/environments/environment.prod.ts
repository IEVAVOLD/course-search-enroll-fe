export const environment = {
  production: true,
  version: '2.1.44',
  snackbarDuration: 4000,
};
