require('dotenv').config();

const AUTH_HEADER_PREFIX = 'CSE_AUTH_HEADER_';
const AUTH_HEADERS = {};
for (const key in process.env) {
  if (key.startsWith(AUTH_HEADER_PREFIX)) {
    const value = process.env[key];
    AUTH_HEADERS[key.slice(AUTH_HEADER_PREFIX.length)] = value;
  }
}

module.exports = {
  // The aggregate and banner endpoings do *NOT* get fake Shibboleth
  // authentication headers. Both of these endpoints map to public S3 buckets
  // and so don't need any special shib protections.
  '/api/search/v1/aggregate': {
    target: 'http://dev-enroll-app-data.s3.us-east-1.amazonaws.com',
    secure: false,
    changeOrigin: true,
    pathRewrite: { '^/api/search/v1/aggregate': '/aggregate.json' },
    logLevel: 'debug',
  },
  '/config/banner.json': {
    target: {
      host: 'dev-enroll-site-config.s3.us-east-2.amazonaws.com',
      protocol: 'https:',
      port: 443,
      path: '/banner.json',
    },
    secure: false,
    changeOrigin: true,
    pathRewrite: { '^/config/banner.json': '' },
    logLevel: 'debug',
  },
  '/api/data/degreeprograms.json': {
    target: 'http://dev-enroll-app-data.s3.us-east-1.amazonaws.com',
    changeOrigin: true,
    pathRewrite: { '^/api/data/degreeprograms.json': '/degreeprograms.json' },
    logLevel: 'debug',
  },

  // The following proxy rewrites *DO* get fake Shibboleth authentication
  // headers. The contents of these shib headers are read from the `.env` file
  // when the local dev server boots.
  '/api': {
    headers: AUTH_HEADERS,
    target: 'https://test.enroll.wisc.edu:8443',
    secure: false,
    changeOrigin: true,
    logLevel: 'debug',
  },
  '/scheduling': {
    headers: AUTH_HEADERS,
    target: 'https://test.enroll.wisc.edu:8443',
    secure: false,
    changeOrigin: true,
    logLevel: 'debug',
  },
  '/wsregister': {
    headers: AUTH_HEADERS,
    target: {
      host: 'z733wct6mk.execute-api.us-east-1.amazonaws.com',
      protocol: 'https:',
      port: 443,
      path: '/dev/authtoken',
    },
    secure: false,
    changeOrigin: true,
    pathRewrite: { '^/wsregister': '' },
    logLevel: 'debug',
  },
};
